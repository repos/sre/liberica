GO_GENERATE = go generate
GO_BUILD = go build
GOARCH ?= amd64
GOOS ?= linux
CGO_ENABLED ?= 0
QEMU_GUEST_RAM_SIZE ?= 768

all: build

clean:
	go clean
	rm -f $(BINARY_NAME)

generate:
	$(GO_GENERATE) ./...
	protoc --proto_path=${PWD} --go_out=. --go_opt=module=gitlab.wikimedia.org/repos/sre/liberica --go-grpc_out=. --go-grpc_opt=module=gitlab.wikimedia.org/repos/sre/liberica pkg/protos/hcforwarder.proto pkg/protos/service.proto pkg/protos/realserver.proto pkg/protos/healthcheck.proto pkg/protos/fp.proto pkg/protos/cp.proto

build: generate
	CGO_ENABLED=$(CGO_ENABLED) GOARCH=$(GOARCH) GOOS=$(GOOS) $(GO_BUILD) -o libericad gitlab.wikimedia.org/repos/sre/liberica/cmd/libericad
	CGO_ENABLED=$(CGO_ENABLED) GOARCH=$(GOARCH) GOOS=$(GOOS) $(GO_BUILD) -o liberica gitlab.wikimedia.org/repos/sre/liberica/cmd/liberica

run: dep build
	./$(BINARY_NAME)

dep:
	go mod download

test:
	QEMU_GUEST_RAM_SIZE=$(QEMU_GUEST_RAM_SIZE) go test -v ./...

state-graph:
	go build ./tools/state-graph
