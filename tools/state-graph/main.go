package main

import (
	"flag"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"slices"
	"strings"
)

func parseSelectorExpr(expr *ast.SelectorExpr) (string, error) {
	expressionIdent, ok := expr.X.(*ast.Ident)
	if !ok {
		return "", fmt.Errorf("expr.X type isn't *ast.Ident, please implement this")
	}
	return fmt.Sprintf("%s.%s", expressionIdent.Name, expr.Sel.Name), nil
}

func main() {
	explainFlag := flag.Bool("e", false, "toggle text output of next states for each state")
	flag.Parse()

	fset := token.NewFileSet()
	file, err := parser.ParseFile(fset, "pkg/cp/cp.go", nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}

	capture := false
	flow := map[string][]string{}
	fname := ""
	ast.Inspect(file, func(n ast.Node) bool {
		switch stmt := n.(type) {
		case *ast.FuncDecl:
			if strings.HasSuffix(stmt.Name.Name, "State") {
				fname = stmt.Name.Name
				capture = true
				flow[fname] = []string{}
			} else {
				capture = false
			}
		case *ast.ReturnStmt:
			if capture && len(stmt.Results) == 3 {
				var nextState string
				var returnedError string
				switch stateFn := stmt.Results[0].(type) {
				case *ast.Ident:
					nextState = stateFn.Name
				}
				switch retErr := stmt.Results[2].(type) {
				case *ast.Ident:
					returnedError = retErr.Name
				case *ast.CallExpr: // ctx.Err(), fmt.Errorf(), errors.New()
					var expression string
					switch fun := retErr.Fun.(type) {
					case *ast.SelectorExpr:
						var err error
						expression, err = parseSelectorExpr(fun)
						if err != nil {
							panic(err)
						}

					default:
						log.Panicf("Missing branch on error value handling, please fix it, unexpected type = %T\n", fun)
					}
					if expression != "ctx.Err" { // ignore ctx.Err() as every state checks for context cancellation
						var errStr string
						for _, arg := range retErr.Args {
							switch arg := arg.(type) {
							case *ast.BasicLit:
								if arg.Kind == token.STRING {
									errStr += " " + arg.Value
								}
							case *ast.Ident:
								errStr += " " + arg.Name
							}
						}
						flow[fname] = append(flow[fname], fmt.Sprintf("crash state machine with error %s", errStr))
					}
				default:
					fmt.Printf("%v\n", stmt.Results)
					fmt.Printf("%T %T %T\n", stmt.Results[0], stmt.Results[1], stmt.Results[2])
					fmt.Printf("%v\n", retErr)

				}
				if nextState != "" && returnedError != "" {
					if nextState == "nil" {
						if returnedError == "nil" {
							flow[fname] = append(flow[fname], "stop state machine")
						} else {
							flow[fname] = append(flow[fname], fmt.Sprintf("crash state machine with error %s", returnedError))
						}
					} else {
						flow[fname] = append(flow[fname], nextState)
					}
				}
			}
		}
		return true
	})

	for fname, states := range flow {
		slices.Sort(states)
		states = slices.Compact(states) // get rid of duplicated entries
		flow[fname] = states
	}

	if *explainFlag {
		for fname, states := range flow {
			fmt.Printf("### Next states and potential errors for %s:\n", fname)
			for _, state := range states {
				fmt.Printf(" - %s\n", state)
			}
		}
	}

	fmt.Println("digraph G {")
	explored := map[string]struct{}{}
	initialStates := []string{"checkEtcdState", "handleEtcdEventState", "handleHealthcheckResultState", "configureBgpPathState"}
	for _, state := range initialStates {
		explored[state] = struct{}{}
		exploreState(state, flow, explored, true)
	}
	fmt.Println("}")

}

func exploreState(state string, flow map[string][]string, explored map[string]struct{}, initialState bool) {
	stop := false
	unconditionalStop := true
	for _, target := range flow[state] {
		if target == "stop state machine" {
			stop = true
		} else if strings.HasSuffix(target, "State") {
			unconditionalStop = false
		}
	}
	if initialState { // box shaped and green color for initial states
		fmt.Printf("\t%s [shape=box,color=green];\n", strings.TrimSuffix(state, "State"))
	} else if unconditionalStop { // box shaped and blue color for unconditional stop states
		fmt.Printf("\t%s [shape=box,color=blue];\n", strings.TrimSuffix(state, "State"))
	} else if stop { // box shaped and red color for states that can stop under certain conditions
		fmt.Printf("\t%s [shape=box,color=red];\n", strings.TrimSuffix(state, "State"))
	}
	for _, target := range flow[state] {
		if strings.HasSuffix(target, "State") {
			fmt.Printf("\t%s -> %s;\n", strings.TrimSuffix(state, "State"), strings.TrimSuffix(target, "State"))
			if _, present := explored[target]; !present {
				exploreState(target, flow, explored, false)
				explored[target] = struct{}{}
			}
		}
	}
}
