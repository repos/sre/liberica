Small tool that using go ast package generates a directed graph of the control plane state machine transitions.
Optionally (using `-e` flag) it can output a human readable list of states and possible errors for each state
