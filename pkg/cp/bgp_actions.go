package cp

import (
	"context"
	"errors"
	"net/netip"
	"sync"
	"sync/atomic"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bgp"
)

type bgpAction uint8

const (
	addPath bgpAction = iota
	deletePath
)

// We need to keep track of bgp prefix usage across different ControlPlane instances to avoid concurrency issues
// Potential issues: two services sharing the same VIP, one on port 80 and the other on port 443
// port 80 service gets decomissioned cause from now on the service will be https only
// port 80 service gets removed from configuration and a config reload is triggered
// without tracking BGP prefix usage the port 80 control plane would accidentallly depool port 443 service
var bgpPrefixMap sync.Map

func runBgpAction(ctx context.Context, action bgpAction, cfg *bgp.BgpConfig, client bgp.BgpClient, ip netip.Addr) error {
	bitLen := ip.BitLen()
	prefix := netip.PrefixFrom(ip, bitLen)
	switch action {
	case addPath:
		val, _ := bgpPrefixMap.LoadOrStore(prefix, new(atomic.Int64))
		counter := val.(*atomic.Int64)
		if counter.Load() > 0 {
			counter.Add(1) // increment it anyways to track current control plane usage of the bgp path
			return nil
		}
		counter.Add(1)
	case deletePath:
		val, loaded := bgpPrefixMap.Load(prefix)
		if !loaded {
			return nil
		}
		counter := val.(*atomic.Int64)
		counter.Add(-1)
		if counter.Load() > 0 {
			return nil
		}
	}

	var nextHop netip.Addr
	if bitLen == 32 {
		nextHop = cfg.NextHop4
	} else {
		nextHop = cfg.NextHop6
	}

	present, err := client.IsPathPresent(ctx, prefix, nextHop)
	if err != nil {
		return err
	}

	if (present && action == addPath) || (!present && action == deletePath) {
		return nil
	}

	switch action {
	case addPath:
		return client.AddPath(ctx, prefix, nextHop)
	case deletePath:
		return client.DeletePath(ctx, prefix, nextHop)
	default:
		return errors.New("unknown BGP action")
	}
}

func addBgpPath(ctx context.Context, cfg *bgp.BgpConfig, client bgp.BgpClient, ip netip.Addr) error {
	return runBgpAction(ctx, addPath, cfg, client, ip)
}

func deleteBgpPath(ctx context.Context, cfg *bgp.BgpConfig, client bgp.BgpClient, ip netip.Addr) error {
	return runBgpAction(ctx, deletePath, cfg, client, ip)
}
