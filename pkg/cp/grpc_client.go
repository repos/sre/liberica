/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cp

import (
	"context"
	"fmt"
	"log/slog"
	"net/netip"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/cp"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"
	pbsvc "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

type Service struct {
	Name     string
	AddrPort netip.AddrPort
}

// use it as a public version of realserver defined on cp.go
type Realserver struct {
	Address  netip.Addr
	Hostname string
	Weight   int
	Mark     uint32
	Pooled   Pooled
	Healthy  bool
}

type CPClient interface {
	ListServices(context.Context) ([]Service, error)
	ListRealservers(context.Context, Service) ([]Realserver, error)
	Close()
}

type CPGrpcClient struct {
	logger *slog.Logger
	conn   *grpc.ClientConn
}

func NewGrpcClient(logger *slog.Logger, network, address string) (CPClient, error) {
	addr := grpcutils.GetConnectAddress(network, address)

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, grpc.WithKeepaliveParams(grpcutils.KeepaliveClientParameters))
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// DialContext returns before connecting, ctx only controls the setup steps. More details on https://pkg.go.dev/google.golang.org/grpc@v1.46.2#DialContext
	conn, err := grpc.DialContext(ctx, addr, opts...)
	if err != nil {
		return nil, fmt.Errorf("unable to create connection: %w", err)
	}

	return &CPGrpcClient{
		logger: logger,
		conn:   conn,
	}, nil
}

func (client *CPGrpcClient) ListServices(ctx context.Context) ([]Service, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}

	c := pb.NewControlPlaneServiceClient(client.conn)

	req := &pb.ListServicesRequest{}

	res, err := c.ListServices(ctx, req, grpc.WaitForReady(true))
	if status.Code(err) != codes.OK {
		return nil, fmt.Errorf("unable to list services: %w", err)
	}

	out := make([]Service, 0, len(res.Svc))

	for _, pbSvc := range res.Svc {
		addr, err := netip.ParseAddr(pbSvc.Address)
		if err != nil {
			client.logger.Warn("invalid address returned by control plane grpc server",
				slog.String("address", pbSvc.Address),
				slog.Any("error", err),
			)
		}
		service := Service{
			Name:     pbSvc.Name,
			AddrPort: netip.AddrPortFrom(addr, uint16(pbSvc.Port)),
		}
		out = append(out, service)
	}

	return out, nil
}

func (client *CPGrpcClient) ListRealservers(ctx context.Context, svc Service) ([]Realserver, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}
	c := pb.NewControlPlaneServiceClient(client.conn)
	req := &pb.ListRealserversRequest{
		Svc: &pbsvc.Service{Name: svc.Name},
	}
	res, err := c.ListRealservers(ctx, req, grpc.WaitForReady(true))
	if status.Code(err) != codes.OK {
		return nil, fmt.Errorf("unable to list realservers: %w", err)
	}

	out := make([]Realserver, 0, len(res.Rs))

	for _, pbRs := range res.Rs {

		address, err := netip.ParseAddr(pbRs.Address)
		if err != nil {
			client.logger.Warn("invalid address returned by control plane grpc server",
				slog.String("address", pbRs.Address),
				slog.Any("error", err),
			)
		}

		var pooled Pooled
		switch pbRs.Pooled {
		case pbrs.Pooled_P_UNSPECIFIED:
			client.logger.Warn("invalid pooled value returned by control plane grpc server: Unspecified")
		case pbrs.Pooled_P_YES:
			pooled = PooledYes
		case pbrs.Pooled_P_NO:
			pooled = PooledNo
		case pbrs.Pooled_P_DEPOOL_BLOCKED:
			pooled = DepoolBlocked
		case pbrs.Pooled_P_DEPOOL_FAILED:
			pooled = DepoolFailed
		case pbrs.Pooled_P_REPOOL_FAILED:
			pooled = RepoolFailed
		case pbrs.Pooled_P_FORCE_REPOOL_FAILED:
			pooled = ForceRepoolFailed
		}

		healthy := false
		if pbRs.Healthy == pbrs.Healthy_H_YES {
			healthy = true
		}

		realserver := Realserver{
			Hostname: pbRs.Hostname,
			Address:  address,
			Mark:     pbRs.Mark,
			Weight:   int(pbRs.Weight),
			Pooled:   pooled,
			Healthy:  healthy,
		}

		out = append(out, realserver)

	}

	return out, nil
}

func (client *CPGrpcClient) Close() {
	if client.conn != nil {
		client.conn.Close()
	}
}
