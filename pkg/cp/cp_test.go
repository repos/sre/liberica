package cp

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log/slog"
	"net/netip"
	"reflect"
	"runtime"
	"sync"
	"testing"
	"time"

	"github.com/spf13/viper"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bgp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/etcd"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/fp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/healthcheck"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"

	"github.com/prometheus/client_golang/prometheus/testutil"
)

// mock etcd client returning ErrNotFound on GetKey()
type notFoundEtcdClient struct{}

func (c *notFoundEtcdClient) Get(context.Context) ([]etcd.Node, error) {
	return nil, etcd.ErrNotFound
}
func (c *notFoundEtcdClient) Watch(context.Context, chan<- etcd.Event) (<-chan bool, <-chan struct{}) {
	return make(chan bool), make(chan struct{})
}
func (c *notFoundEtcdClient) Close() {}

// mock etcd client
type testEtcdClient struct{}

func (c *testEtcdClient) Get(context.Context) ([]etcd.Node, error) {
	return []etcd.Node{
		etcd.Node{Hostname: "localhost", Pooled: etcd.PooledYes},
		etcd.Node{Hostname: "inactive", Pooled: etcd.PooledInactive},
	}, nil
}
func (c *testEtcdClient) Watch(context.Context, chan<- etcd.Event) (<-chan bool, <-chan struct{}) {
	return make(chan bool), make(chan struct{})
}
func (c *testEtcdClient) Close() {}

type testResolver struct{}

func (r *testResolver) LookupNetIP(context.Context, string, string) ([]netip.Addr, error) {
	return []netip.Addr{netip.MustParseAddr("127.0.0.1")}, nil
}

// mock hcforwarder client
type testHcforwarderClient struct{}

func (c *testHcforwarderClient) ListRealservers(context.Context) (map[uint32]netip.Addr, error) {
	ret := map[uint32]netip.Addr{}
	return ret, nil
}
func (c *testHcforwarderClient) CreateRealserver(context.Context, netip.Addr, uint32) error {
	return nil
}
func (c *testHcforwarderClient) DeleteRealserver(context.Context, netip.Addr, uint32) error {
	return nil
}
func (c *testHcforwarderClient) Close() {}

// mock healthcheck client
type testHealthcheckClient struct {
	CreateHealthcheckCalls int
	DeleteHealthcheckCalls int
}

func (c *testHealthcheckClient) WatchResults(ctx context.Context, service string, resultCh chan<- healthcheck.Result) (<-chan bool, chan struct{}) {
	readyCh := make(chan bool)
	ch := make(chan struct{})
	close(ch)
	return readyCh, ch
}
func (c *testHealthcheckClient) CreateHealthcheck(ctx context.Context, rs healthcheck.Realserver, hcsCfg []healthcheck.HealthcheckConfigurer) error {
	c.CreateHealthcheckCalls += 1
	return nil
}
func (c *testHealthcheckClient) DeleteHealthcheck(ctx context.Context, rs healthcheck.Realserver) error {
	c.DeleteHealthcheckCalls += 1
	return nil
}
func (c *testHealthcheckClient) Close() {}

// mock fp client
type testFPClient struct {
	services              []fp.Service
	realservers           []fp.Realserver
	CreateServiceCalls    int
	CreateRealserverCalls int
	DeleteRealserverCalls int
	UpdateRealserverCalls int
}

func NewTestFPClient(services []fp.Service, realservers []fp.Realserver) *testFPClient {
	return &testFPClient{
		services:    services,
		realservers: realservers,
	}
}

func (c *testFPClient) CreateService(context.Context, fp.Service) error {
	c.CreateServiceCalls += 1
	return nil
}
func (c *testFPClient) DeleteService(context.Context, fp.Service) error {
	return nil
}
func (c *testFPClient) ListServices(context.Context) ([]fp.Service, error) {
	return c.services, nil
}
func (c *testFPClient) CreateRealserver(context.Context, fp.Service, fp.Realserver) error {
	c.CreateRealserverCalls += 1
	return nil
}
func (c *testFPClient) DeleteRealserver(context.Context, fp.Service, fp.Realserver) error {
	c.DeleteRealserverCalls += 1
	return nil
}
func (c *testFPClient) UpdateRealserver(context.Context, fp.Service, fp.Realserver) error {
	c.UpdateRealserverCalls += 1
	return nil
}
func (c *testFPClient) ListRealservers(context.Context, fp.Service) ([]fp.Realserver, error) {
	return c.realservers, nil
}

func (c *testFPClient) Close() {}
func (c *testFPClient) Pin()   {}
func (c *testFPClient) Unpin() {}

// mock bgp client
type testBgpClient struct {
	prefixes     map[netip.Prefix]netip.Addr
	addPathCalls int
}

func (c *testBgpClient) AddPath(context.Context, netip.Prefix, netip.Addr) error {
	c.addPathCalls += 1
	return nil
}
func (c *testBgpClient) DeletePath(context.Context, netip.Prefix, netip.Addr) error {
	return nil
}
func (c *testBgpClient) IsPathPresent(ctx context.Context, prefix netip.Prefix, nextHop netip.Addr) (bool, error) {
	for cfgPrefix, cfgNextHop := range c.prefixes {
		if cfgPrefix == prefix && cfgNextHop == nextHop {
			return true, nil
		}
	}
	return false, nil
}
func (c *testBgpClient) AddPeer(context.Context, netip.Addr) error {
	return nil
}
func (c *testBgpClient) DeletePeer(context.Context, netip.Addr) error {
	return nil
}
func (c *testBgpClient) IsPeerPresent(context.Context, netip.Addr) (bool, error) {
	return false, nil
}
func (c *testBgpClient) ListPeers(context.Context, ...netip.Addr) ([]netip.Addr, error) {
	return nil, nil
}

func (c *testBgpClient) SoftResetPeer(context.Context, netip.Addr) error {
	return nil
}

func (c *testBgpClient) EnableCommunities(context.Context, []string) error {
	return nil
}
func (c *testBgpClient) DisableCommunities(context.Context) error {
	return nil
}
func (c *testBgpClient) AreCommunitiesEnabled(context.Context, []string) (bgp.CommunitiesEnabled, error) {
	return bgp.CommunitiesNotPresent, nil
}
func (c *testBgpClient) Close() {}

func getFunctionName(i any) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func TestCheckEtcdState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testCases := []struct {
		name     string
		cp       *ControlPlane
		args     stateFnArgs
		want     stateFn
		wantArgs stateFnArgs
		wantErr  error
	}{
		{
			name: "KeyNotFound",
			cp: &ControlPlane{
				logger:     logger,
				etcdClient: &notFoundEtcdClient{},
			},
			args:     stateFnArgs{},
			want:     retryCheckEtcdState,
			wantArgs: stateFnArgs{},
			wantErr:  nil,
		},
		{
			name: "GotNodes",
			cp: &ControlPlane{
				logger:     logger,
				etcdClient: &testEtcdClient{},
			},
			args:     stateFnArgs{},
			want:     resolveEtcdNodesState,
			wantArgs: stateFnArgs{realservers: []realserver{realserver{hostname: "localhost"}}},
			wantErr:  nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			got, gotArgs, gotErr := checkEtcdState(ctx, tc.cp, tc.args)
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}
			if gotErr != nil {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if len(gotArgs.realservers) != len(tc.wantArgs.realservers) {
				t.Errorf("unexpected args, want %+v, got %+v", tc.wantArgs, gotArgs)
			}
		})
	}
}

func TestRetryCheckEtcdState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cp := &ControlPlane{
		logger: logger,
	}

	timeAfterCalled := false
	timeAfter = func(time.Duration) <-chan time.Time {
		timeAfterCalled = true
		ch := make(chan time.Time)
		close(ch)
		return ch
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	wantArgs := stateFnArgs{
		realservers: []realserver{realserver{hostname: "localhost"}},
	}
	got, gotArgs, gotErr := retryCheckEtcdState(ctx, cp, wantArgs)
	if getFunctionName(got) != getFunctionName(checkEtcdState) {
		t.Errorf("unexpected state, want %s, got %s",
			getFunctionName(checkEtcdState),
			getFunctionName(got))
	}
	if gotErr != nil {
		t.Errorf("unexpected error, want %v, got %v", nil, gotErr)
	}
	if len(gotArgs.realservers) != len(wantArgs.realservers) {
		t.Errorf("unexpected args, want %+v, got %+v", wantArgs, gotArgs)
	}
	if !timeAfterCalled {
		t.Error("timeAfter hasn't been called")
	}
}

func TestResolveEtcdNodesState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	args := stateFnArgs{realservers: []realserver{realserver{hostname: "localhost"}}}
	wantArgs := stateFnArgs{
		realservers: []realserver{
			realserver{
				hostname: "localhost",
				address:  netip.MustParseAddr("127.0.0.1"),
			},
		},
	}
	netResolver = &testResolver{}
	cp := &ControlPlane{
		logger: logger,
		cfg:    controlPlaneConfig{addr: netip.MustParseAddr("127.0.0.2")},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	got, gotArgs, gotErr := resolveEtcdNodesState(ctx, cp, args)
	if gotErr != nil {
		t.Errorf("unexpected error: %v", gotErr)
	}
	if getFunctionName(got) != getFunctionName(configureHCForwarderState) {
		t.Errorf("unexpected state, want %s, got %s",
			getFunctionName(configureHCForwarderState),
			getFunctionName(got))
	}
	if len(gotArgs.realservers) != len(wantArgs.realservers) {
		t.Fatalf("unexpected args, want %+v, got %+v", wantArgs, gotArgs)
		return
	}
	if gotArgs.realservers[0].hostname != wantArgs.realservers[0].hostname {
		t.Fatalf("unexpected args, want %+v, got %+v", wantArgs, gotArgs)
		return
	}
	if gotArgs.realservers[0].address != wantArgs.realservers[0].address {
		t.Fatalf("unexpected args, want %+v, got %+v", wantArgs, gotArgs)
		return
	}
}

func TestConfigureHCForwarderState(t *testing.T) {
	cfg := []byte(`
hcforwarder:
  hashing_algorithm: jenkins
`)
	viper.SetConfigType("yaml")
	viper.ReadConfig(bytes.NewBuffer(cfg))
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name     string
		ft       fp.ForwardType
		args     stateFnArgs
		wantArgs stateFnArgs
	}{
		{
			name: "tunnel",
			ft:   fp.Tunnel,
			args: stateFnArgs{
				realservers: []realserver{
					realserver{
						hostname:  "localhost",
						address:   netip.MustParseAddr("127.0.0.1"),
						admPooled: true,
					},
					realserver{
						hostname:  "other.host",
						address:   netip.MustParseAddr("127.0.0.2"),
						admPooled: true,
					},
				},
			},
			wantArgs: stateFnArgs{
				realservers: []realserver{
					realserver{
						hostname:  "localhost",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						admPooled: true,
					},
					realserver{
						hostname:  "other.host",
						address:   netip.MustParseAddr("127.0.0.2"),
						mark:      1733220839,
						admPooled: true,
					},
				},
			},
		},
		{
			name: "direct_route",
			ft:   fp.DirectRoute,
			args: stateFnArgs{
				realservers: []realserver{
					realserver{
						hostname:  "localhost",
						address:   netip.MustParseAddr("127.0.0.1"),
						admPooled: true,
					},
					realserver{
						hostname:  "other.host",
						address:   netip.MustParseAddr("127.0.0.2"),
						admPooled: true,
					},
				},
			},
			wantArgs: stateFnArgs{
				realservers: []realserver{
					realserver{
						hostname:  "localhost",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      0,
						admPooled: true,
					},
					realserver{
						hostname:  "other.host",
						address:   netip.MustParseAddr("127.0.0.2"),
						mark:      0,
						admPooled: true,
					},
				},
			},
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			cp := &ControlPlane{
				logger:            logger,
				cfg:               controlPlaneConfig{fpForwardType: tc.ft},
				hcforwarderClient: &testHcforwarderClient{},
			}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, gotArgs, gotErr := configureHCForwarderState(ctx, cp, tc.args)
			if getFunctionName(got) != getFunctionName(configureHealthchecksState) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(configureHealthchecksState),
					getFunctionName(got))
			}
			if gotErr != nil {
				t.Errorf("unexpected error: %v", gotErr)
			}
			if len(gotArgs.realservers) != len(tc.wantArgs.realservers) {
				t.Fatalf("unexpected args, want %+v, got %+v", tc.wantArgs, gotArgs)
				return
			}
			for i, rs := range tc.wantArgs.realservers {
				if rs.address != gotArgs.realservers[i].address || rs.hostname != gotArgs.realservers[i].hostname ||
					rs.admPooled != gotArgs.realservers[i].admPooled || rs.mark != gotArgs.realservers[i].mark {
					t.Fatalf("unexpected realserver, want %+v, got %+v", rs, gotArgs.realservers[i])
					return
				}
			}
		})
	}

}

func TestConfigureHealthchecksState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cp := &ControlPlane{
		logger:          logger,
		hcmonitorClient: &testHealthcheckClient{},
	}
	args := stateFnArgs{
		realservers: []realserver{
			realserver{
				hostname: "localhost",
				address:  netip.MustParseAddr("127.0.0.1"),
				mark:     196420273,
			},
			realserver{
				hostname: "other.host",
				address:  netip.MustParseAddr("127.0.0.2"),
				mark:     1733220839,
			},
		},
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	got, _, gotErr := configureHealthchecksState(ctx, cp, args)
	if gotErr != nil {
		t.Errorf("unexpected error: %v", gotErr)
	}
	if getFunctionName(got) != getFunctionName(populateRealserversMapState) {
		t.Errorf("unexpected state, want populateRealserversMapState got %s", getFunctionName(got))
	}
	if cp.hcmonitorClient.(*testHealthcheckClient).CreateHealthcheckCalls != len(cp.realservers) {
		t.Errorf("unexpected number of CreateHealthcheck calls, want %d, got %d",
			len(cp.realservers),
			cp.hcmonitorClient.(*testHealthcheckClient).CreateHealthcheckCalls,
		)
	}
}

func TestReconfigureHealthchecksState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cp := &ControlPlane{
		logger:          logger,
		hcmonitorClient: &testHealthcheckClient{},
		readWrite:       true,
		realservers: map[netip.Addr]realserver{
			netip.MustParseAddr("127.0.0.1"): realserver{
				hostname:  "foo1001.eqiad.wmnet",
				address:   netip.MustParseAddr("127.0.0.1"),
				mark:      196420273,
				admPooled: true,
				pooled:    PooledYes,
				hcResults: map[healthcheck.HealthcheckID]bool{
					healthcheck.HealthcheckID(1337): true,
				},
			},
		},
	}

	healthcheckIDs := map[healthcheck.HealthcheckID]struct{}{}
	for _, rs := range cp.realservers {
		for id, _ := range rs.hcResults {
			if _, present := healthcheckIDs[id]; !present {
				healthcheckIDs[id] = struct{}{}
			}
		}
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, _, gotErr := reconfigureHealthchecksState(ctx, cp, stateFnArgs{})
	if gotErr != nil {
		t.Errorf("unexpected error: %v", gotErr)
	}
	if cp.readWrite {
		t.Error("readWrite should have been disabled")
	}

	for id, _ := range healthcheckIDs {
		for _, rs := range cp.realservers {
			if _, present := rs.hcResults[id]; present {
				t.Errorf("healthcheck results haven't been cleared for healthcheck with id %v", id)
			}
		}
	}

	if cp.hcmonitorClient.(*testHealthcheckClient).CreateHealthcheckCalls != len(cp.realservers) {
		t.Errorf("unexpected number of CreateHealthcheck calls, want %d, got %d",
			len(cp.realservers),
			cp.hcmonitorClient.(*testHealthcheckClient).CreateHealthcheckCalls,
		)
	}
}

func TestPopulateRealserversMapState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name            string
		args            stateFnArgs
		want            stateFn
		wantErr         error
		wantRealservers map[netip.Addr]realserver
	}{
		{"valid", stateFnArgs{
			realservers: []realserver{
				realserver{
					hostname:  "localhost",
					address:   netip.MustParseAddr("127.0.0.1"),
					mark:      196420273,
					admPooled: true,
				},
				realserver{
					hostname:  "other.host",
					address:   netip.MustParseAddr("127.0.0.2"),
					mark:      1733220839,
					admPooled: false,
				},
				realserver{
					hostname: "host.with.invalid.address",
					address:  netip.Addr{},
				},
			},
		}, checkFPServiceState, nil, map[netip.Addr]realserver{
			netip.MustParseAddr("127.0.0.1"): realserver{
				hostname:  "localhost",
				address:   netip.MustParseAddr("127.0.0.1"),
				mark:      196420273,
				admPooled: true,
				pooled:    PooledYes,
			},
		}},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			cp := &ControlPlane{
				logger:      logger,
				realservers: nil,
			}

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := populateRealserversMapState(ctx, cp, tc.args)
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}
			if cp.realservers == nil {
				t.Fatal("realservers map hasn't been populated")
				return
			}
			if len(cp.realservers) != len(tc.wantRealservers) {
				t.Errorf("unexpected number of realservers, want 2 got %d", len(cp.realservers))
			}
			for address, rs := range tc.wantRealservers {
				gotRs, present := cp.realservers[address]
				if !present {
					t.Errorf("realserver with address %v not found", address)
					continue
				}
				if rs.address != gotRs.address || rs.hostname != gotRs.hostname ||
					rs.admPooled != gotRs.admPooled || rs.mark != gotRs.mark || rs.pooled != gotRs.pooled {
					t.Errorf("unexpected realserver for address %v, want %+v, got %+v", address, rs, gotRs)
				}
			}
		})
	}
}

func TestCheckFPServiceState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name       string
		services   []fp.Service
		vipAddress netip.Addr
		vipPort    uint16
		want       stateFn
		wantErr    error
	}{
		{"emptyFP", []fp.Service{}, netip.Addr{}, 0, addServiceForwardingPlaneState, nil},
		{"ExistingService", []fp.Service{fp.Service{Address: netip.MustParseAddr("192.0.0.1"), Port: 80}}, netip.MustParseAddr("192.0.0.1"), 80, checkFPRealserversState, nil},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			fpClient := NewTestFPClient(tc.services, nil)
			cp := &ControlPlane{
				logger:   logger,
				fpClient: fpClient,
				cfg: controlPlaneConfig{
					addr: tc.vipAddress,
					port: tc.vipPort,
				},
			}

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := checkFPServiceState(ctx, cp, stateFnArgs{})
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}
			if getFunctionName(tc.want) != getFunctionName(addServiceForwardingPlaneState) {
				zeroSvc := fp.Service{}
				if cp.fpService == zeroSvc {
					t.Error("control plane fpService should have been initialized")
				}
			}
		})
	}
}

func TestAddServiceForwardingPlaneState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name       string
		vipAddress netip.Addr
		vipPort    uint16
		want       stateFn
		wantErr    error
	}{
		{"addService", netip.MustParseAddr("192.0.0.1"), 80, checkFPRealserversState, nil},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			fpClient := NewTestFPClient([]fp.Service{}, nil)
			cp := &ControlPlane{
				logger:   logger,
				fpClient: fpClient,
				cfg: controlPlaneConfig{
					addr: tc.vipAddress,
					port: tc.vipPort,
				},
			}

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := addServiceForwardingPlaneState(ctx, cp, stateFnArgs{})
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}
			zeroSvc := fp.Service{}
			if cp.fpService == zeroSvc {
				t.Error("control plane fpService should have been initialized")
			}
			if fpClient.CreateServiceCalls == 0 {
				t.Error("addServiceForwardingPlaneState didn't call CreateService()")
			}
		})
	}
}

func TestCheckFPRealserversState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name          string
		vipAddress    netip.Addr
		vipPort       uint16
		realservers   map[netip.Addr]realserver
		fpRealservers []fp.Realserver
		want          stateFn
		wantErr       error
	}{
		{"addRealserver",
			netip.MustParseAddr("192.0.0.1"),
			80,
			map[netip.Addr]realserver{
				netip.MustParseAddr("10.0.0.1"): realserver{
					address:   netip.MustParseAddr("10.0.0.1"),
					admPooled: true,
				},
			},
			nil,
			updateMetricsState,
			nil,
		},
		{"updateRealserver",
			netip.MustParseAddr("192.0.0.1"),
			80,
			map[netip.Addr]realserver{
				netip.MustParseAddr("10.0.0.1"): realserver{
					address:   netip.MustParseAddr("10.0.0.1"),
					admPooled: true,
					weight:    10,
				},
			},
			[]fp.Realserver{
				fp.Realserver{
					Address: netip.MustParseAddr("10.0.0.1"),
					Weight:  20,
				},
			},
			updateMetricsState,
			nil,
		},
		{"deleteRealserver",
			netip.MustParseAddr("192.0.0.1"),
			80,
			map[netip.Addr]realserver{
				netip.MustParseAddr("10.0.0.1"): realserver{
					address:   netip.MustParseAddr("10.0.0.1"),
					admPooled: true,
					pooled:    PooledYes,
				},
			},
			[]fp.Realserver{
				fp.Realserver{
					Address: netip.MustParseAddr("10.0.0.1"),
				},
				fp.Realserver{
					Address: netip.MustParseAddr("10.0.0.2"),
				},
			},
			deleteHealthchecksState,
			nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			fpClient := NewTestFPClient([]fp.Service{}, tc.fpRealservers)
			fpService := fp.Service{
				Address: tc.vipAddress,
				Port:    tc.vipPort,
			}
			cp := &ControlPlane{
				logger:   logger,
				fpClient: fpClient,
				cfg: controlPlaneConfig{
					addr: tc.vipAddress,
					port: tc.vipPort,
				},
				fpService:   fpService,
				realservers: tc.realservers,
			}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := checkFPRealserversState(ctx, cp, stateFnArgs{})
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}
			createdRealservers := 0
			deletedRealservers := 0
			updatedRealservers := 0
			for _, rs := range tc.realservers {
				present := false
				for _, fpRs := range tc.fpRealservers {
					if fpRs.Address == rs.address {
						present = true
						break
					}
				}
				if rs.admPooled && !present {
					createdRealservers += 1
				}
			}

			for _, fpRs := range tc.fpRealservers {
				rs, present := tc.realservers[fpRs.Address]
				if !present {
					deletedRealservers += 1
				} else {
					if present && !rs.admPooled {
						deletedRealservers += 1
					} else if present && rs.admPooled && uint32(rs.weight) != fpRs.Weight {
						updatedRealservers += 1
					}
				}
			}

			if createdRealservers != fpClient.CreateRealserverCalls {
				t.Errorf("unexpected number of CreateRealserver() calls, want %d, got %d", createdRealservers, fpClient.CreateRealserverCalls)
			}
			if deletedRealservers != fpClient.DeleteRealserverCalls {
				t.Errorf("unexpected number of DeleteRealserver() calls, want %d, got %d", deletedRealservers, fpClient.DeleteRealserverCalls)
			}
			if updatedRealservers != fpClient.UpdateRealserverCalls {
				t.Errorf("unexpected number of UpdateRealserver() calls, want %d, got %d", updatedRealservers, fpClient.UpdateRealserverCalls)
			}
		})
	}
}

func TestDeleteHealthchecksState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		want    stateFn
		wantErr error
	}{
		{
			name: "deleteHealthcheck",
			args: stateFnArgs{
				realservers: []realserver{
					realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						mark:    196420273,
					},
				},
			},
			want:    checkDepoolThresholdIsMetState,
			wantErr: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			hcmonitorClient := &testHealthcheckClient{}
			cp := &ControlPlane{
				logger:          logger,
				hcmonitorClient: hcmonitorClient,
			}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := deleteHealthchecksState(ctx, cp, tc.args)
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}

			if len(tc.args.realservers) != hcmonitorClient.DeleteHealthcheckCalls {
				t.Errorf("unexpected number of DeleteHealthcheck() calls, want %d, got %d", len(tc.args.realservers), hcmonitorClient.DeleteHealthcheckCalls)
			}
		})
	}
}

func TestCheckDepoolThresholdIsMetState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		cp      *ControlPlane
		want    stateFn
		wantErr error
	}{
		{
			name: "depool threshold is being met (NOOP)",
			cp: &ControlPlane{
				logger:    logger,
				cfg:       controlPlaneConfig{depoolThreshold: .5},
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledYes,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  PooledNo,
					},
				},
			},
			want:    updateMetricsState,
			wantErr: nil,
		},
		{
			name: "depool threshold is being enforced (NOOP)",
			cp: &ControlPlane{
				logger:    logger,
				cfg:       controlPlaneConfig{depoolThreshold: .5},
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  DepoolBlocked,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  PooledNo,
					},
				},
			},
			want:    updateMetricsState,
			wantErr: nil,
		},
		{
			name: "realservers need to be repooled to enforce depool threshold",
			cp: &ControlPlane{
				logger:    logger,
				cfg:       controlPlaneConfig{depoolThreshold: .5},
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledNo,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  PooledNo,
					},
				},
			},
			want:    enforceDepoolThresholdState,
			wantErr: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := checkDepoolThresholdIsMetState(ctx, tc.cp, stateFnArgs{})
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}

		})
	}
}

func TestEnforceDepoolThresholdState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		cp      *ControlPlane
		want    stateFn
		wantErr error
	}{
		{
			name: "successful operation",
			args: stateFnArgs{
				realservers: []realserver{
					realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledNo,
					},
				},
			},
			cp: &ControlPlane{
				logger:    logger,
				fpClient:  NewTestFPClient(nil, nil),
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledNo,
					},
				},
			},
			want:    checkPendingRealserversState,
			wantErr: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := enforceDepoolThresholdState(ctx, tc.cp, tc.args)
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got))
			}

			for _, argRs := range tc.args.realservers {
				rs, present := tc.cp.realservers[argRs.address]
				if !present {
					t.Errorf("realserver not found on realservers map after being repooled: %s", argRs.address)
				}
				if rs.pooled != DepoolBlocked {
					t.Errorf("after repooling the realserver, it should be flagged as DepoolBlocked, got %v for %v", rs.pooled, rs.address)
				}
			}

		})
	}
}

func TestHandleHealthcheckResultState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))

	testcases := []struct {
		name          string
		args          stateFnArgs
		cp            *ControlPlane
		want          stateFn
		wantErr       error
		wantReadWrite bool
	}{
		{"invalid arguments", stateFnArgs{}, &ControlPlane{logger: logger}, nil, ErrInvalidStateArguments, false},
		{
			name: "unknown realserver",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp:            &ControlPlane{logger: logger},
			want:          nil,
			wantErr:       nil,
			wantReadWrite: false,
		},
		{
			"result triggers a realserver depool",
			stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      false,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			&ControlPlane{
				logger:        logger,
				readWrite:     true,
				configuredHCs: 1,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{
							healthcheck.HealthcheckID(1337): true,
						},
					},
				},
			},
			depoolRealserverState,
			nil,
			true,
		},
		{
			name: "do not attempt to depool an already depooled realserver",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      false,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger:        logger,
				readWrite:     true,
				configuredHCs: 1,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledNo,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{
							healthcheck.HealthcheckID(1337): true,
							healthcheck.HealthcheckID(1338): false,
						},
					},
				},
			},
			want:          checkPendingRealserversState,
			wantErr:       nil,
			wantReadWrite: true,
		},
		{
			"result triggers a realserver repool",
			stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			&ControlPlane{
				logger:        logger,
				readWrite:     true,
				configuredHCs: 1,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledNo,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{
							healthcheck.HealthcheckID(1337): true,
						},
					},
				},
			},
			repoolRealserverState,
			nil,
			true,
		},
		{
			"partial healthy result doesn't trigger a realserver repool",
			stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			&ControlPlane{
				logger:        logger,
				readWrite:     true,
				configuredHCs: 2,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledNo,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{
							healthcheck.HealthcheckID(1337): false,
							healthcheck.HealthcheckID(1338): false,
						},
					},
				},
			},
			checkPendingRealserversState,
			nil,
			true,
		},
		{
			"readWrite is true after receiving healthchecks for all realservers",
			stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			&ControlPlane{
				logger:        logger,
				readWrite:     false,
				configuredHCs: 1,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
				},
			},
			checkPendingRealserversState,
			nil,
			true,
		},
		{
			"readWrite is false after receiving a healthcheck for one realserver",
			stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			&ControlPlane{
				logger:        logger,
				readWrite:     false,
				configuredHCs: 1,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						hostname:  "foo1002.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.2"),
						mark:      1733220839,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
				},
			},
			updateMetricsState,
			nil,
			false,
		},
		{
			"readWrite becomes true after receiving the latest healthcheck and depooling of unhealthy realservers gets triggered",
			stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      false,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			&ControlPlane{
				logger:        logger,
				readWrite:     false,
				configuredHCs: 1,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						hostname:  "foo1002.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.2"),
						mark:      1733220839,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{
							healthcheck.HealthcheckID(1337): false,
						},
					},
				},
			},
			depoolRealserverState,
			nil,
			true,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := handleHealthcheckResultState(ctx, tc.cp, tc.args)
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
			if gotErr != tc.wantErr {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if tc.cp.readWrite != tc.wantReadWrite {
				t.Errorf("unexpected fullPicture, want %v, got %v", tc.wantReadWrite, tc.cp.readWrite)
			}
		})
	}
}

func TestDepoolRealserverState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name     string
		args     stateFnArgs
		cp       *ControlPlane
		want     stateFn
		wantErr  error
		depooled bool
	}{
		{"invalid argument", stateFnArgs{}, &ControlPlane{logger: logger}, nil, ErrInvalidStateArguments, false},
		{
			name: "depool unknown realserver",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger: logger,
			},
			want:     nil,
			wantErr:  ErrUnknownRealserver,
			depooled: false,
		},
		{
			name: "unable to depool in read only mode",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger:    logger,
				readWrite: false,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
				},
			},
			want:     updateMetricsState,
			wantErr:  nil,
			depooled: false,
		},
		{
			name: "unable to depool due to depool threshold",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger:    logger,
				cfg:       controlPlaneConfig{depoolThreshold: 0.5},
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
				},
			},
			want:     updateMetricsState,
			wantErr:  nil,
			depooled: false,
		},
		{
			name: "sucessful depool",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger:    logger,
				cfg:       controlPlaneConfig{depoolThreshold: 0.5},
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						hostname:  "foo1002.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.2"),
						mark:      1733220839,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
				},
			},
			want:     checkPendingRealserversState,
			wantErr:  nil,
			depooled: true,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			fpClient := NewTestFPClient(nil, nil)
			tc.cp.fpClient = fpClient
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			pooledMap := map[netip.Addr]Pooled{}
			for addr, rs := range tc.cp.realservers {
				pooledMap[addr] = rs.pooled
			}
			got, _, gotErr := depoolRealserverState(ctx, tc.cp, tc.args)
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}

			expectedDepoolCalls := 0
			if tc.depooled {
				expectedDepoolCalls = 1
			}

			if fpClient.DeleteRealserverCalls != expectedDepoolCalls {
				t.Errorf("unexpected number of fpClient.DeleteRealserver() want %d, got %d", expectedDepoolCalls, fpClient.DeleteRealserverCalls)
			}

			if !tc.cp.readWrite { // we are in read only mode
				for addr, pooled := range pooledMap {
					cpRs, present := tc.cp.realservers[addr]
					if !present {
						t.Errorf("realserver with address %s deleted from realservers map while being in read only mode", addr.String())
					}
					if cpRs.pooled != pooled {
						t.Errorf("realserver with address %s got its pooled flag modified in read only mode, before %v, after %v",
							addr.String(), pooled, cpRs.pooled)
					}
				}
			}
		})
	}
}

func TestRepoolRealserverState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name     string
		args     stateFnArgs
		cp       *ControlPlane
		want     stateFn
		wantErr  error
		repooled bool
	}{
		{"invalid argument", stateFnArgs{}, &ControlPlane{logger: logger}, nil, ErrInvalidStateArguments, false},
		{
			name: "repool unknown realserver",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger: logger,
			},
			want:     nil,
			wantErr:  ErrUnknownRealserver,
			repooled: false,
		},
		{
			name: "sucessful repool",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger:    logger,
				cfg:       controlPlaneConfig{depoolThreshold: 0.5},
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    PooledNo,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						hostname:  "foo1002.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.2"),
						mark:      1733220839,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
				},
			},
			want:     checkPendingRealserversState,
			wantErr:  nil,
			repooled: true,
		},
		{
			name: "repool a DepoolBlocked realserver",
			args: stateFnArgs{
				hcResult: &healthcheck.Result{
					HealthcheckName: "DummyTest",
					HealthcheckID:   healthcheck.HealthcheckID(1337),
					Successful:      true,
					Realserver: healthcheck.Realserver{
						Service:  "foo",
						Mark:     196420273,
						AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
					},
				},
			},
			cp: &ControlPlane{
				logger:    logger,
				cfg:       controlPlaneConfig{depoolThreshold: 0.5},
				readWrite: true,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.1"),
						mark:      196420273,
						weight:    1,
						pooled:    DepoolBlocked,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						hostname:  "foo1002.eqiad.wmnet",
						address:   netip.MustParseAddr("127.0.0.2"),
						mark:      1733220839,
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
						hcResults: map[healthcheck.HealthcheckID]bool{},
					},
				},
			},
			want:     checkPendingRealserversState,
			wantErr:  nil,
			repooled: false,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			fpClient := NewTestFPClient(nil, nil)
			tc.cp.fpClient = fpClient
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			got, _, gotErr := repoolRealserverState(ctx, tc.cp, tc.args)
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
			expectedRepoolCalls := 0
			if tc.repooled {
				expectedRepoolCalls = 1
			}

			if fpClient.CreateRealserverCalls != expectedRepoolCalls {
				t.Errorf("unexpected number of fpClient.CreateRealserver() want %d, got %d", expectedRepoolCalls, fpClient.CreateRealserverCalls)
			}
		})
	}
}

func TestHandleEtcdEventState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		want    stateFn
		wantErr error
	}{
		{"invalid argument", stateFnArgs{}, nil, ErrInvalidStateArguments},
		{
			name: "unknown etcd event doesn't trigger an unrecoverable error",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventType("foobar"),
				},
			},
			want:    nil,
			wantErr: nil,
		},
		{
			name: "valid delete event",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventDelete,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledYes,
						},
					},
				},
			},
			want:    handleEtcdDeleteEventState,
			wantErr: nil,
		},
		{
			name: "valid update event",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventSet,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledYes,
						},
					},
				},
			},
			want:    handleEtcdUpdateEventState,
			wantErr: nil,
		},
		{
			name: "valid create event",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventCreate,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledYes,
						},
					},
				},
			},
			want:    handleEtcdCreateEventState,
			wantErr: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			cp := &ControlPlane{
				logger: logger,
			}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := handleEtcdEventState(ctx, cp, tc.args)
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
		})
	}
}

func TestHandleEtcdCreateEventState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		want    stateFn
		wantErr error
	}{
		{"invalid argument", stateFnArgs{}, nil, ErrInvalidStateArguments},
		{
			name: "valid event with pooled realservers",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventCreate,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledYes,
						},
					},
				},
			},
			want:    resolveEtcdNodesState,
			wantErr: nil,
		},
		{
			name: "not pooled realservers are ignored",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventCreate,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledNo,
						},
						etcd.Node{
							Hostname: "foo1002.eqiad.wmnet",
							Weight:   2,
							Pooled:   etcd.PooledInactive,
						},
					},
				},
			},
			want:    nil,
			wantErr: nil,
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			cp := &ControlPlane{
				logger: logger,
			}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, gotArgs, gotErr := handleEtcdCreateEventState(ctx, cp, tc.args)
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
			if tc.wantErr != nil {
				return
			}
			admPooledNodes := 0
			for _, node := range tc.args.etcdEvent.Nodes {
				if node.Pooled == etcd.PooledYes {
					admPooledNodes += 1
				}
			}
			if len(gotArgs.realservers) != admPooledNodes {
				t.Errorf("unexpected number of realservers on returned args, want %d, got %d", admPooledNodes, len(gotArgs.realservers))
			}
		})
	}
}

func TestHandleEtcdDeleteEventState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		cp      *ControlPlane
		want    stateFn
		wantErr error
	}{
		{"invalid argument", stateFnArgs{}, &ControlPlane{logger: logger}, nil, ErrInvalidStateArguments},
		{
			name: "deleting an administratively depooled realserver results in NOOP",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventCreate,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledNo,
						},
					},
				},
			},
			cp:      &ControlPlane{logger: logger},
			want:    nil,
			wantErr: nil,
		},
		{
			name: "delete a realserver",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventCreate,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledNo,
						},
					},
				},
			},
			cp: &ControlPlane{
				logger: logger,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
					},
				},
			},
			want:    deleteRealserversState,
			wantErr: nil,
		},
		{
			name: "delete a service",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventCreate,
					Nodes:     []etcd.Node{},
				},
			},
			cp: &ControlPlane{
				logger: logger,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
					},
				},
			},
			want:    nil,
			wantErr: ErrEtcdServiceDeleted,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			got, gotArgs, gotErr := handleEtcdDeleteEventState(ctx, tc.cp, tc.args)
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
			if tc.wantErr != nil {
				return
			}
			deleteRealservers := 0
			for _, node := range tc.args.etcdEvent.Nodes {
				for _, rs := range tc.cp.realservers {
					if node.Hostname == rs.hostname {
						deleteRealservers += 1
					}
				}
			}
			if len(gotArgs.realservers) != deleteRealservers {
				t.Errorf("unexpected amount of realservers to delete, want %d, got %d", deleteRealservers, len(gotArgs.realservers))
			}
		})
	}
}

func TestHandleEtcdUpdateEventState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		cp      *ControlPlane
		want    stateFn
		wantErr error
	}{
		{"invalid argument", stateFnArgs{}, &ControlPlane{logger: logger}, nil, ErrInvalidStateArguments},
		{
			name: "update event repools a realserver",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventSet,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledYes,
						},
					},
				},
			},
			cp: &ControlPlane{
				logger: logger,
			},
			want:    handleEtcdCreateEventState,
			wantErr: nil,
		},
		{
			name: "update event depools a realserver",
			args: stateFnArgs{
				etcdEvent: &etcd.Event{
					EventType: etcd.EventSet,
					Nodes: []etcd.Node{
						etcd.Node{
							Hostname: "foo1001.eqiad.wmnet",
							Weight:   1,
							Pooled:   etcd.PooledNo,
						},
					},
				},
			},
			cp: &ControlPlane{
				logger: logger,
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname:  "foo1001.eqiad.wmnet",
						weight:    1,
						pooled:    PooledYes,
						admPooled: true,
					},
				},
			},
			want:    handleEtcdDeleteEventState,
			wantErr: nil,
		}, /*
			{
				name: "update event updates the weight of a realserver",
			}, */
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := handleEtcdUpdateEventState(ctx, tc.cp, tc.args)
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
		})
	}
}

func TestCheckPendingRealserversState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name            string
		cp              *ControlPlane
		wantDepoolCalls int
		wantRepoolCalls int
	}{
		{
			name: "NOOP",
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				cfg:      controlPlaneConfig{depoolThreshold: .5},
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledYes,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  PooledNo,
					},
				},
			},
		},
		{
			name: "previously blocked realserver gets depooled",
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				cfg:      controlPlaneConfig{depoolThreshold: .5},
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledYes,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  DepoolBlocked,
					},
				},
			},
			wantDepoolCalls: 1,
		},
		{
			name: "previously failed realserver gets depooled",
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				cfg:      controlPlaneConfig{depoolThreshold: .5},
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledYes,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  DepoolFailed,
					},
				},
			},
			wantDepoolCalls: 1,
		},
		{
			name: "previously failed realserver gets repooled",
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				cfg:      controlPlaneConfig{depoolThreshold: .5},
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledYes,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  RepoolFailed,
					},
				},
			},
			wantRepoolCalls: 1,
		},
		{
			name: "previously failed realserver gets repooled and blocked realserver gets depooled as a consequence",
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				cfg:      controlPlaneConfig{depoolThreshold: .5},
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  RepoolFailed,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  DepoolBlocked,
					},
				},
			},
			wantDepoolCalls: 1,
			wantRepoolCalls: 1,
		},
		{
			name: "previously blocked realserver stays blocked due to depool threshold",
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				cfg:      controlPlaneConfig{depoolThreshold: .5},
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledNo,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  DepoolBlocked,
					},
				},
			},
		},
		{
			name: "realserver flagged as PooledYes but unhealthy (hc received while readWrite = false) gets depooled",
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				cfg:      controlPlaneConfig{depoolThreshold: .5},
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						address: netip.MustParseAddr("127.0.0.1"),
						pooled:  PooledYes,
					},
					netip.MustParseAddr("127.0.0.2"): realserver{
						address: netip.MustParseAddr("127.0.0.2"),
						pooled:  PooledYes,
						hcResults: map[healthcheck.HealthcheckID]bool{
							healthcheck.HealthcheckID(1337): false,
						},
					},
				},
			},
			wantDepoolCalls: 1,
			wantRepoolCalls: 0,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			got, _, gotErr := checkPendingRealserversState(ctx, tc.cp, stateFnArgs{})
			if gotErr != nil {
				t.Errorf("unexpected error: %v", gotErr)
			}
			if getFunctionName(got) != getFunctionName(updateMetricsState) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(updateMetricsState),
					getFunctionName(got),
				)
			}

			if tc.wantDepoolCalls != tc.cp.fpClient.(*testFPClient).DeleteRealserverCalls {
				t.Errorf("unexpected number of calls to DeleteRealserver, want %d, got %d",
					tc.wantDepoolCalls,
					tc.cp.fpClient.(*testFPClient).DeleteRealserverCalls,
				)
			}
			if tc.wantRepoolCalls != tc.cp.fpClient.(*testFPClient).CreateRealserverCalls {
				t.Errorf("unexpected number of calls to CreateRealserver, want %d, got %d",
					tc.wantRepoolCalls,
					tc.cp.fpClient.(*testFPClient).CreateRealserverCalls,
				)
			}
		})
	}
}

func TestUpdateMetricsState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	cp := &ControlPlane{
		logger:  logger,
		service: "foo",
		realservers: map[netip.Addr]realserver{
			netip.MustParseAddr("127.0.0.1"): realserver{
				pooled: PooledYes,
			},
			netip.MustParseAddr("127.0.0.2"): realserver{
				pooled: PooledNo,
			},
			netip.MustParseAddr("127.0.0.3"): realserver{
				pooled: DepoolBlocked,
			},
			netip.MustParseAddr("127.0.0.4"): realserver{
				pooled: RepoolFailed,
			},
			netip.MustParseAddr("127.0.0.5"): realserver{
				pooled: PooledYes,
				hcResults: map[healthcheck.HealthcheckID]bool{
					healthcheck.HealthcheckID(1337): true,
					healthcheck.HealthcheckID(1338): false,
				},
			},
		},
	}

	updateMetricsState(ctx, cp, stateFnArgs{})

	if testutil.ToFloat64(metrics.AdministrativelyPooledRealservers) != float64(len(cp.realservers)) {
		t.Error("unexpected value")
	}

	if testutil.ToFloat64(metrics.PooledRealservers) != 1.0 {
		t.Error("unexpected value for PooledRealservers metric")
	}

	if testutil.ToFloat64(metrics.UnhealthyPooledRealservers) != 2.0 {
		t.Error("unexpected value for UnhealthyPooledRealservers")
	}

	if testutil.ToFloat64(metrics.HealthyRealserversNotPooled) != 1.0 {
		t.Error("unexpected value for HealthyRealserversNotPooled")
	}
}

func TestUpdateRealserversState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		cp      *ControlPlane
		want    stateFn
		wantErr error
	}{
		{
			name: "realserver not found in cp.realservers",
			args: stateFnArgs{
				realservers: []realserver{
					realserver{
						hostname: "foo1001.eqiad.wmnet",
						address:  netip.MustParseAddr("127.0.0.1"),
						weight:   2,
					},
				},
			},
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
			},
			want:    nil,
			wantErr: ErrUnknownRealserver,
		},
		{
			name: "successful operation",
			args: stateFnArgs{
				realservers: []realserver{
					realserver{
						hostname: "foo1001.eqiad.wmnet",
						address:  netip.MustParseAddr("127.0.0.1"),
						weight:   2,
					},
				},
			},
			cp: &ControlPlane{
				logger:   logger,
				fpClient: NewTestFPClient(nil, nil),
				realservers: map[netip.Addr]realserver{
					netip.MustParseAddr("127.0.0.1"): realserver{
						hostname: "foo1001.eqiad.wmnet",
						address:  netip.MustParseAddr("127.0.0.1"),
						weight:   1,
					},
				},
			},
			want:    nil,
			wantErr: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			got, _, gotErr := updateRealserversState(ctx, tc.cp, tc.args)
			if !errors.Is(gotErr, tc.wantErr) {
				t.Fatalf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
			if tc.wantErr != nil {
				return
			}
			if len(tc.args.realservers) != tc.cp.fpClient.(*testFPClient).UpdateRealserverCalls {
				t.Errorf("unexpected number of calls to fpClient.UpdateRealserver(), want %d, got %d",
					len(tc.args.realservers),
					tc.cp.fpClient.(*testFPClient).UpdateRealserverCalls,
				)
			}
			for _, argRs := range tc.args.realservers {
				rs, present := tc.cp.realservers[argRs.address]
				if !present {
					t.Errorf("realserver %s still on cp.realservers after being deleted", rs.address.String())
				}
				if argRs.weight != rs.weight {
					t.Errorf("unexpected weight, want %d, got %d", argRs.weight, rs.weight)
				}
			}
		})
	}
}

func TestDeleteRealserversState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name    string
		args    stateFnArgs
		cp      *ControlPlane
		want    stateFn
		wantErr error
	}{
		{
			name: "successful operation",
			args: stateFnArgs{
				realservers: []realserver{
					realserver{
						hostname: "foo1001.eqiad.wmnet",
						address:  netip.MustParseAddr("127.0.0.1"),
					},
				},
			},
			cp: &ControlPlane{
				logger: logger,
				fpClient: NewTestFPClient(nil, []fp.Realserver{
					fp.Realserver{
						Address:     netip.MustParseAddr("127.0.0.1"),
						Weight:      uint32(1),
						Mark:        196420273,
						ForwardType: fp.Tunnel,
					},
				}),
			},
			want:    deleteHealthchecksState,
			wantErr: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			got, _, gotErr := deleteRealserversState(ctx, tc.cp, tc.args)
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if getFunctionName(got) != getFunctionName(tc.want) {
				t.Errorf("unexpected state, want %s, got %s",
					getFunctionName(tc.want),
					getFunctionName(got),
				)
			}
			if len(tc.args.realservers) != tc.cp.fpClient.(*testFPClient).DeleteRealserverCalls {
				t.Errorf("unexpected number of calls to fpClient.DeleteRealserver(), want %d, got %d",
					len(tc.args.realservers),
					tc.cp.fpClient.(*testFPClient).DeleteRealserverCalls,
				)
			}
			for _, rs := range tc.args.realservers {
				if _, present := tc.cp.realservers[rs.address]; present {
					t.Errorf("realserver %s still on cp.realservers after being deleted", rs.address.String())
				}
			}
		})
	}
}

func TestConfigureBgpPathState(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name        string
		bgpPrefixes map[netip.Prefix]netip.Addr
		addPrefix   bool
		cpConfig    controlPlaneConfig
		bgpConfig   *bgp.BgpConfig
	}{
		{
			name: "prefix is already configured",
			bgpPrefixes: map[netip.Prefix]netip.Addr{
				netip.MustParsePrefix("192.0.0.1/32"): netip.MustParseAddr("127.0.0.2"),
			},
			cpConfig: controlPlaneConfig{
				addr: netip.MustParseAddr("192.0.0.1"),
			},
			bgpConfig: &bgp.BgpConfig{
				NextHop4: netip.MustParseAddr("127.0.0.2"),
			},
			addPrefix: false,
		},
		{
			name: "prefix is added",
			cpConfig: controlPlaneConfig{
				addr: netip.MustParseAddr("192.0.0.1"),
			},
			bgpConfig: &bgp.BgpConfig{
				NextHop4: netip.MustParseAddr("127.0.0.2"),
			},
			addPrefix: true,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			// new bgpPrefixMap per test
			bgpPrefixMap = sync.Map{}
			bgpClient := &testBgpClient{
				prefixes: tc.bgpPrefixes,
			}
			cp := &ControlPlane{
				logger:    logger,
				cfg:       tc.cpConfig,
				bgpConfig: tc.bgpConfig,
				bgpClient: bgpClient,
			}

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			args := stateFnArgs{}
			_, _, err := configureBgpPathState(ctx, cp, args)
			if err != nil {
				t.Fatalf("unexpected error: %v", err)
				return
			}

			addPathCalls := 0
			if tc.addPrefix {
				addPathCalls = 1
			}
			if bgpClient.addPathCalls != addPathCalls {
				t.Errorf("unexpected number of AddPath() calls, want %d, got %d", addPathCalls, bgpClient.addPathCalls)
			}

		})
	}
}

func TestValidateDepoolThreshold(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name        string
		threshold   float64
		realservers map[netip.Addr]realserver
		target      netip.Addr
		want        bool
	}{
		{
			name:      "all realservers healthy, depool threshold allows the depool",
			threshold: 0.5,
			realservers: map[netip.Addr]realserver{
				netip.MustParseAddr("127.0.0.1"): realserver{
					address: netip.MustParseAddr("127.0.0.1"),
					pooled:  PooledYes,
				},
				netip.MustParseAddr("127.0.0.2"): realserver{
					address: netip.MustParseAddr("127.0.0.2"),
					pooled:  PooledYes,
				},
			},
			target: netip.MustParseAddr("127.0.0.2"),
			want:   true,
		},
		{
			name:      "some realservers unhealthy, depool threshold blocks the depool",
			threshold: 0.5,
			realservers: map[netip.Addr]realserver{
				netip.MustParseAddr("127.0.0.1"): realserver{
					address: netip.MustParseAddr("127.0.0.1"),
					pooled:  PooledNo,
				},
				netip.MustParseAddr("127.0.0.2"): realserver{
					address: netip.MustParseAddr("127.0.0.2"),
					pooled:  PooledYes,
				},
			},
			target: netip.MustParseAddr("127.0.0.2"),
			want:   false,
		},
		{
			name:      "realserver depool blocked previously, depool threshold allows the depool",
			threshold: 0.5,
			realservers: map[netip.Addr]realserver{
				netip.MustParseAddr("127.0.0.1"): realserver{
					address: netip.MustParseAddr("127.0.0.1"),
					pooled:  DepoolBlocked,
				},
				netip.MustParseAddr("127.0.0.2"): realserver{
					address: netip.MustParseAddr("127.0.0.2"),
					pooled:  PooledYes,
				},
				netip.MustParseAddr("127.0.0.3"): realserver{
					address: netip.MustParseAddr("127.0.0.3"),
					pooled:  PooledYes,
				},
				netip.MustParseAddr("127.0.0.4"): realserver{
					address: netip.MustParseAddr("127.0.0.4"),
					pooled:  PooledYes,
				},
			},
			target: netip.MustParseAddr("127.0.0.1"),
			want:   true,
		},
		{
			name:      "some realservers failed to repool, depool threshold blocks the depool",
			threshold: 0.5,
			realservers: map[netip.Addr]realserver{
				netip.MustParseAddr("127.0.0.1"): realserver{
					address: netip.MustParseAddr("127.0.0.1"),
					pooled:  RepoolFailed,
				},
				netip.MustParseAddr("127.0.0.2"): realserver{
					address: netip.MustParseAddr("127.0.0.2"),
					pooled:  PooledYes,
				},
			},
			target: netip.MustParseAddr("127.0.0.2"),
			want:   false,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			if got := validateDepoolThreshold(logger, tc.threshold, tc.realservers, tc.target); got != tc.want {
				t.Errorf("unexpected result, want %v, got %v", tc.want, got)
			}
		})
	}
}

func TestValidateSettings(t *testing.T) {
	testCases := []struct {
		name    string
		service string
		cfg     []byte
		want    error
	}{
		{
			name:    "valid config",
			service: "ncredir",
			cfg: []byte(`
hcforwarder:
  grpc:
    network: tcp
    address: 127.0.0.1:1234
  hashing_algorithm: jenkins
  interface:
    egress: lo
    v4: ipip0
    v6: ipip60
  prometheus:
    address: 127.0.0.1:3000
healthcheck:
  grpc:
    network: tcp
    address: 127.0.0.1:1235
  prometheus:
    address: 127.0.0.1:3100
fp:
  grpc:
    network: tcp
    address: 127.0.0.1:1236
  prometheus:
    address: 127.0.0.1:3200
  forwarding_plane: ipvs
bgp:
  grpc:
    network: tcp
    address: 127.0.0.1:1237
etcd:
  conftool_domain: eqiad.wmnet
  datacenter: drmrs
services:
  ncredir:
    forward_type: tunnel
    depool_threshold: 0.5
    cluster: ncredir
    service: nginx
    ip: 192.0.0.1
    port: 80
    healthchecks:
      check1:
        type: HTTPCheck
        url: http://en.wikipedia.com
      check2:
        type: HTTPCheck
        url: http://en.wikipedia.com/_status
      check3:
        type: IdleTCPConnectionCheck
        timeout: 60s
`),
			want: nil,
		},
		{
			name:    "missing healthchecks",
			service: "ncredir",
			cfg: []byte(`
hcforwarder:
  grpc:
    network: tcp
    address: 127.0.0.1:1234
  hashing_algorithm: jenkins
  interface:
    egress: lo
    v4: ipip0
    v6: ipip60
  prometheus:
    address: 127.0.0.1:3000
healthcheck:
  gprc:
    network: tcp
    address: 127.0.0.1:1235
  prometheus:
    address: 127.0.0.1:3100
fp:
  grpc:
    network: tcp
    address: 127.0.0.1:1236
  prometheus:
    address: 127.0.0.1:3200
  forwarding_plane: ipvs
etcd:
  conftool_domain: eqiad.wmnet
  datacenter: drmrs
services:
  ncredir:
    forward_type: tunnel
    depool_threshold: 0.5
    cluster: ncredir
    service: nginx
    ip: 192.0.0.1
    port: 80
`),
			want: ErrInvalidControlPlaneConfiguration,
		},
		{
			name:    "missing etcd config",
			service: "ncredir",
			cfg: []byte(`
hcforwarder:
  grpc:
    network: tcp
    address: 127.0.0.1:1234
  hashing_algorithm: jenkins
  interface:
    egress: lo
    v4: ipip0
    v6: ipip60
  prometheus:
    address: 127.0.0.1:3000
healthcheck:
  gprc:
    network: tcp
    address: 127.0.0.1:1235
  prometheus:
    address: 127.0.0.1:3100
fp:
  grpc:
    network: tcp
    address: 127.0.0.1:1236
  prometheus:
    address: 127.0.0.1:3200
  forwarding_plane: ipvs
services:
  ncredir:
    forward_type: tunnel
    depool_threshold: 0.5
    cluster: ncredir
    service: nginx
    ip: 192.0.0.1
    port: 80
`),
			want: ErrInvalidControlPlaneConfiguration,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			viper.SetConfigType("yaml")
			viper.ReadConfig(bytes.NewBuffer(tc.cfg))
			if got := validateSettings(tc.service); !errors.Is(got, tc.want) {
				t.Errorf("unexpected error, want %v, got %v", tc.want, got)
				t.Logf("settings = %+v", viper.AllSettings())
			}
		})
	}
}

func TestRespawnRequired(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testcases := []struct {
		name       string
		currentCfg controlPlaneConfig
		newCfg     []byte
		want       bool
		wantErr    error
	}{
		{
			name:       "invalid config gets rejected",
			currentCfg: controlPlaneConfig{},
			newCfg:     []byte(``),
			want:       false,
			wantErr:    ErrInvalidControlPlaneConfiguration,
		},
		{
			name: "no config changes",
			currentCfg: controlPlaneConfig{
				addr:            netip.MustParseAddr("192.0.0.1"),
				port:            80,
				ipvs:            true,
				fpForwardType:   fp.Tunnel,
				depoolThreshold: .5,
				httpCheckConfig: []healthcheck.HTTPCheckConfig{
					healthcheck.HTTPCheckConfig{
						Url:                "http://en.wikipedia.com",
						HostHeader:         "en.wikipedia.com",
						ExpectedStatusCode: 200,
					},
				},
			},
			newCfg: []byte(`
hcforwarder:
  grpc:
    network: tcp
    address: 127.0.0.1:1234
  hashing_algorithm: jenkins
  interface:
    egress: lo
    v4: ipip0
    v6: ipip60
  prometheus:
    address: 127.0.0.1:3000
healthcheck:
  grpc:
    network: tcp
    address: 127.0.0.1:1235
  prometheus:
    address: 127.0.0.1:3100
fp:
  grpc:
    network: tcp
    address: 127.0.0.1:1236
  prometheus:
    address: 127.0.0.1:3200
  forwarding_plane: ipvs
etcd:
  conftool_domain: eqiad.wmnet
  datacenter: drmrs
bgp:
  grpc:
    network: tcp
    address: 127.0.0.1:1237
services:
  ncredir:
    forward_type: tunnel
    depool_threshold: 0.5
    cluster: ncredir
    service: nginx
    ip: 192.0.0.1
    port: 80
    healthchecks:
      check1:
        type: HTTPCheck
        url: http://en.wikipedia.com
`),
			want:    false,
			wantErr: nil,
		},
		{
			name: "hc config triggers a respawn",
			currentCfg: controlPlaneConfig{
				addr:            netip.MustParseAddr("192.0.0.1"),
				port:            80,
				ipvs:            true,
				fpForwardType:   fp.Tunnel,
				depoolThreshold: .5,
				httpCheckConfig: []healthcheck.HTTPCheckConfig{
					healthcheck.HTTPCheckConfig{
						Url:                "http://en.wikipedia.com",
						HostHeader:         "en.wikipedia.com",
						ExpectedStatusCode: 200,
					},
				},
			},
			newCfg: []byte(`
hcforwarder:
  grpc:
    network: tcp
    address: 127.0.0.1:1234
  hashing_algorithm: jenkins
  interface:
    egress: lo
    v4: ipip0
    v6: ipip60
  prometheus:
    address: 127.0.0.1:3000
healthcheck:
  grpc:
    network: tcp
    address: 127.0.0.1:1235
  prometheus:
    address: 127.0.0.1:3100
fp:
  grpc:
    network: tcp
    address: 127.0.0.1:1236
  prometheus:
    address: 127.0.0.1:3200
  forwarding_plane: ipvs
etcd:
  conftool_domain: eqiad.wmnet
  datacenter: drmrs
bgp:
  grpc:
    network: tcp
    address: 127.0.0.1:1237
services:
  ncredir:
    forward_type: tunnel
    depool_threshold: 0.5
    cluster: ncredir
    service: nginx
    ip: 192.0.0.1
    port: 80
    healthchecks:
      check1:
        type: HTTPCheck
        url: http://en.wikipedia.com
        status_code: 301
`),
			want:    true,
			wantErr: nil,
		},
		{
			name: "depool threshold triggers a config change",
			currentCfg: controlPlaneConfig{
				addr:            netip.MustParseAddr("192.0.0.1"),
				port:            80,
				ipvs:            true,
				fpForwardType:   fp.Tunnel,
				depoolThreshold: .66,
				httpCheckConfig: []healthcheck.HTTPCheckConfig{
					healthcheck.HTTPCheckConfig{
						Url:                "http://en.wikipedia.com",
						HostHeader:         "en.wikipedia.com",
						ExpectedStatusCode: 200,
					},
				},
			},
			newCfg: []byte(`
hcforwarder:
  grpc:
    network: tcp
    address: 127.0.0.1:1234
  hashing_algorithm: jenkins
  interface:
    egress: lo
    v4: ipip0
    v6: ipip60
  prometheus:
    address: 127.0.0.1:3000
healthcheck:
  grpc:
    network: tcp
    address: 127.0.0.1:1235
  prometheus:
    address: 127.0.0.1:3100
fp:
  grpc:
    network: tcp
    address: 127.0.0.1:1236
  prometheus:
    address: 127.0.0.1:3200
  forwarding_plane: ipvs
etcd:
  conftool_domain: eqiad.wmnet
  datacenter: drmrs
bgp:
  grpc:
    network: tcp
    address: 127.0.0.1:1237
services:
  ncredir:
    forward_type: tunnel
    depool_threshold: 0.5
    cluster: ncredir
    service: nginx
    ip: 192.0.0.1
    port: 80
    healthchecks:
      check1:
        type: HTTPCheck
        url: http://en.wikipedia.com
`),
			want:    true,
			wantErr: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			cp := &ControlPlane{
				logger:  logger,
				service: "ncredir",
				cfg:     tc.currentCfg,
			}
			viper.SetConfigType("yaml")
			viper.ReadConfig(bytes.NewBuffer(tc.newCfg))
			got, gotErr := cp.RespawnRequired()
			if !errors.Is(gotErr, tc.wantErr) {
				t.Errorf("unexpected error, want %v, got %v", tc.wantErr, gotErr)
			}
			if tc.want != got {
				t.Errorf("unexpected result, want %v, got %v", tc.want, got)
			}
		})
	}
}
