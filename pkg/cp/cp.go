/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cp

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net"
	"net/netip"
	"net/url"
	"slices"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/viper"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bgp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/etcd"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/fp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/hash"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/hcforwarder"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/healthcheck"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

// allow overriding time.After during tests
var timeAfter = time.After

// allow overriding net.Resolver during tests
var netResolver resolver = &net.Resolver{}

var (
	ErrInvalidControlPlaneConfiguration = errors.New("invalid control plane configuration")
	ErrInvalidStateArguments            = errors.New("invalid state arguments")
	ErrUnknownRealserver                = errors.New("unknown realserver")
	ErrEtcdServiceDeleted               = errors.New("etcd service deleted. Shutting down")
)

const (
	etcdRetry          = 60 * time.Second
	dnsTimeout         = 3 * time.Second
	hcmonitorTimeout   = 300 * time.Second // using long timeouts here because
	hcforwarderTimeout = 300 * time.Second // network transient issues will be handled by)
	fpClientTimeout    = 300 * time.Second // by grpc.WaitForReady(true)
	bgpClientTimeout   = 300 * time.Second
)

type ControlPlaneError struct {
	Err     error
	Service string
}

func (c *ControlPlaneError) Error() string {
	return c.Err.Error()
}

type Pooled uint8

const (
	PooledUnknown Pooled = iota
	PooledYes
	PooledNo
	DepoolBlocked
	DepoolFailed
	RepoolFailed
	ForceRepoolFailed
)

// track control planes along with their cancelFn
type ControlPlaneRun struct {
	CancelFn context.CancelFunc
	Cp       *ControlPlane
}

type ControlPlane struct {
	etcdClient        etcd.EtcdClient
	hcforwarderClient hcforwarder.HCForwarderClient
	hcmonitorClient   healthcheck.HCMonitorClient
	fpClient          fp.FPClient
	bgpClient         bgp.BgpClient
	logger            *slog.Logger
	bgpConfig         *bgp.BgpConfig
	realservers       map[netip.Addr]realserver // this map is guarded by rsMutex
	service           string
	fpService         fp.Service // forwarding plane service
	cfg               controlPlaneConfig
	configuredHCs     int // number of configured healthchecks
	rsMutex           sync.RWMutex
	readWrite         bool // RW is enabled as soon as healthcheck reports for every realserver are received
	bootstrapping     bool
}

// reloadable config
type controlPlaneConfig struct {
	addr            netip.Addr // VIP address
	httpCheckConfig []healthcheck.HTTPCheckConfig
	itcCheckConfig  []healthcheck.IdleTCPConnectionCheckConfig
	depoolThreshold float64
	port            uint16 // VIP port
	fpForwardType   fp.ForwardType
	ipvs            bool // forwarding plane is IPVS
}

type realserver struct {
	address   netip.Addr
	hcResults map[healthcheck.HealthcheckID]bool
	hostname  string
	weight    int
	mark      uint32
	pooled    Pooled // currently pooled in forwarding plane
	admPooled bool   // administratively pooled in etcd
}

type stateFnArgs struct {
	etcdEvent   *etcd.Event
	hcResult    *healthcheck.Result
	realservers []realserver
}

type stateFn func(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error)

// initial fetch of realservers from etcd
// success: resolveEtcdNodesState
// errors:
//
//   - key not found: retry in 60 seconds
//   - any other error: stop the initialization
func checkEtcdState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering checkeEtcdState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	nodes, err := cp.etcdClient.Get(ctx)
	if err != nil {
		if errors.Is(err, etcd.ErrNotFound) { // key not found
			cp.logger.Debug("etcd key not found")
			return retryCheckEtcdState, args, nil
		}
		return nil, stateFnArgs{}, fmt.Errorf("unable to get etcd key: %w", err)
	}

	for _, node := range nodes {
		if node.Pooled != etcd.PooledYes {
			continue // skip not pooled/inactive nodes
		}
		rs := realserver{
			hostname:  node.Hostname,
			admPooled: true,
			weight:    node.Weight,
		}
		args.realservers = append(args.realservers, rs)
	}

	return resolveEtcdNodesState, args, nil
}

// retry checkEtcdState after etcdRetry
// unconditionally returns checkEtcdState
func retryCheckEtcdState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering retryCheckEtcdStatus")

	select {
	case <-ctx.Done():
		return nil, stateFnArgs{}, ctx.Err()
	case <-timeAfter(etcdRetry):
		return checkEtcdState, args, nil
	}
}

// resolve etcd nodes
func resolveEtcdNodesState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering resolveEtcdNodesState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	var network string
	if cp.cfg.addr.Is4() {
		network = "ip4"
	} else {
		network = "ip6"
	}

	for i, rs := range args.realservers {
		func() {
			ctx, cancel := context.WithTimeout(ctx, dnsTimeout)
			defer cancel()
			addr, err := netResolver.LookupNetIP(ctx, network, rs.hostname)
			if err != nil {
				cp.logger.Error("unable to get a valid address",
					slog.String("hostname", rs.hostname),
					slog.Any("error", err),
				)
				return
			}

			if len(addr) > 1 {
				cp.logger.Warn("found more than one IP, using the first one",
					slog.String("hostname", rs.hostname),
					slog.String("address", addr[0].String()),
				)
			}
			cp.logger.Debug("successful DNS query",
				slog.String("hostname", rs.hostname),
				slog.String("address", addr[0].String()),
			)
			args.realservers[i].address = addr[0]
		}()
	}

	return configureHCForwarderState, args, nil
}

// configure hcforwarder
func configureHCForwarderState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering configureHCForwarderState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	algorithm := viper.GetString("hcforwarder.hashing_algorithm")

	for i, rs := range args.realservers {
		if !rs.address.IsValid() {
			cp.logger.Warn("skipping hostname with invalid address",
				slog.String("hostname", rs.hostname),
			)
			continue
		}
		if !rs.admPooled { // don't perform healthchecking for administratively depooled hosts
			continue
		}
		// direct route mode doesn't use marks for healthchecking
		if cp.cfg.fpForwardType == fp.DirectRoute {
			return configureHealthchecksState, args, nil
		}

		hash, err := hash.Hash32Addr(algorithm, rs.address)
		if err != nil {
			cp.logger.Error("failed to obtain realserver (so)mark",
				slog.String("address", rs.address.String()),
				slog.Any("error", err),
			)
			// no need to break the loop here, Hash32Addr will return 0 on error
		}
		cp.logger.Debug("new address <-> mark mapping",
			slog.String("address", rs.address.String()),
			slog.Int64("mark", int64(hash)),
		)
		args.realservers[i].mark = hash
	}

	ctxListRS, cancel := context.WithTimeout(ctx, hcforwarderTimeout)
	realservers, err := cp.hcforwarderClient.ListRealservers(ctxListRS)
	cancel()
	if err != nil {
		return nil, stateFnArgs{}, fmt.Errorf("unable to get realservers: %w", err)
	}

	for _, rs := range args.realservers {
		if rs.mark == 0 {
			continue // skip invalid marks (already logged in the previous stage)
		}
		// check if mark is already configured
		hcfAddr, present := realservers[rs.mark]
		if present {
			// sanity check
			if rs.address != hcfAddr {
				cp.logger.Error("mark mismatch detected between hcforwarder and control plane",
					slog.String("address", rs.address.String()),
					slog.String("hcforwarder_address", hcfAddr.String()),
					slog.Int64("mark", int64(rs.mark)),
				)
				return nil, stateFnArgs{}, nil
			}
			continue
		}
		ctxAddRS, cancel := context.WithTimeout(ctx, hcforwarderTimeout)
		if err := cp.hcforwarderClient.CreateRealserver(ctxAddRS, rs.address, rs.mark); err != nil {
			cp.logger.Error("unable to add realserver to hcforwarder",
				slog.String("address", rs.address.String()),
				slog.Int64("mark", int64(rs.mark)),
				slog.Any("error", err),
			)
		}
		cancel()
	}

	return configureHealthchecksState, args, nil
}

// helper used by both configureHealthchecksState and reconfigureHealthchecksState
func configureHealthchecks(ctx context.Context, cp *ControlPlane, realservers []realserver) {
	if ctx.Err() != nil {
		return
	}

	cp.readWrite = false // configuring new healthchecks toggles readWrite
	cp.logger.Warn("control plane is not aware of the current status of all realservers")

	for _, rs := range realservers {
		if !rs.address.IsValid() || !rs.admPooled {
			continue
		}
		var addrPort netip.AddrPort
		// target the realserver IP address rather than the VIP service address
		// if ipvs is the forwarding plane (https://phabricator.wikimedia.org/T372731#10072517)
		// or direct route is being used and hcforwarder can't do its magic
		if cp.cfg.ipvs || cp.cfg.fpForwardType == fp.DirectRoute {
			addrPort = netip.AddrPortFrom(rs.address, cp.cfg.port)
		} else {
			addrPort = netip.AddrPortFrom(cp.cfg.addr, cp.cfg.port)
		}

		var hcsCfg []healthcheck.HealthcheckConfigurer
		for _, httpCheckConfig := range cp.cfg.httpCheckConfig {
			u, err := url.Parse(httpCheckConfig.Url)
			if err != nil {
				continue // skip invalid healthcheck configuration
			}
			u.Host = addrPort.String()
			httpCheckConfig.Url = u.String()

			cp.logger.Debug("new http check config",
				slog.Any("config", httpCheckConfig),
			)

			hcsCfg = append(hcsCfg, &httpCheckConfig)
		}
		for _, tcpCheckConfig := range cp.cfg.itcCheckConfig {
			tcpCheckConfig.Address = addrPort.String()
			cp.logger.Debug("new tcp check config",
				slog.Any("config", tcpCheckConfig),
			)
			hcsCfg = append(hcsCfg, &tcpCheckConfig)
		}

		hcRs := healthcheck.Realserver{
			Service:  cp.service,
			Mark:     int(rs.mark),
			AddrPort: netip.AddrPortFrom(rs.address, cp.cfg.port),
		}
		ctxReq, cancel := context.WithTimeout(ctx, hcmonitorTimeout)
		if err := cp.hcmonitorClient.CreateHealthcheck(ctxReq, hcRs, hcsCfg); err != nil {
			cp.logger.Error("unable to create healthcheck",
				slog.Any("error", err),
			)
		}
		cancel()
	}
}

// configure healthchecks
func configureHealthchecksState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering configureHealthchecksState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	configureHealthchecks(ctx, cp, args.realservers)

	return populateRealserversMapState, args, nil
}

// reconfigure healthchecks after hcmonitor connection has been recovered
func reconfigureHealthchecksState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering reconfigureHealthchecksState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	realservers := make([]realserver, len(cp.realservers))
	// clear hcResults
	for _, rs := range cp.realservers {
		rs.hcResults = map[healthcheck.HealthcheckID]bool{}
		cp.realservers[rs.address] = rs
		realservers = append(realservers, rs)
	}

	configureHealthchecks(ctx, cp, realservers)

	return nil, stateFnArgs{}, nil
}

// populate ControlPlane realservers map
func populateRealserversMapState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering populateRealserversMapState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	var realservers map[netip.Addr]realserver
	if cp.realservers != nil {
		realservers = cp.realservers
	} else {
		realservers = make(map[netip.Addr]realserver)
	}
	for _, rs := range args.realservers {
		if !rs.address.IsValid() {
			continue
		}
		if !rs.admPooled {
			continue
		}
		rs.pooled = PooledYes
		rs.hcResults = make(map[healthcheck.HealthcheckID]bool)
		realservers[rs.address] = rs
	}

	cp.realservers = realservers

	zero := fp.Service{}
	if cp.fpService == zero { // zero value
		return checkFPServiceState, stateFnArgs{}, nil
	}
	return checkFPRealserversState, stateFnArgs{}, nil
}

// check forwarding plane service
func checkFPServiceState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering checkFPServiceState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	services, err := cp.fpClient.ListServices(ctx)
	if err != nil {
		cp.logger.Error("unable to fetch services from forwarding plane",
			slog.Any("error", err),
		)
		return nil, stateFnArgs{}, nil
	}

	for _, svc := range services {
		if svc.Address == cp.cfg.addr && svc.Port == cp.cfg.port {
			cp.fpService = svc
			return checkFPRealserversState, args, nil
		}
	}

	return addServiceForwardingPlaneState, stateFnArgs{}, nil
}

func addServiceForwardingPlaneState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering addServiceForwardingPlaneState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	var family fp.AddressFamily
	if cp.cfg.addr.Is4() {
		family = fp.INET
	} else {
		family = fp.INET6
	}

	svc := fp.Service{
		Address:   cp.cfg.addr,
		Port:      cp.cfg.port,
		Scheduler: "mh",
		Family:    family,
		Protocol:  fp.TCP,
	}

	if err := cp.fpClient.CreateService(ctx, svc); err != nil {
		cp.logger.Error("unable to create service",
			slog.Any("error", err),
		)
	}

	cp.fpService = svc
	return checkFPRealserversState, stateFnArgs{}, nil
}

// check forwarding plane realservers
func checkFPRealserversState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering checkFPRealserversState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, nil
	}

	// sanity check
	zero := fp.Service{}
	if cp.fpService == zero { // zero value
		return nil, stateFnArgs{}, errors.New("invalid control plane state, fpService should have been initializated before entering checkFPRealserversState")
	}

	realservers, err := cp.fpClient.ListRealservers(ctx, cp.fpService)
	if err != nil {
		cp.logger.Error("unable to fetch realservers from forwarding plane",
			slog.Any("error", err),
		)
	}

	fpRealservers := map[netip.Addr]fp.Realserver{}
	for _, rs := range realservers {
		fpRealservers[rs.Address] = rs
	}

	addRealservers := []realserver{}
	updateRealservers := []realserver{}
	removeRealservers := []fp.Realserver{}

	cp.rsMutex.RLock()
	for address, rs := range cp.realservers {
		if _, present := fpRealservers[address]; !present && rs.admPooled { // realserver in etcd AND not in forwarding plane
			cp.logger.Info("adding realserver to forwarding plane",
				slog.String("hostname", rs.hostname),
				slog.String("address", address.String()),
				slog.Int("weight", rs.weight),
			)
			addRealservers = append(addRealservers, rs)
		}
	}

	for address, fpRs := range fpRealservers {
		if rs, present := cp.realservers[address]; present { // realserver in forwarding plane AND etcd
			if uint32(rs.weight) != fpRs.Weight {
				cp.logger.Info("updating realserver in forwarding plane",
					slog.String("hostname", rs.hostname),
					slog.String("address", address.String()),
					slog.Int("weight", rs.weight),
				)
				// updating realserver, etcd contains the desired state
				updateRealservers = append(updateRealservers, rs)
			}
		} else { // realserver in forwarding plane and not in etcd
			cp.logger.Info("removing realserver from forwarding plane",
				slog.String("address", address.String()),
			)
			removeRealservers = append(removeRealservers, fpRs)
		}
	}
	cp.rsMutex.RUnlock()

	// Add realservers to the forwarding plane. This will effectively pool realservers before healthchecking them
	// SREs shoud guarantee that they won't administratively pool realservers that aren't ready to serve traffic
	for _, rs := range addRealservers {
		ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
		if err := createFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
			cp.logger.Error("unable to add realserver to forwarding plane",
				slog.Any("fp_service", cp.fpService),
				slog.Any("realserver", rs),
				slog.Any("error", err),
			)
		}
		cancel()
	}

	// Update realservers
	for _, rs := range updateRealservers {
		ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
		if err := updateFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
			cp.logger.Error("unable to update realserver to forwarding plane",
				slog.Any("fp_service", cp.fpService),
				slog.Any("realserver", rs),
				slog.Any("error", err),
			)
		}
		cancel()
	}

	// remove realservers
	for _, rs := range removeRealservers {
		ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
		if err := cp.fpClient.DeleteRealserver(ctx, cp.fpService, rs); err != nil {
			cp.logger.Error("unable to delete realserver to forwarding plane",
				slog.Any("fp_service", cp.fpService),
				slog.Any("fp_realserver", rs),
				slog.Any("error", err),
			)
		}
		cancel()
	}

	if len(removeRealservers) > 0 {
		algorithm := viper.GetString("hcforwarder.hashing_algorithm")
		args := stateFnArgs{}
		for _, fpRs := range removeRealservers {
			hash, err := hash.Hash32Addr(algorithm, fpRs.Address)
			if err != nil {
				cp.logger.Error("failed to obtain realserver (so)mark",
					slog.String("address", fpRs.Address.String()),
					slog.Any("error", err),
				)
				continue
			}
			rs := realserver{
				address: fpRs.Address,
				mark:    hash,
			}
			args.realservers = append(args.realservers, rs)
		}
		return deleteHealthchecksState, args, nil
	}

	return updateMetricsState, stateFnArgs{}, nil
}

// delete healthchecks after an administrative depool of one or more realservers
func deleteHealthchecksState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering deleteHealthchecksState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}
	for _, rs := range args.realservers {
		ctx, cancel := context.WithTimeout(ctx, hcmonitorTimeout)
		if err := deleteHealthcheck(ctx, cp.hcmonitorClient, cp.service, cp.cfg.port, rs); err != nil {
			cp.logger.Error("unable to delete healthcheck",
				slog.Any("error", err),
			)
		}
		cancel()
	}

	return checkDepoolThresholdIsMetState, stateFnArgs{}, nil
}

// checks that depool threshold is still happy after administratively depooling one or more realservers
func checkDepoolThresholdIsMetState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering checkDepoolThresholdIsMetState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}
	if !cp.readWrite {
		cp.logger.Debug("not enforcing depool threshold while control plane isn't on read write mode")
		return nil, stateFnArgs{}, nil
	}

	cp.rsMutex.RLock()
	defer cp.rsMutex.RUnlock()
	totalRealserversCount := float64(len(cp.realservers))
	pooledRealserversCount := 0.0
	blockedDepoolRealserversCount := 0.0
	for _, rs := range cp.realservers {
		if rs.pooled == PooledYes {
			pooledRealserversCount += 1.0
		} else if rs.pooled == DepoolBlocked {
			blockedDepoolRealserversCount += 1.0
		}
	}

	if pooledRealserversCount >= totalRealserversCount*cp.cfg.depoolThreshold {
		// depool threshold is still happy
		cp.logger.Debug("depool threshold is being met")
		return updateMetricsState, stateFnArgs{}, nil
	} else if (pooledRealserversCount + blockedDepoolRealserversCount) >= totalRealserversCount*cp.cfg.depoolThreshold {
		// depool threshold is currently being enforced
		cp.logger.Warn("depool threshold is being enforced at the moment",
			slog.Float64("pooled_realservers_count", pooledRealserversCount),
			slog.Float64("total_realservers_count", totalRealserversCount),
			slog.Float64("depool_threshold", cp.cfg.depoolThreshold),
		)
		return updateMetricsState, stateFnArgs{}, nil
	}

	// actions required to enforce depool threshold
	enforceDTArgs := stateFnArgs{}
	for _, rs := range cp.realservers {
		if rs.pooled != PooledNo {
			continue // we can only act here on depooled realservers
		}
		enforceDTArgs.realservers = append(enforceDTArgs.realservers, rs)
		blockedDepoolRealserversCount += 1.0
		if (pooledRealserversCount + blockedDepoolRealserversCount) >= totalRealserversCount*cp.cfg.depoolThreshold {
			break
		}
	}
	if len(enforceDTArgs.realservers) > 0 {
		return enforceDepoolThresholdState, enforceDTArgs, nil
	}
	return updateMetricsState, stateFnArgs{}, nil
}

// repool one or more realservers and flag them as DepoolBlocked
func enforceDepoolThresholdState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering enforceDepoolThresholdState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	for _, argRs := range args.realservers {
		rs, present := cp.realservers[argRs.address]
		if !present {
			cp.logger.Error("unknown realserver",
				slog.String("hostname", argRs.hostname),
				slog.String("address", argRs.address.String()),
			)
			return nil, stateFnArgs{}, fmt.Errorf("%w: %s", ErrUnknownRealserver, argRs.address.String())
		}
		rs.pooled = DepoolBlocked
		ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
		if err := createFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
			cp.logger.Error("unable to add realserver to forwarding plane",
				slog.Any("fp_service", cp.fpService),
				slog.Any("realserver", rs),
				slog.Any("error", err),
			)
			rs.pooled = ForceRepoolFailed
		}
		cancel()
		cp.realservers[rs.address] = rs
	}
	return checkPendingRealserversState, stateFnArgs{}, nil
}

// handle a healthcheck result
func handleHealthcheckResultState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering handleHealthcheckResultState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	// sanity check
	if args.hcResult == nil {
		return nil, stateFnArgs{}, ErrInvalidStateArguments
	}

	addr := args.hcResult.Realserver.AddrPort.Addr()
	healthcheckID := args.hcResult.HealthcheckID

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	rs, present := cp.realservers[addr]
	if !present {
		cp.logger.Warn("received healthcheck result for unknown realserver",
			slog.String("address", addr.String()),
		)
		return nil, stateFnArgs{}, nil
	}

	reportedHCs := len(rs.hcResults)
	completeStatus := reportedHCs == cp.configuredHCs
	oldSuccesful, resultPresent := rs.hcResults[healthcheckID]
	if resultPresent {
		if oldSuccesful != args.hcResult.Successful {
			cp.logger.Info("detected healthcheck state change",
				slog.String("hostname", rs.hostname),
				slog.String("address", addr.String()),
				slog.String("healthcheck_name", args.hcResult.HealthcheckName),
				slog.Int("healthcheck_id", int(healthcheckID)),
				slog.Bool("healthcheck_result_old", oldSuccesful),
				slog.Bool("healthcheck_result", args.hcResult.Successful),
			)
		}
	} else {
		// this should happen:
		// - after control plane initialization
		// - after a new realserver has been added to etcd
		cp.logger.Info("new healthcheck result received",
			slog.String("hostname", rs.hostname),
			slog.String("address", addr.String()),
			slog.String("healthcheck_name", args.hcResult.HealthcheckName),
			slog.Int("healthcheck_id", int(healthcheckID)),
			slog.Bool("healthcheck_result", args.hcResult.Successful),
		)
	}

	cp.realservers[addr].hcResults[healthcheckID] = args.hcResult.Successful
	if len(cp.realservers[addr].hcResults) == cp.configuredHCs && !completeStatus {
		cp.logger.Debug("all healthchecks reported status for realserver",
			slog.String("address", addr.String()),
		)
		if !cp.readWrite {
			readWrite := true
			for _, rs := range cp.realservers {
				if len(rs.hcResults) < cp.configuredHCs {
					readWrite = false
					break
				}
			}
			if readWrite {
				cp.readWrite = true
				cp.logger.Info("control plane is now aware of the current status of all realservers")
			}
		}
	}

	if !args.hcResult.Successful && rs.pooled == PooledYes {
		return depoolRealserverState, args, nil
	}

	healthy := true
	for _, successful := range cp.realservers[addr].hcResults {
		if !successful {
			healthy = false
			break
		}
	}
	if healthy && rs.pooled != PooledYes {
		return repoolRealserverState, args, nil
	}

	if cp.readWrite {
		return checkPendingRealserversState, stateFnArgs{}, nil
	}

	return updateMetricsState, stateFnArgs{}, nil
}

// depool a single realserver
func depoolRealserverState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering depoolRealserverState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}
	// sanity check
	if args.hcResult == nil {
		return nil, stateFnArgs{}, ErrInvalidStateArguments
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	addr := args.hcResult.Realserver.AddrPort.Addr()
	rs, present := cp.realservers[addr]
	if !present {
		return nil, stateFnArgs{}, fmt.Errorf("%w: %s", ErrUnknownRealserver, addr.String())
	}

	// in read only mode we don't update the pooled flag of realservers
	if !cp.readWrite { // we cannot proceed if we don't have the status of all the configured realservers
		cp.logger.Debug("refusing to depool realservers until we know the status of all of them")
		return updateMetricsState, stateFnArgs{}, nil
	}

	rs.pooled = DepoolBlocked
	// check that depool threshold is happy with depooling the server
	if !validateDepoolThreshold(cp.logger, cp.cfg.depoolThreshold, cp.realservers, addr) {
		cp.logger.Warn("unable to depool due to depool threshold enforcement",
			slog.String("hostname", rs.hostname),
			slog.String("address", addr.String()),
		)
		cp.realservers[addr] = rs
		return updateMetricsState, stateFnArgs{}, nil
	}

	rs.pooled = PooledNo
	ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
	defer cancel()
	if err := deleteFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
		cp.logger.Error("unable to delete realserver from forwarding plane",
			slog.Any("fp_service", cp.fpService),
			slog.Any("realserver", rs),
			slog.Any("error", err),
		)
		rs.pooled = DepoolFailed
	}
	cp.realservers[addr] = rs

	return checkPendingRealserversState, stateFnArgs{}, nil
}

// check pending depools / repools and enforce them if possible
func checkPendingRealserversState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering checkPendingRealserversState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	for retry := true; retry; {
		retry = false // single attempt by default
		for _, rs := range cp.realservers {
			switch rs.pooled {
			case PooledNo:
				continue
			case PooledYes, DepoolFailed, DepoolBlocked:
				if rs.pooled == PooledYes {
					healthy := true
					for _, successful := range rs.hcResults {
						if !successful {
							healthy = false
							break
						}
					}
					if healthy {
						continue
					}
				}
				if !validateDepoolThreshold(cp.logger, cp.cfg.depoolThreshold, cp.realservers, rs.address) {
					continue
				}
				rs.pooled = PooledNo
				ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
				if err := deleteFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
					cp.logger.Error("unable to delete realserver from forwarding plane",
						slog.Any("fp_service", cp.fpService),
						slog.Any("realserver", rs),
						slog.Any("error", err),
					)
					rs.pooled = DepoolFailed
				}
				cancel()
				cp.realservers[rs.address] = rs
			case RepoolFailed, ForceRepoolFailed:
				forced := false
				if rs.pooled == RepoolFailed {
					rs.pooled = PooledYes
				} else {
					forced = true
					rs.pooled = DepoolBlocked
				}
				ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
				if err := createFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
					cp.logger.Error("unable to add realserver to forwarding plane",
						slog.Any("fp_service", cp.fpService),
						slog.Any("realserver", rs),
						slog.Any("error", err),
					)
					if forced {
						rs.pooled = ForceRepoolFailed
					} else {
						rs.pooled = RepoolFailed
					}
				} else {
					if !forced {
						retry = true // check for potentially DepoolBlocked realservers that could be depooled after a successful repool
					}
				}
				cancel()
				cp.realservers[rs.address] = rs
			}
		}
	}

	return updateMetricsState, stateFnArgs{}, nil
}

// update prometheus metrics
func updateMetricsState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering updateMetricsState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	cp.rsMutex.RLock()
	defer cp.rsMutex.RUnlock()
	realserversCount := float64(len(cp.realservers))
	healthyRealserversCount := 0.0
	unhealthyRealserversPooled := 0.0
	healthyRealserversNotPooled := 0.0

	for _, rs := range cp.realservers {
		switch rs.pooled {
		// realservers are flagged as PooledYes without considering healthchecks
		// when fetched from etcd (see populateRealserversMapState)
		case PooledYes:
			healthy := true
			for _, successful := range rs.hcResults {
				if !successful {
					healthy = false
					break
				}
			}
			if healthy {
				healthyRealserversCount += 1.0
			} else {
				unhealthyRealserversPooled += 1.0
			}
		case DepoolBlocked, DepoolFailed:
			unhealthyRealserversPooled += 1.0
		case RepoolFailed:
			healthyRealserversNotPooled += 1.0
		}
	}

	labels := prometheus.Labels{"service": cp.service}
	metrics.AdministrativelyPooledRealservers.With(labels).Set(realserversCount)
	metrics.PooledRealservers.With(labels).Set(healthyRealserversCount)
	metrics.UnhealthyPooledRealservers.With(labels).Set(unhealthyRealserversPooled)
	metrics.HealthyRealserversNotPooled.With(labels).Set(healthyRealserversNotPooled)

	return nil, stateFnArgs{}, nil
}

// repool a single realserver
func repoolRealserverState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering repoolRealserverState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}
	// sanity check
	if args.hcResult == nil {
		return nil, stateFnArgs{}, ErrInvalidStateArguments
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	addr := args.hcResult.Realserver.AddrPort.Addr()
	rs, present := cp.realservers[addr]
	if !present {
		return nil, stateFnArgs{}, fmt.Errorf("%w: %s", ErrUnknownRealserver, addr.String())
	}

	switch rs.pooled {
	case PooledNo, RepoolFailed:
		rs.pooled = PooledYes
		if err := createFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
			cp.logger.Error("unable to add realserver to forwarding plane",
				slog.Any("fp_service", cp.fpService),
				slog.Any("realserver", rs),
				slog.Any("error", err),
			)
			rs.pooled = RepoolFailed
		}
	case DepoolBlocked, DepoolFailed: // nothing to do here besides flagging the server as pooled = yes
		rs.pooled = PooledYes
	default:
		cp.logger.Warn("unexpected realserver pooled value on repoolRealserverState. NOOP",
			slog.String("hostname", rs.hostname),
			slog.Any("pooled", rs.pooled),
		)
	}

	cp.realservers[addr] = rs

	return checkPendingRealserversState, stateFnArgs{}, nil
}

// handle an incoming etcd event
func handleEtcdEventState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering handleEtcdEventState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	// sanity check
	if args.etcdEvent == nil {
		return nil, stateFnArgs{}, ErrInvalidStateArguments
	}

	switch args.etcdEvent.EventType {
	case etcd.EventDelete, etcd.EventCompareAndDelete:
		// remove realserver
		return handleEtcdDeleteEventState, args, nil
	case etcd.EventSet, etcd.EventUpdate, etcd.EventCompareAndSwap:
		// update realserver
		return handleEtcdUpdateEventState, args, nil
	case etcd.EventCreate:
		// create realserver
		return handleEtcdCreateEventState, args, nil
	default:
		cp.logger.Debug("ignoring unexpected etcd event")
		return nil, stateFnArgs{}, nil
	}
}

// handle etcd create events
func handleEtcdCreateEventState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering handleEtcdCreateEventState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	// sanity check
	if args.etcdEvent == nil {
		return nil, stateFnArgs{}, ErrInvalidStateArguments
	}

	resolveEtcdNodesStateArgs := stateFnArgs{}

	for _, node := range args.etcdEvent.Nodes {
		if node.Pooled != etcd.PooledYes {
			continue // skip not pooled nodes
		}
		rs := realserver{
			hostname:  node.Hostname,
			admPooled: true,
			weight:    node.Weight,
		}
		resolveEtcdNodesStateArgs.realservers = append(resolveEtcdNodesStateArgs.realservers, rs)
	}

	if len(resolveEtcdNodesStateArgs.realservers) > 0 {
		return resolveEtcdNodesState, resolveEtcdNodesStateArgs, nil
	}

	return nil, stateFnArgs{}, nil
}

// handle etcd delete events
func handleEtcdDeleteEventState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering handleEtcdDeleteEventState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	// sanity check
	if args.etcdEvent == nil {
		return nil, stateFnArgs{}, ErrInvalidStateArguments
	}

	if len(args.etcdEvent.Nodes) == 0 { // whole service has been deleted
		cp.logger.Warn("etcd service has been deleted. shutting down control plane")
		return nil, stateFnArgs{}, ErrEtcdServiceDeleted
	}

	cp.rsMutex.RLock()
	defer cp.rsMutex.RUnlock()

	// we are handling a delete event but it could happen that we aren't using those realservers anymore
	// if the latest pooled value was inactive
	deleteArgs := stateFnArgs{}
	for _, node := range args.etcdEvent.Nodes {
		for _, rs := range cp.realservers { // since we index realservers per address and not hostname we need to loop here
			if rs.hostname == node.Hostname {
				deleteArgs.realservers = append(deleteArgs.realservers, rs)
			}
		}
	}

	if len(deleteArgs.realservers) > 0 {
		return deleteRealserversState, deleteArgs, nil
	}

	return nil, stateFnArgs{}, nil
}

// handle etcd update events
func handleEtcdUpdateEventState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering handleEtcdUpdateEventState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	// sanity check
	if args.etcdEvent == nil {
		return nil, stateFnArgs{}, ErrInvalidStateArguments
	}

	// for updates we have 3 scenarios:
	// - adm pooled RS has been depooled (delete)
	// - adm depooled RS has been pooled (create)
	// - adm pooled RS got its weight updated

	cp.rsMutex.RLock()
	defer cp.rsMutex.RUnlock()
	updateArgs := stateFnArgs{}
	for _, node := range args.etcdEvent.Nodes {
		found := false
		for _, rs := range cp.realservers {
			if rs.hostname == node.Hostname {
				found = true
				switch node.Pooled {
				case etcd.PooledYes:
					rs.weight = node.Weight
					updateArgs.realservers = append(updateArgs.realservers, rs)
				default:
					return handleEtcdDeleteEventState, args, nil
				}
				break
			}
		}
		if !found && node.Pooled == etcd.PooledYes {
			return handleEtcdCreateEventState, args, nil
		}
	}
	if len(updateArgs.realservers) > 0 {
		return updateRealserversState, updateArgs, nil
	}

	return nil, stateFnArgs{}, nil
}

// updates the weight of already existing realservers
func updateRealserversState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering updateRealserversState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	for _, argRs := range args.realservers {
		rs, present := cp.realservers[argRs.address]
		if !present {
			return nil, stateFnArgs{}, fmt.Errorf("%w: %s", ErrUnknownRealserver, argRs.address.String())
		}

		rs.weight = argRs.weight

		ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
		if err := updateFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
			cp.logger.Error("unable to update realserver to forwarding plane",
				slog.Any("fp_service", cp.fpService),
				slog.Any("realserver", rs),
				slog.Any("error", err),
			)
		}
		cancel()
		cp.realservers[rs.address] = rs
	}

	return nil, stateFnArgs{}, nil
}

// delete realservers from forwarding plane unconditionally (depool threshold is not enforced). this is triggered by an administrative depool
func deleteRealserversState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering deleteRealserversState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	fpRealservers, err := cp.fpClient.ListRealservers(ctx, cp.fpService)
	if err != nil {
		return nil, stateFnArgs{}, err
	}

	cp.rsMutex.Lock()
	defer cp.rsMutex.Unlock()
	for _, rs := range args.realservers {
		for _, fpRs := range fpRealservers {
			if rs.address == fpRs.Address {
				ctx, cancel := context.WithTimeout(ctx, fpClientTimeout)
				if err := deleteFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
					cp.logger.Error("unable to delete realserver from forwarding plane",
						slog.Any("fp_service", cp.fpService),
						slog.Any("realserver", rs),
						slog.Any("error", err),
					)
				}
				cancel()
				break // stop looping fpRealservers if we found the realserver
			}
		}
		cp.logger.Info("realserver deleted",
			slog.String("hostname", rs.hostname),
			slog.String("address", rs.address.String()),
			slog.Int("mark", int(rs.mark)),
		)
		delete(cp.realservers, rs.address)
	}

	return deleteHealthchecksState, args, nil
}

// Request VIP to be added as a BGP path
func configureBgpPathState(ctx context.Context, cp *ControlPlane, args stateFnArgs) (stateFn, stateFnArgs, error) {
	cp.logger.Debug("entering configureBgpPathState")
	if ctx.Err() != nil {
		return nil, stateFnArgs{}, ctx.Err()
	}

	ctx, cancel := context.WithTimeout(ctx, bgpClientTimeout)
	defer cancel()

	if err := addBgpPath(ctx, cp.bgpConfig, cp.bgpClient, cp.cfg.addr); err != nil {
		cp.logger.Error("unable to add BGP path",
			slog.String("path", cp.cfg.addr.String()),
			slog.Any("error", err),
		)
		return nil, stateFnArgs{}, fmt.Errorf("unable to add BGP path: %w", err)
	}

	return nil, stateFnArgs{}, nil
}

// validate depool action of target realserver
func validateDepoolThreshold(logger *slog.Logger, depoolThreshold float64, realservers map[netip.Addr]realserver, target netip.Addr) bool {
	pooledRealserversCount := 0.0
	totalRealserversCount := float64(len(realservers))

	var targetHostname string
	for _, rs := range realservers {
		if rs.address == target {
			targetHostname = rs.hostname
			continue
		}
		if rs.pooled == PooledYes {
			pooledRealserversCount += 1.0
		}
	}

	ret := pooledRealserversCount >= totalRealserversCount*depoolThreshold
	logger.Debug("depool threshold validator",
		slog.String("target", targetHostname),
		slog.Bool("threshold_validation_result", ret),
		slog.Float64("pooled_realservers_count", pooledRealserversCount),
		slog.Float64("total_realservers_count", totalRealserversCount),
		slog.Float64("depool_threshold", depoolThreshold),
	)
	return ret
}

// validate settings provided by viper
func validateSettings(service string) error {
	requiredSettings := []string{
		"etcd.conftool_domain",
		"etcd.datacenter",
		"fp.forwarding_plane",
		"fp.grpc.network",
		"fp.grpc.address",
		"hcforwarder.grpc.network",
		"hcforwarder.grpc.address",
		"hcforwarder.hashing_algorithm",
		"healthcheck.grpc.network",
		"healthcheck.grpc.address",
		"bgp.grpc.network",
		"bgp.grpc.address",
		fmt.Sprintf("services.%s.forward_type", service),
		fmt.Sprintf("services.%s.cluster", service),
		fmt.Sprintf("services.%s.service", service),
		fmt.Sprintf("services.%s.ip", service),
		fmt.Sprintf("services.%s.port", service),
		fmt.Sprintf("services.%s.depool_threshold", service),
		fmt.Sprintf("services.%s.healthchecks", service),
	}

	for _, name := range requiredSettings {
		if !viper.IsSet(name) {
			err := fmt.Errorf("required setting %s is missing", name)
			return errors.Join(ErrInvalidControlPlaneConfiguration, err)
		}
	}
	return nil
}

func newControlPlaneConfig(service string) (controlPlaneConfig, error) {
	ret := controlPlaneConfig{}
	addr, err := netip.ParseAddr(viper.GetString(fmt.Sprintf("services.%s.ip", service)))
	if err != nil {
		return ret, errors.Join(ErrInvalidControlPlaneConfiguration, err)
	}
	port := viper.GetUint16(fmt.Sprintf("services.%s.port", service))

	httpCheckConfig, err := healthcheck.LoadHTTPCheckConfig(service)
	if err != nil {
		return ret, errors.Join(ErrInvalidControlPlaneConfiguration,
			fmt.Errorf("unable to parse HTTPCheck configuration: %w", err),
		)
	}
	itcCheckConfig, err := healthcheck.LoadIdleTcpConnectionCheckConfig(service)
	if err != nil {
		return ret, errors.Join(ErrInvalidControlPlaneConfiguration,
			fmt.Errorf("unable to parse IdleTCPConnectionCheck configuration: %w", err),
		)
	}
	ipvs := false
	if viper.GetString("fp.forwarding_plane") == "ipvs" {
		ipvs = true
	}

	var fpForwardType fp.ForwardType
	ft := viper.GetString(fmt.Sprintf("services.%s.forward_type", service))
	switch ft {
	case "tunnel":
		fpForwardType = fp.Tunnel
	case "direct_route":
		fpForwardType = fp.DirectRoute
	default:
		return ret, fmt.Errorf("unknown forward type: %s", ft)
	}

	depoolThreshold := viper.GetFloat64(fmt.Sprintf("services.%s.depool_threshold", service))

	ret.addr = addr
	ret.port = port
	ret.httpCheckConfig = httpCheckConfig
	ret.itcCheckConfig = itcCheckConfig
	ret.ipvs = ipvs
	ret.fpForwardType = fpForwardType
	ret.depoolThreshold = depoolThreshold

	return ret, nil
}

// service contains the service ID in liberica configuration, not to be
// confused with etcd cluster/service configuration
func NewControlPlane(ctx context.Context, logger *slog.Logger, service string) (*ControlPlane, error) {
	// Validate and parse configuration
	if err := validateSettings(service); err != nil {
		return nil, fmt.Errorf("invalid settings: %w", err)
	}

	dc := viper.GetString("etcd.datacenter")
	cluster := viper.GetString(fmt.Sprintf("services.%s.cluster", service))
	etcdSvc := viper.GetString(fmt.Sprintf("services.%s.service", service))

	cfg, err := newControlPlaneConfig(service)
	if err != nil {
		return nil, err
	}

	// Configure etcd client
	endpoints, err := etcd.DiscoverEndpoints(ctx, viper.GetString("etcd.conftool_domain"))
	if err != nil {
		return nil, fmt.Errorf("unable to fetch etcd endpoints: %w", err)
	}

	etcdKey := fmt.Sprintf("/conftool/v1/pools/%s/%s/%s", dc, cluster, etcdSvc)

	etcdClient, err := etcd.NewEtcdv2Client(logger, endpoints, etcdKey)
	if err != nil {
		return nil, fmt.Errorf("unable to get an Etcdv2 client (%w) against endpoints %v", err, endpoints)
	}

	// Configure HCForwarder client
	hcforwarderClient, err := hcforwarder.NewGrpcClient(logger,
		viper.GetString("hcforwarder.grpc.network"),
		viper.GetString("hcforwarder.grpc.address"))
	if err != nil {
		return nil, fmt.Errorf("unable to get a hcforwarder client: %w", err)
	}

	// Configure HCMonitor client
	hcmonitorClient, err := healthcheck.NewGrpcClient(logger,
		viper.GetString("healthcheck.grpc.network"),
		viper.GetString("healthcheck.grpc.address"),
	)
	if err != nil {
		return nil, fmt.Errorf("unable to get a healthcheck monitor client: %w", err)
	}

	// Configure Forwarding Plane client
	fpClient, err := fp.NewGrpcClient(logger,
		viper.GetString("fp.grpc.network"),
		viper.GetString("fp.grpc.address"),
	)
	if err != nil {
		return nil, fmt.Errorf("unable to get forwarding plane client: %w", err)
	}

	// Configure BGP client
	bgpConfig, err := bgp.LoadBgpConfig()
	if err != nil {
		return nil, fmt.Errorf("unable to get bgp configuration: %w", err)
	}
	bgpClient, err := bgp.NewGoBgpClient(logger, bgpConfig)
	if err != nil {
		return nil, fmt.Errorf("unable to get bgp client: %w", err)
	}

	cpLogger := logger.With(
		slog.String("service", service),
	)

	configuredHCs := len(cfg.httpCheckConfig) + len(cfg.itcCheckConfig)
	if configuredHCs == 0 {
		cpLogger.Warn("service configured without any healthchecks")
	}

	metrics.DepoolThreshold.With(prometheus.Labels{
		"service": service,
	}).Set(cfg.depoolThreshold)

	return &ControlPlane{
		logger:            cpLogger,
		service:           service,
		etcdClient:        etcdClient,
		configuredHCs:     configuredHCs,
		hcforwarderClient: hcforwarderClient,
		hcmonitorClient:   hcmonitorClient,
		fpClient:          fpClient,
		bgpClient:         bgpClient,
		bgpConfig:         bgpConfig,
		cfg:               cfg,
	}, nil
}

// given that we need to spawn several ControlPlane instances at the same time
// we get a doneCh here and send the service name when Run() is done rather
// than returning a new channel and closing it
func (cp *ControlPlane) Run(ctx context.Context, doneCh chan<- *ControlPlaneError) {
	go func() {
		args := stateFnArgs{}
		cp.logger.Info("starting control plane initialization")
		for state := checkEtcdState; state != nil; {
			var err error
			state, args, err = state(ctx, cp, args)
			if err != nil {
				cp.logger.Error("unrecoverable error during control plane initialization", slog.Any("error", err))
				doneCh <- &ControlPlaneError{
					Service: cp.service,
					Err:     err,
				}
				return
			}
		}
		cp.bootstrapping = true
		cp.logger.Info("initialization done. Watching events")
		etcdEventsCh := make(chan etcd.Event, 128)
		hcResultsCh := make(chan healthcheck.Result, 128)

		etcdWatchReadyCh, etcdWatchDoneCh := cp.etcdClient.Watch(ctx, etcdEventsCh)
		etcdAttempts := 1
		etcdLastAttempt := time.Now()
		hcmonitorReadyCh, hcmonitorDoneCh := cp.hcmonitorClient.WatchResults(ctx, cp.service, hcResultsCh)
		hcmonitorAttempts := 1
		hcmonitorLastAttempt := time.Now()
		defer cp.logger.Info("stopping control plane")

		for {
			var state stateFn
			var args stateFnArgs

			select {
			case etcdEvent := <-etcdEventsCh:
				state = handleEtcdEventState
				args = stateFnArgs{
					etcdEvent: &etcdEvent,
				}
			case hcResult := <-hcResultsCh:
				state = handleHealthcheckResultState
				args = stateFnArgs{
					hcResult: &hcResult,
				}
			case <-etcdWatchReadyCh:
				cp.logger.Info("etcd Watcher is ready")
				etcdAttempts = 0
			case <-etcdWatchDoneCh:
				if ctx.Err() == nil { // checking that parent context hasn't been cancelled
					cp.logger.Warn("etcd watcher finished unexpectedly")
					if time.Since(etcdLastAttempt) < 1*time.Second {
						cp.logger.Info("waiting 1 second before next etcd reconnection")
						select {
						case <-time.After(1 * time.Second):
						case <-ctx.Done():
							doneCh <- &ControlPlaneError{
								Service: cp.service,
								Err:     ctx.Err(),
							}
							return
						}
					}
					etcdAttempts += 1
					etcdWatchReadyCh, etcdWatchDoneCh = cp.etcdClient.Watch(ctx, etcdEventsCh)
					etcdLastAttempt = time.Now()
				}
			case <-hcmonitorReadyCh:
				cp.logger.Info("hcmonitor is online")
				hcmonitorAttempts = 0
				if cp.bootstrapping {
					cp.logger.Info("bootstrapping completed, configuring BGP path")
					cp.bootstrapping = false
					state = configureBgpPathState
				} else {
					state = reconfigureHealthchecksState
				}
			case <-hcmonitorDoneCh:
				if ctx.Err() == nil { // checking that parent context hasn't been cancelled
					cp.logger.Warn("healthcheck WatchResults finished unexpectedly")
					if time.Since(hcmonitorLastAttempt) < 1*time.Second {
						cp.logger.Info("waiting 1 second before next hcmonitor reconnection")
						select {
						case <-time.After(1 * time.Second):
						case <-ctx.Done():
							doneCh <- &ControlPlaneError{
								Service: cp.service,
								Err:     ctx.Err(),
							}
							return
						}
					}
					hcmonitorAttempts += 1
					hcmonitorReadyCh, hcmonitorDoneCh = cp.hcmonitorClient.WatchResults(ctx, cp.service, hcResultsCh)
					hcmonitorLastAttempt = time.Now()
				}
			case <-ctx.Done():
				doneCh <- &ControlPlaneError{
					Service: cp.service,
					Err:     ctx.Err(),
				}
				return
			}
			for state != nil {
				var err error
				state, args, err = state(ctx, cp, args)
				if err != nil {
					cp.logger.Error("unrecoverable error handling event", slog.Any("error", err))
					doneCh <- &ControlPlaneError{
						Service: cp.service,
						Err:     err,
					}
					return
				}
			}
		}
	}()
}

func (cp *ControlPlane) RespawnRequired() (bool, error) {
	if err := validateSettings(cp.service); err != nil {
		return false, err
	}

	newCfg, err := newControlPlaneConfig(cp.service)
	if err != nil {
		return false, err
	}

	if cp.cfg.addr != newCfg.addr {
		return true, nil
	}

	if cp.cfg.port != newCfg.port {
		return true, nil
	}

	if cp.cfg.depoolThreshold != newCfg.depoolThreshold {
		return true, nil
	}

	if cp.cfg.fpForwardType != newCfg.fpForwardType {
		return true, nil
	}

	if cp.cfg.ipvs != newCfg.ipvs {
		return true, nil
	}

	if !slices.EqualFunc(cp.cfg.httpCheckConfig, newCfg.httpCheckConfig, func(a, b healthcheck.HTTPCheckConfig) bool {
		if c := healthcheck.HTTPCheckConfigSortFn(a, b); c == 0 {
			return true
		}
		return false
	}) {
		return true, nil
	}

	if !slices.EqualFunc(cp.cfg.itcCheckConfig, newCfg.itcCheckConfig, func(a, b healthcheck.IdleTCPConnectionCheckConfig) bool {
		if c := healthcheck.IdleTCPConnectionCheckConfigSortFn(a, b); c == 0 {
			return true
		}
		return false
	}) {
		return true, nil
	}

	return false, nil
}

func (cp *ControlPlane) Close(seamless bool) {
	cp.rsMutex.RLock()
	defer cp.rsMutex.RUnlock()

	if !seamless {
		cp.logger.Info("removing BGP path")
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		if err := deleteBgpPath(ctx, cp.bgpConfig, cp.bgpClient, cp.cfg.addr); err != nil {
			cp.logger.Error("unable to delete bgp path",
				slog.String("address", cp.cfg.addr.String()),
				slog.Any("error", err),
			)
		}
		cancel()

		cp.logger.Info("cleaning up forwarding plane")
		for _, rs := range cp.realservers {
			if rs.pooled == PooledNo {
				continue
			}
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			if err := deleteFPRealserver(ctx, cp.fpClient, cp.fpService, cp.cfg.fpForwardType, rs); err != nil {
				cp.logger.Error("unable to delete realserver from forwarding plane",
					slog.String("realserver", rs.hostname),
					slog.String("address", rs.address.String()),
					slog.Any("error", err),
				)
			}
			cancel()
		}
		ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
		if err := cp.fpClient.DeleteService(ctx, cp.fpService); err != nil {
			cp.logger.Error("unable to delete service from forwarding plane",
				slog.Any("error", err),
			)
		}
		cancel()
	}
	// no need to keep healthchecking the realservers if we are going away
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	for _, rs := range cp.realservers {
		if err := deleteHealthcheck(ctx, cp.hcmonitorClient, cp.service, cp.cfg.port, rs); err != nil {
			cp.logger.Warn("unable to delete healthcheck",
				slog.String("realserver", rs.hostname),
				slog.Any("error", err),
			)
		}
	}
	cp.etcdClient.Close()
	cp.hcforwarderClient.Close()
	cp.hcmonitorClient.Close()
	cp.fpClient.Close()
	cp.bgpClient.Close()
}
