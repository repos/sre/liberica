package cp

import (
	"context"
	"io"
	"log/slog"
	"net/netip"
	"slices"
	"sync"
	"testing"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
)

const (
	GRPC_NETWORK      = "tcp"
	GRPC_BIND_ADDRESS = "127.0.0.1:7890"
)

func TestGrpc(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cplane := &ControlPlane{
		logger: logger,
		realservers: map[netip.Addr]realserver{
			netip.MustParseAddr("127.0.0.2"): realserver{
				hostname: "foo1001.eqiad.wmnet",
				address:  netip.MustParseAddr("127.0.0.2"),
				weight:   10,
				pooled:   PooledYes,
			},
		},
	}
	_, cancelFn := context.WithCancel(context.Background())
	defer cancelFn()
	var m sync.Map

	m.Store("foo", &ControlPlaneRun{
		Cp:       cplane,
		CancelFn: cancelFn,
	})

	signalCtx, signalCancel := context.WithCancel(context.Background())
	srv := NewGrpcServer(logger, &m)
	serverCh, err := grpcutils.RunServer(signalCtx, logger, srv, GRPC_NETWORK, GRPC_BIND_ADDRESS)
	if err != nil {
		t.Fatalf("unable to run grpc server: %v", err)
		return
	}

	client, err := NewGrpcClient(logger, GRPC_NETWORK, GRPC_BIND_ADDRESS)
	if err != nil {
		t.Fatalf("unable to gat a grpc client: %v", err)
		return
	}

	t.Run("ListServices", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		res, err := client.ListServices(ctx)
		if err != nil {
			t.Fatalf("unable to list services: %v", err)
			return
		}
		if len(res) != 1 {
			t.Error("unexpected number of services")
		}
		if !slices.Contains(res, Service{Name: "foo"}) {
			t.Error("foo service isn't present on ListServices response")
		}
	})

	t.Run("ListRealservers", func(t *testing.T) {
		testcases := []struct {
			name       string
			svc        Service
			successful bool
		}{
			{
				name:       "known service",
				svc:        Service{Name: "foo"},
				successful: true,
			},
			{
				name:       "unknown service",
				svc:        Service{Name: "bar"},
				successful: false,
			},
		}
		for _, tc := range testcases {
			t.Run(tc.name, func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()
				res, err := client.ListRealservers(ctx, tc.svc)
				successful := true
				if err != nil {
					successful = false
				}
				if successful != tc.successful {
					t.Fatalf("unexpected result, want successful = %v, got successful = %v", tc.successful, successful)
					return
				}

				if successful {
					if res[0].Address != netip.MustParseAddr("127.0.0.2") {
						t.Errorf("unexpected address: %v", res[0].Address)
					}
					if res[0].Weight != 10 {
						t.Errorf("unexpected weight: %d", res[0].Weight)
					}
					if res[0].Pooled != PooledYes {
						t.Errorf("unexpected pooled value: %v", res[0].Pooled)
					}
				}
			})
		}
	})

	signalCancel()
	select {
	case <-serverCh:
	case <-time.After(5 * time.Second):
		t.Error("cmd didn't react to SIGTERM in 5 seconds")
	}
}
