# Control plane state machine

The control plane state machine has been built following the state machine design described by Rob Pike in https://www.youtube.com/watch?v=HxaD_trXwRE.
An automatically generated list of following states and potential errors for each state can be found [here](STATES.md)

## States
### checkEtcd
Used during the initialization stage of the control plane, performs an etcd.Get() action to fetch the current etcd state of the service. Possible following states:
* resolveEtcdNodes
* retryCheckEtcd if etcd.Get() returns a "key not found" error
* unrecoverable error for any other error retrieving data from etcd
### retryCheckEtcd
Used during the initialization stage of the control plane, waits 60 seconds (`etcdRetry` value) and moves the control flow back to checkEtcd state
### resolveEtcdNodes
Using a list of realservers retrieved from etcd, performs the DNS resolution of the hostnames stored in etcd. It will skip hostnames on failed DNS queries and it will prefer the first address if more than one address is returned.
It moves the execution to configureHCForwarder
### configureHCForwarder
This state is responsible of mapping realserver to SOMARKs using the configured hashing algorithm and configure the HCForwarder service accordingly.
It moves the execution to configureHealthchecks
### configureHealthchecks
Configure healthchecks for administratively pooled hosts.
It moves the execution flow to populateRealserversMap
### populateRealserversMap
Populates the internal realservers map used by the control plane with all the realservers information.
Moves the execution flow to checkFPService
### checkFPService
Determines if the service (VIP+port tuple) is already present on the forwarding plane.
Possible states:
* addServiceForwardingPlane if the service is not in the forwarding plane
* checkFPRealservers if the service is already present in the forwarding plane
### addServiceForwardingPlane
Configures the service in the forwarding plane and moves the execution flow to checkFPRealservers
### checkFPRealservers
This state configures the forwarding plane realservers as described in the internal realservers map that has been populated by populateRealserversMap. It will perform the following actions:
* Add missing realservers
* Update realservers that are already present in the forwarding plane
* Delete realservers from the forwarding plane if needed.
Possible states:
* nil (finishes control plane initialization state)
* deleteHealthchecks if any realserver needed to be removed from the forwarding plane
### deleteHealthchecks
Deletes healthchecks and moves the execution flow to checkDepoolThresholdIsMet
### checkDepoolThresholdIsMet
After the control plane is flagged as read write, check if the depool threshold is currently being met, and if isn't it will redirect the execution flow to enforceDepoolThreshold triggering a forced repool of the minimum amount of realservers required to comply with the depool threshold
### enforceDepoolThreshold
Repools one or more realservers flagging them as `pooled = DepoolBlocked`. After it's done it will move the execution flow to checkPendingRealservers
### handleHealthcheckResultState
Triggered by the reception of a healthcheck result, it performs the following actions:
* checks that realserver of the healthcheck result is known, stops the execution otherwise
* stores the result of the healthcheck
* if the control plane is currently in read only mode, checks if all realservers already received one result for each configured healthcheck and sets the control plane as read write

After performing this actions and based on the healthcheck result moves the execution flow to one of the following states:
* depoolRealserver if the healthcheck reports that the realserver is down
* repoolRealserver if the healthcheck reports that the reaserver is up and all the other healthchecks configured for the realserver report the same
### depoolRealserver
Depools a realserver from the forwarding plane as notified by one healthcheck result. Before performing the depool action it performs the following checks:
* Control plane is in real write mode
* Depool threshold allows depooling that specific realserver
If any of those checks fails, realserver is flagged as `pooled = DepoolBlocked`. If all checks succeed, the realserver is flagged as `pooled = PooledNo` and deleted from the forwarding plane or as `pooled = DepoolFailed` if there is an error attempting to depool the realserver.
After a successful depool execution flow is moved to checkPendingRealservers
### repoolRealserver
Repools a realserver after the control plane has been notified by a healthcheck result and all healthchecks for that realserver report it as healthy.
After a succesful repool the realserver is flagged as `pooled = PooledYes`, otherwise it will be flagged as `pooled = RepoolFailed`.
Execution flow is then moved to checkPendingRealservers.
### checkPendingRealservers
This state revisits blocked/failed depool/repool realservers  and tries to depool/repool them given that depool thresold would allow it.
### handleEtcdEvent
This state takes care of handling an etcd event and redirects the execution flow to the appropiate state based on the type of etcd event:
* Delete events are handled by handleEtcdDeleteEvent
* Updates are handled by handleEtcdUpdateEvent
* Create events are handled by handleEtcdCreateEvent
### handleEtcdCreateEvent
This states handles etcd create events, it will scan the received etcd nodes attached to the event and redirect the execution flow to resolveEtcdNodes as long as the event contains one or more administratively pooled realservers.
### handleEtcdUpdateEvent
Probably the most complext etcd event as it could result on etcd nodes being actually created, deleted or updated:
* Previously administratively pooled realservers are getting depooled trigger handleEtcdDeleteEvent
* Previously administratively depooled realservers are getting pooled trigger handleEtcdCreateEvent
* Administratively pooled realservers get their weight updated trigger updateRealserversState
### handleEtcdDeleteEvent
This state handles an etcd delete event / administrative depool. Execution flow is handled to deleteRealserversState
### updateRealservers
This state takes care to enforce an etcd update event, updating the weight of existing realservers
### deleteRealserversState
This state enforces an etcd delete/administrative depool event removing the realservers from the forwarding plane and redirecting the execution flow to deleteHealthchecks
### configureBgpPathState
This state adds the BGP prefix assuming a /32 for IPv4 VIPs and a /128 for IPv6 VIPs. This state is triggered just once after control plane has finished its bootstraping (control plane initialization succeeded and it's ready to receive healthcheck results)

## Graph
Legend:
* green: initial state
* red: stops the state machine under certain conditions
* blue: unconditional stop of the state machine
![state machine graph](state-machine-graph.svg)
