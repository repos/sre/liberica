package cp

import (
	"context"
	"log/slog"
	"sync"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/cp"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"
	pbsvc "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct {
	pb.UnimplementedControlPlaneServiceServer
	cplanes *sync.Map
	logger  *slog.Logger
}

func NewGrpcServer(logger *slog.Logger, cplanes *sync.Map) *grpc.Server {
	s := &server{
		logger:  logger,
		cplanes: cplanes,
	}

	var opts []grpc.ServerOption
	opts = append(opts, grpc.KeepaliveParams(grpcutils.KeepaliveServerParameters))
	srv := grpc.NewServer(opts...)
	pb.RegisterControlPlaneServiceServer(srv, s)

	return srv
}

func (srv *server) ListServices(context.Context, *pb.ListServicesRequest) (*pb.ListServicesResponse, error) {
	result := &pb.ListServicesResponse{}
	srv.cplanes.Range(func(key, value any) bool {
		name := key.(string)
		cp := value.(*ControlPlaneRun).Cp
		svc := &pbsvc.Service{
			Name:    name,
			Address: cp.cfg.addr.String(),
			Port:    uint32(cp.cfg.port),
		}
		result.Svc = append(result.Svc, svc)
		return true
	})

	return result, nil
}

func (srv *server) ListRealservers(ctx context.Context, in *pb.ListRealserversRequest) (*pb.ListRealserversResponse, error) {
	svc := in.GetSvc()
	if svc.Name == "" {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Svc.Name")
	}
	cpr, present := srv.cplanes.Load(svc.Name)
	if !present {
		return nil, status.Errorf(codes.NotFound, "service not found: %s", svc)
	}
	cplane := cpr.(*ControlPlaneRun).Cp

	cplane.rsMutex.RLock()
	defer cplane.rsMutex.RUnlock()

	out := &pb.ListRealserversResponse{}
	out.Rs = make([]*pbrs.Realserver, 0, len(cplane.realservers))
	for address, rs := range cplane.realservers {
		healthy := pbrs.Healthy_H_YES
		for _, hcResult := range rs.hcResults {
			if !hcResult {
				healthy = pbrs.Healthy_H_NO
				break
			}
		}
		pooled := pbrs.Pooled_P_UNSPECIFIED
		switch rs.pooled {
		case PooledYes:
			pooled = pbrs.Pooled_P_YES
		case PooledNo:
			pooled = pbrs.Pooled_P_NO
		case DepoolBlocked:
			pooled = pbrs.Pooled_P_DEPOOL_BLOCKED
		case DepoolFailed:
			pooled = pbrs.Pooled_P_DEPOOL_FAILED
		case RepoolFailed:
			pooled = pbrs.Pooled_P_REPOOL_FAILED
		case ForceRepoolFailed:
			pooled = pbrs.Pooled_P_FORCE_REPOOL_FAILED
		}

		ret := &pbrs.Realserver{
			Address:  address.String(),
			Hostname: rs.hostname,
			Mark:     rs.mark,
			Weight:   uint32(rs.weight),
			Pooled:   pooled,
			Healthy:  healthy,
		}
		out.Rs = append(out.Rs, ret)
	}

	return out, nil
}
