package cp

import (
	"context"
	"errors"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/fp"
)

type fpAction uint8

const (
	createRs fpAction = iota
	updateRs
	deleteRs
)

func runFPAction(ctx context.Context, action fpAction, client fp.FPClient, svc fp.Service, ft fp.ForwardType, rs realserver) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	fpRs := fp.Realserver{
		Address:     rs.address,
		Weight:      uint32(rs.weight),
		Mark:        rs.mark,
		ForwardType: ft,
	}

	switch action {
	case createRs:
		return client.CreateRealserver(ctx, svc, fpRs)
	case updateRs:
		return client.UpdateRealserver(ctx, svc, fpRs)
	case deleteRs:
		return client.DeleteRealserver(ctx, svc, fpRs)
	default:
		return errors.New("unknown fp action")
	}
}

func deleteFPRealserver(ctx context.Context, client fp.FPClient, svc fp.Service, ft fp.ForwardType, rs realserver) error {
	return runFPAction(ctx, deleteRs, client, svc, ft, rs)
}

func createFPRealserver(ctx context.Context, client fp.FPClient, svc fp.Service, ft fp.ForwardType, rs realserver) error {
	return runFPAction(ctx, createRs, client, svc, ft, rs)
}

func updateFPRealserver(ctx context.Context, client fp.FPClient, svc fp.Service, ft fp.ForwardType, rs realserver) error {
	return runFPAction(ctx, updateRs, client, svc, ft, rs)
}
