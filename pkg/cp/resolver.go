package cp

import (
	"context"
	"net/netip"
)

// interface targetting stdlib resolver LookupNetIP
// this is required to be able to mock it during tests
// https://pkg.go.dev/net#Resolver.LookupNetIP
type resolver interface {
	LookupNetIP(context.Context, string, string) ([]netip.Addr, error)
}
