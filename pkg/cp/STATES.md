# state machine
This is an automatically generated list of the control plane state machine states, its subsequent states and potential errors

### Next states and potential errors for checkEtcdState:
 - crash state machine with error  "unable to get etcd key: %w" err
 - resolveEtcdNodesState
 - retryCheckEtcdState
### Next states and potential errors for populateRealserversMapState:
 - checkFPRealserversState
 - checkFPServiceState
### Next states and potential errors for checkFPRealserversState:
 - crash state machine with error  "invalid control plane state, fpService should have been initializated before entering checkFPRealserversState"
 - deleteHealthchecksState
 - stop state machine
 - updateMetricsState
### Next states and potential errors for deleteHealthchecksState:
 - checkDepoolThresholdIsMetState
### Next states and potential errors for checkPendingRealserversState:
 - updateMetricsState
### Next states and potential errors for handleEtcdCreateEventState:
 - crash state machine with error ErrInvalidStateArguments
 - resolveEtcdNodesState
 - stop state machine
### Next states and potential errors for handleEtcdDeleteEventState:
 - crash state machine with error ErrEtcdServiceDeleted
 - crash state machine with error ErrInvalidStateArguments
 - deleteRealserversState
 - stop state machine
### Next states and potential errors for handleEtcdUpdateEventState:
 - crash state machine with error ErrInvalidStateArguments
 - handleEtcdCreateEventState
 - handleEtcdDeleteEventState
 - stop state machine
 - updateRealserversState
### Next states and potential errors for retryCheckEtcdState:
 - checkEtcdState
### Next states and potential errors for configureHealthchecksState:
 - populateRealserversMapState
### Next states and potential errors for addServiceForwardingPlaneState:
 - checkFPRealserversState
### Next states and potential errors for handleHealthcheckResultState:
 - checkPendingRealserversState
 - crash state machine with error ErrInvalidStateArguments
 - depoolRealserverState
 - repoolRealserverState
 - stop state machine
 - updateMetricsState
### Next states and potential errors for updateMetricsState:
 - stop state machine
### Next states and potential errors for repoolRealserverState:
 - checkPendingRealserversState
 - crash state machine with error  "%w: %s" ErrUnknownRealserver
 - crash state machine with error ErrInvalidStateArguments
### Next states and potential errors for reconfigureHealthchecksState:
 - stop state machine
### Next states and potential errors for configureBgpPathState:
 - crash state machine with error  "unable to add BGP path: %w" err
 - stop state machine
### Next states and potential errors for handleEtcdEventState:
 - crash state machine with error ErrInvalidStateArguments
 - handleEtcdCreateEventState
 - handleEtcdDeleteEventState
 - handleEtcdUpdateEventState
 - stop state machine
### Next states and potential errors for updateRealserversState:
 - crash state machine with error  "%w: %s" ErrUnknownRealserver
 - stop state machine
### Next states and potential errors for resolveEtcdNodesState:
 - configureHCForwarderState
### Next states and potential errors for configureHCForwarderState:
 - configureHealthchecksState
 - crash state machine with error  "unable to get realservers: %w" err
 - stop state machine
### Next states and potential errors for checkFPServiceState:
 - addServiceForwardingPlaneState
 - checkFPRealserversState
 - stop state machine
### Next states and potential errors for checkDepoolThresholdIsMetState:
 - enforceDepoolThresholdState
 - stop state machine
 - updateMetricsState
### Next states and potential errors for enforceDepoolThresholdState:
 - checkPendingRealserversState
 - crash state machine with error  "%w: %s" ErrUnknownRealserver
### Next states and potential errors for depoolRealserverState:
 - checkPendingRealserversState
 - crash state machine with error  "%w: %s" ErrUnknownRealserver
 - crash state machine with error ErrInvalidStateArguments
 - updateMetricsState
### Next states and potential errors for deleteRealserversState:
 - crash state machine with error err
 - deleteHealthchecksState
