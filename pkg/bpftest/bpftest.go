package bpftest

import (
	"log"
	"net"
	"os/exec"
	"time"

	"github.com/florianl/go-tc"
	"github.com/florianl/go-tc/core"
	"golang.org/x/sys/unix"
)

const bpftoolPath = "/usr/sbin/bpftool"

type BpfObject uint8

const (
	BpfProgram BpfObject = iota
	BpfMap
	BpfLink
)

func (b BpfObject) String() string {
	switch b {
	case BpfProgram:
		return "prog"
	case BpfMap:
		return "map"
	case BpfLink:
		return "link"
	}

	return "unknown"
}

// functions used by several integration tests

// check if an eBPF object is loaded
// checks during 1 second every 10ms
func IsBpfObjectLoaded(object BpfObject, name string, expect bool) bool {
	pollingTime := 10 * time.Millisecond
	var found bool
	for i := time.Duration(0); i < time.Second; i += pollingTime {
		cmd := exec.Command(bpftoolPath, object.String(), "list", "name", name)
		err := cmd.Run()
		if err == nil {
			found = true
		} else {
			found = false
		}
		if found && expect || !found && !expect { // stop polling ASAP
			return found
		}
	}
	return found
}

// attach clscact qdisc to a network interface
func AttachClsact(ifaceName string) error {
	iface, err := net.InterfaceByName(ifaceName)
	if err != nil {
		return err
	}
	tcnl, err := tc.Open(&tc.Config{})
	if err != nil {
		return err
	}
	qdisc := tc.Object{
		Msg: tc.Msg{
			Family:  unix.AF_UNSPEC,
			Ifindex: uint32(iface.Index),
			Handle:  core.BuildHandle(tc.HandleRoot, 0x0000),
			Parent:  tc.HandleIngress,
			Info:    0,
		},
		Attribute: tc.Attribute{
			Kind: "clsact",
		},
	}

	return tcnl.Qdisc().Add(&qdisc)
}

func MustRun(command string, args ...string) {
	cmd := exec.Command(command, args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("Error running %v: %v, %v", command, err, out)
	}
}
