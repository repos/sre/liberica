package bgp

import (
	"context"
	"io"
	"log/slog"
	"net/netip"
	"testing"
	"time"
)

func TestPath(t *testing.T) {
	if !localGobgpdAvailable() {
		t.Skip("gobgpd not available")
	}
	h, err := newGobgpdHarness("unix")
	if err != nil {
		t.Fatalf("unable to set up test gobgpd instance: %v", err)
	}
	defer h.Close()

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cfg := &BgpConfig{
		LocalAsn: 64600,
	}
	cfg.Grpc.Network = "unix"
	cfg.Grpc.Address = h.endpoint
	client, err := NewGoBgpClient(logger, cfg)
	if err != nil {
		t.Fatalf("unable to get gobgp api client: %v", err)
	}
	defer client.Close()

	testcases := []struct {
		name       string
		prefix     netip.Prefix
		nextHop    netip.Addr
		notNextHop netip.Addr // another valid address to verify that IsPathPresent returns false
	}{
		{
			name:       "IPv4",
			prefix:     netip.MustParsePrefix("10.0.0.0/24"),
			nextHop:    netip.MustParseAddr("127.0.0.2"),
			notNextHop: netip.MustParseAddr("127.0.0.3"),
		},
		{
			name:       "IPv6",
			prefix:     netip.MustParsePrefix("2620:0:861:ed1a::9/128"),
			nextHop:    netip.MustParseAddr("2620:0:861:109:10:64:130:16"),
			notNextHop: netip.MustParseAddr("2620:0:861:109:10:64:130:17"),
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			// Add prefix
			if err := client.AddPath(ctx, tc.prefix, tc.nextHop); err != nil {
				t.Fatalf("unexpected error adding path: %v", err)
				return
			}
			// check that IsPathPresent returns true
			present, err := client.IsPathPresent(ctx, tc.prefix, tc.nextHop)
			if err != nil || !present {
				t.Fatalf("prefix not found after successful AddPath() call: %v / %v", err, present)
				return
			}

			// check that IsPathPresent returns false if nextHop doesn't match
			present, err = client.IsPathPresent(ctx, tc.prefix, tc.notNextHop)
			if err != nil || present {
				t.Fatal("prefix should be flagged as not present with a non-configured nextHop")
				return
			}

			// Delete the prefix
			if err := client.DeletePath(ctx, tc.prefix, tc.nextHop); err != nil {
				t.Fatalf("unable to delete prefix: %v", err)
				return
			}

			// Check that IsPathPresent returns false
			present, err = client.IsPathPresent(ctx, tc.prefix, tc.nextHop)
			if err != nil || present {
				t.Fatal("prefix found after successful DeletePath() call")
			}
		})
	}

}

func TestAddPeer(t *testing.T) {
	h, err := newGobgpdHarness("tcp")
	if err != nil {
		t.Fatalf("unable to set up test gobgpd instance: %v", err)
	}
	defer h.Close()
	if !localGobgpdAvailable() {
		t.Skip("gobgpd not available")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cfg := &BgpConfig{
		LocalAsn: 64600,
	}
	cfg.Grpc.Network = "tcp"
	cfg.Grpc.Address = h.endpoint
	client, err := NewGoBgpClient(logger, cfg)
	if err != nil {
		t.Fatalf("unable to get gobgp api client: %v", err)
	}
	defer client.Close()

	addr := netip.MustParseAddr("127.0.0.2")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Add peer
	if err := client.AddPeer(ctx, addr); err != nil {
		t.Fatalf("unable to add peer: %v", err)
	}

	// Check peer is present
	if present, err := client.IsPeerPresent(ctx, addr); err != nil || !present {
		t.Fatal("unable to check if added peer is present")
	}

	// Delete peer
	if err := client.DeletePeer(ctx, addr); err != nil {
		t.Fatalf("unable to delete peer: %v", err)
	}

	// Check that peer isn't present anymore
	if present, err := client.IsPeerPresent(ctx, addr); err != nil || present {
		t.Fatal("peer still present after being deleted")
	}
}

func TestListPeers(t *testing.T) {
	h, err := newGobgpdHarness("tcp")
	if err != nil {
		t.Fatalf("unable to set up test gobgpd instance: %v", err)
	}
	defer h.Close()
	if !localGobgpdAvailable() {
		t.Skip("gobgpd not available")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cfg := &BgpConfig{
		LocalAsn: 64600,
	}
	cfg.Grpc.Network = "tcp"
	cfg.Grpc.Address = h.endpoint
	client, err := NewGoBgpClient(logger, cfg)
	if err != nil {
		t.Fatalf("unable to get gobgp api client: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// no peers present
	if peers, err := client.ListPeers(ctx); len(peers) != 0 || err != nil {
		t.Fatal("unable to list peers")
	}

	addr := netip.MustParseAddr("127.0.0.2")
	// Add peer
	if err := client.AddPeer(ctx, addr); err != nil {
		t.Fatalf("unable to add peer: %v", err)
	}

	// peer present
	if peers, err := client.ListPeers(ctx); len(peers) != 1 || peers[0] != addr || err != nil {
		t.Fatal("unable to list peers")
	}
}

func TestSoftResetPeer(t *testing.T) {
	h, err := newGobgpdHarness("tcp")
	if err != nil {
		t.Fatalf("unable to set up test gobgpd instance: %v", err)
	}
	defer h.Close()
	if !localGobgpdAvailable() {
		t.Skip("gobgpd not available")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cfg := &BgpConfig{
		LocalAsn: 64600,
	}
	cfg.Grpc.Network = "tcp"
	cfg.Grpc.Address = h.endpoint
	client, err := NewGoBgpClient(logger, cfg)
	if err != nil {
		t.Fatalf("unable to get gobgp api client: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Add Peer
	peer := netip.MustParseAddr("127.0.0.2")
	if err := client.AddPeer(ctx, peer); err != nil {
		t.Fatalf("unable to add peer: %v", err)
	}

	if err := client.SoftResetPeer(ctx, peer); err != nil {
		t.Fatalf("Unable to softreset peer: %v", err)
	}
}

func TestEnableCommunities(t *testing.T) {
	h, err := newGobgpdHarness("tcp")
	if err != nil {
		t.Fatalf("unable to set up test gobgpd instance: %v", err)
	}
	defer h.Close()
	if !localGobgpdAvailable() {
		t.Skip("gobgpd not available")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cfg := &BgpConfig{
		LocalAsn: 64600,
	}
	cfg.Grpc.Network = "tcp"
	cfg.Grpc.Address = h.endpoint
	cfg.Communities = []string{"14907:0"}
	client, err := NewGoBgpClient(logger, cfg)
	if err != nil {
		t.Fatalf("unable to get gobgp api client: %v", err)
	}
	defer client.Close()

	prefix := netip.MustParsePrefix("10.0.0.0/24")
	nextHop := netip.MustParseAddr("192.168.88.5")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Add prefix
	if err := client.AddPath(ctx, prefix, nextHop); err != nil {
		t.Fatalf("unexpected error adding path: %v", err)
	}

	if err := client.EnableCommunities(ctx, cfg.Communities); err != nil {
		t.Fatalf("unable to set communities policy: %v", err)
	}

	addr := netip.MustParseAddr("127.0.0.2")

	// Add peer
	if err := client.AddPeer(ctx, addr); err != nil {
		t.Fatalf("unable to add peer: %v", err)
	}

	if enabled, err := client.AreCommunitiesEnabled(ctx, cfg.Communities); err != nil || enabled != CommunitiesMatch {
		t.Fatalf("unable to check if communities are enabled: %v", err)
	}
}

func TestDisableCommunities(t *testing.T) {
	h, err := newGobgpdHarness("tcp")
	if err != nil {
		t.Fatalf("unable to set up test gobgpd instance: %v", err)
	}
	defer h.Close()
	if !localGobgpdAvailable() {
		t.Skip("gobgpd not available")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	cfg := &BgpConfig{
		LocalAsn: 64600,
	}
	cfg.Grpc.Network = "tcp"
	cfg.Grpc.Address = h.endpoint
	cfg.Communities = []string{"14907:0"}
	client, err := NewGoBgpClient(logger, cfg)
	if err != nil {
		t.Fatalf("unable to get gobgp api client: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	if err := client.EnableCommunities(ctx, cfg.Communities); err != nil {
		t.Fatalf("unable to set communities policy: %v", err)
	}
	cancel()

	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	if enabled, err := client.AreCommunitiesEnabled(ctx, cfg.Communities); err != nil || enabled != CommunitiesMatch {
		t.Fatalf("unable to check if communities are enabled: %v", err)
	}
	cancel()

	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	if err := client.DisableCommunities(ctx); err != nil {
		t.Fatalf("unable to disable communities: %v", err)
	}
	cancel()

	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	if enabled, err := client.AreCommunitiesEnabled(ctx, cfg.Communities); err != nil || enabled != CommunitiesNotPresent {
		t.Fatalf("unable to check if communities are enabled: %v", err)
	}
	cancel()

}
