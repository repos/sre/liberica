package bgp

import (
	"net/netip"
	"slices"
	"testing"

	"github.com/mitchellh/mapstructure"
)

func TestStringToAddrHookFunc(t *testing.T) {
	cfgMap := map[string]any{
		"peers":         []string{"127.0.0.2", "127.0.0.3"},
		"next_hop_ipv4": "127.0.0.1",
		"next_hop_ipv6": "::1",
	}

	var bgpConfig BgpConfig
	decoderCfg := &mapstructure.DecoderConfig{
		DecodeHook: stringToAddrHookFunc(),
		Result:     &bgpConfig,
	}

	decoder, err := mapstructure.NewDecoder(decoderCfg)
	if err != nil {
		t.Fatalf("unable to get a decoder: %v", err)
	}

	decoder.Decode(cfgMap)

	if bgpConfig.NextHop4 != netip.MustParseAddr("127.0.0.1") {
		t.Errorf("unexpected NextHop4 value: %v", bgpConfig.NextHop4)
	}
	if bgpConfig.NextHop6 != netip.MustParseAddr("::1") {
		t.Errorf("unexpected NextHop4 value: %v", bgpConfig.NextHop6)
	}

	if !slices.Equal(bgpConfig.Peers,
		[]netip.Addr{
			netip.MustParseAddr("127.0.0.2"),
			netip.MustParseAddr("127.0.0.3"),
		},
	) {
		t.Errorf("unexpected peers value: %v", bgpConfig.Peers)
	}

}
