package bgp

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"os/exec"
	"path"
	"time"

	gbapi "github.com/osrg/gobgp/v3/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

type harness struct {
	gobgpdInstance *exec.Cmd
	tmpDir         string
	endpoint       string
	bgpPort        uint16
}

// localGobgpdAvailable returns true if an gobgpd binary is available on PATH
func localGobgpdAvailable() bool {
	_, err := exec.LookPath("gobgpd")
	return err == nil
}

// Allocate a TCP port on 127.0.0.1
func allocateLocalAddress() (string, error) {
	l, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		return "", err
	}
	defer l.Close()

	return l.Addr().String(), nil
}

var minimalConfig = []byte(`
[global.config]
  as = 64512
  router-id = "127.0.0.1"
  port = -1
`)

func newGobgpdHarness(grpcNetwork string) (*harness, error) {
	gobgpdBinary, err := exec.LookPath("gobgpd")
	if err != nil {
		return nil, fmt.Errorf("unable to find gobgpd binary: %w", err)
	}

	h := &harness{}
	tmpDir, err := os.MkdirTemp("", "gobgpd.harness")
	if err != nil {
		return nil, fmt.Errorf("unable to create temp dir: %w", err)
	}
	h.tmpDir = tmpDir

	cfgFilePath := path.Join(tmpDir, "gobgpd.config")
	if err := os.WriteFile(cfgFilePath, minimalConfig, 0600); err != nil {
		return nil, fmt.Errorf("unable to create cfg file %s: %w", cfgFilePath, err)
	}

	var apiHosts string
	switch grpcNetwork {
	case "tcp":
		var err error
		apiHosts, err = allocateLocalAddress()
		if err != nil {
			return nil, fmt.Errorf("unable to allocate a tcp port: %w", err)
		}
	case "unix":
		apiHosts = "unix://" + path.Join(tmpDir, "grpc.socket")
	default:
		return nil, fmt.Errorf("unsupported network for grpc %s", grpcNetwork)
	}

	h.gobgpdInstance = exec.Command(
		gobgpdBinary,
		"--pprof-disable",
		"--config-file="+cfgFilePath,
		"--api-hosts="+apiHosts,
	)
	h.gobgpdInstance.Stderr = io.Discard
	h.gobgpdInstance.Stdout = io.Discard
	h.endpoint = apiHosts

	if err := h.gobgpdInstance.Start(); err != nil {
		h.Close()
		return nil, fmt.Errorf("unable to start gobgpd instance: %w", err)
	}

	if err := h.pollGobgpdForReadiness(); err != nil {
		h.Close()
		return nil, fmt.Errorf("gobgpd isn't ready: %w", err)
	}

	return h, nil
}
func (h *harness) pollGobgpdForReadiness() error {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	// DialContext returns before connecting, ctx only controls the setup steps. More details on https://pkg.go.dev/google.golang.org/grpc@v1.46.2#DialContext
	conn, err := grpc.DialContext(ctx, h.endpoint, opts...)
	if err != nil {
		return fmt.Errorf("unable to create connection: %w", err)
	}
	cancel()
	client := gbapi.NewGobgpApiClient(conn)
	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	for {
		ctx, cancel := context.WithTimeout(ctx, 100*time.Millisecond)
		_, err := client.GetBgp(ctx, &gbapi.GetBgpRequest{})
		cancel()
		if err == nil {
			return nil
		}
		// checking errors.Is(err, context.DeadlineExceeded) doesn't work as expected
		if status.Code(err) == codes.DeadlineExceeded {
			return errors.New("timeout waiting for a successful response")
		}
		time.Sleep(100 * time.Millisecond)
	}
}

func (h *harness) Close() error {
	if h.gobgpdInstance != nil {
		defer func() {
			if h.tmpDir != "" {
				if err := os.RemoveAll(h.tmpDir); err != nil {
					fmt.Fprintf(os.Stderr, "unable to remove temp dir %s: %v", h.tmpDir, err)
				}
			}
		}()
		if err := h.gobgpdInstance.Process.Kill(); err != nil {
			return fmt.Errorf("unable to kill gobgpd process: %w", err)
		}
		stopped := make(chan struct{})
		go func() {
			h.gobgpdInstance.Wait()
			close(stopped)
		}()
		select {
		case <-time.After(30 * time.Second):
			return errors.New("timeout stopping gobgpd")
		case <-stopped:
		}
	}
	return nil
}
