/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package bgp

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/netip"
	"slices"
	"strings"
	"time"

	gbapi "github.com/osrg/gobgp/v3/api"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/anypb"
)

const (
	CommunitiesPolicyName     = "CommunitiesPolicy"
	CommunitiesDefinedSetName = "CommunitiesDefinedSet"
)

type CommunitiesEnabled uint8

const (
	CommunitiesNotPresent CommunitiesEnabled = iota // Communities not configured
	CommunitiesMatch                                // Communities configured and up to date
	CommunitiesNeedUpdate                           // Communitities configured but need updating
)

type BgpClient interface {
	AddPath(context.Context, netip.Prefix, netip.Addr) error
	DeletePath(context.Context, netip.Prefix, netip.Addr) error
	IsPathPresent(context.Context, netip.Prefix, netip.Addr) (bool, error)
	AddPeer(context.Context, netip.Addr) error
	DeletePeer(context.Context, netip.Addr) error
	SoftResetPeer(context.Context, netip.Addr) error
	IsPeerPresent(context.Context, netip.Addr) (bool, error)
	ListPeers(context.Context, ...netip.Addr) ([]netip.Addr, error)
	EnableCommunities(context.Context, []string) error
	DisableCommunities(context.Context) error
	AreCommunitiesEnabled(context.Context, []string) (CommunitiesEnabled, error)
	Close()
}

type GoBgpClient struct {
	logger   *slog.Logger
	conn     *grpc.ClientConn
	localAsn uint32
}

type GoBgpClientOption func(*GoBgpClient)

func NewGoBgpClient(logger *slog.Logger, cfg *BgpConfig) (BgpClient, error) {
	addr := grpcutils.GetConnectAddress(cfg.Grpc.Network, cfg.Grpc.Address)

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	// skip KeepaliveParams since we cannot tune gobgpd side
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// DialContext returns before connecting, ctx only controls the setup steps. More details on https://pkg.go.dev/google.golang.org/grpc@v1.46.2#DialContext
	conn, err := grpc.DialContext(ctx, addr, opts...)
	if err != nil {
		return nil, fmt.Errorf("unable to create connection: %w", err)
	}

	client := &GoBgpClient{
		logger:   logger,
		conn:     conn,
		localAsn: cfg.LocalAsn,
	}

	return client, nil
}

func getPath(prefix netip.Prefix, nextHop netip.Addr) (*gbapi.Path, error) {
	nlri, err := anypb.New(&gbapi.IPAddressPrefix{
		PrefixLen: uint32(prefix.Bits()),
		Prefix:    prefix.Addr().String(),
	})
	if err != nil {
		return nil, err
	}
	nh, err := anypb.New(&gbapi.NextHopAttribute{
		NextHop: nextHop.String(),
	})
	if err != nil {
		return nil, err
	}
	origin, err := anypb.New(&gbapi.OriginAttribute{
		Origin: 0,
	})
	if err != nil {
		return nil, err
	}

	var afi gbapi.Family_Afi
	if prefix.Addr().Is4() {
		afi = gbapi.Family_AFI_IP
	} else {
		afi = gbapi.Family_AFI_IP6
	}

	return &gbapi.Path{
		Family: &gbapi.Family{Afi: afi, Safi: gbapi.Family_SAFI_UNICAST},
		Nlri:   nlri,
		Pattrs: []*anypb.Any{nh, origin},
	}, nil
}

// Add the specified prefix via nextHop
func (c *GoBgpClient) AddPath(ctx context.Context, prefix netip.Prefix, nextHop netip.Addr) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	path, err := getPath(prefix, nextHop)
	if err != nil {
		return err
	}

	addPathRequest := &gbapi.AddPathRequest{
		TableType: gbapi.TableType_GLOBAL,
		Path:      path,
	}

	client := gbapi.NewGobgpApiClient(c.conn)
	if _, err := client.AddPath(ctx, addPathRequest); err != nil {
		return fmt.Errorf("unable to add path: %w\n", err)
	}

	return nil
}

// Delete the specified prefix via nextHop
func (c *GoBgpClient) DeletePath(ctx context.Context, prefix netip.Prefix, nextHop netip.Addr) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	path, err := getPath(prefix, nextHop)
	if err != nil {
		return err
	}

	deletePathRequest := &gbapi.DeletePathRequest{
		TableType: gbapi.TableType_GLOBAL,
		Path:      path,
	}

	client := gbapi.NewGobgpApiClient(c.conn)
	if _, err := client.DeletePath(ctx, deletePathRequest); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to delete path: %w\n", err)
	}

	return nil
}

// Check if prefix is present on the global table of gobgpd
func (c *GoBgpClient) IsPathPresent(ctx context.Context, prefix netip.Prefix, nextHop netip.Addr) (bool, error) {
	if ctx.Err() != nil {
		return false, ctx.Err()
	}

	var afi gbapi.Family_Afi
	if prefix.Addr().Is4() {
		afi = gbapi.Family_AFI_IP
	} else {
		afi = gbapi.Family_AFI_IP6
	}

	p := &gbapi.TableLookupPrefix{
		Prefix: prefix.String(),
		Type:   gbapi.TableLookupPrefix_EXACT,
	}

	listPathRequest := &gbapi.ListPathRequest{
		TableType: gbapi.TableType_GLOBAL,
		Family:    &gbapi.Family{Afi: afi, Safi: gbapi.Family_SAFI_UNICAST},
		Prefixes:  []*gbapi.TableLookupPrefix{p},
	}

	client := gbapi.NewGobgpApiClient(c.conn)
	reqCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	stream, err := client.ListPath(reqCtx, listPathRequest)
	if status.Code(err) != codes.OK {
		return false, fmt.Errorf("unable to list paths: %w\n", err)
	}

	for {
		listPathResponse, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			return false, nil
		}
		switch status.Code(err) {
		default:
			return false, fmt.Errorf("unable to receive ListPath results: %w", err)
		case codes.Canceled:
			return false, nil
		case codes.OK:
			if listPathResponse == nil {
				continue
			}
			recvPrefix, err := netip.ParsePrefix(listPathResponse.Destination.Prefix)
			if err != nil {
				c.logger.Warn("unable to parse prefix coming from BGP daemon",
					slog.Any("error", err),
				)
				continue
			}
			// prefix matches
			if recvPrefix == prefix {
				for _, path := range listPathResponse.Destination.Paths {
					for _, attr := range path.Pattrs {
						attrValue, err := attr.UnmarshalNew()
						if err != nil {
							continue
						}
						switch value := attrValue.(type) {
						case *gbapi.NextHopAttribute: // IPv4 paths
							recvNextHop, err := netip.ParseAddr(value.NextHop)
							if err != nil {
								c.logger.Warn("unable to parse IP address coming from BGP daemon",
									slog.Any("error", err),
								)
							}
							// nexthop also matches
							if recvNextHop == nextHop {
								return true, nil
							}
						case *gbapi.MpReachNLRIAttribute: // IPv6 paths
							for _, nh := range value.NextHops {
								recvNextHop, err := netip.ParseAddr(nh)
								if err != nil {
									c.logger.Warn("unable to parse IP address coming from BGP daemon",
										slog.Any("error", err),
									)
									continue
								}
								// nexthop also matches
								if recvNextHop == nextHop {
									return true, nil
								}
							}
						}
					}
				}
			}
		}
	}
}

// Add peer
func (c *GoBgpClient) AddPeer(ctx context.Context, addr netip.Addr) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	peerConf := &gbapi.PeerConf{
		NeighborAddress: addr.String(),
		LocalAsn:        c.localAsn,
	}
	ip4 := &gbapi.AfiSafi{
		Config: &gbapi.AfiSafiConfig{
			Family:  &gbapi.Family{Afi: gbapi.Family_AFI_IP, Safi: gbapi.Family_SAFI_UNICAST},
			Enabled: true,
		},
	}
	ip6 := &gbapi.AfiSafi{
		Config: &gbapi.AfiSafiConfig{
			Family:  &gbapi.Family{Afi: gbapi.Family_AFI_IP6, Safi: gbapi.Family_SAFI_UNICAST},
			Enabled: true,
		},
	}
	gracefulRestart := &gbapi.GracefulRestart{
		Enabled: true,
	}
	peer := &gbapi.Peer{
		Conf:            peerConf,
		AfiSafis:        []*gbapi.AfiSafi{ip4, ip6},
		GracefulRestart: gracefulRestart,
	}
	addPeerRequest := &gbapi.AddPeerRequest{Peer: peer}
	client := gbapi.NewGobgpApiClient(c.conn)

	if _, err := client.AddPeer(ctx, addPeerRequest); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to add peer: %w", err)
	}

	return nil
}

// Delete peer
func (c *GoBgpClient) DeletePeer(ctx context.Context, addr netip.Addr) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	req := &gbapi.DeletePeerRequest{
		Address: addr.String(),
	}
	client := gbapi.NewGobgpApiClient(c.conn)

	if _, err := client.DeletePeer(ctx, req); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to delete peer: %w", err)
	}

	return nil
}

// Perform a softresetout operation against the specified peer
func (c *GoBgpClient) SoftResetPeer(ctx context.Context, addr netip.Addr) error {
	req := &gbapi.ResetPeerRequest{
		Address:   addr.String(),
		Soft:      true,
		Direction: gbapi.ResetPeerRequest_OUT,
	}

	client := gbapi.NewGobgpApiClient(c.conn)
	if _, err := client.ResetPeer(ctx, req); err != nil {
		return fmt.Errorf("unable to perform a soft reset: %w", err)
	}

	return nil
}

// check if peer is present
func (c *GoBgpClient) IsPeerPresent(ctx context.Context, addr netip.Addr) (bool, error) {
	peers, err := c.ListPeers(ctx, addr)
	if err != nil {
		return false, err
	}

	return len(peers) == 1, nil
}

// List Peers
func (c *GoBgpClient) ListPeers(ctx context.Context, addr ...netip.Addr) ([]netip.Addr, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}
	if len(addr) > 1 {
		return nil, errors.New("unsupported")
	}

	req := &gbapi.ListPeerRequest{}
	if len(addr) > 0 {
		req.Address = addr[0].String()
	}

	client := gbapi.NewGobgpApiClient(c.conn)
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	stream, err := client.ListPeer(ctx, req)
	if status.Code(err) != codes.OK {
		return nil, fmt.Errorf("unable to list peers: %w", err)
	}

	var results []netip.Addr
	for {
		peer, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			slices.SortFunc(results, func(a, b netip.Addr) int {
				return strings.Compare(a.String(), b.String())
			})
			return results, nil
		}
		switch status.Code(err) {
		default:
			return nil, fmt.Errorf("unable to receive ListPeer results: %w", err)
		case codes.Canceled:
			return nil, nil
		case codes.OK:
			recvAddr, err := netip.ParseAddr(peer.Peer.Conf.NeighborAddress)
			if err != nil {
				c.logger.Warn("unable to parse peer address coming from BGP daemon",
					slog.Any("error", err),
				)
				continue
			}
			results = append(results, recvAddr)
		}
	}
}

// configuring AS prepend requires the following steps on gobgpd:
// 1. Create a Defined set that will match the routes where we want to perform AS prepending
// We create a set that will match the local ASN used by liberica on the AS_PATH
// 2. Configure a Policy with condition set to match the AS_PATH defined set and action set to AS Prepend
// ~$ gobgp policy -p 45143
// Name AsPrependPolicy:
//
//	StatementName AS Prepend statement:
//	  Conditions:
//	    AsPathSet: any ds0
//	  Actions:
//	     ASPathPrepend:  prepend 64512 3 times
//
// 3. Apply the configured policy to the global RIB
// ~$ gobgp global policy
// Import policy:
//
//	Default: ACCEPT
//
// Export policy:
//
//	Default: ACCEPT
//	Name AsPrependPolicy:
//	    StatementName AS Prepend statement:
//	      Conditions:
//	        AsPathSet: any ds0
//	      Actions:
//	         ASPathPrepend:  prepend 64512 3 times
func (c *GoBgpClient) EnableCommunities(ctx context.Context, communities []string) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	if len(communities) == 0 {
		return ErrInvalidBgpConfig
	}

	ds := &gbapi.DefinedSet{
		DefinedType: gbapi.DefinedType_AS_PATH,
		Name:        CommunitiesDefinedSetName,
		List:        []string{fmt.Sprintf("%d", c.localAsn)},
	}

	dsReq := &gbapi.AddDefinedSetRequest{
		DefinedSet: ds,
	}

	client := gbapi.NewGobgpApiClient(c.conn)
	if _, err := client.AddDefinedSet(ctx, dsReq); err != nil {
		c.logger.Error("unable to create defined set",
			slog.Any("error", err),
		)
		return fmt.Errorf("unable to set AS prepend: %w", err)
	}

	policy := &gbapi.Policy{
		Name: CommunitiesPolicyName,
		Statements: []*gbapi.Statement{
			&gbapi.Statement{
				Name: "Communities statement",
				Conditions: &gbapi.Conditions{
					AsPathSet: &gbapi.MatchSet{
						Name: ds.Name,
						Type: gbapi.MatchSet_ANY,
					},
				},
				Actions: &gbapi.Actions{
					Community: &gbapi.CommunityAction{
						Type:        gbapi.CommunityAction_ADD,
						Communities: communities,
					},
				},
			},
		},
	}

	request := &gbapi.AddPolicyRequest{
		Policy:                  policy,
		ReferExistingStatements: false,
	}

	if _, err := client.AddPolicy(ctx, request); err != nil {
		c.logger.Error("unable to add communities policy",
			slog.Any("error", err),
		)
		return err
	}

	paRequest := &gbapi.SetPolicyAssignmentRequest{
		Assignment: &gbapi.PolicyAssignment{
			Name:          "global",
			Direction:     gbapi.PolicyDirection_EXPORT,
			DefaultAction: gbapi.RouteAction_ACCEPT,
			Policies:      []*gbapi.Policy{policy},
		},
	}

	if _, err := client.SetPolicyAssignment(ctx, paRequest); err != nil {
		c.logger.Error("unable to assign policy",
			slog.Any("error", err),
		)
		return err
	}

	return nil
}

func (c *GoBgpClient) DisableCommunities(ctx context.Context) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	client := gbapi.NewGobgpApiClient(c.conn)

	policy := &gbapi.Policy{
		Name: CommunitiesPolicyName,
	}
	ds := &gbapi.DefinedSet{
		DefinedType: gbapi.DefinedType_AS_PATH,
		Name:        CommunitiesDefinedSetName,
	}

	paRequest := &gbapi.DeletePolicyAssignmentRequest{
		Assignment: &gbapi.PolicyAssignment{
			Name:          "global",
			Direction:     gbapi.PolicyDirection_EXPORT,
			DefaultAction: gbapi.RouteAction_ACCEPT,
			Policies:      []*gbapi.Policy{policy},
		},
	}

	if _, err := client.DeletePolicyAssignment(ctx, paRequest); err != nil {
		// gobgp API always returns status code unknown so we need to check the
		// status message
		s, ok := status.FromError(err)
		if !ok || !strings.HasPrefix(s.Message(), "not found") {
			c.logger.Error("unable to delete policy assignment",
				slog.Any("error", err),
			)
			return fmt.Errorf("unable to delete policy assignment: %w", err)
		}
	}

	policyRequest := &gbapi.DeletePolicyRequest{
		Policy:             policy,
		All:                true,
		PreserveStatements: false,
	}

	if _, err := client.DeletePolicy(ctx, policyRequest); err != nil {
		s, ok := status.FromError(err)
		if !ok || !strings.HasPrefix(s.Message(), "not found") {
			c.logger.Error("unable to delete policy",
				slog.Any("error", err),
			)
			return fmt.Errorf("unable to delete policy: %w", err)
		}
	}

	definedSetRequest := &gbapi.DeleteDefinedSetRequest{
		DefinedSet: ds,
		All:        true,
	}

	if _, err := client.DeleteDefinedSet(ctx, definedSetRequest); err != nil {
		s, ok := status.FromError(err)
		if !ok || !strings.HasPrefix(s.Message(), "not found") {
			c.logger.Error("unable to delete defined set",
				slog.Any("error", err),
			)
			return fmt.Errorf("unable to delete defined set: %w", err)
		}
	}

	return nil
}

// checks if as prepending is being applied. disableAsPrepend() should be
// called prior to enableAsPrepend() to ensure a clean execution of
// enableAsPrepend()
func (c *GoBgpClient) AreCommunitiesEnabled(ctx context.Context, communities []string) (CommunitiesEnabled, error) {
	if ctx.Err() != nil {
		return CommunitiesNotPresent, ctx.Err()
	}

	client := gbapi.NewGobgpApiClient(c.conn)
	reqCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	req := &gbapi.ListPolicyAssignmentRequest{
		Direction: gbapi.PolicyDirection_EXPORT,
	}

	stream, err := client.ListPolicyAssignment(reqCtx, req)
	if status.Code(err) != codes.OK {
		return CommunitiesNotPresent, fmt.Errorf("unable to policy assignments: %w\n", err)
	}

	for {
		assignmentResponse, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			return CommunitiesNotPresent, nil
		}
		switch status.Code(err) {
		default:
			return CommunitiesNotPresent, fmt.Errorf("unable to receive ListPolicy results: %w", err)
		case codes.Canceled:
			return CommunitiesNotPresent, nil
		case codes.OK:
			if assignmentResponse.Assignment.Name != "global" {
				continue
			}
			for _, policy := range assignmentResponse.Assignment.Policies {
				if policy.Name == CommunitiesPolicyName && len(policy.Statements) == 1 {
					if policy.Statements[0].Actions.Community.Type == gbapi.CommunityAction_ADD {
						slices.Sort(policy.Statements[0].Actions.Community.Communities)
						if slices.Equal(policy.Statements[0].Actions.Community.Communities, communities) {
							return CommunitiesMatch, nil
						}
						return CommunitiesNeedUpdate, nil
					}
				}
			}
		}
	}

	return CommunitiesNotPresent, nil
}

func (c *GoBgpClient) Close() {
	if c.conn != nil {
		c.conn.Close()
	}
}
