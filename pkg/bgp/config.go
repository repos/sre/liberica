/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package bgp

import (
	"errors"
	"fmt"
	"net/netip"
	"reflect"
	"slices"
	"strings"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

var ErrInvalidBgpConfig = errors.New("invalid BGP config")

type BgpConfig struct {
	NextHop4 netip.Addr `mapstructure:"next_hop_ipv4"`
	NextHop6 netip.Addr `mapstructure:"next_hop_ipv6"`
	Grpc     struct {
		Network string `mapstructure:"network"`
		Address string `mapstructure:"address"`
	}
	Communities []string     `mapstructure:"communities"`
	Peers       []netip.Addr `mapstructure:"peers"`
	LocalAsn    uint32       `mapstructure:"asn"`
}

// netip.Addr decoder hook
func stringToAddrHookFunc() mapstructure.DecodeHookFunc {
	return func(f, t reflect.Type, data any) (any, error) {
		if f.Kind() != reflect.String {
			return data, nil
		}
		if t != reflect.TypeOf(netip.Addr{}) {
			return data, nil
		}

		addr, err := netip.ParseAddr(data.(string))
		if err != nil {
			fmt.Printf("failed parsing address %s: %v", data.(string), err)
			return nil, fmt.Errorf("failed parsing address %v: %w", data, err)
		}

		return addr, nil
	}
}

// Load BGP config using viper
func LoadBgpConfig() (*BgpConfig, error) {
	cfgMap := viper.GetStringMap("bgp")
	var bgpConfig BgpConfig
	decoderCfg := &mapstructure.DecoderConfig{
		DecodeHook: stringToAddrHookFunc(),
		Result:     &bgpConfig,
	}
	decoder, err := mapstructure.NewDecoder(decoderCfg)
	if err != nil {
		return nil, err
	}
	decoder.Decode(cfgMap)

	if bgpConfig.LocalAsn == 0 {
		return nil, ErrInvalidBgpConfig
	}

	slices.Sort(bgpConfig.Communities)
	slices.SortFunc(bgpConfig.Peers, func(a, b netip.Addr) int {
		return strings.Compare(a.String(), b.String())
	})

	return &bgpConfig, nil
}
