package fp

import (
	"encoding/binary"
	"math/rand"
	"net/netip"
	"testing"
	"time"
)

func TestFPState(t *testing.T) {
	fps := FPState{}
	svc := Service{
		Address:   netip.MustParseAddr("192.0.2.1"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  TCP,
	}

	if err := fps.AddService(svc); err != nil {
		t.Errorf("unable to add %+v to an empty fp state", err)
	}
	if err := fps.AddService(svc); err == nil {
		t.Error("adding a duplicated service should trigger an error")
	}

	rs := Realserver{
		Address:     netip.MustParseAddr("192.168.0.1"),
		Weight:      1,
		ForwardType: Tunnel,
	}

	if err := fps.AddRealserver(svc, rs); err != nil {
		t.Errorf("unable to add realserver %+v to service %+v", rs, svc)
	}
	if err := fps.AddRealserver(svc, rs); err != nil {
		t.Error("adding a duplicated realserver shouldn't trigger an error")
	}

	if err := fps.AddRealserver(Service{}, Realserver{}); err == nil {
		t.Error("adding a realserver to an unexistent service should fail")
	}
}

func fetchFPStateEvents(old, current FPState) (events []FPEvent) {
	ch := make(chan FPEvent, 1024)
	diffFPState(old, current, ch)
	close(ch)
	for event := range ch {
		events = append(events, event)
	}
	return events
}

func validateFPEvents(t *testing.T, want, got []FPEvent) {
	// drop when golang 1.21 is available in favor of slices.Contains()
	contains := func(s []FPEvent, fpe FPEvent) bool {
		for _, v := range s {
			if v == fpe {
				return true
			}
		}
		return false
	}
	if len(want) != len(got) {
		t.Errorf("Unexpected number of events received. Expected %v, got %v", len(want), len(got))
	}
	for i, _ := range got {
		if !contains(want, got[i]) {
			t.Errorf("unexpected event %+v", got[i])
		}
	}
}

func generateRandomIPv4() netip.Addr {
	rand.Seed(time.Now().UnixNano())
	ipBytes := make([]byte, 4)
	value := rand.Uint32()
	binary.LittleEndian.PutUint32(ipBytes, value)
	arrayBytes := (*[4]byte)(ipBytes)
	return netip.AddrFrom4(*arrayBytes)
}

func generateService() Service {
	svc := Service{
		Address:   generateRandomIPv4(),
		Port:      443,
		Scheduler: "rr",
		Family:    INET,
		Protocol:  TCP,
	}
	return svc
}

func generateRealserver() Realserver {
	return Realserver{
		Address:     generateRandomIPv4(),
		Weight:      1,
		ForwardType: Tunnel,
	}
}

func TestEmptyDiffFpState(t *testing.T) {
	old := FPState{}
	current := FPState{}
	events := fetchFPStateEvents(old, current)
	validateFPEvents(t, []FPEvent{}, events)
}

func TestDiffFPState(t *testing.T) {
	old := FPState{}
	current := FPState{}

	svc := generateService()
	rs := generateRealserver()

	current.AddService(svc)
	current.AddRealserver(svc, rs)

	// new service
	t.Run("New service", func(t *testing.T) {
		want := []FPEvent{
			{svc, Realserver{}, NewService},
			{svc, rs, NewRealserver},
		}
		got := fetchFPStateEvents(old, current)
		validateFPEvents(t, want, got)
	})
	old = current.Clone()
	current.RemoveRealserver(svc, rs)
	// remove a realserver
	t.Run("Remove a single realserver", func(t *testing.T) {
		want := []FPEvent{
			{svc, rs, RemoveRealserver},
		}
		got := fetchFPStateEvents(old, current)
		validateFPEvents(t, want, got)
	})
	old = current.Clone()
	current.RemoveService(svc)
	// remove an empty service
	t.Run("Remove an empty service", func(t *testing.T) {
		want := []FPEvent{
			{svc, Realserver{}, RemoveService},
		}
		got := fetchFPStateEvents(old, current)
		validateFPEvents(t, want, got)
	})
	current.AddService(svc)
	current.AddRealserver(svc, rs)
	old = current.Clone()
	updatedRs := rs
	updatedRs.Weight = 100
	current.AddRealserver(svc, updatedRs)
	t.Run("Update an existing realserver", func(t *testing.T) {
		// update a realserver
		want := []FPEvent{
			{svc, updatedRs, UpdateRealserver},
		}
		got := fetchFPStateEvents(old, current)
		validateFPEvents(t, want, got)
	})
	// add a realserver to an existing service
	old = current.Clone()
	newRs := generateRealserver()
	current.AddRealserver(svc, newRs)
	t.Run("Add a realserver to an existing service", func(t *testing.T) {
		want := []FPEvent{
			{svc, newRs, NewRealserver},
		}
		got := fetchFPStateEvents(old, current)
		validateFPEvents(t, want, got)
	})
	// remove a service with realservers
	old = current.Clone()
	current.RemoveService(svc)
	t.Run("Remove a service with realservers", func(t *testing.T) {
		want := []FPEvent{
			{svc, Realserver{}, RemoveService},
			{svc, updatedRs, RemoveRealserver},
			{svc, newRs, RemoveRealserver},
		}
		got := fetchFPStateEvents(old, current)
		validateFPEvents(t, want, got)
	})
}
