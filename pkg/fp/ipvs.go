/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package fp

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"sync"

	"github.com/cloudflare/ipvs"
	"github.com/cloudflare/ipvs/netmask"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

type destinationAction uint16

const (
	addRealserver destinationAction = iota
	updateRealserver
	removeRealserver
)

type IPVSForwardingPlane struct {
	client ipvs.Client
	logger *slog.Logger
	m      sync.Mutex
}

func NewIPVSForwardingPlane(logger *slog.Logger) (ForwardingPlane, error) {
	client, err := ipvs.New()
	if err != nil {
		return nil, fmt.Errorf("unable to get IPVS client: %w", err)
	}

	return &IPVSForwardingPlane{
		client: client,
		logger: logger,
	}, nil
}

var ErrProcotolNotSupported = errors.New("Protocol not supported by IPVS Forwarding Plane")
var ErrForwardTypeNotSupported = errors.New("ForwardType not supported by IPVS Forwarding Plane")

// Helper function to translate Service to ipvs.Service
func getIpvsService(svc Service) (*ipvs.Service, error) {
	if svc.Protocol == QUIC {
		return nil, ErrProcotolNotSupported
	}

	ipvsSvc := ipvs.Service{
		Address:   svc.Address,
		Scheduler: svc.Scheduler,
		Port:      svc.Port,
	}

	switch svc.Family {
	case INET:
		ipvsSvc.Family = ipvs.INET
		ipvsSvc.Netmask = netmask.MaskFrom4([...]byte{255, 255, 255, 255})
	case INET6:
		ipvsSvc.Family = ipvs.INET6
		ipvsSvc.Netmask = netmask.MaskFrom16([...]byte{
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		})
	}

	switch svc.Protocol {
	case TCP:
		ipvsSvc.Protocol = ipvs.TCP
	case UDP:
		ipvsSvc.Protocol = ipvs.UDP
	default:
		return nil, ErrProcotolNotSupported
	}

	return &ipvsSvc, nil
}

// Helper function to translate Realserver to ipvs.Destination
func getIpvsDestination(svc Service, rs Realserver) (*ipvs.Destination, error) {
	var ft ipvs.ForwardType
	switch rs.ForwardType {
	case DirectRoute:
		ft = ipvs.DirectRoute
	case Tunnel:
		ft = ipvs.Tunnel
	default:
		return nil, ErrForwardTypeNotSupported
	}

	return &ipvs.Destination{
		Address:   rs.Address,
		Weight:    rs.Weight,
		FwdMethod: ft,
		Port:      svc.Port,
	}, nil
}

func (ifp *IPVSForwardingPlane) AddService(ctx context.Context, svc Service) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}
	ipvsSvc, err := getIpvsService(svc)
	if err != nil {
		return err
	}

	c := make(chan struct{}, 1)
	ifp.m.Lock()
	defer ifp.m.Unlock()
	go func() {
		defer close(c)
		err = ifp.client.CreateService(*ipvsSvc)
		metrics.IncResultCounter(ifp.logger, metrics.IpvsFpWriteOps, err)
	}()

	select {
	case <-c:
		return err
	case <-ctx.Done():
		return ctx.Err()
	}
}

func (ifp *IPVSForwardingPlane) Services(ctx context.Context) ([]Service, error) {
	c := make(chan struct{}, 1)
	var svcs []ipvs.ServiceExtended
	var err error

	if ctx.Err() != nil {
		return nil, ctx.Err()
	}
	ifp.m.Lock()
	defer ifp.m.Unlock()
	go func() {
		defer close(c)
		svcs, err = ifp.client.Services()
		metrics.IncResultCounter(ifp.logger, metrics.IpvsFpReadOps, err)
	}()
	select {
	case <-c:
	case <-ctx.Done():
		return nil, ctx.Err()
	}
	if err != nil && err != os.ErrNotExist {
		return nil, err
	}

	var services []Service
	for _, svc := range svcs {
		var proto Protocol

		switch svc.Protocol {
		case ipvs.TCP:
			proto = TCP
		case ipvs.UDP:
			proto = UDP
		}

		var af AddressFamily
		switch svc.Family {
		case ipvs.INET:
			af = INET
		case ipvs.INET6:
			af = INET6
		}

		service := Service{
			Address:   svc.Address,
			Scheduler: svc.Scheduler,
			Port:      svc.Port,
			Protocol:  proto,
			Family:    af,
		}
		services = append(services, service)
	}
	return services, nil
}

func (ifp *IPVSForwardingPlane) RemoveService(ctx context.Context, svc Service) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	ipvsSvc, err := getIpvsService(svc)
	if err != nil {
		return err
	}

	c := make(chan struct{}, 1)
	ifp.m.Lock()
	defer ifp.m.Unlock()
	go func() {
		defer close(c)
		err = ifp.client.RemoveService(*ipvsSvc)
		metrics.IncResultCounter(ifp.logger, metrics.IpvsFpWriteOps, err)
	}()

	select {
	case <-c:
		return err
	case <-ctx.Done():
		return ctx.Err()
	}
}

func (ifp *IPVSForwardingPlane) Realservers(ctx context.Context, svc Service) ([]Realserver, error) {
	var af ipvs.AddressFamily
	switch svc.Family {
	case INET:
		af = ipvs.INET
	case INET6:
		af = ipvs.INET6
	}

	var proto ipvs.Protocol
	switch svc.Protocol {
	case TCP:
		proto = ipvs.TCP
	case UDP:
		proto = ipvs.UDP
	}

	ipvsSvc := ipvs.Service{
		Address:  svc.Address,
		Port:     svc.Port,
		Family:   af,
		Protocol: proto,
	}

	c := make(chan struct{}, 1)
	var dsts []ipvs.DestinationExtended
	var err error

	go func() {
		defer close(c)
		dsts, err = ifp.client.Destinations(ipvsSvc)
		metrics.IncResultCounter(ifp.logger, metrics.IpvsFpReadOps, err)
	}()
	select {
	case <-c:
	case <-ctx.Done():
		return nil, ctx.Err()
	}

	if err != nil && err != os.ErrNotExist {
		return nil, err
	}

	var realservers []Realserver
	for _, dst := range dsts {
		var ft ForwardType
		switch dst.FwdMethod {
		case ipvs.DirectRoute:
			ft = DirectRoute
		case ipvs.Tunnel:
			ft = Tunnel
		}

		rs := Realserver{
			Address:     dst.Address,
			Weight:      dst.Weight,
			Mark:        0, // not implemented by IPVS
			ForwardType: ft,
		}
		realservers = append(realservers, rs)
	}
	return realservers, nil
}

func (ifp *IPVSForwardingPlane) AddRealserver(ctx context.Context, svc Service, rs Realserver) error {
	return ifp.runDestinationAction(ctx, addRealserver, svc, rs)
}

func (ifp *IPVSForwardingPlane) UpdateRealserver(ctx context.Context, svc Service, rs Realserver) error {
	return ifp.runDestinationAction(ctx, updateRealserver, svc, rs)
}

func (ifp *IPVSForwardingPlane) RemoveRealserver(ctx context.Context, svc Service, rs Realserver) error {
	return ifp.runDestinationAction(ctx, removeRealserver, svc, rs)
}

func (ifp *IPVSForwardingPlane) runDestinationAction(ctx context.Context, action destinationAction, svc Service, rs Realserver) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	ipvsSvc, err := getIpvsService(svc)
	if err != nil {
		return err
	}

	ipvsDst, err := getIpvsDestination(svc, rs)
	if err != nil {
		return err
	}
	c := make(chan struct{}, 1)
	ifp.m.Lock()
	defer ifp.m.Unlock()
	go func() {
		defer close(c)
		switch action {
		case addRealserver:
			err = ifp.client.CreateDestination(*ipvsSvc, *ipvsDst)
		case removeRealserver:
			err = ifp.client.RemoveDestination(*ipvsSvc, *ipvsDst)
		case updateRealserver:
			err = ifp.client.UpdateDestination(*ipvsSvc, *ipvsDst)
		}
		metrics.IncResultCounter(ifp.logger, metrics.IpvsFpWriteOps, err)
	}()

	select {
	case <-c:
		return err
	case <-ctx.Done():
		return ctx.Err()
	}
}

// IPVS doesn't require any explicit action to preserve the state on process exit
func (ifp *IPVSForwardingPlane) Pin() error {
	return nil
}

func (ifp *IPVSForwardingPlane) Unpin() error {
	ifp.m.Lock()
	defer ifp.m.Unlock()

	svcs, err := ifp.client.Services()
	if err != nil {
		ifp.logger.Error("unable to fetch list of services", slog.Any("error", err))
		return err
	}
	for _, svc := range svcs {
		if err := ifp.client.RemoveService(svc.Service); err != nil {
			ifp.logger.Error("unable to remove service",
				slog.Any("service", svc),
				slog.Any("error", err),
			)
			continue
		}
	}

	return nil
}
