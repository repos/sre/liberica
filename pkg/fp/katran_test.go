package fp

import "testing"

func TestmacToUint64LE(t *testing.T) {
	// sample value obtained using original KatranLB implementation
	want := uint64(3335840901764)
	if got := macToUint64LE([6]byte{0x84, 0xaa, 0x9c, 0xaf, 0x08, 0x03}); got != want {
		t.Fatalf("unexpected value, want %v, got %v", want, got)
	}
}
