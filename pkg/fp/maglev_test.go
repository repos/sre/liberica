package fp

import "testing"

func TestGenerateHashRing(t *testing.T) {
	endpoints := []maglevReal{
		maglevReal{
			num:    2,
			weight: 1,
			hash:   42424242,
		},
		maglevReal{
			num:    3,
			weight: 1,
			hash:   12345,
		},
	}

	ring := generateHashRing(endpoints, 65537)
	m := make(map[int]uint32, 3)
	for _, value := range ring {
		m[value]++
	}

	for realNum, weight := range m {
		w := float64(float64(weight) / float64(65537))
		w = w * 100
		t.Logf("%v would get  %v%% of the traffic", realNum, w)
	}

}
