/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package fp

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"maps"
	"net/netip"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"
	pbsvc "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/service"
)

type AddressFamily uint16

const (
	INET AddressFamily = iota
	INET6
)

var (
	ErrInvalidAF       = errors.New("invalid AddressFamily")
	ErrInvalidProtocol = errors.New("invalid protocol")
	ErrInvalidFT       = errors.New("invalid ForwardType")
)

func (af AddressFamily) AsProto() pbsvc.AddressFamily {
	switch af {
	case INET:
		return pbsvc.AddressFamily_AF_INET
	case INET6:
		return pbsvc.AddressFamily_AF_INET6
	default:
		return pbsvc.AddressFamily_AF_UNSPECIFIED
	}
}

func NewAddressFamily(af pbsvc.AddressFamily) (AddressFamily, error) {
	switch af {
	case pbsvc.AddressFamily_AF_INET:
		return INET, nil
	case pbsvc.AddressFamily_AF_INET6:
		return INET6, nil
	default:
		return 0, ErrInvalidAF
	}
}

type Protocol uint16

const (
	TCP Protocol = iota
	UDP
	QUIC
)

func (pt Protocol) AsProto() pbsvc.Protocol {
	switch pt {
	case TCP:
		return pbsvc.Protocol_PROTOCOL_TCP
	case UDP:
		return pbsvc.Protocol_PROTOCOL_UDP
	case QUIC:
		return pbsvc.Protocol_PROTOCOL_QUIC
	default:
		return pbsvc.Protocol_PROTOCOL_UNSPECIFIED
	}
}

func NewProtocol(proto pbsvc.Protocol) (Protocol, error) {
	switch proto {
	case pbsvc.Protocol_PROTOCOL_TCP:
		return TCP, nil
	case pbsvc.Protocol_PROTOCOL_UDP:
		return UDP, nil
	case pbsvc.Protocol_PROTOCOL_QUIC:
		return QUIC, nil
	default:
		return 0, ErrInvalidProtocol
	}
}

type ForwardType uint16

const (
	DirectRoute ForwardType = iota
	Tunnel
)

func (ft ForwardType) AsProto() pbrs.ForwardType {
	switch ft {
	case DirectRoute:
		return pbrs.ForwardType_FT_DIRECT_ROUTE
	case Tunnel:
		return pbrs.ForwardType_FT_TUNNEL
	default:
		return pbrs.ForwardType_FT_UNSPECIFIED
	}
}

func NewForwardType(ft pbrs.ForwardType) (ForwardType, error) {
	switch ft {
	case pbrs.ForwardType_FT_DIRECT_ROUTE:
		return DirectRoute, nil
	case pbrs.ForwardType_FT_TUNNEL:
		return Tunnel, nil
	default:
		return 0, ErrInvalidFT
	}
}

type Service struct {
	Address   netip.Addr
	Scheduler string
	Port      uint16
	Family    AddressFamily
	Protocol  Protocol
}

type Realserver struct {
	Address     netip.Addr
	Weight      uint32
	Mark        uint32
	ForwardType ForwardType
}

type ForwardingPlane interface {
	AddService(context.Context, Service) error
	Services(context.Context) ([]Service, error)
	RemoveService(context.Context, Service) error
	AddRealserver(context.Context, Service, Realserver) error
	Realservers(context.Context, Service) ([]Realserver, error)
	UpdateRealserver(context.Context, Service, Realserver) error
	RemoveRealserver(context.Context, Service, Realserver) error
	Pin() error
	Unpin() error
}

type FPAction uint16

const (
	NewService    FPAction = iota
	UpdateService          // not used at the moment
	RemoveService
	NewRealserver
	UpdateRealserver
	RemoveRealserver
)

func (fpa FPAction) String() string {
	switch fpa {
	case NewService:
		return "New service"
	case UpdateService:
		return "Update service"
	case RemoveService:
		return "Remove service"
	case NewRealserver:
		return "New realserver"
	case UpdateRealserver:
		return "Update realserver"
	case RemoveRealserver:
		return "Remove realserver"
	default:
		return "Unknown"
	}
}

type FPEvent struct {
	Service    Service
	Realserver Realserver // will be nil for non realserver actions
	// TODO: Provide old and new version of Service/RS?
	Action FPAction
}

type FPMonitor struct {
	Fp     ForwardingPlane
	logger *slog.Logger
}

func NewFPMonitor(logger *slog.Logger, fp ForwardingPlane) *FPMonitor {
	return &FPMonitor{
		Fp:     fp,
		logger: logger,
	}
}

// Monitor a forwarding plane for changes on configured Services
// or Realservers
// Monitor will not block sending to c
func (fpm *FPMonitor) Monitor(ctx context.Context, c chan<- FPEvent) {
	if ctx.Err() != nil {
		return
	}

	fpCtx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	current, err := fpm.getSnapshot(fpCtx)
	cancel()
	metrics.IncResultCounter(fpm.logger, metrics.FpGetSnapshotOps, err)
	if err != nil {
		fpm.logger.Error("unable to get base forwarding plane snapshot", slog.Any("error", err))
		return
	}

	t := time.NewTicker(500 * time.Millisecond)
	defer t.Stop()
	old := current.Clone()
	for {
		select {
		case <-t.C:
			fpCtx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
			current, err := fpm.getSnapshot(fpCtx)
			cancel()
			metrics.IncResultCounter(fpm.logger, metrics.FpGetSnapshotOps, err)
			if err != nil {
				fpm.logger.Error("unable to get forwarding plane snapshot", slog.Any("error", err))
				return
			}
			diffFPState(old, current, c)
			old = current.Clone()
		case <-ctx.Done():
			return
		}
	}
}

// used by FPMonitor.Monitor() to get a single snapshot (FPState instance)
func (fpm *FPMonitor) getSnapshot(ctx context.Context) (FPState, error) {
	services, err := fpm.Fp.Services(ctx)
	if err != nil {
		return nil, err
	}
	snapshot := FPState{}
	for _, svc := range services {
		if ctx.Err() != nil {
			return nil, ctx.Err()
		}

		snapshot.AddService(svc)
		realservers, err := fpm.Fp.Realservers(ctx, svc)
		if err != nil {
			if err == ctx.Err() {
				return nil, err
			}
			// service could have been removed between queries to the FP
			continue
		}
		for _, rs := range realservers {
			snapshot.AddRealserver(svc, rs)
		}
	}
	return snapshot, nil
}

type FPState map[Service]map[netip.Addr]Realserver

func (fps FPState) AddService(svc Service) error {
	if _, present := fps[svc]; present {
		return fmt.Errorf("%v already exists in FP state", svc)
	}
	fps[svc] = make(map[netip.Addr]Realserver)
	return nil
}

func (fps FPState) RemoveService(svc Service) {
	delete(fps, svc)
}

func (fps FPState) AddRealserver(svc Service, rs Realserver) error {
	rsKey, present := fps[svc]
	if !present || rsKey == nil {
		return fmt.Errorf("service %v doesn't exist", svc)
	}
	fps[svc][rs.Address] = rs
	return nil
}

func (fps FPState) RemoveRealserver(svc Service, rs Realserver) {
	rsKey, present := fps[svc]
	if !present || rsKey == nil {
		return
	}
	delete(fps[svc], rs.Address)
}

func (fps FPState) Clone() FPState {
	clone := FPState{}
	for svc, rsMap := range fps {
		clone[svc] = maps.Clone(rsMap)
	}
	return clone
}

// diffFPState use the events channel to represent the diff
// between old and current FPState
// diffFPState won't block if the channel isn't consumed fast enough
func diffFPState(old, current FPState, events chan<- FPEvent) {
	for svc, _ := range old {
		if _, present := current[svc]; !present { // service removed
			select {
			case events <- FPEvent{
				Action:  RemoveService,
				Service: svc,
			}:
			default:
			}
			// as a corollary all realservers have been removed as well
			for _, rs := range old[svc] {
				select {
				case events <- FPEvent{
					Action:     RemoveRealserver,
					Service:    svc,
					Realserver: rs,
				}:
				default:
				}
			}

		} else { // service is still there
			for _, rs := range old[svc] {
				if _, present := current[svc][rs.Address]; !present { // Realserver isn't there anymore
					select {
					case events <- FPEvent{
						Action:     RemoveRealserver,
						Service:    svc,
						Realserver: rs,
					}:
					default:
					}
				} else { // Realserver could have been updated
					if rs != current[svc][rs.Address] {
						select {
						case events <- FPEvent{
							Action:     UpdateRealserver,
							Service:    svc,
							Realserver: current[svc][rs.Address],
						}:
						default:
						}
					}
				}

			}
			for _, rs := range current[svc] {
				if _, present := old[svc][rs.Address]; !present { // new Realserver
					select {
					case events <- FPEvent{
						Action:     NewRealserver,
						Service:    svc,
						Realserver: rs,
					}:
					default:
					}
				}
			}
		}
	}

	for svc := range current {
		if _, present := old[svc]; !present { // new service
			select {
			case events <- FPEvent{
				Action:  NewService,
				Service: svc,
			}:
			default:
			}
			for _, rs := range current[svc] { // as a corollary every realserver must be a new one
				select {
				case events <- FPEvent{
					Action:     NewRealserver,
					Service:    svc,
					Realserver: rs,
				}:
				default:
				}
			}
		}
	}
}
