//go:build linux
// +build linux

/*
Copyright 2023 Wikimedia Foundation Inc.
Copyright 2018 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fp

import (
	"context"
	"io"
	"log"
	"log/slog"
	"maps"
	"net"
	"net/netip"
	"os"
	"os/exec"
	"path/filepath"
	"slices"
	"syscall"
	"testing"
	"time"

	"gitlab.wikimedia.org/repos/sre/go-qemutest/pkg/qemutest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bpftest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/testutils"
	"golang.org/x/sys/unix"
)

const (
	NETWORK_INTERFACE = "lo"
	BPF_PROGRAM_NAME  = "balancer_ingress"
)

// binaries that will be shipped on initrd
var initrdBinaries = []string{"/usr/sbin/bpftool", "/sbin/ipvsadm"}

type QemuTest struct{}

func TestMain(m *testing.M) {
	if qemutest.InQemu() {
		// pinning requires /sys/fs/bpf
		if err := unix.Mount("bpf", "/sys/fs/bpf", "bpf", 0, ""); err != nil {
			log.Fatalf("failed to mount /sys/fs/bpf: %v", err)
		}
	}
	os.Exit(qemutest.QemuTestMain(m))
}

func TestQemu(t *testing.T) {
	if qemutest.InQemu() {
		qemutest.RunQemuTests(t, QemuTest{})
		return
	}
	tdPath, err := testutils.GetTestdata()
	if err != nil {
		t.Fatalf("unable to find testdata path: %v", err)
	}
	testCases := []qemutest.QemuTestCase{
		{
			KernelImage: filepath.Join(tdPath, "bzImage.6.1"),
			TestId:      "6.1.69-bookworm",
			Verbose:     true,
		},
	}

	qemutest.RunQemu(t, testCases, initrdBinaries)
}

func setSocketMark(fd, mark int) error {
	if err := syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_MARK, mark); err != nil {
		return os.NewSyscallError("failed to set mark", err)
	}
	return nil
}

func tcpClient(address, port string, mark int) error {
	d := net.Dialer{Timeout: 10 * time.Millisecond,
		Control: func(network, address string, rawc syscall.RawConn) error {
			var fdErr error
			ctl := func(fd uintptr) {
				fdErr = setSocketMark(int(fd), mark)
			}
			if err := rawc.Control(ctl); err != nil {
				return err
			}
			return fdErr
		},
	}
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	conn, err := d.DialContext(ctx, "tcp", address+":"+port)
	if err != nil {
		return err
	}
	conn.Close()
	return nil
}

func mustRun(t *testing.T, command string, args ...string) {
	cmd := exec.Command(command, args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		t.Fatalf("Error running %v: %v, %s", command, err, out)
	}
}

func getEvent(t *testing.T, ch <-chan FPEvent) FPEvent {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	select {
	case event := <-ch:
		return event
	case <-ctx.Done():
		t.Error("timeout waiting an event")
		return FPEvent{}
	}
}

func (QemuTest) TestFPMonitor(t *testing.T) {
	// start from a clean slate
	mustRun(t, "/sbin/ipvsadm", "-C")
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	fp, err := NewIPVSForwardingPlane(logger)
	if err != nil {
		t.Fatalf("unable to get an IPVS forwarding plane instance: %v", err)
		return
	}
	fpm := FPMonitor{Fp: fp}
	ch := make(chan FPEvent, 1024)
	defer close(ch)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go fpm.Monitor(ctx, ch)

	svc := Service{
		Address:   netip.MustParseAddr("127.0.0.1"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  TCP,
	}
	rs := Realserver{
		Address:     netip.MustParseAddr("127.0.0.2"),
		Weight:      10,
		ForwardType: DirectRoute,
	}
	rsTunnel := Realserver{
		Address:     netip.MustParseAddr("127.0.0.3"),
		Weight:      10,
		ForwardType: Tunnel,
	}

	// create a new VIP
	t.Run("New service", func(t *testing.T) {
		mustRun(t, "/sbin/ipvsadm", "-A", "-t", "127.0.0.1:443", "-s", "rr")
		want := FPEvent{
			Action:  NewService,
			Service: svc,
		}
		got := getEvent(t, ch)
		if want != got {
			t.Errorf("Expected: %+v, got: %+v", want, got)
		}
	})

	// Add a realserver to the VIP
	t.Run("Add a realserver", func(t *testing.T) {
		mustRun(t, "/sbin/ipvsadm", "-a", "-t", "127.0.0.1:443", "-r", "127.0.0.2:443", "-w", "10")
		want := FPEvent{
			Action:     NewRealserver,
			Service:    svc,
			Realserver: rs,
		}
		got := getEvent(t, ch)
		if want != got {
			t.Errorf("Expected: %+v, got: %+v", want, got)
		}
	})

	// Update rs weight
	t.Run("Update realserver weight", func(t *testing.T) {
		rs.Weight = 20
		mustRun(t, "/sbin/ipvsadm", "-e", "-t", "127.0.0.1:443", "-r", "127.0.0.2:443", "-w", "20")
		want := FPEvent{
			Action:     UpdateRealserver,
			Service:    svc,
			Realserver: rs,
		}
		got := getEvent(t, ch)
		if want != got {
			t.Errorf("Expected %+v, got: %+v", want, got)
		}
	})

	// Add a realserver with IPIP encapsulation
	t.Run("Add a realserver with IPIP encapsulation", func(t *testing.T) {
		mustRun(t, "/sbin/ipvsadm", "-a", "-t", "127.0.0.1:443", "-r", "127.0.0.3:443", "-w", "10", "-i")
		want := FPEvent{
			Action:     NewRealserver,
			Service:    svc,
			Realserver: rsTunnel,
		}
		got := getEvent(t, ch)
		if want != got {
			t.Errorf("Expected %+v, got: %+v", want, got)
		}
	})

	// Remove realserver
	t.Run("Remove realserver", func(t *testing.T) {
		mustRun(t, "/sbin/ipvsadm", "-d", "-t", "127.0.0.1:443", "-r", "127.0.0.2:443")
		want := FPEvent{
			Action:     RemoveRealserver,
			Service:    svc,
			Realserver: rs,
		}
		got := getEvent(t, ch)
		if want != got {
			t.Errorf("Expected %+v, got: %+v", want, got)
		}
	})
	// Remove service
	t.Run("Remove service", func(t *testing.T) {
		mustRun(t, "/sbin/ipvsadm", "-D", "-t", "127.0.0.1:443")
		want := FPEvent{
			Action:  RemoveService,
			Service: svc,
		}
		got := getEvent(t, ch)
		if want != got {
			t.Errorf("Expected %+v, got: %+v", want, got)
		}
		want = FPEvent{
			Action:     RemoveRealserver,
			Service:    svc,
			Realserver: rsTunnel,
		}
		got = getEvent(t, ch)
		if want != got {
			t.Errorf("Expected %+v, got: %+v", want, got)
		}
	})
	if len(ch) > 0 {
		t.Errorf("%d unexpected events", len(ch))
	}
}

func (QemuTest) TestIPVSAddService(t *testing.T) {
	// start from a clean slate
	mustRun(t, "/sbin/ipvsadm", "-C")
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	fp, err := NewIPVSForwardingPlane(logger)
	if err != nil {
		t.Fatalf("unable to get an IPVS forwarding plane instance: %v", err)
		return
	}

	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 0 {
		t.Fatalf("unexpected state, wanted 0 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	svc := Service{
		Address:   netip.MustParseAddr("127.0.0.1"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  TCP,
	}

	if err := fp.AddService(context.Background(), svc); err != nil {
		t.Fatalf("unable to create service: %v", err)
		return
	}
	svcs, err := fp.Services(context.Background())
	if err != nil {
		t.Fatalf("unable to fetch IPVS services: %v", err)
		return
	}
	if len(svcs) != 1 {
		t.Errorf("unexpected number of services, got %v, wanted 1", len(svcs))
	}
	if svcs[0].Address != svc.Address {
		t.Errorf("unexpected address, got %v, wanted: %v", svcs[0].Address, svc.Address)
	}
	if svcs[0].Scheduler != svc.Scheduler {
		t.Errorf("unexpected scheduler, got %v, wanted: %v", svcs[0].Scheduler, svc.Scheduler)
	}
	if svcs[0].Port != svc.Port {
		t.Errorf("unexpected port, got %v, wanted: %v", svcs[0].Port, svc.Port)
	}
	if svcs[0].Family != svc.Family {
		t.Errorf("unexpected family, got %v, wanted: %v", svcs[0].Family, svc.Family)
	}
	if svcs[0].Protocol != svc.Protocol {
		t.Errorf("unexpected protocol, got %v, wanted: %v", svcs[0].Protocol, svc.Protocol)
	}

	quicSvc := Service{
		Address:   netip.MustParseAddr("127.0.0.2"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  QUIC,
	}
	if err := fp.AddService(context.Background(), quicSvc); err != ErrProcotolNotSupported {
		t.Errorf("unexpected return value while adding a QUIC protocol service: %v", err)
	}
	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 1 {
		t.Fatalf("unexpected state, wanted 1 services and no errors, got: %v, %v", err, len(svcs))
		return
	}
}

func (QemuTest) TestIPVSRemoveService(t *testing.T) {
	// start from a clean slate
	mustRun(t, "/sbin/ipvsadm", "-C")
	// Add a new service
	mustRun(t, "/sbin/ipvsadm", "-A", "-t", "127.0.0.1:443", "-s", "rr")
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	fp, err := NewIPVSForwardingPlane(logger)
	if err != nil {
		t.Fatalf("unable to get an IPVS forwarding plane instance: %v", err)
		return
	}

	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 1 {
		t.Fatalf("unexpected state, wanted 1 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	svc := Service{
		Address:   netip.MustParseAddr("127.0.0.1"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  TCP,
	}

	if err := fp.RemoveService(context.Background(), svc); err != nil {
		t.Fatalf("unexpected error while removing service: %v", err)
		return
	}

	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 0 {
		t.Fatalf("unexpected state, wanted 0 services and no errors, got: %v, %v", err, len(svcs))
		return
	}
}

func (QemuTest) TestIPVSAddRealserver(t *testing.T) {
	// start from a clean slate
	mustRun(t, "/sbin/ipvsadm", "-C")
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	fp, err := NewIPVSForwardingPlane(logger)
	if err != nil {
		t.Fatalf("unable to get an IPVS forwarding plane instance: %v", err)
		return
	}

	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 0 {
		t.Fatalf("unexpected state, wanted 0 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	svc := Service{
		Address:   netip.MustParseAddr("127.0.0.1"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  TCP,
	}

	if err := fp.AddService(context.Background(), svc); err != nil {
		t.Fatalf("unable to create service: %v", err)
		return
	}
	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 1 {
		t.Fatalf("unexpected state, wanted 1 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	rs := Realserver{
		Address:     netip.MustParseAddr("127.0.0.2"),
		ForwardType: Tunnel,
		Weight:      100,
	}

	if err := fp.AddRealserver(context.Background(), svc, rs); err != nil {
		t.Fatalf("unexpected error while adding realserver: %v", err)
		return
	}

	realservers, err := fp.Realservers(context.Background(), svc)
	if err != nil {
		t.Fatalf("unexpected error fetching existing services: %v", err)
		return
	}
	if len(realservers) != 1 {
		t.Fatalf("unexpected number of realservers, got %v, wanted 1", len(realservers))
		return
	}

	if realservers[0].Address != rs.Address {
		t.Errorf("unexpected address, got %v, wanted %v", realservers[0].Address, rs.Address)
	}
}

func (QemuTest) TestIPVSUpdateRealserver(t *testing.T) {
	// start from a clean slate
	mustRun(t, "/sbin/ipvsadm", "-C")
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	fp, err := NewIPVSForwardingPlane(logger)
	if err != nil {
		t.Fatalf("unable to get an IPVS forwarding plane instance: %v", err)
		return
	}

	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 0 {
		t.Fatalf("unexpected state, wanted 0 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	svc := Service{
		Address:   netip.MustParseAddr("127.0.0.1"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  TCP,
	}

	if err := fp.AddService(context.Background(), svc); err != nil {
		t.Fatalf("unable to create service: %v", err)
		return
	}
	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 1 {
		t.Fatalf("unexpected state, wanted 1 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	rs := Realserver{
		Address:     netip.MustParseAddr("127.0.0.2"),
		ForwardType: Tunnel,
		Weight:      100,
	}

	if err := fp.AddRealserver(context.Background(), svc, rs); err != nil {
		t.Fatalf("unexpected error while adding realserver: %v", err)
		return
	}

	realservers, err := fp.Realservers(context.Background(), svc)
	if err != nil {
		t.Fatalf("unexpected error fetching existing services: %v", err)
		return
	}
	if len(realservers) != 1 {
		t.Fatalf("unexpected number of realservers, got %v, wanted 1", len(realservers))
		return
	}

	if realservers[0].Weight != rs.Weight {
		t.Errorf("unexpected weight, got %v, wanted %v", realservers[0].Weight, rs.Weight)
	}

	rs.Weight = 200
	if err := fp.UpdateRealserver(context.Background(), svc, rs); err != nil {
		t.Fatalf("unexpected error updating realserver: %v", err)
		return
	}

	realservers, err = fp.Realservers(context.Background(), svc)
	if err != nil {
		t.Fatalf("unexpected error fetching existing services: %v", err)
		return
	}
	if len(realservers) != 1 {
		t.Fatalf("unexpected number of realservers, got %v, wanted 1", len(realservers))
		return
	}

	if realservers[0].Weight != rs.Weight {
		t.Errorf("unexpected weight, got %v, wanted %v", realservers[0].Weight, rs.Weight)
	}

}

func (QemuTest) TestIPVSRemoveRealserver(t *testing.T) {
	// start from a clean slate
	mustRun(t, "/sbin/ipvsadm", "-C")
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	fp, err := NewIPVSForwardingPlane(logger)
	if err != nil {
		t.Fatalf("unable to get an IPVS forwarding plane instance: %v", err)
		return
	}

	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 0 {
		t.Fatalf("unexpected state, wanted 0 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	svc := Service{
		Address:   netip.MustParseAddr("127.0.0.1"),
		Scheduler: "rr",
		Port:      443,
		Family:    INET,
		Protocol:  TCP,
	}

	if err := fp.AddService(context.Background(), svc); err != nil {
		t.Fatalf("unable to create service: %v", err)
		return
	}
	// sanity check
	if svcs, err := fp.Services(context.Background()); err != nil || len(svcs) != 1 {
		t.Fatalf("unexpected state, wanted 1 services and no errors, got: %v, %v", err, len(svcs))
		return
	}

	rs := Realserver{
		Address:     netip.MustParseAddr("127.0.0.2"),
		ForwardType: Tunnel,
		Weight:      100,
	}

	if err := fp.AddRealserver(context.Background(), svc, rs); err != nil {
		t.Fatalf("unexpected error while adding realserver: %v", err)
		return
	}

	if realservers, err := fp.Realservers(context.Background(), svc); err != nil || len(realservers) != 1 {
		t.Fatalf("unexpected state, wanted 1 realserver and no errors, got: %v, %v", err, len(realservers))
		return
	}

	if err := fp.RemoveRealserver(context.Background(), svc, rs); err != nil {
		t.Fatalf("unexpected error removing realserver: %v", err)
		return
	}

	if realservers, err := fp.Realservers(context.Background(), svc); err != nil || len(realservers) != 0 {
		t.Fatalf("unexpected state, wanted 0 realserver and no errors, got: %v, %v", err, len(realservers))
		return
	}

}

func (QemuTest) TestKatranPin(t *testing.T) {
	iface, err := net.InterfaceByName(NETWORK_INTERFACE)
	if err != nil {
		t.Fatalf("unable to find interface: %s", NETWORK_INTERFACE)
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))

	k, err := NewKatranForwardingPlane(logger, iface, []uint32{0}, NoNumaNode, CONNTRACK_SIZE)
	if err != nil {
		t.Fatalf("unable to get katran fp instance: %v", err)
	}
	if err := k.Pin(); err != nil {
		t.Fatalf("unable to pin eBPF program/maps/link: %v", err)
	}
	k.(*KatranForwardingPlane).Close()

	if !bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, true) {
		t.Error("bpftool doesn't show the eBPF program loaded")
	}
	if !bpftest.IsBpfObjectLoaded(bpftest.BpfMap, "ch_rings", true) {
		t.Error("bpftool doesn't show the eBPF map loaded")
	}

	k, err = NewKatranForwardingPlane(logger, iface, []uint32{0}, NoNumaNode, CONNTRACK_SIZE)
	if err != nil {
		t.Fatalf("unable to get katran fp instance: %v", err)
	}

	if err := k.Unpin(); err != nil {
		t.Fatalf("unable to unpin eBPF program/maps/link: %v", err)
	}
	k.(*KatranForwardingPlane).Close()

	if bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, false) {
		t.Error("bpftool shows the eBPF program loaded")
	}
}

func (QemuTest) TestKatranAddService(t *testing.T) {
	iface, err := net.InterfaceByName(NETWORK_INTERFACE)
	if err != nil {
		t.Fatalf("unable to find interface: %s", NETWORK_INTERFACE)
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))

	k, err := NewKatranForwardingPlane(logger, iface, []uint32{0}, NoNumaNode, CONNTRACK_SIZE)
	if err != nil {
		t.Fatalf("unable to get katran fp instance: %v", err)
	}

	svcs := []Service{
		Service{
			Address:   netip.MustParseAddr("127.0.0.1"),
			Port:      443,
			Family:    INET,
			Protocol:  TCP,
			Scheduler: "mh",
		},
		Service{
			Address:   netip.MustParseAddr("::1"),
			Port:      443,
			Family:    INET6,
			Protocol:  TCP,
			Scheduler: "mh",
		},
	}

	t.Run("add services", func(t *testing.T) {
		for _, svc := range svcs {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			if err := k.AddService(ctx, svc); err != nil {
				t.Errorf("unable to add service: %v", err)
			}
		}
	})

	testRetrieveServicesFn := func(tName string) {
		t.Run(tName, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			readSvcs, err := k.Services(ctx)
			if err != nil {
				t.Errorf("unable to fetch services: %v", err)
			}
			if len(readSvcs) != len(svcs) {
				t.Errorf("unexpected amount of services, want %d, got %d", len(svcs), len(readSvcs))
			}
			for _, svc := range svcs {
				found := false
				for _, readSvc := range readSvcs {
					if svc == readSvc {
						found = true
						break
					}
				}
				if !found {
					t.Errorf("service %v missing", svc)
				}
			}
		})
	}

	testRetrieveServicesFn("retrieve services")

	if err := k.Pin(); err != nil {
		t.Fatalf("unable to pin eBPF program/maps/link: %v", err)
	}
	k.(*KatranForwardingPlane).Close()
	k, err = NewKatranForwardingPlane(logger, iface, []uint32{0}, NoNumaNode, CONNTRACK_SIZE)
	if err != nil {
		t.Fatalf("unable to get katran fp instance: %v", err)
	}

	testRetrieveServicesFn("retrieve services after restoring a pinned instance")
	if err := k.Unpin(); err != nil {
		t.Fatalf("unable to unpin eBPF program/maps/link: %v", err)
	}
	k.(*KatranForwardingPlane).Close()

	if bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, false) {
		t.Error("bpftool shows the eBPF program loaded")
	}
}

func (QemuTest) TestKatranModifyRealserver(t *testing.T) {
	iface, err := net.InterfaceByName(NETWORK_INTERFACE)
	if err != nil {
		t.Fatalf("unable to find interface: %s", NETWORK_INTERFACE)
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))

	k, err := NewKatranForwardingPlane(logger, iface, []uint32{0}, NoNumaNode, CONNTRACK_SIZE)
	if err != nil {
		t.Fatalf("unable to get katran fp instance: %v", err)
	}

	svcs := []Service{
		Service{
			Address:   netip.MustParseAddr("127.0.0.1"),
			Port:      443,
			Family:    INET,
			Protocol:  TCP,
			Scheduler: "mh",
		},
		Service{
			Address:   netip.MustParseAddr("::1"),
			Port:      443,
			Family:    INET6,
			Protocol:  TCP,
			Scheduler: "mh-port",
		},
	}

	for _, svc := range svcs {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		if err := k.AddService(ctx, svc); err != nil {
			t.Fatalf("unable to add service: %v", err)
		}
		cancel()
	}

	testcases := []struct {
		name   string
		svc    Service
		rs     Realserver
		action katranRealserverAction
	}{
		{
			name: "Add #1 IPv4 realserver",
			svc:  svcs[0],
			rs: Realserver{
				Address: netip.MustParseAddr("10.0.0.1"),
				Weight:  10,
			},
			action: addKatranRealserver,
		},
		{
			name: "Add #2 IPv4 realserver",
			svc:  svcs[0],
			rs: Realserver{
				Address: netip.MustParseAddr("10.0.0.2"),
				Weight:  10,
			},
			action: addKatranRealserver,
		},
		{
			name: "Add #3 IPv4 realserver",
			svc:  svcs[0],
			rs: Realserver{
				Address: netip.MustParseAddr("10.0.0.3"),
				Weight:  10,
			},
			action: addKatranRealserver,
		},
		{
			name: "Delete #3 IPv4 realserver",
			svc:  svcs[0],
			rs: Realserver{
				Address: netip.MustParseAddr("10.0.0.3"),
				Weight:  10,
			},
			action: removeKatranRealserver,
		},
		{
			name: "Add #1 IPv6 realserver",
			svc:  svcs[1],
			rs: Realserver{
				Address: netip.MustParseAddr("fc00::1"),
				Weight:  10,
			},
			action: addKatranRealserver,
		},
		{
			name: "Update #1 IPv4 realserver",
			svc:  svcs[0],
			rs: Realserver{
				Address: netip.MustParseAddr("10.0.0.1"),
				Weight:  20,
			},
			action: updateKatranRealserver,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			katranInstance := k.(*KatranForwardingPlane)
			realsNumsBefore := make([]uint32, 0, len(katranInstance.realsMap))
			for num, _ := range katranInstance.realsMap {
				realsNumsBefore = append(realsNumsBefore, num)
			}
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			if err := katranInstance.modifyRealserver(ctx, tc.action, tc.svc, tc.rs); err != nil {
				t.Errorf("unable to modify realserver: %v", err)
			}

			switch tc.action {
			case addKatranRealserver:
				var newRealNum uint32
				for num, _ := range katranInstance.realsMap {
					if !slices.Contains(realsNumsBefore, num) {
						if newRealNum != 0 {
							t.Fatal("several insertions detected on realsMap")
							return
						}
						newRealNum = num
					}
				}
				if newRealNum == 0 {
					t.Fatal("unable to find the new realserver on the realsMap")
					return
				}
				numIndex := katranInstance.realsNums.Index(func(n uint32) bool {
					if n == newRealNum {
						return true
					}
					return false
				})
				if numIndex >= 0 {
					t.Fatalf("realNum %d still available to be assigned to a new realserver", newRealNum)
				}
			}
		})
	}

	if err := k.Pin(); err != nil {
		t.Fatalf("unable to pin eBPF program/maps/link: %v", err)
	}
	k.(*KatranForwardingPlane).Close()

	k2, err := NewKatranForwardingPlane(logger, iface, []uint32{0}, NoNumaNode, CONNTRACK_SIZE)
	if err != nil {
		t.Fatalf("unable to get katran fp instance: %v", err)
	}
	// oldInstance has the data populated from the actions performed (AddService / AddRealserver)
	oldInstance := k.(*KatranForwardingPlane)
	// newInstance recovered the same data from the eBPF maps
	newInstance := k2.(*KatranForwardingPlane)
	if oldInstance.svcSize != newInstance.svcSize {
		t.Errorf("svcSize mismatch, old: %d, new: %d", oldInstance.svcSize, newInstance.svcSize)
	}
	if oldInstance.realsSize != newInstance.realsSize {
		t.Errorf("realsSize mismatch, old: %d, new: %d", oldInstance.realsSize, newInstance.realsSize)
	}
	if !maps.Equal(oldInstance.realsMap, newInstance.realsMap) {
		t.Error("reals map before and after pinning don't match")
		t.Errorf("realsMap before %v", oldInstance.realsMap)
		t.Errorf("realsMap after %v", newInstance.realsMap)
	}
	if !maps.Equal(oldInstance.realsRefs, newInstance.realsRefs) {
		t.Error("realsRefs map before and after pinning don't match")
		t.Errorf("realsRefs before %v", oldInstance.realsRefs)
		t.Errorf("realsRefs after %v", newInstance.realsRefs)
	}
	for _, svc := range svcs {
		svcKey := serviceToVip(svc)
		if !slices.Equal(oldInstance.svcMap[svcKey].chRing, newInstance.svcMap[svcKey].chRing) {
			t.Errorf("chRing before and after pinning don't match for svc %s:%d", svc.Address.String(), svc.Port)
			t.Errorf("chRing before %v", oldInstance.svcMap[svcKey].chRing)
			t.Errorf("chRing after %v", newInstance.svcMap[svcKey].chRing)
		}
		if !maps.Equal(oldInstance.svcMap[svcKey].reals, newInstance.svcMap[svcKey].reals) {
			t.Error("reals before and after pinning don't match")
		}
	}

	if err := k2.Unpin(); err != nil {
		t.Fatalf("unable to unpin eBPF program/maps/link: %v", err)
	}
	k2.(*KatranForwardingPlane).Close()
}
