package fp

import (
	"github.com/prometheus/client_golang/prometheus"
)

type KatranMetricCollector struct {
	Fplane *KatranForwardingPlane
}

var (
	KatranUpDesc = prometheus.NewDesc(
		"liberica_fp_katran_up",
		"Reports if the current scrape has been successful or not",
		nil, nil,
	)
	KatranVipBytesDesc = prometheus.NewDesc(
		"liberica_fp_katran_vip_bytes_total",
		"Total amount of bytes handled",
		[]string{"service_address"}, nil,
	)
	KatranVipPacketsDesc = prometheus.NewDesc(
		"liberica_fp_katran_vip_packets_total",
		"Total amount of packets handled",
		[]string{"service_address"}, nil,
	)
	KatranFallbackGlobalLRU = prometheus.NewDesc(
		"liberica_fp_katran_fallback_global_lru_total",
		"Number of times that katran failed to find the per cpu/core lru, it should be 0 in production",
		nil, nil,
	)
	KatranInvalidRealID = prometheus.NewDesc(
		"liberica_fp_katran_invalid_real_id",
		"Number of times that katran got a real id that triggered an out of bounds on the reals map",
		nil, nil,
	)
	KatranMissingReals = prometheus.NewDesc(
		"liberica_fp_katran_missing_reals",
		"Number of times that katran got a real id 0, usually caused by a misconfigured VIP",
		nil, nil,
	)
)

func (kmc KatranMetricCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(kmc, ch)
}

func (kmc KatranMetricCollector) Collect(ch chan<- prometheus.Metric) {
	successful := float64(1)
	stats, err := kmc.Fplane.GetStats()
	if err != nil {
		successful = float64(0)
	}

	ch <- prometheus.MustNewConstMetric(
		KatranUpDesc,
		prometheus.CounterValue,
		successful,
	)

	if err != nil {
		return
	}

	for vipAddr, vipStats := range stats.VipStats {
		ch <- prometheus.MustNewConstMetric(KatranVipBytesDesc,
			prometheus.CounterValue,
			vipStats.Bytes,
			vipAddr,
		)
		ch <- prometheus.MustNewConstMetric(KatranVipPacketsDesc,
			prometheus.CounterValue,
			vipStats.Packets,
			vipAddr,
		)
	}

	ch <- prometheus.MustNewConstMetric(KatranFallbackGlobalLRU,
		prometheus.CounterValue,
		stats.FallbackGlobalLRU,
	)

	ch <- prometheus.MustNewConstMetric(KatranInvalidRealID,
		prometheus.CounterValue,
		stats.InvalidRealID,
	)
	ch <- prometheus.MustNewConstMetric(KatranMissingReals,
		prometheus.CounterValue,
		stats.MissingReals,
	)
}
