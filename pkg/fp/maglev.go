package fp

import "slices"

const (
	kHashSeed0 uint32 = 0
	kHashSeed1 uint32 = 2307
	kHashSeed2 uint32 = 42
	kHashSeed3 uint32 = 2718281828
)

func rotl64(x uint64, r int8) uint64 {
	return (x << r) | (x >> (64 - r))
}

// directly translated from Katran implementation in C++:  https://github.com/facebookincubator/katran/blob/0413e06fc04a4b0c7ca13a1bd994feda6ffba340/katran/lib/MurmurHash3.cpp#L26
func murmurHash3(A, B uint64, seed uint32) uint64 {
	h1 := uint64(seed)
	h2 := uint64(seed)

	c1 := uint64(0x87c37b91114253d5)
	c2 := uint64(0x4cf5ad432745937f)

	//----------
	// body

	k1 := A
	k2 := B

	k1 *= c1
	k1 = rotl64(k1, 31)
	k1 *= c2
	h1 ^= k1

	h1 = rotl64(h1, 27)
	h1 += h2
	h1 = h1*5 + 0x52dce729

	k2 *= c2
	k2 = rotl64(k2, 33)
	k2 *= c1
	h2 ^= k2

	h2 = rotl64(h2, 31)
	h2 += h1
	h2 = h2*5 + 0x38495ab5

	//----------
	// finalization

	h1 ^= 16
	h2 ^= 16

	h1 += h2
	h2 += h1

	h1 ^= h1 >> 33
	h1 *= uint64(0xff51afd7ed558ccd)
	h1 ^= h1 >> 33
	h1 *= uint64(0xc4ceb9fe1a85ec53)
	h1 ^= h1 >> 33

	h2 ^= h2 >> 33
	h2 *= uint64(0xff51afd7ed558ccd)
	h2 ^= h2 >> 33
	h2 *= uint64(0xc4ceb9fe1a85ec53)
	h2 ^= h2 >> 33

	h1 += h2

	return h1
}

func genMaglevPermutation(permutation []uint32, endpoint maglevReal, pos, ringSize uint32) {
	offsetHash := murmurHash3(endpoint.hash, uint64(kHashSeed2), kHashSeed0)
	offset := offsetHash % uint64(ringSize)

	skipHash := murmurHash3(endpoint.hash, uint64(kHashSeed3), kHashSeed1)
	skip := (skipHash % (uint64(ringSize) - 1)) + 1

	permutation[2*pos] = uint32(offset)
	permutation[2*pos+1] = uint32(skip)
}

// We use MaglevV2 from Katran so we can set weights easily instead of having to comply with
// sum(weights) = ringSize
func generateHashRing(endpoints []maglevReal, ringSize uint32) []int {
	var result []int
	switch len(endpoints) {
	case 0:
		return slices.Repeat([]int{-1}, int(ringSize))
	case 1:
		return slices.Repeat([]int{int(endpoints[0].num)}, int(ringSize))
	default:
		result = slices.Repeat([]int{-1}, int(ringSize))
	}

	maxWeight := uint32(0)
	for _, endpoint := range endpoints {
		if endpoint.weight > maxWeight {
			maxWeight = endpoint.weight
		}
	}

	runs := uint32(0)
	permutation := make([]uint32, len(endpoints)*2)
	next := make([]uint32, len(endpoints))
	cumWeight := make([]uint32, len(endpoints))

	for i, endpoint := range endpoints {
		genMaglevPermutation(permutation, endpoint, uint32(i), ringSize)
	}

	for {
		for i, endpoint := range endpoints {
			cumWeight[i] += endpoint.weight
			if cumWeight[i] >= maxWeight {
				cumWeight[i] -= maxWeight
				offset := permutation[2*i]
				skip := permutation[2*i+1]
				cur := (offset + next[i]*skip) % ringSize
				for result[cur] >= 0 {
					next[i]++
					cur = (offset + next[i]*skip) % ringSize
				}
				result[cur] = int(endpoint.num)
				next[i]++
				runs++
				if runs == ringSize {
					return result
				}
			}
		}
	}
}
