package fp

import (
	"context"
	"io"
	"log/slog"
	"net"
	"net/netip"
	"testing"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	GRPC_NETWORK      = "tcp"
	GRPC_BIND_ADDRESS = "127.0.0.1:6789"
	CONNTRACK_SIZE    = 1_000
)

// QemuTest is defined on integration_test.go
func (QemuTest) TestGrpc(t *testing.T) {
	// start from a clean slate
	mustRun(t, "/sbin/ipvsadm", "-C")
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	ipvs, err := NewIPVSForwardingPlane(logger)
	if err != nil {
		t.Fatalf("unable to get IPVS forwarding plane: %v", err)
	}
	iface, err := net.InterfaceByName(NETWORK_INTERFACE)
	if err != nil {
		t.Fatalf("unable to find interface: %s", NETWORK_INTERFACE)
	}
	katran, err := NewKatranForwardingPlane(logger, iface, []uint32{0}, NoNumaNode, CONNTRACK_SIZE)
	if err != nil {
		t.Fatalf("unable to get katran forwarding plane: %v", err)
	}
	defer katran.(*KatranForwardingPlane).Close()
	testcases := []struct {
		name   string
		fplane ForwardingPlane
	}{
		{
			name:   "IPVS",
			fplane: ipvs,
		},
		{
			name:   "katran",
			fplane: katran,
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			signalCtx, signalCancel := context.WithCancel(context.Background())
			srv := NewGrpcServer(logger, tc.fplane)
			serverCh, err := grpcutils.RunServer(signalCtx, logger, srv, GRPC_NETWORK, GRPC_BIND_ADDRESS)
			if err != nil {
				t.Fatalf("unable to run grpc server: %v", err)
				return
			}
			client, err := NewGrpcClient(logger, GRPC_NETWORK, GRPC_BIND_ADDRESS)
			if err != nil {
				t.Fatalf("unable to get grpc client %v", err)
				return
			}
			defer client.Close()
			svc := Service{
				Address:   netip.MustParseAddr("127.0.0.1"),
				Scheduler: "mh",
				Port:      uint16(80),
				Family:    INET,
				Protocol:  TCP,
			}

			rs := Realserver{
				Address:     netip.MustParseAddr("127.0.0.2"),
				Weight:      10,
				ForwardType: Tunnel,
			}

			t.Run("ListServices/Empty", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				services, err := client.ListServices(ctx)
				if err != nil {
					t.Errorf("unable to list services: %v", err)
				}
				if len(services) > 0 {
					t.Errorf("unexpected amount of services received, got %d, want 0", len(services))
				}
			})
			t.Run("CreateService", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()
				if err := client.CreateService(ctx, svc); err != nil {
					t.Errorf("unable to create service: %v", err)
				}
			})
			t.Run("ListServices/OneService", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				got, err := client.ListServices(ctx)
				if err != nil {
					t.Errorf("unable to list services: %v", err)
				}
				if len(got) != 1 {
					t.Fatalf("unexpected amount of services received, got %d, want 1", len(got))
					return
				}
				if svc.Address != got[0].Address {
					t.Errorf("unexpected address, got %v, want %v", got[0].Address, svc.Address)
				}
				if svc.Scheduler != got[0].Scheduler {
					t.Errorf("unexpected scheduler, got %v, want %v", got[0].Scheduler, svc.Scheduler)
				}
				if svc.Port != got[0].Port {
					t.Errorf("unexpected port, got %v, want %v", got[0].Port, svc.Port)
				}
				if svc.Family != got[0].Family {
					t.Errorf("unexpected family, got %v, want %v", got[0].Family, svc.Family)
				}
				if svc.Protocol != got[0].Protocol {
					t.Errorf("unexpected protocol, got %v, want %v", got[0].Protocol, svc.Protocol)
				}
			})

			t.Run("ListRealservers/Empty", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				got, err := client.ListRealservers(ctx, svc)
				if err != nil {
					t.Errorf("unable to list realservers: %v", err)
				}

				if len(got) != 0 {
					t.Errorf("unexpected amount of realservers received, got %d, want 0", len(got))
				}
			})

			t.Run("CreateRealserver", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				if err := client.CreateRealserver(ctx, svc, rs); err != nil {
					t.Errorf("unable to create realserver: %v", err)
				}
			})

			t.Run("ListRealservers/OneRS", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				got, err := client.ListRealservers(ctx, svc)
				if err != nil {
					t.Fatalf("unable to list realservers: %v", err)
					return
				}
				if len(got) != 1 {
					t.Errorf("unexpected amount of realservers received, got %d, want 1", len(got))
				}
			})

			t.Run("UpdateRealserver", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				rs.Weight = 200
				if err := client.UpdateRealserver(ctx, svc, rs); err != nil {
					t.Fatalf("unable to update realserver: %v", err)
					return
				}
				got, err := client.ListRealservers(ctx, svc)
				if err != nil {
					t.Fatalf("unable to list realservers: %v", err)
					return
				}
				if len(got) != 1 {
					t.Errorf("unexpected amount of realservers received, got %d, want 1", len(got))
				}
				if got[0].Weight != rs.Weight {
					t.Errorf("unexpected Weight, want %d, got %d", rs.Weight, got[0].Weight)
				}
			})

			t.Run("DeleteRealserver", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				if err := client.DeleteRealserver(ctx, svc, rs); err != nil {
					t.Fatalf("unable to delete realserver: %v", err)
					return
				}

				ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				got, err := client.ListRealservers(ctx, svc)
				if status.Code(err) != codes.OK {
					t.Fatalf("unable to list realservers: %v", err)
					return
				}
				if len(got) != 0 {
					t.Errorf("unexpected amount of realservers received, got %d, want 0", len(got))
				}

			})

			t.Run("DeleteService", func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				if err := client.DeleteService(ctx, svc); err != nil {
					t.Errorf("unable to delete service: %v", err)
				}

				ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				got, err := client.ListServices(ctx)
				if err != nil {
					t.Errorf("unable to list services: %v", err)
				}
				if len(got) != 0 {
					t.Errorf("unexpected amount of services received, got %d, want 0", len(got))
				}
			})

			wait := true
			for wait {
				signalCancel()
				select {
				case <-serverCh:
					wait = false
				case <-time.After(5 * time.Second):
					t.Error("cmd didn't react to SIGTERM in 5 seconds")
					wait = false
				}
			}
		})
	}
}
