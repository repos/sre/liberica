package fp

import (
	"errors"
	"fmt"
	"net/netip"
	"testing"

	"github.com/cloudflare/ipvs"
	"github.com/cloudflare/ipvs/netmask"
)

func TestGetIpvsService(t *testing.T) {
	testCases := []struct {
		input Service
		want  *ipvs.Service
		err   error
	}{
		{Service{Protocol: QUIC}, nil, ErrProcotolNotSupported},
		{
			Service{
				Address:   netip.MustParseAddr("127.0.0.1"),
				Scheduler: "rr",
				Port:      443,
				Family:    INET,
				Protocol:  TCP,
			}, &ipvs.Service{
				Address:   netip.MustParseAddr("127.0.0.1"),
				Scheduler: "rr",
				Port:      443,
				Family:    ipvs.INET,
				Protocol:  ipvs.TCP,
				Netmask:   netmask.MaskFrom4([...]byte{255, 255, 255, 255}),
			}, nil,
		},
	}

	for i, tc := range testCases {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			got, err := getIpvsService(tc.input)
			if err != tc.err {
				t.Errorf("unexpected err value, got: %v, wanted: %v", err, tc.err)
				if got != nil {
					t.Errorf("unexpected IPVS service, got: %+v, wanted: nil", got)
					return
				}
			}
			if got != nil && tc.want != nil && *got != *tc.want {
				t.Errorf("unexpected IPVS Service, got: %+v, wanted: %+v", got, tc.want)
			}
		})
	}
}

func TestGetIpvsDestination(t *testing.T) {
	testCases := []struct {
		svc   Service
		input Realserver
		want  *ipvs.Destination
		err   error
	}{
		{
			Service{Port: 80},
			Realserver{
				Address:     netip.MustParseAddr("127.0.0.1"),
				Weight:      100,
				ForwardType: Tunnel,
			}, &ipvs.Destination{
				Address:   netip.MustParseAddr("127.0.0.1"),
				Weight:    100,
				FwdMethod: ipvs.Tunnel,
				Port:      80,
			},
			nil,
		},
		{
			Service{Port: 80},
			Realserver{
				Address:     netip.MustParseAddr("127.0.0.1"),
				Weight:      100,
				ForwardType: 1234, // invalid ForwardType
			},
			nil,
			ErrForwardTypeNotSupported,
		},
	}

	for i, tc := range testCases {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			got, gotErr := getIpvsDestination(tc.svc, tc.input)
			if !errors.Is(gotErr, tc.err) {
				t.Errorf("Unexpected error calling getIpvsDestination(). got %v, want %v", gotErr, tc.err)
			}
			if gotErr == nil && tc.want != nil {
				if *got != *tc.want {
					t.Errorf("unexpected IPVS destination. got %+v, want %+v.", *got, *tc.want)
				}
			}
		})
	}
}
