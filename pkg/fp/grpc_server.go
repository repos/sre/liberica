package fp

import (
	"context"
	"fmt"
	"log/slog"
	"net/netip"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/fp"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"
	pbsvc "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct {
	pb.UnimplementedForwardingPlaneServiceServer
	fplane ForwardingPlane
	logger *slog.Logger
}

func NewGrpcServer(logger *slog.Logger, fplane ForwardingPlane) *grpc.Server {
	s := &server{
		logger: logger,
		fplane: fplane,
	}

	var opts []grpc.ServerOption
	opts = append(opts, grpc.KeepaliveParams(grpcutils.KeepaliveServerParameters))
	srv := grpc.NewServer(opts...)
	pb.RegisterForwardingPlaneServiceServer(srv, s)

	return srv
}

func parseService(svc *pbsvc.Service) (*Service, error) {
	if svc == nil {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Svc")
	}
	if svc.Scheduler == "" {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Svc.Scheduler")
	}
	if svc.Port == 0 || svc.Port > 65535 {
		return nil, status.Errorf(codes.InvalidArgument, "request missing/invalid required field: Svc.Port")
	}
	addr, err := netip.ParseAddr(svc.Address)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid request field: Svc.Address: %v", err)
	}
	proto, err := NewProtocol(svc.Protocol)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid request field: Svc.Protocol")
	}
	af, err := NewAddressFamily(svc.Family)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid request field: Svc.AddressFamily")
	}

	return &Service{
		Address:   addr,
		Scheduler: svc.Scheduler,
		Port:      uint16(svc.Port),
		Family:    af,
		Protocol:  proto,
	}, nil
}

func parseRealserver(rs *pbrs.Realserver) (*Realserver, error) {
	if rs == nil {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Rs")
	}
	addr, err := netip.ParseAddr(rs.Address)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid field: Rs.Address")
	}

	if rs.Weight == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "invalid field: Rs.Weight")
	}

	ft, err := NewForwardType(rs.ForwardType)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid field: Rs.ForwardType")
	}

	return &Realserver{
		Address:     addr,
		Weight:      rs.Weight,
		Mark:        rs.Mark,
		ForwardType: ft,
	}, nil
}

func (srv *server) CreateService(ctx context.Context, in *pb.CreateServiceRequest) (*pb.CreateServiceResponse, error) {
	svc, err := parseService(in.Svc)
	if err != nil {
		return nil, err
	}
	if err := srv.fplane.AddService(ctx, *svc); err != nil {
		return nil, err
	}

	return &pb.CreateServiceResponse{}, nil
}

func (srv *server) DeleteService(ctx context.Context, in *pb.DeleteServiceRequest) (*pb.DeleteServiceResponse, error) {
	svc, err := parseService(in.Svc)
	if err != nil {
		return nil, err
	}
	if err := srv.fplane.RemoveService(ctx, *svc); err != nil {
		return nil, err
	}
	return &pb.DeleteServiceResponse{}, nil
}

func (srv *server) ListServices(ctx context.Context, in *pb.ListServicesRequest) (*pb.ListServicesResponse, error) {
	svcs, err := srv.fplane.Services(ctx)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch services: %w", err)
	}

	out := &pb.ListServicesResponse{}
	for _, svc := range svcs {
		out.Svc = append(out.Svc, &pbsvc.Service{
			Address:   svc.Address.String(),
			Scheduler: svc.Scheduler,
			Port:      uint32(svc.Port),
			Family:    svc.Family.AsProto(),
			Protocol:  svc.Protocol.AsProto(),
		})
	}

	return out, nil
}

func (srv *server) ListRealservers(ctx context.Context, in *pb.ListRealserversRequest) (*pb.ListRealserversResponse, error) {
	svc, err := parseService(in.Svc)
	if err != nil {
		return nil, err
	}
	out := &pb.ListRealserversResponse{}
	rss, err := srv.fplane.Realservers(ctx, *svc)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch realservers: %w", err)
	}

	for _, rs := range rss {
		out.Rs = append(out.Rs, &pbrs.Realserver{
			Address:     rs.Address.String(),
			Weight:      rs.Weight,
			Mark:        rs.Mark,
			ForwardType: rs.ForwardType.AsProto(),
		})
	}

	return out, nil
}

func (srv *server) CreateRealserver(ctx context.Context, in *pb.CreateRealserverRequest) (*pb.CreateRealserverResponse, error) {
	svc, err := parseService(in.Svc)
	if err != nil {
		return nil, err
	}

	rs, err := parseRealserver(in.Rs)
	if err != nil {
		return nil, err
	}

	if err := srv.fplane.AddRealserver(ctx, *svc, *rs); err != nil {
		return nil, fmt.Errorf("unable to create realserver: %w", err)
	}

	return &pb.CreateRealserverResponse{}, nil
}

func (srv *server) UpdateRealserver(ctx context.Context, in *pb.UpdateRealserverRequest) (*pb.UpdateRealserverResponse, error) {
	svc, err := parseService(in.Svc)
	if err != nil {
		return nil, err
	}

	rs, err := parseRealserver(in.Rs)
	if err != nil {
		return nil, err
	}

	if err := srv.fplane.UpdateRealserver(ctx, *svc, *rs); err != nil {
		return nil, fmt.Errorf("unable to update realserver: %w", err)
	}

	return &pb.UpdateRealserverResponse{}, nil
}

func (srv *server) DeleteRealserver(ctx context.Context, in *pb.DeleteRealserverRequest) (*pb.DeleteRealserverResponse, error) {
	svc, err := parseService(in.Svc)
	if err != nil {
		return nil, err
	}

	rs, err := parseRealserver(in.Rs)
	if err != nil {
		return nil, err
	}

	if err := srv.fplane.RemoveRealserver(ctx, *svc, *rs); err != nil {
		return nil, fmt.Errorf("unable to delete realserver: %w", err)
	}

	return &pb.DeleteRealserverResponse{}, nil
}
