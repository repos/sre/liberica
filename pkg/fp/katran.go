/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package fp

import (
	"cmp"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"log/slog"
	"maps"
	"net"
	"net/netip"
	"os"
	"path/filepath"
	"slices"
	"strconv"
	"strings"
	"sync"
	"syscall"

	"github.com/cilium/ebpf"
	"github.com/cilium/ebpf/link"
	"github.com/gammazero/deque"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/hash"
	"golang.org/x/sys/unix"
)

//go:generate go run github.com/cilium/ebpf/cmd/bpf2go -cc clang -cflags "-O2 -D__KERNEL__ -Wno-unused-value -Wno-pointer-sign -Wno-compare-distinct-pointer-types" balancer balancer.bpf.c -- -I../../headers

const (
	basePinPath    = "/sys/fs/bpf"
	programPinPath = "/sys/fs/bpf/balancer"
	linkPinPath    = "/sys/fs/bpf/balancer-xdp-link"
	NoNumaNode     = -1
)

// real flags (from balancer_consts.h)
const (
	flagIpv6Address = 1 << 0
)

// vip flags (from balancer_consts.h)
const (
	flagHashNoSrcPort = 1 << 0
)

// index defined on balancer.bpf.c#L709
const (
	macAddressIndex = uint32(0)
)

// stats indexes defined on balancer_consts.h)
const (
	FallbackLRUCounter       = uint32(3)
	ConstantHashingDropStats = uint32(9)
)

var (
	ErrorBPFNotLoaded          = errors.New("BPF program not loaded")
	ErrorKatranNotAttached     = errors.New("not attached to any interface")
	ErrorServiceAlreadyPresent = errors.New("service is already present")
	ErrorBalancerAtCapacity    = errors.New("load balancer is full")
	ErrorServiceNotFound       = errors.New("service not found")
	ErrorRealserverNotFound    = errors.New("realserver not found")
	ErrorInvalidMACAddress     = errors.New("invalid MAC address")
)

type katranRealserverAction uint8

const (
	addKatranRealserver katranRealserverAction = iota
	removeKatranRealserver
	updateKatranRealserver
)

type maglevReal struct {
	num    uint32
	weight uint32
	hash   uint64
}

func compareMaglevReals(a, b maglevReal) int {
	return cmp.Compare(a.hash, b.hash)
}

type vip struct {
	reals  map[uint32]maglevReal
	chRing []int
	meta   balancerVipMeta
}

type KatranForwardingPlane struct {
	objs            balancerObjects
	link            link.Link
	logger          *slog.Logger
	svcMap          map[balancerWmfVipDefinition]vip
	svcNums         *deque.Deque[uint32]
	realsNums       *deque.Deque[uint32]
	realsMap        map[uint32]balancerWmfRealDefinition
	realsRefs       map[uint32]int       // track the number of times that a realserver is referenced
	lruMaps         map[uint32]*ebpf.Map // per CPU LRU maps
	forwardingCores []uint32
	possibleCPU     int // max number of CPUs
	numaNode        int // NUMA node of the forwarding Cores, we currently don't support several NUMA nodes
	mutex           sync.RWMutex
	realsCap        uint32 // capacity for the underlying eBPF map used to store realservers
	svcCap          uint32 // capacity for the underlying eBPF map used to store services
	statsCap        uint32 // capacity for the underlying eBPF map used to store stats
	conntrackSize   uint32
	realsSize       uint32
	ringSize        uint32
	svcSize         uint32
	loaded          bool
	attached        bool
	pinned          bool // true if eBPF program and maps are pinned
}

// create per CPU LRU maps used by katran to store conntrack data
func (k *KatranForwardingPlane) createLruMaps() error {
	var s balancerSpecs
	spec, err := loadBalancer()
	if err != nil {
		return err
	}
	if err := spec.Assign(&s); err != nil {
		return err
	}

	for _, core := range k.forwardingCores {
		innerMapSpec := s.LruMapping.InnerMap.Copy()
		// map name is limited to 16 bytes
		innerMapSpec.Name = fmt.Sprintf("katran_lru_%d", core)
		innerMapSpec.MaxEntries = k.conntrackSize / uint32(len(k.forwardingCores))
		if k.numaNode != NoNumaNode {
			innerMapSpec.Flags |= unix.BPF_F_NUMA_NODE
			innerMapSpec.NumaNode = uint32(k.numaNode)
		}
		var err error
		k.lruMaps[core], err = ebpf.NewMap(innerMapSpec)
		if err != nil {
			return err
		}
	}
	k.logger.Debug("per CPU LRU maps created", slog.Int("amount", len(k.lruMaps)))

	return nil
}

// handle eBPF program & maps loading from a clean slate state
// and from a pined state
func (k *KatranForwardingPlane) load() error {
	if _, err := os.Stat(programPinPath); err != nil {
		if err := k.createLruMaps(); err != nil {
			return err
		}
		spec, err := loadBalancer()
		if err != nil {
			return err
		}

		if err := spec.LoadAndAssign(&k.objs, nil); err != nil {
			return err
		}

		for cpu, m := range k.lruMaps {
			if err := k.objs.LruMapping.Put(cpu, m); err != nil {
				return err
			}
		}

		return nil
	}
	var err error

	k.objs.BalancerIngress, err = ebpf.LoadPinnedProgram(programPinPath, nil)
	if err != nil {
		k.logger.Error("unable to load pinned program", slog.Any("error", err))
		return err
	}

	k.link, err = link.LoadPinnedLink(linkPinPath, &ebpf.LoadPinOptions{})
	if err != nil {
		k.logger.Error("unable to load pinned link", slog.Any("error", err))
		return err
	}

	maps := []struct {
		ebpfMap **ebpf.Map
		name    string
	}{
		{&k.objs.ChRings, "ch_rings"},
		{&k.objs.CtlArray, "ctl_array"},
		{&k.objs.DecapVipStats, "decap_vip_stats"},
		{&k.objs.FallbackCache, "fallback_cache"},
		{&k.objs.LruMapping, "lru_mapping"},
		{&k.objs.LruMissStats, "lru_miss_stats"},
		{&k.objs.QuicStatsMap, "quic_stats_map"},
		{&k.objs.Reals, "reals"},
		{&k.objs.RealsStats, "reals_stats"},
		{&k.objs.RealsWeights, "reals_weights"},
		{&k.objs.ServerIdMap, "server_id_map"},
		{&k.objs.ServerIdStats, "server_id_stats"},
		{&k.objs.StableRtStats, "stable_rt_stats"},
		{&k.objs.Stats, "stats"},
		{&k.objs.TprStatsMap, "tpr_stats_map"},
		{&k.objs.VipMap, "vip_map"},
		{&k.objs.VipMissStats, "vip_miss_stats"},
	}

	for _, m := range maps {
		path := filepath.Join(basePinPath, m.name)
		if os.Stat(path); err != nil {
			return err
		}
		var err error
		*m.ebpfMap, err = ebpf.LoadPinnedMap(path, &ebpf.LoadPinOptions{})
		if err != nil {
			return err
		}
	}
	// fetch per CPU LRU maps
	entries := k.objs.LruMapping.Iterate()
	var key uint32
	var value *ebpf.Map
	for entries.Next(&key, &value) {
		// We discard the *ebpf.Map instance returned by Next()
		// cause Unpin() won't work with that one as its internal pinpath is nil
		value.Close()
		path := filepath.Join(basePinPath, fmt.Sprintf("katran_lru_%d", key))
		var err error
		k.lruMaps[key], err = ebpf.LoadPinnedMap(path, &ebpf.LoadPinOptions{})
		if err != nil {
			return err
		}
	}

	k.attached = true
	k.pinned = true
	return nil
}

func NewKatranForwardingPlane(logger *slog.Logger, iface *net.Interface, forwardingCores []uint32, numaNode int, conntrackSize uint32) (ForwardingPlane, error) {
	k := &KatranForwardingPlane{
		objs:            balancerObjects{},
		logger:          logger,
		loaded:          false,
		attached:        false,
		pinned:          false,
		forwardingCores: forwardingCores,
		conntrackSize:   conntrackSize,
		numaNode:        numaNode,
	}

	k.lruMaps = make(map[uint32]*ebpf.Map, len(k.forwardingCores))
	// Load eBPF program, populate maps and attach to interface
	if err := k.load(); err != nil {
		return nil, fmt.Errorf("unable to load eBPF program/maps: %w", err)
	}
	k.loaded = true

	if !k.attached {
		if err := k.attach(iface); err != nil {
			return nil, fmt.Errorf("unable to attach XDP program to interface: %w", err)
		}
	}

	k.svcCap = k.objs.VipMap.MaxEntries()
	k.logger.Debug("eBPF service map capacity",
		slog.Int("capacity", int(k.svcCap)),
	)
	k.svcMap = make(map[balancerWmfVipDefinition]vip, k.svcCap)
	k.svcNums = new(deque.Deque[uint32])
	k.svcNums.SetBaseCap(int(k.svcCap))
	for i := uint32(0); i < k.svcCap; i++ {
		k.svcNums.PushBack(i)
	}

	k.realsCap = k.objs.Reals.MaxEntries()
	k.logger.Debug("eBPF reals map capacity",
		slog.Int("capacity", int(k.realsCap)),
	)
	k.realsMap = make(map[uint32]balancerWmfRealDefinition, k.realsCap)
	k.realsRefs = make(map[uint32]int, k.realsCap)
	k.realsNums = new(deque.Deque[uint32])
	k.realsNums.SetBaseCap(int(k.realsCap))

	// We are skipping here real-id 0. Why? KatranLB provides this rationale:
	// Why avoid real-id 0?
	// BPF arrays are initialized with value of 0. So it's hard to disambiguate
	// issues where '0' is returned as server at index 0 vs error cases where it
	// couldn't find the server. So we preserve 0 as the invalid entry to reals
	// array.
	// NOTE(vgutierrez): even if we skip index 0, eBPF array gets an empty index 0
	for i := uint32(1); i < k.realsCap; i++ {
		k.realsNums.PushBack(i)
	}

	k.ringSize = k.objs.ChRings.MaxEntries() / k.svcCap
	k.logger.Debug("eBPF maglev ring size detected",
		slog.Int("ring_size", int(k.ringSize)),
	)

	k.statsCap = k.objs.Stats.MaxEntries()
	k.logger.Debug("eBPF stats map capacity",
		slog.Int("capacity", int(k.statsCap)),
	)

	possibleCPU, err := ebpf.PossibleCPU()
	if err != nil {
		return nil, err
	}
	k.possibleCPU = possibleCPU

	if k.pinned {
		// order is important here, we need reals data
		// to regenerate each service<->real mapping
		if err := k.restoreReals(); err != nil {
			return nil, err
		}
		if err := k.restoreServices(); err != nil {
			return nil, err
		}
	}

	return k, nil
}

// Attach to ingress interface
func (k *KatranForwardingPlane) attach(iface *net.Interface) error {
	if !k.loaded {
		return ErrorBPFNotLoaded
	}
	var err error
	k.link, err = link.AttachXDP(link.XDPOptions{
		Program:   k.objs.BalancerIngress,
		Interface: iface.Index,
	})
	if err != nil {
		k.logger.Error("unable to attach XDP program to interface",
			slog.String("interface", iface.Name),
			slog.Any("error", err),
		)
		return err
	}

	k.attached = true
	return nil
}

// Detach from ingress interface
func (k *KatranForwardingPlane) detach() error {
	if !k.attached {
		return ErrorKatranNotAttached
	}

	if err := k.link.Close(); err != nil {
		return err
	}

	k.attached = false
	return nil
}

// Restore k.svcMap, k.svcSize, k.svcNums based on eBPF contents
func (k *KatranForwardingPlane) restoreServices() error {
	if !k.loaded {
		return ErrorBPFNotLoaded
	}

	k.mutex.Lock()
	defer k.mutex.Unlock()

	// fetch vips data
	entries := k.objs.VipMap.Iterate()
	var key balancerWmfVipDefinition
	var vipMeta balancerVipMeta
	// key: vipNum, value: balancerWmfVipDefinition
	vipNumDefinition := make(map[uint32]balancerWmfVipDefinition, k.svcCap)
	for entries.Next(&key, &vipMeta) {
		v := vip{
			meta:   vipMeta,
			reals:  make(map[uint32]maglevReal),
			chRing: make([]int, k.ringSize),
		}
		k.svcMap[key] = v
		k.svcSize++
		vipNumDefinition[vipMeta.VipNum] = key
		numIndex := k.svcNums.Index(func(n uint32) bool {
			if n == vipMeta.VipNum {
				return true
			}
			return false
		})
		if numIndex < 0 {
			return fmt.Errorf("potential liberica bug, vipNum %d not found", vipMeta.VipNum)
		}
		k.svcNums.Remove(numIndex)
	}

	if err := entries.Err(); err != nil {
		return err
	}

	// fetch constant hashing rings
	k.logger.Debug("fetching eBPF ch_rings map")
	var cursor ebpf.MapBatchCursor
	recoveredVipNums := 0
	for recoveredVipNums != int(k.svcSize) {
		chRingKeys := make([]uint32, k.ringSize)
		chRingValues := make([]uint32, k.ringSize)
		if _, err := k.objs.ChRings.BatchLookup(&cursor, chRingKeys, chRingValues, nil); err != nil {
			// technically err could be ebpf.ErrKeyNoExist and that would mean that we've already read
			// ChRings entirely. This should never happen though cause we should be smart enough to
			// stop reading as soon as we got the ring for each configured VIP, if that's not the case
			// we are in trouble and crashing here is OK
			return err
		}

		vipNum := chRingKeys[0] / k.ringSize
		svcKey, present := vipNumDefinition[vipNum]
		if !present { // skip chRing for unconfigured VIP
			continue
		}

		recoveredVipNums++

		if chRingValues[0] <= 0 { // on a initialized ring, real num will always be >0
			// no need to continue, VIP without any realservers configured
			continue
		}
		for i := 0; i < int(k.ringSize); i++ {
			realNum := chRingValues[i]
			k.svcMap[svcKey].chRing[chRingKeys[i]%k.ringSize] = int(realNum)
		}
	}
	k.logger.Debug("eBPF ch_rings dump completed")

	// fetch real's weight data
	// small cache of real hashes, key: realNum, value: real hash
	realHash := make(map[uint32]uint64, k.realsSize)
	var rWeightKey balancerWmfVipRealKey
	var weight uint32
	rWeightEntries := k.objs.RealsWeights.Iterate()
	for rWeightEntries.Next(&rWeightKey, &weight) {
		realNum := rWeightKey.RealNum
		vipNum := rWeightKey.VipNum

		h, present := realHash[realNum]
		if !present {
			address := bytesToAddr(k.realsMap[realNum].Dst)
			var err error
			h, err = hash.Hash64Addr(hash.SipHash, address)
			if err != nil {
				return err
			}
			realHash[realNum] = h
		}
		key, present := vipNumDefinition[vipNum]
		if !present {
			return fmt.Errorf("liberica bug: vip %d not present on vipNumDefinition", vipNum)
		}
		k.svcMap[key].reals[realNum] = maglevReal{
			num:    realNum,
			weight: weight,
			hash:   h,
		}
		k.realsRefs[realNum]++
	}

	if err := rWeightEntries.Err(); err != nil {
		return err
	}

	return nil
}

// Restore k.realsMap, k.realsSize and k.realsNums based on eBPF contents
func (k *KatranForwardingPlane) restoreReals() error {
	if !k.loaded {
		return ErrorBPFNotLoaded
	}

	k.mutex.Lock()
	defer k.mutex.Unlock()

	entries := k.objs.Reals.Iterate()
	var key uint32
	var value balancerWmfRealDefinition
	for entries.Next(&key, &value) {
		if emptyReal(value) {
			continue
		}
		k.realsMap[key] = value
		k.realsSize++
		numIndex := k.realsNums.Index(func(n uint32) bool {
			if n == key {
				return true
			}
			return false
		})
		if numIndex < 0 {
			return fmt.Errorf("potential liberica bug, realNum %d not found", key)
		}
		k.realsNums.Remove(numIndex)
	}

	if err := entries.Err(); err != nil {
		return err
	}

	return nil
}

// Add service
func (k *KatranForwardingPlane) AddService(ctx context.Context, svc Service) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	if !k.loaded {
		return ErrorBPFNotLoaded
	}

	k.mutex.Lock()
	defer k.mutex.Unlock()

	if k.svcSize >= k.svcCap {
		return fmt.Errorf("%w: capacity (%d)", ErrorBalancerAtCapacity, k.svcCap)
	}

	key := serviceToVip(svc)
	if _, present := k.svcMap[key]; present {
		return ErrorServiceAlreadyPresent
	}

	flags := uint32(0)
	switch svc.Scheduler {
	case "mh":
		flags |= flagHashNoSrcPort
	case "mh-port":
		flags = 0
	}

	vipNum := k.svcNums.PopFront()
	vipMeta := balancerVipMeta{
		Flags:  flags,
		VipNum: vipNum,
	}

	if err := k.objs.VipMap.Put(key, vipMeta); err != nil {
		slog.Error("unable to update vips map", slog.Any("error", err))
		k.svcNums.PushBack(vipNum)
		return err
	}

	k.svcMap[key] = vip{
		meta:   vipMeta,
		reals:  make(map[uint32]maglevReal),
		chRing: make([]int, k.ringSize),
	}
	k.svcSize++

	return nil
}

// List services
func (k *KatranForwardingPlane) Services(ctx context.Context) ([]Service, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}

	k.mutex.RLock()
	defer k.mutex.RUnlock()

	ret := make([]Service, 0, k.svcSize)
	for bpfSvc, vip := range k.svcMap {
		scheduler := "mh-port"
		if vip.meta.Flags != 0 {
			scheduler = "mh"
		}
		svc := Service{
			Address: bytesToAddr(bpfSvc.Vip),
			Port: binary.LittleEndian.Uint16([]byte{
				byte(bpfSvc.Port >> 8),
				byte(bpfSvc.Port),
			}),
			Scheduler: scheduler,
		}

		if svc.Address.Is4() {
			svc.Family = INET
		} else {
			svc.Family = INET6
		}

		switch bpfSvc.Proto {
		case syscall.IPPROTO_TCP:
			svc.Protocol = TCP
		case syscall.IPPROTO_UDP:
			svc.Protocol = UDP
		}
		ret = append(ret, svc)

	}
	return ret, nil
}

// delete services
func (k *KatranForwardingPlane) RemoveService(ctx context.Context, svc Service) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}
	if !k.loaded {
		return ErrorBPFNotLoaded
	}

	key := serviceToVip(svc)

	k.mutex.Lock()
	defer k.mutex.Unlock()

	value, ok := k.svcMap[key]
	if !ok {
		return fmt.Errorf("%w: (%s:%d)", ErrorServiceNotFound, svc.Address.String(), svc.Port)
	}

	if err := k.objs.VipMap.Delete(key); err != nil {
		return fmt.Errorf("unable to delete service: %w", err)
	}

	for realNum, _ := range value.reals {
		k.realsRefs[realNum]--
		rWeightKey := balancerWmfVipRealKey{
			VipNum:  value.meta.VipNum,
			RealNum: realNum,
		}
		if err := k.objs.RealsWeights.Delete(rWeightKey); err != nil {
			return fmt.Errorf("unable to delete real weight mapping: %w", err)
		}
		if k.realsRefs[realNum] == 0 { // realserver isn't referenced on any other service
			// cannot use .Delete() with Reals cause it's a BPF_MAP_TYPE_ARRAY
			if err := k.objs.Reals.Update(realNum, balancerWmfRealDefinition{}, ebpf.UpdateExist); err != nil {
				// no need to fail here, we got an orphan real definition that's been efectively removed from every service, no harm done
				k.logger.Warn("unable to delete realserver from reals eBPF map",
					slog.Any("error", err),
				)
			}
			delete(k.realsMap, realNum)
			k.realsSize--
			k.realsNums.PushBack(realNum)
		}
	}
	k.svcSize--
	delete(k.svcMap, key)
	k.svcNums.PushBack(value.meta.VipNum)

	return nil
}

// add realservers
func (k *KatranForwardingPlane) AddRealserver(ctx context.Context, svc Service, rs Realserver) error {
	return k.modifyRealserver(ctx, addKatranRealserver, svc, rs)
}

// list realservers
func (k *KatranForwardingPlane) Realservers(ctx context.Context, svc Service) ([]Realserver, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}
	if !k.loaded {
		return nil, ErrorBPFNotLoaded
	}

	svcKey := serviceToVip(svc)
	k.mutex.RLock()
	defer k.mutex.RUnlock()

	if _, ok := k.svcMap[svcKey]; !ok {
		return nil, fmt.Errorf("%w: (%s%d)", ErrorServiceNotFound, svc.Address.String(), svc.Port)
	}

	ret := make([]Realserver, 0, len(k.svcMap[svcKey].reals))
	for num, meta := range k.svcMap[svcKey].reals {
		realDef := k.realsMap[num]
		rs := Realserver{
			Address: bytesToAddr(realDef.Dst),
			Weight:  meta.weight,
		}
		ret = append(ret, rs)
	}

	return ret, nil
}

func (k *KatranForwardingPlane) UpdateRealserver(ctx context.Context, svc Service, rs Realserver) error {
	return k.modifyRealserver(ctx, updateKatranRealserver, svc, rs)
}

func (k *KatranForwardingPlane) RemoveRealserver(ctx context.Context, svc Service, rs Realserver) error {
	return k.modifyRealserver(ctx, removeKatranRealserver, svc, rs)
}

func (k *KatranForwardingPlane) modifyRealserver(ctx context.Context, action katranRealserverAction, svc Service, rs Realserver) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}
	if !k.loaded {
		return ErrorBPFNotLoaded
	}

	// parameters validation
	svcKey := serviceToVip(svc)
	h, err := hash.Hash64Addr(hash.SipHash, rs.Address)
	if err != nil {
		return err
	}

	k.mutex.Lock()
	defer k.mutex.Unlock()

	if _, ok := k.svcMap[svcKey]; !ok {
		return fmt.Errorf("%w: (%s%d)", ErrorServiceNotFound, svc.Address.String(), svc.Port)
	}

	var realNum uint32
	for num, meta := range k.svcMap[svcKey].reals {
		if meta.hash == h {
			realNum = num
			break
		}
	}

	switch realNum {
	case 0:
		if action != addKatranRealserver {
			return fmt.Errorf("%w: %s not present on service %s:%d", ErrorRealserverNotFound,
				rs.Address.String(), svc.Address.String(), svc.Port)
		}
	default:
		switch action {
		case addKatranRealserver:
			return fmt.Errorf("realserver already present")
		case updateKatranRealserver:
			if k.svcMap[svcKey].reals[realNum].weight == rs.Weight {
				// NOOP
				return nil
			}
		}
	}

	var newReal bool
	rMeta := maglevReal{
		num:    realNum,
		weight: rs.Weight,
		hash:   h,
	}
	rWeightKey := balancerWmfVipRealKey{
		VipNum:  k.svcMap[svcKey].meta.VipNum,
		RealNum: realNum,
	}

	// copy the reals map to avoid modifying it
	// until we got a successful chRing eBPF map update
	reals := maps.Clone(k.svcMap[svcKey].reals)

	switch action {
	case addKatranRealserver:
		r := balancerWmfRealDefinition{
			Dst: addrToBytes(rs.Address),
		}
		if rs.Address.Is6() {
			r.Flags |= flagIpv6Address
		}
		newReal = true
		for num, realDef := range k.realsMap {
			// realserver already present in the load balancer
			if realDef == r {
				newReal = false
				realNum = num
				rMeta.num = realNum
				rWeightKey.RealNum = realNum
				break
			}
		}
		if newReal {
			// Check that we have the capacity to add a new realserver
			if k.realsSize >= k.realsCap {
				return ErrorBalancerAtCapacity
			}
			realNum = k.realsNums.PopFront()
			rMeta.num = realNum
			rWeightKey.RealNum = realNum
			// Insert the realserver on the reals map
			if err := k.objs.Reals.Put(realNum, r); err != nil {
				return err
			}
			k.realsMap[realNum] = r
			k.realsSize++
		}
		if err := k.objs.RealsWeights.Put(rWeightKey, rs.Weight); err != nil {
			if newReal {
				// Rollback Real creation
				k.realsNums.PushBack(realNum)
				if rollbackErr := k.objs.Reals.Delete(realNum); rollbackErr != nil {
					k.logger.Error("unable to rollback Reals map insertion after a failed reals weight insertion",
						slog.Any("error", err),
					)
					return err
				}
			}
			return err
		}

		reals[realNum] = rMeta
	case updateKatranRealserver:
		if err := k.objs.RealsWeights.Put(rWeightKey, rs.Weight); err != nil {
			return err
		}
		reals[realNum] = rMeta
	case removeKatranRealserver:
		delete(reals, realNum)
	}

	endpoints := make([]maglevReal, 0, len(reals))
	for _, rMeta := range reals {
		endpoints = append(endpoints, rMeta)
	}
	// generateHashRing() output depends on endpoints order
	slices.SortFunc(endpoints, compareMaglevReals)

	chRing := generateHashRing(endpoints, k.ringSize)

	// write on the eBPF map the positions of the chRing
	// that have been updated
	var deltaIndex []uint32
	var deltaRealNum []uint32
	for i := 0; i < len(chRing); i++ {
		if chRing[i] != k.svcMap[svcKey].chRing[i] {
			k.svcMap[svcKey].chRing[i] = chRing[i]
			index := k.svcMap[svcKey].meta.VipNum*k.ringSize + uint32(i)
			deltaIndex = append(deltaIndex, index)
			deltaRealNum = append(deltaRealNum, uint32(chRing[i]))
		}
	}

	if _, err := k.objs.ChRings.BatchUpdate(deltaIndex, deltaRealNum, nil); err != nil {
		if newReal {
			// Rollback Real creation
			k.realsNums.PushBack(realNum)
			if rollbackErr := k.objs.Reals.Delete(realNum); rollbackErr != nil {
				k.logger.Error("unable to rollback Reals map insertion after a failed chRings BatchUpdate",
					slog.Any("error", err),
				)
				return err
			}
		}
		return err
	}

	k.svcMap[svcKey] = vip{
		meta:   k.svcMap[svcKey].meta,
		reals:  reals,
		chRing: k.svcMap[svcKey].chRing,
	}

	switch action {
	case addKatranRealserver:
		k.realsRefs[realNum]++
	case updateKatranRealserver:
		// NOOP
	case removeKatranRealserver:
		k.realsRefs[realNum]--
		if err := k.objs.RealsWeights.Delete(rWeightKey); err != nil {
			// no need to fail here
			k.logger.Warn("unable to delete real weight mapping",
				slog.Any("error", err),
			)
		}
		if k.realsRefs[realNum] == 0 {
			// cannot use .Delete() with Reals cause it's a BPF_MAP_TYPE_ARRAY, setting the value to zero has the same effect
			if err := k.objs.Reals.Update(realNum, balancerWmfRealDefinition{}, ebpf.UpdateExist); err != nil {
				// no need to fail here, we got an orphan real definition that's been efectevely removed from every service, no harm done
				k.logger.Warn("unable to delete realserver from reals eBPF map",
					slog.Any("error", err),
				)
			}
			delete(k.realsRefs, realNum)
			delete(k.realsMap, realNum)
			k.realsSize--
			k.realsNums.PushBack(realNum)
		}
	}

	return nil
}

func (k *KatranForwardingPlane) SetMacAddress(address string) error {
	var mac [6]byte

	hexParts := strings.Split(address, ":")
	if len(hexParts) != 6 {
		return ErrorInvalidMACAddress
	}

	for i, hexPart := range hexParts {
		byteValue, err := strconv.ParseUint(hexPart, 16, 8)
		if err != nil {
			return fmt.Errorf("%w: %w", ErrorInvalidMACAddress, err)
		}
		mac[i] = byte(byteValue)
	}

	value := macToUint64LE(mac)

	if err := k.objs.CtlArray.Put(macAddressIndex, balancerCtlValue{Value: value}); err != nil {
		return fmt.Errorf("unable to write MAC address on ctl_array eBPF map: %w", err)
	}

	return nil
}

type KatranVipStats struct {
	Bytes   float64
	Packets float64
}

type KatranStats struct {
	VipStats          map[string]KatranVipStats
	FallbackGlobalLRU float64 // should be 0 in production
	InvalidRealID     float64 // real ID out of bounds in Reals map
	MissingReals      float64 // returned real ID is 0, usually caused by VIP not having configured any real
}

func (k *KatranForwardingPlane) GetStats() (*KatranStats, error) {
	if !k.loaded {
		return nil, ErrorBPFNotLoaded
	}

	possibleCPU := uint32(k.possibleCPU)

	var cursor ebpf.MapBatchCursor
	statsKeys := make([]uint32, k.statsCap)
	statsValues := make([]balancerLbStats, k.statsCap*possibleCPU)
	if _, err := k.objs.Stats.BatchLookup(&cursor, statsKeys, statsValues, nil); err != nil {
		// technically err could be ebpf.ErrKeyNoExist and that would mean that we've already read
		// Stats entirely.
		return nil, err
	}

	values := make(map[uint32]balancerLbStats, k.statsCap)
	for i := uint32(0); i < k.statsCap; i++ {
		index := statsKeys[i]
		stat := balancerLbStats{}
		for j := uint32(0); j < possibleCPU; j++ {
			stat.V1 += statsValues[(i*possibleCPU)+j].V1
			stat.V2 += statsValues[(i*possibleCPU)+j].V2
		}
		values[index] = stat
	}

	ret := &KatranStats{
		VipStats:          make(map[string]KatranVipStats, k.svcSize),
		FallbackGlobalLRU: float64(values[k.svcCap+FallbackLRUCounter].V1),
		InvalidRealID:     float64(values[k.svcCap+ConstantHashingDropStats].V1),
		MissingReals:      float64(values[k.svcCap+ConstantHashingDropStats].V2),
	}

	// now that we have all the stats values dumped, let's annotate them

	k.mutex.RLock()
	defer k.mutex.RUnlock()

	for bpfSvc, vip := range k.svcMap {
		index := vip.meta.VipNum
		vipStats, present := values[index]
		if !present {
			k.logger.Warn("VIP stats missing",
				slog.Int("vip_num", int(index)),
			)
			continue
		}
		addr := bytesToAddr(bpfSvc.Vip)
		port := binary.LittleEndian.Uint16([]byte{
			byte(bpfSvc.Port >> 8),
			byte(bpfSvc.Port),
		})
		svcKey := netip.AddrPortFrom(addr, port).String()
		svcStats := KatranVipStats{
			Bytes:   float64(vipStats.V2),
			Packets: float64(vipStats.V1),
		}
		ret.VipStats[svcKey] = svcStats
	}

	return ret, nil
}

func (k *KatranForwardingPlane) Pin() error {
	if !k.loaded {
		return ErrorBPFNotLoaded
	}
	if err := k.objs.BalancerIngress.Pin(programPinPath); err != nil {
		return err
	}

	if err := k.link.Pin(linkPinPath); err != nil {
		return err
	}

	for cpu, m := range k.lruMaps {
		path := filepath.Join(basePinPath, fmt.Sprintf("katran_lru_%d", cpu))
		if err := m.Pin(path); err != nil {
			return err
		}
	}

	maps := []struct {
		ebpfMap *ebpf.Map
		name    string
	}{
		{k.objs.ChRings, "ch_rings"},
		{k.objs.CtlArray, "ctl_array"},
		{k.objs.DecapVipStats, "decap_vip_stats"},
		{k.objs.FallbackCache, "fallback_cache"},
		{k.objs.LruMapping, "lru_mapping"},
		{k.objs.LruMissStats, "lru_miss_stats"},
		{k.objs.QuicStatsMap, "quic_stats_map"},
		{k.objs.Reals, "reals"},
		{k.objs.RealsStats, "reals_stats"},
		{k.objs.RealsWeights, "reals_weights"},
		{k.objs.ServerIdMap, "server_id_map"},
		{k.objs.ServerIdStats, "server_id_stats"},
		{k.objs.StableRtStats, "stable_rt_stats"},
		{k.objs.Stats, "stats"},
		{k.objs.TprStatsMap, "tpr_stats_map"},
		{k.objs.VipMap, "vip_map"},
		{k.objs.VipMissStats, "vip_miss_stats"},
	}

	for _, m := range maps {
		path := filepath.Join(basePinPath, m.name)
		if err := m.ebpfMap.Pin(path); err != nil {
			return err
		}
	}

	k.pinned = true
	return nil
}

func (k *KatranForwardingPlane) Unpin() error {
	if err := k.objs.BalancerIngress.Unpin(); err != nil {
		return err
	}

	if err := k.link.Unpin(); err != nil {
		return err
	}

	maps := []*ebpf.Map{
		k.objs.ChRings,
		k.objs.CtlArray,
		k.objs.DecapVipStats,
		k.objs.FallbackCache,
		k.objs.LruMapping,
		k.objs.LruMissStats,
		k.objs.QuicStatsMap,
		k.objs.Reals,
		k.objs.RealsStats,
		k.objs.RealsWeights,
		k.objs.ServerIdMap,
		k.objs.ServerIdStats,
		k.objs.StableRtStats,
		k.objs.Stats,
		k.objs.TprStatsMap,
		k.objs.VipMap,
		k.objs.VipMissStats,
	}

	for _, m := range maps {
		if err := m.Unpin(); err != nil {
			return err
		}
	}

	for _, m := range k.lruMaps {
		if err := m.Unpin(); err != nil {
			return err
		}
	}

	k.pinned = false
	return nil
}

func (k *KatranForwardingPlane) Close() {
	if k.attached {
		// We need to detach (close link FD) to avoid a FD leak
		// link will only be removed if pinned = false
		if err := k.detach(); err != nil {
			k.logger.Error("unable to detach",
				slog.Any("error", err),
			)
		}
	}

	if k.loaded {
		if err := k.objs.Close(); err != nil {
			k.logger.Error("unable to close",
				slog.Any("error", err),
			)
		}
		for _, m := range k.lruMaps {
			if err := m.Close(); err != nil {
				k.logger.Error("unable to close",
					slog.Any("error", err),
				)
			}
		}
	}
}

// helper that tranlastes from Service to balancerWmfVipDefinition
func serviceToVip(svc Service) balancerWmfVipDefinition {
	ret := balancerWmfVipDefinition{
		Vip: addrToBytes(svc.Address),
		Port: binary.BigEndian.Uint16([]byte{
			byte(svc.Port),
			byte(svc.Port >> 8),
		}),
	}

	switch svc.Protocol {
	case TCP:
		ret.Proto = syscall.IPPROTO_TCP
	case UDP:
		ret.Proto = syscall.IPPROTO_UDP
	}

	return ret
}

func emptyReal(r balancerWmfRealDefinition) bool {
	return slices.Equal(r.Dst[:], []uint32{0, 0, 0, 0})
}

// translate a netip.Addr to [4]uint32 in Big Endian as expected by Katran
func addrToBytes(a netip.Addr) [4]uint32 {
	var ret [4]uint32

	aBytes := a.AsSlice() // already in BigEndian
	// we leverage NativeEndian because we want to avoid additional
	// byte order transformations
	if a.Is4() {
		ret[0] = binary.NativeEndian.Uint32(aBytes[:])
	} else {
		ret[0] = binary.NativeEndian.Uint32(aBytes[:4])
		ret[1] = binary.NativeEndian.Uint32(aBytes[4:8])
		ret[2] = binary.NativeEndian.Uint32(aBytes[8:12])
		ret[3] = binary.NativeEndian.Uint32(aBytes[12:16])
	}

	return ret
}

// translate from Katran's [4]uint32 to netip.Addr
func bytesToAddr(b [4]uint32) netip.Addr {
	if b[1] == 0 && b[2] == 0 && b[3] == 0 {
		bytes := [4]byte{}
		binary.NativeEndian.PutUint32(bytes[:], b[0])
		return netip.AddrFrom4(bytes)
	}

	bytes := [16]byte{}
	binary.NativeEndian.PutUint32(bytes[:4], b[0])
	binary.NativeEndian.PutUint32(bytes[4:8], b[1])
	binary.NativeEndian.PutUint32(bytes[8:12], b[2])
	binary.NativeEndian.PutUint32(bytes[12:16], b[3])
	return netip.AddrFrom16(bytes)
}

// translate a MAC address to uint64 LE
func macToUint64LE(mac [6]byte) uint64 {
	var ret uint64
	for index, value := range mac {
		ret |= uint64(value) << (8 * index)
	}
	return ret
}
