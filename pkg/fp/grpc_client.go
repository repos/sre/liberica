package fp

import (
	"context"
	"fmt"
	"log/slog"
	"net/netip"
	"time"

	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/fp"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"
	pbsvc "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/service"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

type FPClient interface {
	ListServices(context.Context) ([]Service, error)
	ListRealservers(context.Context, Service) ([]Realserver, error)
	CreateService(context.Context, Service) error
	DeleteService(context.Context, Service) error
	CreateRealserver(context.Context, Service, Realserver) error
	UpdateRealserver(context.Context, Service, Realserver) error
	DeleteRealserver(context.Context, Service, Realserver) error
	Close()
}

type FPGrpcClient struct {
	logger *slog.Logger
	conn   *grpc.ClientConn
}

func NewGrpcClient(logger *slog.Logger, network, address string) (FPClient, error) {
	addr := grpcutils.GetConnectAddress(network, address)

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, grpc.WithKeepaliveParams(grpcutils.KeepaliveClientParameters))
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// DialContext returns before connecting, ctx only controls the setup steps. More details on https://pkg.go.dev/google.golang.org/grpc@v1.46.2#DialContext
	conn, err := grpc.DialContext(ctx, addr, opts...)
	if err != nil {
		return nil, fmt.Errorf("unable to create connection: %w", err)
	}

	return &FPGrpcClient{
		logger: logger,
		conn:   conn,
	}, nil
}

func (client *FPGrpcClient) ListServices(ctx context.Context) ([]Service, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}

	c := pb.NewForwardingPlaneServiceClient(client.conn)

	req := &pb.ListServicesRequest{}

	res, err := c.ListServices(ctx, req, grpc.WaitForReady(true))
	if status.Code(err) != codes.OK {
		return nil, fmt.Errorf("unable to list services: %w", err)
	}

	var out []Service
	for _, pbSvc := range res.Svc {
		address, err := netip.ParseAddr(pbSvc.Address)
		if err != nil {
			client.logger.Warn("invalid address returned by forwarding plane grpc server",
				slog.String("address", pbSvc.Address),
				slog.Any("error", err),
			)
			continue
		}
		af, err := NewAddressFamily(pbSvc.Family)
		if err != nil {
			client.logger.Warn("invalid address family returned by forwarding plane grpc server",
				slog.Int("address_family", int(pbSvc.Family)),
				slog.Any("error", err),
			)
		}
		protocol, err := NewProtocol(pbSvc.Protocol)
		if err != nil {
			client.logger.Warn("invalid protocol returned by forwarding grpc server",
				slog.Int("protocol", int(pbSvc.Protocol)),
				slog.Any("error", err),
			)
		}
		svc := Service{
			Address:   address,
			Scheduler: pbSvc.Scheduler,
			Port:      uint16(pbSvc.Port),
			Family:    af,
			Protocol:  protocol,
		}
		out = append(out, svc)
	}

	return out, nil
}

func (client *FPGrpcClient) ListRealservers(ctx context.Context, svc Service) ([]Realserver, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}
	c := pb.NewForwardingPlaneServiceClient(client.conn)

	req := &pb.ListRealserversRequest{
		Svc: &pbsvc.Service{
			Address:   svc.Address.String(),
			Scheduler: svc.Scheduler,
			Port:      uint32(svc.Port),
			Family:    svc.Family.AsProto(),
			Protocol:  svc.Protocol.AsProto(),
		},
	}

	res, err := c.ListRealservers(ctx, req, grpc.WaitForReady(true))
	if status.Code(err) != codes.OK {
		return nil, fmt.Errorf("unable to list realservers: %w", err)
	}
	var out []Realserver
	for _, pbRs := range res.Rs {
		address, err := netip.ParseAddr(pbRs.Address)
		if err != nil {
			slog.Warn("invalid address returned by forwarding plane grpc server",
				slog.String("address", pbRs.Address),
				slog.Any("error", err),
			)
		}
		ft, err := NewForwardType(pbRs.ForwardType)
		if err != nil {
			slog.Warn("invalid ForwardType returned by forwarding plane grpc server",
				slog.Int("forward_type", int(pbRs.ForwardType)),
				slog.Any("error", err),
			)
		}
		rs := Realserver{
			Address:     address,
			Weight:      pbRs.Weight,
			Mark:        pbRs.Mark,
			ForwardType: ft,
		}
		out = append(out, rs)
	}
	return out, nil
}
func (client *FPGrpcClient) CreateService(ctx context.Context, svc Service) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	c := pb.NewForwardingPlaneServiceClient(client.conn)
	req := &pb.CreateServiceRequest{
		Svc: &pbsvc.Service{
			Address:   svc.Address.String(),
			Scheduler: svc.Scheduler,
			Port:      uint32(svc.Port),
			Family:    svc.Family.AsProto(),
			Protocol:  svc.Protocol.AsProto(),
		},
	}

	if _, err := c.CreateService(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to create service: %w", err)
	}
	return nil
}

func (client *FPGrpcClient) DeleteService(ctx context.Context, svc Service) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	c := pb.NewForwardingPlaneServiceClient(client.conn)
	req := &pb.DeleteServiceRequest{
		Svc: &pbsvc.Service{
			Address:   svc.Address.String(),
			Scheduler: svc.Scheduler,
			Port:      uint32(svc.Port),
			Family:    svc.Family.AsProto(),
			Protocol:  svc.Protocol.AsProto(),
		},
	}

	if _, err := c.DeleteService(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to delete service: %w", err)
	}
	return nil
}

func (client *FPGrpcClient) CreateRealserver(ctx context.Context, svc Service, rs Realserver) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	c := pb.NewForwardingPlaneServiceClient(client.conn)

	req := &pb.CreateRealserverRequest{
		Svc: &pbsvc.Service{
			Address:   svc.Address.String(),
			Scheduler: svc.Scheduler,
			Port:      uint32(svc.Port),
			Family:    svc.Family.AsProto(),
			Protocol:  svc.Protocol.AsProto(),
		},
		Rs: &pbrs.Realserver{
			Address:     rs.Address.String(),
			Weight:      rs.Weight,
			Mark:        rs.Mark,
			ForwardType: rs.ForwardType.AsProto(),
		},
	}

	if _, err := c.CreateRealserver(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to create realserver: %w", err)
	}

	return nil
}

func (client *FPGrpcClient) UpdateRealserver(ctx context.Context, svc Service, rs Realserver) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	c := pb.NewForwardingPlaneServiceClient(client.conn)

	req := &pb.UpdateRealserverRequest{
		Svc: &pbsvc.Service{
			Address:   svc.Address.String(),
			Scheduler: svc.Scheduler,
			Port:      uint32(svc.Port),
			Family:    svc.Family.AsProto(),
			Protocol:  svc.Protocol.AsProto(),
		},
		Rs: &pbrs.Realserver{
			Address:     rs.Address.String(),
			Weight:      rs.Weight,
			Mark:        rs.Mark,
			ForwardType: rs.ForwardType.AsProto(),
		},
	}

	if _, err := c.UpdateRealserver(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to update realserver: %w", err)
	}

	return nil
}

func (client *FPGrpcClient) DeleteRealserver(ctx context.Context, svc Service, rs Realserver) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	c := pb.NewForwardingPlaneServiceClient(client.conn)

	req := &pb.DeleteRealserverRequest{
		Svc: &pbsvc.Service{
			Address:   svc.Address.String(),
			Scheduler: svc.Scheduler,
			Port:      uint32(svc.Port),
			Family:    svc.Family.AsProto(),
			Protocol:  svc.Protocol.AsProto(),
		},
		Rs: &pbrs.Realserver{
			Address:     rs.Address.String(),
			Weight:      rs.Weight,
			Mark:        rs.Mark,
			ForwardType: rs.ForwardType.AsProto(),
		},
	}

	if _, err := c.DeleteRealserver(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to delete realserver: %w", err)
	}

	return nil
}

func (client *FPGrpcClient) Close() {
	if client.conn != nil {
		client.conn.Close()
	}
}
