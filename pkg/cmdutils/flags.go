package cmdutils

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// map command flags to viper config settings
// cfgMap key should match the command flag name
// and the value must be the config key
func BindFlagsToSettings(cfgMap map[string]string, cmd *cobra.Command) {
	for flagName, cfgKey := range cfgMap {
		viper.BindPFlag(cfgKey, cmd.Flags().Lookup(flagName))
		if viper.IsSet(cfgKey) {
			cmd.Flags().Set(flagName, viper.GetString(cfgKey))
		}
	}
}
