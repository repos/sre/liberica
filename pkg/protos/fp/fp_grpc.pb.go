// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package fp

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ForwardingPlaneServiceClient is the client API for ForwardingPlaneService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ForwardingPlaneServiceClient interface {
	CreateService(ctx context.Context, in *CreateServiceRequest, opts ...grpc.CallOption) (*CreateServiceResponse, error)
	DeleteService(ctx context.Context, in *DeleteServiceRequest, opts ...grpc.CallOption) (*DeleteServiceResponse, error)
	ListServices(ctx context.Context, in *ListServicesRequest, opts ...grpc.CallOption) (*ListServicesResponse, error)
	ListRealservers(ctx context.Context, in *ListRealserversRequest, opts ...grpc.CallOption) (*ListRealserversResponse, error)
	CreateRealserver(ctx context.Context, in *CreateRealserverRequest, opts ...grpc.CallOption) (*CreateRealserverResponse, error)
	UpdateRealserver(ctx context.Context, in *UpdateRealserverRequest, opts ...grpc.CallOption) (*UpdateRealserverResponse, error)
	DeleteRealserver(ctx context.Context, in *DeleteRealserverRequest, opts ...grpc.CallOption) (*DeleteRealserverResponse, error)
}

type forwardingPlaneServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewForwardingPlaneServiceClient(cc grpc.ClientConnInterface) ForwardingPlaneServiceClient {
	return &forwardingPlaneServiceClient{cc}
}

func (c *forwardingPlaneServiceClient) CreateService(ctx context.Context, in *CreateServiceRequest, opts ...grpc.CallOption) (*CreateServiceResponse, error) {
	out := new(CreateServiceResponse)
	err := c.cc.Invoke(ctx, "/fp.ForwardingPlaneService/CreateService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *forwardingPlaneServiceClient) DeleteService(ctx context.Context, in *DeleteServiceRequest, opts ...grpc.CallOption) (*DeleteServiceResponse, error) {
	out := new(DeleteServiceResponse)
	err := c.cc.Invoke(ctx, "/fp.ForwardingPlaneService/DeleteService", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *forwardingPlaneServiceClient) ListServices(ctx context.Context, in *ListServicesRequest, opts ...grpc.CallOption) (*ListServicesResponse, error) {
	out := new(ListServicesResponse)
	err := c.cc.Invoke(ctx, "/fp.ForwardingPlaneService/ListServices", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *forwardingPlaneServiceClient) ListRealservers(ctx context.Context, in *ListRealserversRequest, opts ...grpc.CallOption) (*ListRealserversResponse, error) {
	out := new(ListRealserversResponse)
	err := c.cc.Invoke(ctx, "/fp.ForwardingPlaneService/ListRealservers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *forwardingPlaneServiceClient) CreateRealserver(ctx context.Context, in *CreateRealserverRequest, opts ...grpc.CallOption) (*CreateRealserverResponse, error) {
	out := new(CreateRealserverResponse)
	err := c.cc.Invoke(ctx, "/fp.ForwardingPlaneService/CreateRealserver", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *forwardingPlaneServiceClient) UpdateRealserver(ctx context.Context, in *UpdateRealserverRequest, opts ...grpc.CallOption) (*UpdateRealserverResponse, error) {
	out := new(UpdateRealserverResponse)
	err := c.cc.Invoke(ctx, "/fp.ForwardingPlaneService/UpdateRealserver", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *forwardingPlaneServiceClient) DeleteRealserver(ctx context.Context, in *DeleteRealserverRequest, opts ...grpc.CallOption) (*DeleteRealserverResponse, error) {
	out := new(DeleteRealserverResponse)
	err := c.cc.Invoke(ctx, "/fp.ForwardingPlaneService/DeleteRealserver", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ForwardingPlaneServiceServer is the server API for ForwardingPlaneService service.
// All implementations must embed UnimplementedForwardingPlaneServiceServer
// for forward compatibility
type ForwardingPlaneServiceServer interface {
	CreateService(context.Context, *CreateServiceRequest) (*CreateServiceResponse, error)
	DeleteService(context.Context, *DeleteServiceRequest) (*DeleteServiceResponse, error)
	ListServices(context.Context, *ListServicesRequest) (*ListServicesResponse, error)
	ListRealservers(context.Context, *ListRealserversRequest) (*ListRealserversResponse, error)
	CreateRealserver(context.Context, *CreateRealserverRequest) (*CreateRealserverResponse, error)
	UpdateRealserver(context.Context, *UpdateRealserverRequest) (*UpdateRealserverResponse, error)
	DeleteRealserver(context.Context, *DeleteRealserverRequest) (*DeleteRealserverResponse, error)
	mustEmbedUnimplementedForwardingPlaneServiceServer()
}

// UnimplementedForwardingPlaneServiceServer must be embedded to have forward compatible implementations.
type UnimplementedForwardingPlaneServiceServer struct {
}

func (UnimplementedForwardingPlaneServiceServer) CreateService(context.Context, *CreateServiceRequest) (*CreateServiceResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateService not implemented")
}
func (UnimplementedForwardingPlaneServiceServer) DeleteService(context.Context, *DeleteServiceRequest) (*DeleteServiceResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteService not implemented")
}
func (UnimplementedForwardingPlaneServiceServer) ListServices(context.Context, *ListServicesRequest) (*ListServicesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListServices not implemented")
}
func (UnimplementedForwardingPlaneServiceServer) ListRealservers(context.Context, *ListRealserversRequest) (*ListRealserversResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListRealservers not implemented")
}
func (UnimplementedForwardingPlaneServiceServer) CreateRealserver(context.Context, *CreateRealserverRequest) (*CreateRealserverResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateRealserver not implemented")
}
func (UnimplementedForwardingPlaneServiceServer) UpdateRealserver(context.Context, *UpdateRealserverRequest) (*UpdateRealserverResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateRealserver not implemented")
}
func (UnimplementedForwardingPlaneServiceServer) DeleteRealserver(context.Context, *DeleteRealserverRequest) (*DeleteRealserverResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteRealserver not implemented")
}
func (UnimplementedForwardingPlaneServiceServer) mustEmbedUnimplementedForwardingPlaneServiceServer() {
}

// UnsafeForwardingPlaneServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ForwardingPlaneServiceServer will
// result in compilation errors.
type UnsafeForwardingPlaneServiceServer interface {
	mustEmbedUnimplementedForwardingPlaneServiceServer()
}

func RegisterForwardingPlaneServiceServer(s *grpc.Server, srv ForwardingPlaneServiceServer) {
	s.RegisterService(&_ForwardingPlaneService_serviceDesc, srv)
}

func _ForwardingPlaneService_CreateService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateServiceRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ForwardingPlaneServiceServer).CreateService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fp.ForwardingPlaneService/CreateService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ForwardingPlaneServiceServer).CreateService(ctx, req.(*CreateServiceRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ForwardingPlaneService_DeleteService_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteServiceRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ForwardingPlaneServiceServer).DeleteService(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fp.ForwardingPlaneService/DeleteService",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ForwardingPlaneServiceServer).DeleteService(ctx, req.(*DeleteServiceRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ForwardingPlaneService_ListServices_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListServicesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ForwardingPlaneServiceServer).ListServices(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fp.ForwardingPlaneService/ListServices",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ForwardingPlaneServiceServer).ListServices(ctx, req.(*ListServicesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ForwardingPlaneService_ListRealservers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListRealserversRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ForwardingPlaneServiceServer).ListRealservers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fp.ForwardingPlaneService/ListRealservers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ForwardingPlaneServiceServer).ListRealservers(ctx, req.(*ListRealserversRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ForwardingPlaneService_CreateRealserver_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateRealserverRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ForwardingPlaneServiceServer).CreateRealserver(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fp.ForwardingPlaneService/CreateRealserver",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ForwardingPlaneServiceServer).CreateRealserver(ctx, req.(*CreateRealserverRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ForwardingPlaneService_UpdateRealserver_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateRealserverRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ForwardingPlaneServiceServer).UpdateRealserver(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fp.ForwardingPlaneService/UpdateRealserver",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ForwardingPlaneServiceServer).UpdateRealserver(ctx, req.(*UpdateRealserverRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ForwardingPlaneService_DeleteRealserver_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteRealserverRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ForwardingPlaneServiceServer).DeleteRealserver(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fp.ForwardingPlaneService/DeleteRealserver",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ForwardingPlaneServiceServer).DeleteRealserver(ctx, req.(*DeleteRealserverRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ForwardingPlaneService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "fp.ForwardingPlaneService",
	HandlerType: (*ForwardingPlaneServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateService",
			Handler:    _ForwardingPlaneService_CreateService_Handler,
		},
		{
			MethodName: "DeleteService",
			Handler:    _ForwardingPlaneService_DeleteService_Handler,
		},
		{
			MethodName: "ListServices",
			Handler:    _ForwardingPlaneService_ListServices_Handler,
		},
		{
			MethodName: "ListRealservers",
			Handler:    _ForwardingPlaneService_ListRealservers_Handler,
		},
		{
			MethodName: "CreateRealserver",
			Handler:    _ForwardingPlaneService_CreateRealserver_Handler,
		},
		{
			MethodName: "UpdateRealserver",
			Handler:    _ForwardingPlaneService_UpdateRealserver_Handler,
		},
		{
			MethodName: "DeleteRealserver",
			Handler:    _ForwardingPlaneService_DeleteRealserver_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "pkg/protos/fp.proto",
}
