// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package hcforwarder

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// HCForwarderServiceClient is the client API for HCForwarderService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type HCForwarderServiceClient interface {
	ListRealservers(ctx context.Context, in *ListRealserversRequest, opts ...grpc.CallOption) (*ListRealserversResponse, error)
	CreateRealserver(ctx context.Context, in *CreateRealserverRequest, opts ...grpc.CallOption) (*CreateRealserverResponse, error)
	DeleteRealserver(ctx context.Context, in *DeleteRealserverRequest, opts ...grpc.CallOption) (*DeleteRealserverResponse, error)
}

type hCForwarderServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewHCForwarderServiceClient(cc grpc.ClientConnInterface) HCForwarderServiceClient {
	return &hCForwarderServiceClient{cc}
}

func (c *hCForwarderServiceClient) ListRealservers(ctx context.Context, in *ListRealserversRequest, opts ...grpc.CallOption) (*ListRealserversResponse, error) {
	out := new(ListRealserversResponse)
	err := c.cc.Invoke(ctx, "/hcforwarder.HCForwarderService/ListRealservers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hCForwarderServiceClient) CreateRealserver(ctx context.Context, in *CreateRealserverRequest, opts ...grpc.CallOption) (*CreateRealserverResponse, error) {
	out := new(CreateRealserverResponse)
	err := c.cc.Invoke(ctx, "/hcforwarder.HCForwarderService/CreateRealserver", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hCForwarderServiceClient) DeleteRealserver(ctx context.Context, in *DeleteRealserverRequest, opts ...grpc.CallOption) (*DeleteRealserverResponse, error) {
	out := new(DeleteRealserverResponse)
	err := c.cc.Invoke(ctx, "/hcforwarder.HCForwarderService/DeleteRealserver", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// HCForwarderServiceServer is the server API for HCForwarderService service.
// All implementations must embed UnimplementedHCForwarderServiceServer
// for forward compatibility
type HCForwarderServiceServer interface {
	ListRealservers(context.Context, *ListRealserversRequest) (*ListRealserversResponse, error)
	CreateRealserver(context.Context, *CreateRealserverRequest) (*CreateRealserverResponse, error)
	DeleteRealserver(context.Context, *DeleteRealserverRequest) (*DeleteRealserverResponse, error)
	mustEmbedUnimplementedHCForwarderServiceServer()
}

// UnimplementedHCForwarderServiceServer must be embedded to have forward compatible implementations.
type UnimplementedHCForwarderServiceServer struct {
}

func (UnimplementedHCForwarderServiceServer) ListRealservers(context.Context, *ListRealserversRequest) (*ListRealserversResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListRealservers not implemented")
}
func (UnimplementedHCForwarderServiceServer) CreateRealserver(context.Context, *CreateRealserverRequest) (*CreateRealserverResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateRealserver not implemented")
}
func (UnimplementedHCForwarderServiceServer) DeleteRealserver(context.Context, *DeleteRealserverRequest) (*DeleteRealserverResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteRealserver not implemented")
}
func (UnimplementedHCForwarderServiceServer) mustEmbedUnimplementedHCForwarderServiceServer() {}

// UnsafeHCForwarderServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to HCForwarderServiceServer will
// result in compilation errors.
type UnsafeHCForwarderServiceServer interface {
	mustEmbedUnimplementedHCForwarderServiceServer()
}

func RegisterHCForwarderServiceServer(s *grpc.Server, srv HCForwarderServiceServer) {
	s.RegisterService(&_HCForwarderService_serviceDesc, srv)
}

func _HCForwarderService_ListRealservers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListRealserversRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HCForwarderServiceServer).ListRealservers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hcforwarder.HCForwarderService/ListRealservers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HCForwarderServiceServer).ListRealservers(ctx, req.(*ListRealserversRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HCForwarderService_CreateRealserver_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateRealserverRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HCForwarderServiceServer).CreateRealserver(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hcforwarder.HCForwarderService/CreateRealserver",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HCForwarderServiceServer).CreateRealserver(ctx, req.(*CreateRealserverRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HCForwarderService_DeleteRealserver_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteRealserverRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HCForwarderServiceServer).DeleteRealserver(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hcforwarder.HCForwarderService/DeleteRealserver",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HCForwarderServiceServer).DeleteRealserver(ctx, req.(*DeleteRealserverRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _HCForwarderService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "hcforwarder.HCForwarderService",
	HandlerType: (*HCForwarderServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListRealservers",
			Handler:    _HCForwarderService_ListRealservers_Handler,
		},
		{
			MethodName: "CreateRealserver",
			Handler:    _HCForwarderService_CreateRealserver_Handler,
		},
		{
			MethodName: "DeleteRealserver",
			Handler:    _HCForwarderService_DeleteRealserver_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "pkg/protos/hcforwarder.proto",
}
