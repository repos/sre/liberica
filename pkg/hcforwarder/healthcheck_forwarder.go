/*
Copyright © 2023 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
//go:generate go run github.com/cilium/ebpf/cmd/bpf2go -cc clang -cflags -O2 ipip healthchecking_ipip.c -- -I../../headers
package hcforwarder

import (
	"encoding/binary"
	"errors"
	"fmt"
	"log/slog"
	"net"
	"net/netip"
	"os"
	"path/filepath"
	"sync"

	"github.com/cilium/ebpf"
	"github.com/florianl/go-tc"
	"github.com/florianl/go-tc/core"
	"github.com/mdlayher/netlink"
	"golang.org/x/sys/unix"
)

// Indexes defined on healthchecking_ipip.c#L94-L95
const (
	v4IndexPosition = 1
	v6IndexPosition = 2
)

// defined on healthchecking_ipip.c#L49
const flagv6Address = 1 << 0

const (
	bpfName        = "liberica-hcforwarder"
	basePinPath    = "/sys/fs/bpf"
	programPinPath = "/sys/fs/bpf/hcforwarder"
)

var (
	ErrorBPFNotLoaded  = errors.New("BPF program not loaded")
	ErrorHFNotAttached = errors.New("not attached to any interface")
)

// Check if clsact (https://lwn.net/Articles/671458/) is present
// on a specific interface
func isClsactPresent(logger *slog.Logger, hcInterface net.Interface) bool {
	// open a rtnetlink socket
	rtnl, err := tc.Open(&tc.Config{})
	if err != nil {
		logger.Error("could not open rtnetlink socket", slog.Any("error", err))
		return false
	}
	defer func() {
		if err := rtnl.Close(); err != nil {
			logger.Error("could not close rtnetlink socket", slog.Any("error", err))
		}
	}()

	// For enhanced error messages from the kernel, it is recommended to set
	// option `NETLINK_EXT_ACK`, which is supported since 4.12 kernel.
	//
	// If not supported, `unix.ENOPROTOOPT` is returned.

	err = rtnl.SetOption(netlink.ExtendedAcknowledge, true)
	if err != nil {
		logger.Error("could not set option ExtendedAcknowledge", slog.Any("error", err))
		return false
	}

	// get all the qdiscs from all interfaces
	qdiscs, err := rtnl.Qdisc().Get()
	if err != nil {
		logger.Error("could not get qdiscs", slog.Any("error", err))
		return false
	}

	for _, qdisc := range qdiscs {
		iface, err := net.InterfaceByIndex(int(qdisc.Ifindex))
		if err != nil {
			logger.Error("could not get interface",
				slog.Int64("interface_index", int64(qdisc.Ifindex)),
				slog.Any("error", err))
			continue
		}
		if iface.Index == hcInterface.Index {
			if qdisc.Kind == "clsact" {
				return true
			}
		}
	}
	return false
}

type HealthcheckForwarderOption func(*HealthcheckForwarder)

type HealthcheckForwarder struct {
	objs            ipipObjects
	logger          *slog.Logger
	egressInterface *net.Interface
	v4Interface     *net.Interface
	v6Interface     *net.Interface
	tcnl            *tc.Tc
	data            map[uint32]ipipWmfHcRealDefinition
	programFD       int
	m               sync.RWMutex
	capacity        uint32 // underlying eBPF map has a fixed capacity that needs to be enforced
	size            uint32
	pinned          bool // true if eBPF program and maps are pinned
	loaded          bool
	attached        bool
}

func NewHealthcheckForwarder(options ...HealthcheckForwarderOption) *HealthcheckForwarder {
	hf := &HealthcheckForwarder{
		objs:     ipipObjects{},
		loaded:   false,
		attached: false,
	}
	for _, opt := range options {
		opt(hf)
	}

	return hf
}

// Set interface used to send IPv4 or IPv6 healthchecks.
// Check consts of this package for valid mapIndex values
func (hf *HealthcheckForwarder) SetInterface(mapIndex uint32, iface *net.Interface) error {
	if !hf.loaded {
		return ErrorBPFNotLoaded
	}

	if err := hf.objs.HcCtrlMap.Put(mapIndex, uint32(iface.Index)); err != nil {
		hf.logger.Error("failed to write HcCtrlMap",
			slog.Int("interface_index", iface.Index),
			slog.String("interface_name", iface.Name),
			slog.Any("error", err))
		return err
	}

	if mapIndex == v4IndexPosition {
		hf.v4Interface = iface
	} else {
		hf.v6Interface = iface
	}
	return nil
}

// Attach eBPF program to egress interface
func (hf *HealthcheckForwarder) AttachToInterface(egressInterface *net.Interface) error {
	if !hf.loaded {
		return ErrorBPFNotLoaded
	}
	if hf.attached {
		return fmt.Errorf("already attached to %v", hf.egressInterface.Name)
	}

	if !isClsactPresent(hf.logger, *egressInterface) {
		return fmt.Errorf("clsact not found on %v", egressInterface.Name)
	}

	hf.egressInterface = egressInterface

	var err error
	hf.tcnl, err = tc.Open(&tc.Config{})
	if err != nil {
		hf.logger.Error("could not open rtlnetlink socket", slog.Any("error", err))
		return err
	}

	fd := uint32(hf.programFD)
	flags := uint32(0x1)
	name := bpfName

	filter := tc.Object{
		tc.Msg{
			Family:  unix.AF_UNSPEC,
			Ifindex: uint32(hf.egressInterface.Index),
			Handle:  0,
			Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
			Info:    0x300,
		},
		tc.Attribute{
			Kind: "bpf",
			BPF: &tc.Bpf{
				FD:    &fd,
				Flags: &flags,
				Name:  &name,
			},
		},
	}
	if err := hf.tcnl.Filter().Add(&filter); err != nil {
		hf.logger.Error("could not attach filter for eBPF program", slog.Any("error", err))
		hf.tcnl.Close()
		return err
	}

	hf.attached = true
	return nil
}

// Detach from egress interface
func (hf *HealthcheckForwarder) Detach() error {
	if !hf.attached {
		return ErrorHFNotAttached
	}

	msg := tc.Msg{
		Family:  unix.AF_UNSPEC,
		Ifindex: uint32(hf.egressInterface.Index),
		Handle:  0,
		Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
		// Info isn't set here because the value used on AttachToInterface isn't preserved
	}

	filters, err := hf.tcnl.Filter().Get(&msg)
	if err != nil {
		hf.logger.Error("unable to fetch filters", slog.Any("error", err))
		return err
	}

	if len(filters) == 0 {
		return fmt.Errorf("Unable to find TC filters on interface: %v\n", hf.egressInterface.Name)
	}

	removed := false
	for _, filter := range filters {
		if filter.Attribute.Kind == "bpf" && filter.Attribute.BPF != nil {
			if *filter.Attribute.BPF.Name == bpfName {
				if err := hf.tcnl.Filter().Delete(&filter); err != nil {
					return err
				}
				removed = true
			}
		}
	}

	if !removed {
		return fmt.Errorf("Unable to find TC BPF filter %v on interface: %v\n", bpfName, hf.egressInterface.Name)
	}

	hf.egressInterface = nil
	hf.attached = false

	return nil
}

// handle eBPF program & maps loading from a clean slate state
// and from a pinned state
func (hf *HealthcheckForwarder) load() error {
	if _, err := os.Stat(programPinPath); err != nil {
		// clean slate
		spec, err := loadIpip()
		if err != nil {
			return err
		}

		return spec.LoadAndAssign(&hf.objs, nil)
	}
	var err error
	hf.objs.HealthcheckEncap, err = ebpf.LoadPinnedProgram(programPinPath, nil)
	if err != nil {
		hf.logger.Error("unable to load pinned program", slog.Any("error", err))
		return err
	}
	maps := []struct {
		ebpfMap **ebpf.Map
		name    string
	}{
		{&hf.objs.HcCtrlMap, "hc_ctrl_map"},
		{&hf.objs.HcRealsMap, "hc_reals_map"},
		{&hf.objs.HcStatsMap, "hc_stats_map"},
	}
	for _, m := range maps {
		path := filepath.Join(basePinPath, m.name)
		if os.Stat(path); err != nil {
			return err
		}
		var err error
		*m.ebpfMap, err = ebpf.LoadPinnedMap(path, &ebpf.LoadPinOptions{})
		if err != nil {
			return err
		}
	}
	hf.pinned = true
	return nil
}

// Check if eBPF program is attached to the configured egress interface
func (hf *HealthcheckForwarder) isAttached() (bool, error) {
	var err error
	hf.tcnl, err = tc.Open(&tc.Config{})
	if err != nil {
		hf.logger.Error("could not open rtnetlink socket", slog.Any("error", err))
		return false, err
	}

	msg := tc.Msg{
		Family:  unix.AF_UNSPEC,
		Ifindex: uint32(hf.egressInterface.Index),
		Handle:  0,
		Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
		// Info isn't set here because the value used on AttachToInterface isn't preserved
	}

	filters, err := hf.tcnl.Filter().Get(&msg)
	if err != nil {
		hf.logger.Error("unable to fetch filters", slog.Any("error", err))
		return false, err
	}

	for _, filter := range filters {
		if filter.Attribute.Kind == "bpf" && filter.Attribute.BPF != nil {
			if *filter.Attribute.BPF.Name == bpfName {
				return true, nil
			}
		}
	}
	return false, nil
}

// Restore hf.v4Interface and hf.v6Interface based on HcCtrlMap contents
func (hf *HealthcheckForwarder) recoverInterfaces() error {
	ifaces := []struct {
		iface    **net.Interface
		position uint32
	}{
		{&hf.v4Interface, v4IndexPosition},
		{&hf.v6Interface, v6IndexPosition},
	}
	for _, iface := range ifaces {
		var index uint32
		if err := hf.objs.HcCtrlMap.Lookup(iface.position, &index); err != nil {
			return err
		}
		var err error
		*iface.iface, err = net.InterfaceByIndex(int(index))
		if err != nil {
			return err
		}
	}
	return nil
}

// Restore hf.data and hf.size based on HcRealsMap contents
func (hf *HealthcheckForwarder) recoverData() error {
	if !hf.loaded {
		return ErrorBPFNotLoaded
	}

	hf.m.Lock()
	defer hf.m.Unlock()

	entries := hf.objs.HcRealsMap.Iterate()
	var key uint32
	var value ipipWmfHcRealDefinition

	for entries.Next(&key, &value) {
		hf.data[key] = value
		hf.size++
	}

	if err := entries.Err(); err != nil {
		return err
	}
	return nil
}

// Load eBPF program, populate maps and attach to interface
func (hf *HealthcheckForwarder) Init(egressIface *net.Interface) error {
	// load eBPF program
	if err := hf.load(); err != nil {
		//	if err := loadIpipObjects(&hf.objs, nil); err != nil {
		hf.logger.Error("unable to load eBPF program", slog.Any("error", err))
		return err
	}
	hf.programFD = hf.objs.HealthcheckEncap.FD()
	hf.capacity = hf.objs.HcRealsMap.MaxEntries()
	hf.data = make(map[uint32]ipipWmfHcRealDefinition, hf.capacity)
	hf.loaded = true

	if hf.pinned {
		hf.egressInterface = egressIface
		if err := hf.recoverInterfaces(); err != nil {
			return err
		}
		if err := hf.recoverData(); err != nil {
			return err
		}
		var err error
		hf.attached, err = hf.isAttached()
		if err != nil {
			return err
		}
	} else {
		// populate hc_ctrl_map
		if err := hf.SetInterface(v4IndexPosition, hf.v4Interface); err != nil {
			return err
		}
		if err := hf.SetInterface(v6IndexPosition, hf.v6Interface); err != nil {
			return err
		}

		// attach to egress interface
		if err := hf.AttachToInterface(egressIface); err != nil {
			return err
		}
	}

	return nil
}

// Fetch eBPF program stats
func (hf *HealthcheckForwarder) GetStats() (ipipHcStats, error) {
	var stats ipipHcStats
	if !hf.attached {
		return stats, ErrorHFNotAttached
	}
	mapKey := uint32(0)
	var value []ipipHcStats

	if err := hf.objs.HcStatsMap.Lookup(mapKey, &value); err != nil {
		return stats, err
	}

	// HcStatsMap is a per cpu array (BPF_MAP_TYPE_PERCPU_ARRAY)
	for _, cpuStats := range value {
		stats.PcktsProcessed += cpuStats.PcktsProcessed
		stats.PcktsDropped += cpuStats.PcktsDropped
		stats.PcktsSkipped += cpuStats.PcktsSkipped
		stats.PcktsTooBig += cpuStats.PcktsTooBig
	}

	return stats, nil
}

// Add real server
// If the specified already exists the realserver configuration will be overwritten
func (hf *HealthcheckForwarder) AddReal(ip string, somark uint32) error {
	if !hf.loaded {
		return ErrorBPFNotLoaded
	}
	realAddr, err := netip.ParseAddr(ip)
	if err != nil {
		hf.logger.Error("invalid IP address",
			slog.String("IP", ip),
			slog.Any("error", err))
		return err
	}

	hf.m.Lock()
	defer hf.m.Unlock()

	update := false
	if _, ok := hf.data[somark]; ok {
		update = true
	}

	if !update && hf.size >= hf.capacity {
		return fmt.Errorf("hcforwarder already at full capacity (%d)", hf.capacity)
	}

	var r ipipWmfHcRealDefinition
	realBytes := realAddr.AsSlice()
	// really puzzled about this but both integration tests and dumping
	// hc_reals_map with bpftool confirm that this is working
	if realAddr.Is4() {
		r.Daddr[0] = binary.BigEndian.Uint32(realBytes)
	} else {
		r.Daddr[0] = binary.LittleEndian.Uint32(realBytes[:4])
		r.Daddr[1] = binary.LittleEndian.Uint32(realBytes[4:8])
		r.Daddr[2] = binary.LittleEndian.Uint32(realBytes[8:12])
		r.Daddr[3] = binary.LittleEndian.Uint32(realBytes[12:16])
		r.Flags |= flagv6Address
	}

	if err := hf.objs.HcRealsMap.Put(somark, r); err != nil {
		slog.Error("unable to update reals map", slog.Any("error", err))
		return err
	}

	if !update {
		hf.size += 1
	}
	hf.data[somark] = r
	return nil
}

// Return the current list of configured servers
func (hf *HealthcheckForwarder) Realservers() (map[uint32]string, error) {
	if !hf.loaded {
		return nil, ErrorBPFNotLoaded
	}
	hf.m.RLock()
	ret := make(map[uint32]string, hf.capacity)
	for somark, value := range hf.data {
		var ip netip.Addr
		if value.Flags == 0 { // IPv4
			bytes := [4]byte{}
			binary.BigEndian.PutUint32(bytes[:], value.Daddr[0])
			ip = netip.AddrFrom4(bytes)
		} else { // IPv6
			bytes := [16]byte{}
			binary.LittleEndian.PutUint32(bytes[:4], value.Daddr[0])
			binary.LittleEndian.PutUint32(bytes[4:8], value.Daddr[1])
			binary.LittleEndian.PutUint32(bytes[8:12], value.Daddr[2])
			binary.LittleEndian.PutUint32(bytes[12:16], value.Daddr[3])
			ip = netip.AddrFrom16(bytes)
		}
		ret[somark] = ip.String()
	}
	hf.m.RUnlock()

	return ret, nil
}

// Remove the specified realserver
func (hf *HealthcheckForwarder) DeleteReal(somark uint32) error {
	if !hf.loaded {
		return ErrorBPFNotLoaded
	}

	hf.m.Lock()
	defer hf.m.Unlock()

	if err := hf.objs.HcRealsMap.Delete(somark); err != nil {
		hf.logger.Error("unable to delete realserver",
			slog.Int64("somark", int64(somark)),
			slog.Any("error", err))
	}

	hf.size -= 1
	delete(hf.data, somark)

	return nil
}

// Pin the required eBPF maps and programs to be able to restart the control
// plane without dropping healthchecks
func (hf *HealthcheckForwarder) Pin() error {
	if !hf.loaded {
		return ErrorBPFNotLoaded
	}
	if err := hf.objs.HealthcheckEncap.Pin(programPinPath); err != nil {
		return err
	}
	maps := []struct {
		ebpfMap *ebpf.Map
		name    string
	}{
		{hf.objs.HcCtrlMap, "hc_ctrl_map"},
		{hf.objs.HcRealsMap, "hc_reals_map"},
		{hf.objs.HcStatsMap, "hc_stats_map"},
	}
	for _, m := range maps {
		path := filepath.Join(basePinPath, m.name)
		if err := m.ebpfMap.Pin(path); err != nil {
			return err
		}
	}
	hf.pinned = true
	return nil
}

// Unpin eBPF maps and programs to be able to perform a complete shutdown
func (hf *HealthcheckForwarder) Unpin() error {
	if err := hf.objs.HealthcheckEncap.Unpin(); err != nil {
		return err
	}
	maps := []*ebpf.Map{hf.objs.HcCtrlMap, hf.objs.HcRealsMap, hf.objs.HcStatsMap}
	for _, m := range maps {
		if err := m.Unpin(); err != nil {
			return err
		}
	}
	hf.pinned = false
	return nil
}

func (hf *HealthcheckForwarder) Close() {
	if hf.attached {
		if !hf.objs.HealthcheckEncap.IsPinned() {
			hf.Detach()
		}
		hf.tcnl.Close()
	}
	if hf.loaded {
		hf.objs.Close()
	}
}

func WithIPv4Interface(iface *net.Interface) HealthcheckForwarderOption {
	return func(hf *HealthcheckForwarder) {
		hf.v4Interface = iface
	}
}

func WithIPv6Interface(iface *net.Interface) HealthcheckForwarderOption {
	return func(hf *HealthcheckForwarder) {
		hf.v6Interface = iface
	}
}

func WithLogger(logger *slog.Logger) HealthcheckForwarderOption {
	return func(hf *HealthcheckForwarder) {
		hf.logger = logger
	}
}
