package hcforwarder

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"net/netip"
	"testing"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
)

const (
	GRPC_NETWORK      = "tcp"
	GRPC_BIND_ADDRESS = "127.0.0.1:5678"
)

// QemuTest is declared on integration_test.go
func (QemuTest) TestGrpc(t *testing.T) {
	signalCtx, signalCancel := context.WithCancel(context.Background())
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	hf := getHealthcheckForwarder(t, true)
	srv := NewGrpcServer(logger, hf)
	serverCh, err := grpcutils.RunServer(signalCtx, logger, srv, GRPC_NETWORK, GRPC_BIND_ADDRESS)
	if err != nil {
		t.Fatalf("unable to run grpc server: %v", err)
		return
	}
	reals := []struct {
		ip     netip.Addr
		somark uint32
	}{
		{netip.MustParseAddr("127.0.0.2"), 1001},
		{netip.MustParseAddr("fc00::1"), 1002},
	}
	client, err := NewGrpcClient(logger, GRPC_NETWORK, GRPC_BIND_ADDRESS)
	if err != nil {
		t.Fatalf("unable to get conn: %v", err)
		return
	}
	defer client.Close()
	t.Run("gRPC/ListRealservers/empty", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		ret, err := client.ListRealservers(ctx)
		if err != nil {
			t.Fatalf("ListRealservers() failed: %v", err)
			return
		}
		if len(ret) != 0 {
			t.Errorf("Unexpected number of realservers. Got %v, wanted 0", len(ret))
		}
	})
	for _, rs := range reals {
		t.Run(fmt.Sprintf("gRPC/CreateRealserver/%s", rs.ip), func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancel()
			if err := client.CreateRealserver(ctx, rs.ip, rs.somark); err != nil {
				t.Fatalf("CreateRealserver(%+v) failed: %v", rs, err)
				return
			}
		})
	}
	t.Run("gRPC/ListRealservers", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		ret, err := client.ListRealservers(ctx)
		if err != nil {
			t.Fatalf("ListRealservers() failed: %v", err)
			return
		}
		for _, rs := range reals {
			if addr, present := ret[rs.somark]; present {
				if addr != rs.ip {
					t.Errorf("unable to find %+v", rs)
				}
			} else {
				t.Errorf("unable to find %+v", rs)
			}
		}
	})
	t.Run("gRPC/DeleteRealserver", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		mark := uint32(1002)
		err := client.DeleteRealserver(ctx, netip.Addr{}, mark)
		cancel()
		if err != nil {
			t.Fatalf("unable to delete realserver with mark %v: %v", mark, err)
			return
		}
		ctx, cancel = context.WithTimeout(context.Background(), 1*time.Second)
		ret, err := client.ListRealservers(ctx)
		cancel()
		if err != nil {
			t.Fatalf("ListRealservers() failed: %v", err)
			return
		}
		if _, present := ret[mark]; present {
			t.Fatalf("realserver with mark %v on Listservers() output after being deleted", mark)
			return
		}
		// a second attempt should trigger an error
		ctx, cancel = context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		err = client.DeleteRealserver(ctx, netip.Addr{}, mark)
		if err != nil {
			t.Fatalf("DeleteRealserver() call failed: %v", err)
			return
		}
	})

	wait := true
	for wait {
		signalCancel()
		select {
		case <-serverCh:
			wait = false
		case <-time.After(5 * time.Second):
			t.Error("cmd didn't react to SIGTERM in 5 seconds")
			wait = false
		}
	}
	hf.Close()
}
