//go:build linux
// +build linux

/*
Copyright 2023 Wikimedia Foundation Inc.
Copyright 2018 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package hcforwarder

import (
	"context"
	"io"
	"log"
	"log/slog"
	"net"
	"os"
	"path/filepath"
	"syscall"
	"testing"
	"time"

	"gitlab.wikimedia.org/repos/sre/go-qemutest/pkg/qemutest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bpftest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/testutils"
	"golang.org/x/sys/unix"

	"github.com/gopacket/gopacket"
	"github.com/gopacket/gopacket/layers"
	"github.com/gopacket/gopacket/pcap"
)

const (
	NETWORK_INTERFACE = "lo"
	IPIP_INTERFACE    = "ipip0"
	IPIP6_INTERFACE   = "ipip60"
	BPF_PROGRAM_NAME  = "healthcheck_encap"
)

// binaries that will be shipped on initrd
var initrdBinaries = []string{"/usr/sbin/bpftool", "/bin/ip"}

type QemuTest struct{}

func TestMain(m *testing.M) {
	// Before being able to use the eBPF program clsact qdisc
	// needs to be attached to the network interface
	if qemutest.InQemu() {
		// pinning requires /sys/fs/bpf
		if err := unix.Mount("bpf", "/sys/fs/bpf", "bpf", 0, ""); err != nil {
			log.Fatalf("failed to mount /sys/fs/bpf: %v", err)
		}

		if err := bpftest.AttachClsact(NETWORK_INTERFACE); err != nil {
			log.Fatalf("failed to attach clsact to %s: %v", NETWORK_INTERFACE, err)
		}

		// set up ipip0 && ipip60
		bpftest.MustRun("/bin/ip", "link", "add", "name", IPIP_INTERFACE, "type", "ipip", "external")
		bpftest.MustRun("/bin/ip", "link", "add", "name", IPIP6_INTERFACE, "type", "ip6tnl", "external")
		bpftest.MustRun("/bin/ip", "link", "set", "up", "dev", IPIP_INTERFACE)
		bpftest.MustRun("/bin/ip", "link", "set", "up", "dev", IPIP6_INTERFACE)
		// Add a IPv6 address to the loopback interface to be able to send traffic there
		bpftest.MustRun("/bin/ip", "address", "add", "fc00::1", "dev", "lo")
	}

	os.Exit(qemutest.QemuTestMain(m))
}

func TestQemu(t *testing.T) {
	if qemutest.InQemu() {
		qemutest.RunQemuTests(t, QemuTest{})
		return
	}
	tdPath, err := testutils.GetTestdata()
	if err != nil {
		t.Fatalf("unable to find testdata path: %v", err)
	}
	testCases := []qemutest.QemuTestCase{
		{
			KernelImage: filepath.Join(tdPath, "bzImage.6.1"),
			TestId:      "6.1.69-bookworm",
			Verbose:     true,
		},
	}

	qemutest.RunQemu(t, testCases, initrdBinaries)
}

func setSocketMark(fd, mark int) error {
	if err := syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_MARK, mark); err != nil {
		return os.NewSyscallError("failed to set mark", err)
	}
	return nil
}

func tcpClient(address, port string, mark int) error {
	d := net.Dialer{Timeout: 10 * time.Millisecond,
		Control: func(network, address string, rawc syscall.RawConn) error {
			var fdErr error
			ctl := func(fd uintptr) {
				fdErr = setSocketMark(int(fd), mark)
			}
			if err := rawc.Control(ctl); err != nil {
				return err
			}
			return fdErr
		},
	}
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	conn, err := d.DialContext(ctx, "tcp", address+":"+port)
	if err != nil {
		return err
	}
	conn.Close()
	return nil
}

type IPIP struct {
	outerIP string
	innerIP string
}

func getIPIP(ctx context.Context, c chan<- IPIP) <-chan bool {
	ready := make(chan bool, 1)
	go func() {
		handle, err := pcap.OpenLive(NETWORK_INTERFACE, 262144, true, pcap.BlockForever)
		if err != nil {
			panic(err)
		}
		defer handle.Close()

		packets := gopacket.NewPacketSource(
			handle, handle.LinkType()).PacketsCtx(ctx)
		ready <- true

		// check if packet is IP[6]IP[6]
		for pkt := range packets {
			var outerLayer gopacket.Layer
			var innerLayer gopacket.Layer
			packetLayers := pkt.Layers()
			for i, layer := range packetLayers {
				ltype := layer.LayerType()
				if ltype == layers.LayerTypeIPv4 || ltype == layers.LayerTypeIPv6 {
					if outerLayer == nil {
						outerLayer = packetLayers[i]
					} else {
						innerLayer = packetLayers[i]
						break
					}
				}
			}
			if innerLayer == nil {
				continue
			}
			if outerLayer.LayerType() == layers.LayerTypeIPv4 {
				outerIP := outerLayer.(*layers.IPv4)
				innerIP := innerLayer.(*layers.IPv4)
				select {
				case c <- IPIP{outerIP.DstIP.String(), innerIP.DstIP.String()}:
					return
				default:
					log.Println("Dropping packet")
				}
			} else if outerLayer.LayerType() == layers.LayerTypeIPv6 {
				outerIP := outerLayer.(*layers.IPv6)
				innerIP := innerLayer.(*layers.IPv6)
				select {

				case c <- IPIP{"[" + outerIP.DstIP.String() + "]", "[" + innerIP.DstIP.String() + "]"}:
					return
				default:
					log.Println("Dropping packet")
				}
			}
		}
	}()
	return ready
}

func getHealthcheckForwarder(t *testing.T, ifaces bool) *HealthcheckForwarder {

	iface, err := net.InterfaceByName(NETWORK_INTERFACE)
	if err != nil {
		t.Fatalf("unable to find lo interface")
	}

	v4Iface, err := net.InterfaceByName(IPIP_INTERFACE)
	if err != nil {
		t.Fatalf("unable to find %s interface: %v", IPIP_INTERFACE, err)
	}
	v6Iface, err := net.InterfaceByName(IPIP6_INTERFACE)
	if err != nil {
		t.Fatalf("unable to find %s interface: %v", IPIP6_INTERFACE, err)
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	var hf *HealthcheckForwarder
	if ifaces {
		hf = NewHealthcheckForwarder(
			WithIPv4Interface(v4Iface),
			WithIPv6Interface(v6Iface),
			WithLogger(logger),
		)
	} else {
		hf = NewHealthcheckForwarder(
			WithLogger(logger),
		)
	}
	if err := hf.Init(iface); err != nil {
		t.Fatalf("Unable to load eBPF program: %v", err)
	}
	return hf
}

func (QemuTest) TestLoadingProgram(t *testing.T) {

	t.Log("Loading eBPF program")
	hf := getHealthcheckForwarder(t, true)

	reals := []struct {
		ip     string
		somark uint32
	}{
		{"127.0.0.2", 1001},
		{"fc00::1", 1002},
	}
	for _, realServer := range reals {
		if err := hf.AddReal(realServer.ip, realServer.somark); err != nil {
			t.Errorf("Unable to add real: %v", err)
		}
	}

	// Check that realservers have been configured as expected
	t.Run("Realservers()", func(t *testing.T) {
		want := map[uint32]string{
			1001: "127.0.0.2",
			1002: "fc00::1",
		}
		got, err := hf.Realservers()
		if err != nil {
			t.Fatalf("unable to fetch realservers: %v", err)
		}
		if len(want) != len(got) {
			t.Error("unexpected number of realservers")
		}
		for key, value := range want {
			gotValue, ok := got[key]
			if !ok {
				t.Errorf("key %v is missing", key)
				continue
			}
			if value != gotValue {
				t.Errorf("want %+v, got %+v", value, gotValue)
			}
		}
	})

	if !bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, true) {
		t.Error("bpftool doesn't show the eBPF program loaded")
	}

	testCases := []struct {
		outerIP string
		innerIP string
		somark  int
	}{
		{"127.0.0.2", "127.0.0.1", 1001},
		{"[fc00::1]", "[::1]", 1002},
	}

	for _, tc := range testCases {
		t.Run(tc.innerIP, func(t *testing.T) {
			c := make(chan IPIP, 1)
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			ready := getIPIP(ctx, c)
			select {
			case <-ready: // wait till getIPIP is ready to capture packets
			case <-time.After(5 * time.Second):
				t.Error("getIPIP didn't get ready in 5 seconds")
				return
			}
			tcpClient(tc.innerIP, "443", tc.somark)
			select {
			case ipip := <-c:
				if ipip.outerIP != tc.outerIP {
					t.Errorf("Unexpected outerIP, got: %v, want: %v", ipip.outerIP, tc.outerIP)
				}
				if ipip.innerIP != tc.innerIP {
					t.Errorf("Unexpected innerIP, got %v, want: %v", ipip.innerIP, tc.innerIP)
				}
			case <-time.After(5 * time.Second):
				t.Error("getIPIP didn't send any packet on the channel")
			}
		})
	}
	hf.Close()
	if bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, false) {
		t.Error("bpftool shows eBPF program loaded after Close()")
	}
	if bpftest.IsBpfObjectLoaded(bpftest.BpfMap, "hc_ctrl_map", false) {
		t.Error("bpftool shows eBPF map loaded after Close()")
	}
}

func (QemuTest) TestGetStats(t *testing.T) {
	t.Log("Loading eBPF program")
	hf := getHealthcheckForwarder(t, true)

	reals := []struct {
		ip     string
		somark uint32
	}{
		{"127.0.0.2", 1001},
		{"fc00::1", 1002},
	}
	for _, realServer := range reals {
		if err := hf.AddReal(realServer.ip, realServer.somark); err != nil {
			t.Errorf("Unable to add real: %v", err)
		}
	}

	testCases := []struct {
		outerIP string
		innerIP string
		somark  int
	}{
		{"127.0.0.2", "127.0.0.1", 1001},
		{"[fc00::1]", "[::1]", 1002},
	}

	for _, tc := range testCases {
		// generate some traffic to show up on the stats
		tcpClient(tc.innerIP, "443", tc.somark)
	}
	stats, err := hf.GetStats()
	if err != nil {
		t.Errorf("unable to fetch stats: %v", err)
	}
	if stats.PcktsProcessed != uint64(len(testCases)) {
		t.Errorf("unexpected number of processed packets, want: %d, got: %d",
			len(testCases), stats.PcktsProcessed)
	}
	hf.Close()
	if bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, false) {
		t.Error("bpftool shows eBPF program loaded after Close()")
	}
}

func (QemuTest) TestPin(t *testing.T) {
	hf := getHealthcheckForwarder(t, true)

	reals := []struct {
		ip     string
		somark uint32
	}{
		{"127.0.0.2", 1001},
		{"fc00::1", 1002},
	}
	for _, realServer := range reals {
		if err := hf.AddReal(realServer.ip, realServer.somark); err != nil {
			t.Errorf("Unable to add real: %v", err)
		}
	}

	if err := hf.Pin(); err != nil {
		t.Errorf("unable to pin %v", err)
	}
	hf.Close()
	if !bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, true) {
		t.Error("bpftool doesn't show the eBPF program loaded")
	}

	if !bpftest.IsBpfObjectLoaded(bpftest.BpfMap, "hc_ctrl_map", true) {
		t.Error("bpftool doesn't show the eBPF map loaded after Close()")
	}
	testCases := []struct {
		outerIP string
		innerIP string
		somark  int
	}{
		{"127.0.0.2", "127.0.0.1", 1001},
		{"[fc00::1]", "[::1]", 1002},
	}

	for _, tc := range testCases {
		t.Run("pinned/"+tc.innerIP, func(t *testing.T) {
			c := make(chan IPIP, 1)
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			ready := getIPIP(ctx, c)
			select {
			case <-ready: // wait till getIPIP is ready to capture packets
			case <-time.After(5 * time.Second):
				t.Error("getIPIP didn't get ready in 5 seconds")
				return
			}
			tcpClient(tc.innerIP, "443", tc.somark)
			select {
			case ipip := <-c:
				if ipip.outerIP != tc.outerIP {
					t.Errorf("Unexpected outerIP, got: %v, want: %v", ipip.outerIP, tc.outerIP)
				}
				if ipip.innerIP != tc.innerIP {
					t.Errorf("Unexpected innerIP, got %v, want: %v", ipip.innerIP, tc.innerIP)
				}
			case <-time.After(5 * time.Second):
				t.Error("getIPIP didn't send any packet on the channel")
			}
		})
	}

	hf = getHealthcheckForwarder(t, false)

	pinnedReals, err := hf.Realservers()
	if err != nil {
		t.Errorf("unable to fetch realservers: %v", err)
	}

	if len(pinnedReals) != len(reals) {
		t.Errorf("want %d reals, got %d reals", len(reals), len(pinnedReals))
	}

	if hf.size != uint32(len(reals)) {
		t.Errorf("wrong hf.size value, want %d, got %d", len(reals), hf.size)
	}

	for _, rs := range reals {
		if pinnedReals[rs.somark] != rs.ip {
			t.Errorf("unexpected ip %s for somark %d", pinnedReals[rs.somark], rs.somark)
		}
	}

	if err := hf.Unpin(); err != nil {
		t.Errorf("unable to unpin eBPF programs: %v", err)
	}
	hf.Close()
	if bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, false) {
		t.Error("bpftool shows eBPF program loaded after Close()")
	}
}

func (QemuTest) TestDeleteReal(t *testing.T) {
	hf := getHealthcheckForwarder(t, true)
	mark := uint32(1001)
	if err := hf.AddReal("127.0.0.2", mark); err != nil {
		t.Fatalf("unable to add real: %v", err)
	}
	if hf.size != 1 {
		t.Fatalf("unexpected hf.size value, got %v, wanted 1", hf.size)
	}
	if _, ok := hf.data[mark]; !ok {
		t.Fatalf("hf.data doesn't contain somark %v", mark)
	}
	if err := hf.DeleteReal(mark); err != nil {
		t.Fatalf("unable to remove real: %v", err)
	}
	if hf.size != 0 {
		t.Fatalf("unexpected hf.size value, got %v, wanted 1", hf.size)
	}
	if _, ok := hf.data[mark]; ok {
		t.Fatalf("hf.data still contains somark %v", mark)
	}
	hf.Close()
	if bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, false) {
		t.Error("bpftool shows eBPF program loaded after Close()")
	}
}

func (QemuTest) TestUpdateReal(t *testing.T) {
	hf := getHealthcheckForwarder(t, true)
	mark := uint32(1001)
	if err := hf.AddReal("127.0.0.2", mark); err != nil {
		t.Fatalf("unable to add real: %v", err)
	}
	if hf.size != 1 {
		t.Fatalf("unexpected hf.size value, got %v, wanted 1", hf.size)
	}
	if _, ok := hf.data[mark]; !ok {
		t.Fatalf("hf.data doesn't contain somark %v", mark)
	}
	if err := hf.AddReal("127.0.0.3", mark); err != nil {
		t.Fatalf("unable to update real: %v", err)
	}
	if hf.size != 1 {
		t.Fatalf("unexpected hf.size value, got %v, wanted 1", hf.size)
	}
	if _, ok := hf.data[mark]; !ok {
		t.Fatalf("hf.data doesn't contain somark %v", mark)
	}
	hf.Close()
	if bpftest.IsBpfObjectLoaded(bpftest.BpfProgram, BPF_PROGRAM_NAME, false) {
		t.Error("bpftool shows eBPF program loaded after Close()")
	}
}
