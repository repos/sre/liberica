# hcforwarder package
Provides healthcheck forwarding capabilites on a load balancer server via a TC action provided by katran: https://github.com/facebookincubator/katran/blob/main/katran/lib/bpf/healthchecking_ipip.c
## Dependencies
llvm & clang need to be installed on the developer machine, it's been tested against llvm-14 on bookworm
## Building it
```
BPF_CLANG=clang go generate ./...
go build
```
