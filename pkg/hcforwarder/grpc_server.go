package hcforwarder

import (
	"context"
	"log/slog"
	"net/netip"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/hcforwarder"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct {
	pb.UnimplementedHCForwarderServiceServer
	hf     *HealthcheckForwarder
	logger *slog.Logger
}

func NewGrpcServer(logger *slog.Logger, hf *HealthcheckForwarder) *grpc.Server {
	s := &server{
		logger: logger,
		hf:     hf,
	}

	var opts []grpc.ServerOption
	opts = append(opts, grpc.KeepaliveParams(grpcutils.KeepaliveServerParameters))
	srv := grpc.NewServer(opts...)
	pb.RegisterHCForwarderServiceServer(srv, s)

	return srv
}

func (srv *server) ListRealservers(context.Context, *pb.ListRealserversRequest) (*pb.ListRealserversResponse, error) {
	realservers, err := srv.hf.Realservers()
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "unable to fetch realservers: %v", err)
	}

	result := &pb.ListRealserversResponse{}

	for mark, address := range realservers {
		ret := &pbrs.Realserver{
			Address: address,
			Mark:    mark,
		}
		result.Realservers = append(result.Realservers, ret)
	}

	return result, nil
}

func (srv *server) CreateRealserver(ctx context.Context, in *pb.CreateRealserverRequest) (*pb.CreateRealserverResponse, error) {
	if in.Realserver.Mark == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "missing required field: Realserver.Mark")
	}
	if _, err := netip.ParseAddr(in.Realserver.Address); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid field: Realserver.Address")
	}
	if err := srv.hf.AddReal(in.Realserver.Address, in.Realserver.Mark); err != nil {
		return nil, status.Errorf(codes.Aborted, "unable to add realserver: %v", err)
	}

	return &pb.CreateRealserverResponse{}, nil
}

func (srv *server) DeleteRealserver(ctx context.Context, in *pb.DeleteRealserverRequest) (*pb.DeleteRealserverResponse, error) {
	if in.Realserver.Mark == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "missing required field: Realserver.Mark")
	}
	if err := srv.hf.DeleteReal(in.Realserver.Mark); err != nil {
		return nil, status.Errorf(codes.Aborted, "unable to delete realserver: %v", err)
	}

	return &pb.DeleteRealserverResponse{}, nil
}
