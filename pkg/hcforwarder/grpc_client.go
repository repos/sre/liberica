package hcforwarder

import (
	"context"
	"fmt"
	"log/slog"
	"net/netip"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/hcforwarder"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

type HCForwarderClient interface {
	ListRealservers(context.Context) (map[uint32]netip.Addr, error)
	CreateRealserver(context.Context, netip.Addr, uint32) error
	DeleteRealserver(context.Context, netip.Addr, uint32) error
	Close()
}

type HCForwarderGrpcClient struct {
	logger *slog.Logger
	conn   *grpc.ClientConn
}

func NewGrpcClient(logger *slog.Logger, network, address string) (HCForwarderClient, error) {
	addr := grpcutils.GetConnectAddress(network, address)

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, grpc.WithKeepaliveParams(grpcutils.KeepaliveClientParameters))
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// DialContext returns before connecting, ctx only controls the setup steps. More details on https://pkg.go.dev/google.golang.org/grpc@v1.46.2#DialContext
	conn, err := grpc.DialContext(ctx, addr, opts...)
	if err != nil {
		return nil, fmt.Errorf("unable to create connection: %w", err)
	}

	return &HCForwarderGrpcClient{
		logger: logger,
		conn:   conn,
	}, nil
}

func (client *HCForwarderGrpcClient) ListRealservers(ctx context.Context) (map[uint32]netip.Addr, error) {
	if ctx.Err() != nil {
		return nil, ctx.Err()
	}
	c := pb.NewHCForwarderServiceClient(client.conn)
	req := &pb.ListRealserversRequest{}
	res, err := c.ListRealservers(ctx, req, grpc.WaitForReady(true))
	if err != nil {
		return nil, fmt.Errorf("unable to list realservers: %w", err)
	}

	ret := map[uint32]netip.Addr{}

	for _, rs := range res.Realservers {
		// sanity check, server shouldn't return invalid realservers
		if rs.Address == "" || rs.Mark == 0 {
			continue
		}
		addr, err := netip.ParseAddr(rs.Address)
		if err != nil {
			client.logger.Warn("invalid address returned by hcforwader grpc server",
				slog.String("address", rs.Address),
				slog.Any("error", err),
			)
			continue
		}
		ret[rs.Mark] = addr
	}

	return ret, nil
}

func (client *HCForwarderGrpcClient) CreateRealserver(ctx context.Context, addr netip.Addr, mark uint32) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	c := pb.NewHCForwarderServiceClient(client.conn)
	req := &pb.CreateRealserverRequest{
		Realserver: &pbrs.Realserver{
			Address: addr.String(),
			Mark:    mark,
		},
	}

	if _, err := c.CreateRealserver(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to create realserver: %w", err)
	}

	return nil
}

func (client *HCForwarderGrpcClient) DeleteRealserver(ctx context.Context, addr netip.Addr, mark uint32) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	c := pb.NewHCForwarderServiceClient(client.conn)
	req := &pb.DeleteRealserverRequest{
		Realserver: &pbrs.Realserver{
			Address: addr.String(),
			Mark:    mark,
		},
	}

	if _, err := c.DeleteRealserver(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to delete realserver: %w", err)
	}

	return nil
}

func (client *HCForwarderGrpcClient) Close() {
	if client.conn != nil {
		client.conn.Close()
	}
}
