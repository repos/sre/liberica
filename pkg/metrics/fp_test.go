package metrics

import (
	"fmt"
	"io"
	"log/slog"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	dto "github.com/prometheus/client_model/go"
)

func getIPVSCounterValue(metric *prometheus.CounterVec, result string) float64 {
	var m = &dto.Metric{}
	if err := metric.WithLabelValues(result).Write(m); err != nil {
		return 0
	}
	return m.Counter.GetValue()
}
func TestIncResultCounter(t *testing.T) {
	// sanity tests
	for _, label := range []string{"ok", "ko"} {
		if value := getIPVSCounterValue(IpvsFpReadOps, label); value != 0.0 {
			t.Errorf("IpvsFpReadOps should be 0.0, got %v instead", value)
		}
	}
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	testCases := []struct {
		name    string
		counter *prometheus.CounterVec
		result  error
		want    float64
	}{
		{"ok", IpvsFpReadOps, nil, 1.0},
		{"ko", IpvsFpReadOps, fmt.Errorf("failed"), 1.0},
		{"ok", FpGetSnapshotOps, nil, 1.0},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			IncResultCounter(logger, testCase.counter, testCase.result)
			if got := getIPVSCounterValue(IpvsFpReadOps, testCase.name); got != testCase.want {
				t.Errorf("Unexpected value, got %v, want %v", got, testCase.want)
			}
		})
	}

	t.Run("increment not result counter", func(t *testing.T) {
		IncResultCounter(logger, HttpCheck, nil)
		t.Log("incrementing a non-result counter didn't trigger a panic()")
	})
}
