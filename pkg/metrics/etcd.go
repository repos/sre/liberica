/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	EtcdEventsReceived = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_etcd_events_received_total",
			Help: "Number of etcd events received"},
		[]string{"event_type", "key"},
	)

	EtcdErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_etcd_errors_total",
			Help: "Number of failed operations"},
		[]string{"key"},
	)

	EtcdEventsDiscarded = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_etcd_events_discarded_total",
			Help: "Number of etcd events discarded tue to control plane congestion"},
		[]string{"event_type", "key"},
	)
)
