/*
Copyright © 2022-2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package metrics

import "github.com/prometheus/client_golang/prometheus"

var (
	DepoolThreshold = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "liberica_cp_depool_threshold",
			Help: "configured depool threshold",
		},
		[]string{"service"},
	)

	AdministrativelyPooledRealservers = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "liberica_cp_adm_pooled_realservers_total",
			Help: "total number of administratively pooled realservers",
		},
		[]string{"service"},
	)

	PooledRealservers = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "liberica_cp_pooled_realservers_total",
			Help: "total number of healthy pooled realservers",
		},
		[]string{"service"},
	)

	UnhealthyPooledRealservers = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "liberica_cp_unhealthy_pooled_realservers_total",
			Help: "total number of unhealthy pooled realservers",
		},
		[]string{"service"},
	)

	HealthyRealserversNotPooled = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "liberica_cp_healthy_not_pooled_realservers_total",
			Help: "total number of healthy not pooled realservers",
		},
		[]string{"service"},
	)

	ControlPlaneConfigReload = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_cp_configuration_reloads_total",
			Help: "total number of configuration reloads",
		},
		[]string{"result"},
	)
	// metrics to be used with liberica cp check cmd
	ControlPlaneMatchForwardingPlane = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "liberica_cp_match_fp",
			Help: "control plane state matches forwarding plane state",
		},
	)
)
