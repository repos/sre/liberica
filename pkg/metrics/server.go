/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package metrics

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func RunPrometheusServer(ctx context.Context, logger *slog.Logger, addr string, cs ...prometheus.Collector) (chan struct{}, error) {
	ch := make(chan struct{})
	if err := ctx.Err(); err != nil {
		close(ch)
		return ch, err
	}

	reg := prometheus.NewPedanticRegistry()

	for _, collector := range cs {
		if err := reg.Register(collector); err != nil {
			close(ch)
			return ch, fmt.Errorf("prometheus.Register() failed: %w", err)
		}
	}

	if err := reg.Register(prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{})); err != nil {
		close(ch)
		return ch, fmt.Errorf("prometheus.Register() failed to register ProcessCollector: %w", err)
	}
	if err := reg.Register(prometheus.NewGoCollector()); err != nil {
		close(ch)
		return ch, fmt.Errorf("prometheus.Register() failed to register GoCollector: %w", err)
	}

	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	server := &http.Server{
		Addr:    addr,
		Handler: mux,
	}

	go func() {
		defer close(ch)
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Error("ListenAndServe() failed",
				slog.String("bind_address", addr),
				slog.Any("error", err))

		}
	}()

	// wait till ctx is done
	go func() {
		select {
		case <-ch:
			logger.Warn("prometheus server stopped unexpectedly")
		case <-ctx.Done():
			logger.Info("Gracefully stopping prometheus server (30 seconds timeout)")
			shutdownCtx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()
			server.Shutdown(shutdownCtx)
			select { // wait till the clean shutdown is done or it timeouts
			case <-ch:
			case <-ctx.Done():
			}
		}
	}()

	return ch, nil
}
