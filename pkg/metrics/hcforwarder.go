/*
Copyright © 2023 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/hcforwarder"
)

const (
	HCFORWARDER_SKIPPED_LABEL   = "skipped"
	HCFORWARDER_PROCESSED_LABEL = "processed"
	HCFORWARDER_DROPPED_LABEL   = "dropped"
	HCFORWARDER_TOOBIG_LABEL    = "too-big"
)

var (
	HCForwarderPacketsDesc = prometheus.NewDesc(
		"liberica_healthcheck_forwarder_packets_total",
		"Number of total packets per interface",
		[]string{"interface", "state"}, nil,
	)
	// https://prometheus.io/docs/instrumenting/writing_exporters/#failed-scrapes
	HCForwarderUpDesc = prometheus.NewDesc(
		"liberica_healthcheck_forwarder_up",
		"Reports if the current scrape was successful or not",
		[]string{"interface"}, nil,
	)
)

type HCForwarderMetricCollector struct {
	HealthcheckForwarder *hcforwarder.HealthcheckForwarder
	IfaceName            string
}

func (hfcm HCForwarderMetricCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(hfcm, ch)
}

func (hfcm HCForwarderMetricCollector) Collect(ch chan<- prometheus.Metric) {
	successful := float64(1)
	stats, err := hfcm.HealthcheckForwarder.GetStats()
	if err != nil {
		successful = float64(0)
	}

	ch <- prometheus.MustNewConstMetric(
		HCForwarderUpDesc,
		prometheus.CounterValue,
		successful,
		hfcm.IfaceName,
	)

	if err != nil {
		return
	}

	ch <- prometheus.MustNewConstMetric(
		HCForwarderPacketsDesc,
		prometheus.CounterValue,
		float64(stats.PcktsProcessed),
		hfcm.IfaceName, HCFORWARDER_PROCESSED_LABEL)

	ch <- prometheus.MustNewConstMetric(
		HCForwarderPacketsDesc,
		prometheus.CounterValue,
		float64(stats.PcktsDropped),
		hfcm.IfaceName, HCFORWARDER_DROPPED_LABEL)

	ch <- prometheus.MustNewConstMetric(
		HCForwarderPacketsDesc,
		prometheus.CounterValue,
		float64(stats.PcktsSkipped),
		hfcm.IfaceName, HCFORWARDER_SKIPPED_LABEL)

	ch <- prometheus.MustNewConstMetric(
		HCForwarderPacketsDesc,
		prometheus.CounterValue,
		float64(stats.PcktsTooBig),
		hfcm.IfaceName, HCFORWARDER_TOOBIG_LABEL)
}
