/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package metrics

import (
	"log/slog"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	IpvsFpReadOps = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_fp_ipvs_read_operations_total",
			Help: "Number of read operations performed against the IPVS forwarding plane",
		},
		[]string{"result"},
	)

	IpvsFpWriteOps = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_fp_ipvs_write_operations_total",
			Help: "Number of write operations performed against the IPVS forwarding plane",
		},
		[]string{"result"},
	)

	FpGetSnapshotOps = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_fp_get_snapshot_total",
			Help: "Number of get_snaphot operations performed against any forwarding plane",
		},
		[]string{"result"},
	)
)

// Increment the specified result counter.
// counter must have a result label
func IncResultCounter(logger *slog.Logger, counter *prometheus.CounterVec, result error) {
	resultLabel := "ok"
	if result != nil {
		resultLabel = "ko"
	}
	// we cannot use With() here and risk a panic()
	metric, err := counter.GetMetricWith(prometheus.Labels{"result": resultLabel})
	if err != nil {
		logger.Error("unable to get metric from CounterVec",
			slog.Any("counter", counter),
			slog.Any("error", err))
		return
	}
	metric.Inc()
}
