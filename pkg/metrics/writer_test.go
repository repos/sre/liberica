package metrics

import (
	"bufio"
	"os"
	"strings"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func TestMetricsWriter(t *testing.T) {
	f, err := os.CreateTemp("", "metrics.prom")
	if err != nil {
		t.Fatalf("unable to create temp file: %v", err)
	}
	defer os.Remove(f.Name())
	f.Close()

	writer := NewMetricsWriter(f.Name())

	counter := promauto.NewCounter(prometheus.CounterOpts{
		Name: "test_counter",
		Help: "Test counter",
	})

	gauge := promauto.NewGauge(prometheus.GaugeOpts{
		Name: "test_gauge",
		Help: "Test gauge",
	})

	if err := writer.Register(counter); err != nil {
		t.Fatalf("unable to register counter: %v", err)
	}
	if err := writer.Register(gauge); err != nil {
		t.Fatalf("unable to register gauge: %v", err)
	}

	counter.Inc()
	gauge.Set(42.0)

	if err := writer.WriteToFile(); err != nil {
		t.Fatalf("unable to write metrics file: %v", err)
	}

	// Read and validate file contents
	file, err := os.Open(f.Name())
	if err != nil {
		t.Fatalf("Failed to open metrics file: %v", err)
	}
	defer file.Close()

	expectedLines := map[string]bool{
		"# HELP test_counter Test counter": false,
		"# TYPE test_counter counter":      false,
		"test_counter 1":                   false,
		"# HELP test_gauge Test gauge":     false,
		"# TYPE test_gauge gauge":          false,
		"test_gauge 42":                    false,
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		found := false
		for expected := range expectedLines {
			if strings.TrimSpace(line) == expected {
				expectedLines[expected] = true
				found = true
				break
			}
		}
		if !found {
			t.Errorf("Unexpected line in metrics file: %s", line)
		}
	}

	if err := scanner.Err(); err != nil {
		t.Fatalf("Error reading metrics file: %v", err)
	}

	for line, found := range expectedLines {
		if !found {
			t.Errorf("Expected line not found in metrics file: %s", line)
		}
	}
}
