package metrics

import (
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
)

// MetricsWriter handles writing Prometheus metrics to a file
type MetricsWriter struct {
	registry *prometheus.Registry
	file     string
}

func NewMetricsWriter(file string) *MetricsWriter {
	return &MetricsWriter{
		registry: prometheus.NewRegistry(),
		file:     file,
	}
}

// Register adds a new collector to the registry
func (w *MetricsWriter) Register(c prometheus.Collector) error {
	return w.registry.Register(c)
}

// WriteToFile writes all metrics from the registry to the specified file
func (w *MetricsWriter) WriteToFile() error {
	// Create or truncate the file
	f, err := os.Create(w.file)
	if err != nil {
		return err
	}
	defer f.Close()

	// Gather all metrics from registry
	metrics, err := w.registry.Gather()
	if err != nil {
		return err
	}

	// Create an encoder for the text format
	encoder := expfmt.NewEncoder(f, expfmt.FmtText)

	// Encode all metric families
	for _, mf := range metrics {
		if err := encoder.Encode(mf); err != nil {
			return err
		}
	}

	return nil
}
