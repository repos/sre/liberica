package hash

import (
	"fmt"
	"net/netip"

	"github.com/dchest/siphash"
	"github.com/mtchavez/jenkins"
)

const (
	Jenkins = "jenkins"
	SipHash = "SipHash"
)

var sipHashKey = []byte{
	0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7,
	0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf}

func Hash32Addr(hash string, addr netip.Addr) (uint32, error) {
	switch hash {
	case Jenkins:
		return jenkinsHash(addr)
	case SipHash:
		h, err := sipHash(addr)
		return uint32(h), err
	default:
		return 0, fmt.Errorf("unsupported hash algorithm: %s", hash)
	}
}

func Hash64Addr(hash string, addr netip.Addr) (uint64, error) {
	switch hash {
	case SipHash:
		return sipHash(addr)
	default:
		return 0, fmt.Errorf("unsupported hash algorithm: %s", hash)
	}
}

func jenkinsHash(addr netip.Addr) (uint32, error) {
	h := jenkins.New()
	if _, err := h.Write(addr.AsSlice()); err != nil {
		return 0, fmt.Errorf("unable to calculate jenkins hash: %w", err)
	}

	return h.Sum32(), nil
}

func sipHash(addr netip.Addr) (uint64, error) {
	h := siphash.New(sipHashKey)
	if _, err := h.Write(addr.AsSlice()); err != nil {
		return 0, fmt.Errorf("unable to calculate SipHash: %w", err)
	}

	return h.Sum64(), nil
}
