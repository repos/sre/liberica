package hash

import (
	"net/netip"
	"testing"
)

func TestHash32Addr(t *testing.T) {
	testCases := []struct {
		name      string
		algorithm string
		in        netip.Addr
		want      uint32
	}{
		{"ipv4-jenkins", Jenkins, netip.MustParseAddr("127.0.0.1"), 196420273},
		{"ipv6-jenkins", Jenkins, netip.MustParseAddr("::1"), 307143837},
		{"ipv4-siphash", SipHash, netip.MustParseAddr("127.0.0.1"), 1849765895},
		{"ipv6-siphash", SipHash, netip.MustParseAddr("::1"), 1695769421},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := Hash32Addr(tc.algorithm, tc.in)
			if err != nil {
				t.Fatalf("unexpected error: %v", err)
				return
			}
			if tc.want != got {
				t.Errorf("unexpected hash value, want %d, got %d", tc.want, got)
			}
		})
	}
}

func TestHash64Addr(t *testing.T) {
	testCases := []struct {
		name      string
		algorithm string
		in        netip.Addr
		want      uint64
	}{
		{"ipv4-siphash", SipHash, netip.MustParseAddr("127.0.0.1"), 3206713883163176967},
		{"ipv6-siphash", SipHash, netip.MustParseAddr("::1"), 16148497841136034637},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := Hash64Addr(tc.algorithm, tc.in)
			if err != nil {
				t.Fatalf("unexpected error: %v", err)
				return
			}
			if tc.want != got {
				t.Errorf("unexpected hash value, want %d, got %d", tc.want, got)
			}
		})
	}
}
