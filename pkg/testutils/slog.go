package testutils

import (
	"io"
	"log/slog"
)

func MockSlogNew(slog.Handler) *slog.Logger {
	return slog.New(slog.NewTextHandler(io.Discard, nil))
}
