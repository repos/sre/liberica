package testutils

import (
	"context"
	"os"
)

type TestSignalHandler struct {
	ParentCtx   map[os.Signal]context.Context
	ReturnedCtx map[os.Signal]context.Context
}

func (tsh TestSignalHandler) NotifyContext(_ context.Context, signals ...os.Signal) (context.Context, context.CancelFunc) {
	var parentCtx context.Context
	for _, signal := range signals {
		// if caller is attempting to get a n+1 handler for the same signal return a new one instead of overwritting ReturnedCtx
		if _, alreadyUsed := tsh.ReturnedCtx[signal]; alreadyUsed {
			return context.WithCancel(context.Background())
		}
		ctx, present := tsh.ParentCtx[signal]
		if present {
			parentCtx = ctx
			break
		}
	}

	if parentCtx == nil {
		parentCtx = context.Background()
	}

	for _, s := range signals {
		tsh.ReturnedCtx[s] = parentCtx
	}

	return context.WithCancel(parentCtx)
}
