package grpcutils

import (
	"context"
	"fmt"
	"log/slog"
	"net"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

var KeepaliveServerParameters = keepalive.ServerParameters{
	Time: 9 * time.Second,
}

func RunServer(ctx context.Context, logger *slog.Logger, srv *grpc.Server, network, addr string) (chan struct{}, error) {
	ch := make(chan struct{})
	if err := ctx.Err(); err != nil {
		return ch, err
	}

	wg := sync.WaitGroup{}
	listener, err := net.Listen(network, addr)
	if err != nil {
		return ch, fmt.Errorf("net.Listen() failed: %w", err)
	}

	go func() {
		logger.Info("starting gRPC server")
		wg.Add(1)
		defer wg.Done()
		defer listener.Close()

		if err := srv.Serve(listener); err != nil {
			logger.Error("grpc.Serve() failed",
				slog.String("bind_address", addr),
				slog.Any("error", err),
			)
		}
	}()

	// wait till ctx is done
	go func() {
		<-ctx.Done()
		logger.Info("Gracefully stoppping gRPC server (30 seconds timeout)")
		stopped := make(chan struct{})
		// https://github.com/grpc/grpc-go/issues/2448
		go func() {
			srv.GracefulStop()
			close(stopped)
		}()
		select {
		case <-time.After(30 * time.Second):
			srv.Stop()
		case <-stopped:
			srv.Stop()
		}
		wg.Wait()
		close(ch)
	}()

	return ch, nil
}
