package grpcutils

import (
	"fmt"
	"strings"
	"time"

	"google.golang.org/grpc/keepalive"
)

var KeepaliveClientParameters = keepalive.ClientParameters{
	Time:                10 * time.Second,
	Timeout:             time.Second,
	PermitWithoutStream: true,
}

func GetConnectAddress(network, address string) string {
	switch network {
	case "unix":
		if !strings.HasPrefix(address, "unix:") {
			return fmt.Sprintf("unix:%s", address)
		}
		return address
	default:
		return address
	}
}
