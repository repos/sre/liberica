package healthcheck

import (
	"testing"
	"time"
)

func TestHTTPCheckConfigSortFn(t *testing.T) {
	testcases := []struct {
		name string
		a    HTTPCheckConfig
		b    HTTPCheckConfig
		want int
	}{
		{
			name: "two zero instances are considered equal",
			a:    HTTPCheckConfig{},
			b:    HTTPCheckConfig{},
			want: 0,
		},
		{
			name: "two instances with the same values are considered equal",
			a: HTTPCheckConfig{
				Url:                "https://www.example.com",
				HostHeader:         "example.com",
				ExpectedStatusCode: 200,
				Timeout:            3 * time.Second,
				CheckPeriod:        5 * time.Second,
			},
			b: HTTPCheckConfig{
				Url:                "https://www.example.com",
				HostHeader:         "example.com",
				ExpectedStatusCode: 200,
				Timeout:            3 * time.Second,
				CheckPeriod:        5 * time.Second,
			},
			want: 0,
		},
		{
			name: "a > b",
			a: HTTPCheckConfig{
				Url:                "https://www.example.com/foobar",
				HostHeader:         "example.com",
				ExpectedStatusCode: 200,
				Timeout:            3 * time.Second,
				CheckPeriod:        5 * time.Second,
			},
			b: HTTPCheckConfig{
				Url:                "https://www.example.com",
				HostHeader:         "example.com",
				ExpectedStatusCode: 200,
				Timeout:            3 * time.Second,
				CheckPeriod:        5 * time.Second,
			},
			want: 1,
		},
		{
			name: "a < b",
			a: HTTPCheckConfig{
				Url:                "https://www.example.com",
				HostHeader:         "example.com",
				ExpectedStatusCode: 200,
				Timeout:            1 * time.Second,
				CheckPeriod:        5 * time.Second,
			},
			b: HTTPCheckConfig{
				Url:                "https://www.example.com",
				HostHeader:         "example.com",
				ExpectedStatusCode: 200,
				Timeout:            3 * time.Second,
				CheckPeriod:        5 * time.Second,
			},
			want: -1,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			got := HTTPCheckConfigSortFn(tc.a, tc.b)
			if tc.want != got {
				t.Errorf("unexpected result, want %d, got %d", tc.want, got)
			}
		})
	}
}

func TestIdleTCPConnectionCheckConfigSortFn(t *testing.T) {
	testcases := []struct {
		name string
		a    IdleTCPConnectionCheckConfig
		b    IdleTCPConnectionCheckConfig
		want int
	}{
		{
			name: "two zero instances are considered equal",
			a:    IdleTCPConnectionCheckConfig{},
			b:    IdleTCPConnectionCheckConfig{},
			want: 0,
		},
		{
			name: "two instances with the same values are considered equal",
			a: IdleTCPConnectionCheckConfig{
				Address:         "127.0.0.1",
				ConnectTimeout:  1 * time.Second,
				CheckPeriod:     2 * time.Second,
				ReconnectPeriod: 3 * time.Second,
			},
			b: IdleTCPConnectionCheckConfig{
				Address:         "127.0.0.1",
				ConnectTimeout:  1 * time.Second,
				CheckPeriod:     2 * time.Second,
				ReconnectPeriod: 3 * time.Second,
			},
			want: 0,
		},
		{
			name: "a > b",
			a: IdleTCPConnectionCheckConfig{
				Address:         "127.0.0.1",
				ConnectTimeout:  1 * time.Second,
				CheckPeriod:     2 * time.Second,
				ReconnectPeriod: 30 * time.Second,
			},
			b: IdleTCPConnectionCheckConfig{
				Address:         "127.0.0.1",
				ConnectTimeout:  1 * time.Second,
				CheckPeriod:     2 * time.Second,
				ReconnectPeriod: 3 * time.Second,
			},
			want: 1,
		},
		{
			name: "a < b",
			a: IdleTCPConnectionCheckConfig{
				Address:         "127.0.0.1",
				ConnectTimeout:  1 * time.Second,
				CheckPeriod:     2 * time.Second,
				ReconnectPeriod: 30 * time.Second,
			},
			b: IdleTCPConnectionCheckConfig{
				Address:         "127.0.0.1",
				ConnectTimeout:  15 * time.Second,
				CheckPeriod:     2 * time.Second,
				ReconnectPeriod: 3 * time.Second,
			},
			want: -1,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			got := IdleTCPConnectionCheckConfigSortFn(tc.a, tc.b)
			if tc.want != got {
				t.Errorf("unexpected result, want %d, got %d", tc.want, got)
			}
		})
	}
}
