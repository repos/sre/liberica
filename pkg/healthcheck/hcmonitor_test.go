package healthcheck

import (
	"context"
	"io"
	"log/slog"
	"net/netip"
	"testing"
	"time"
)

const anotherTestHealthcheckName = "another test healthcheck"

type anotherTestHealthcheck struct {
	rs Realserver
}

func (atc anotherTestHealthcheck) GetCheckPeriod() time.Duration {
	return 1 * time.Millisecond
}

func (atc anotherTestHealthcheck) Close() {}

func (atc anotherTestHealthcheck) SingleCheck() Result {
	return Result{
		HealthcheckName: anotherTestHealthcheckName,
		Successful:      true,
		Realserver:      atc.rs,
	}
}

func (atc anotherTestHealthcheck) AsProto() any {
	return nil
}

func (atc anotherTestHealthcheck) GetID() HealthcheckID {
	id, _ := newHealthcheckID()
	return id
}

func TestHCMonitor(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	hcm := NewHCMonitor(logger)
	// TestHealthcheck issues a new result every 1ms
	results := map[Realserver]uint{}
	markHealthchecks := []struct {
		rs Realserver
	}{
		{Realserver{AddrPort: netip.MustParseAddrPort("192.0.0.1:80"), Mark: 1, Service: "Test svc #1"}},
		{Realserver{AddrPort: netip.MustParseAddrPort("192.0.0.2:80"), Mark: 2, Service: "Test svc #2"}},
	}
	for _, markHc := range markHealthchecks {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		hcs := []Healthcheck{&TestHealthcheck{Rs: markHc.rs}}
		if err := hcm.AddRealserver(ctx, markHc.rs, hcs); err != nil {
			t.Errorf("unable to add realserver %+v: %v", markHc.rs, err)
			continue
		}
		results[markHc.rs] = 0
	}

	t.Run("SubscribePerService", func(t *testing.T) {
		resultCh := make(chan Result, 128)
		defer close(resultCh)
		subscriptionCtx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		wantService := "Test svc #1"
		doneCh, err := hcm.Subscribe(subscriptionCtx, resultCh, wantService)
		if err != nil {
			t.Fatalf("unable to subscribe: %v", err)
			return
		}
		subscribed := true
		results := 0
		watchdog := time.NewTimer(3 * time.Second)
		expectedResults := 1 // client subscribes to Test svc #1 and we only have 1 realserver with 1 healthcheck
		defer watchdog.Stop()
		for subscribed {
			select {
			case result := <-resultCh:
				results += 1
				if result.Realserver.Service != wantService {
					t.Errorf("got result from unexpected service, want %s, got %s", wantService, result.Realserver.Service)
				}
				if results > expectedResults {
					t.Errorf("unexpected number of results, want %d, got %d", results, expectedResults)
				}
				if results >= expectedResults {
					if err := hcm.Unsubscribe(context.Background(), resultCh); err == nil {
						subscribed = false
					}
				}
			case <-doneCh:
				if subscribed {
					t.Error("subscription ended unexpectedly")
				}
			case <-watchdog.C:
				t.Error("test timeout")
				return
			}
		}
	})

	// Test that unsubcribing triggers doneCh to be closed
	t.Run("Unsubscribe", func(t *testing.T) {
		resultCh := make(chan Result, 128)
		defer close(resultCh)
		subscriptionCtx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		doneCh, err := hcm.Subscribe(subscriptionCtx, resultCh, "")
		if err != nil {
			t.Fatalf("unable to subscribe: %v", err)
			return
		}
		watchdog := time.NewTimer(1 * time.Second)
		defer watchdog.Stop()
		if err := hcm.Unsubscribe(context.Background(), resultCh); err != nil {
			t.Fatalf("failed to unsubscribe: %v", err)
		}
		for {
			select {
			case <-doneCh:
				return
			case <-watchdog.C:
				t.Error("test timeout")
				return
			}

		}
	})

	t.Run("UpgradeHCs", func(t *testing.T) {
		resultCh := make(chan Result, 128)
		defer close(resultCh)
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		doneCh, err := hcm.Subscribe(ctx, resultCh, "")
		if err != nil {
			t.Fatalf("unable to subscribe: %v", err)
			return
		}

		watchdog := time.NewTimer(5 * time.Second)
		defer watchdog.Stop()
		upgradeCtx, cancelUpgrade := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancelUpgrade()
		hcs := []Healthcheck{&anotherTestHealthcheck{rs: markHealthchecks[1].rs}}
		if err := hcm.AddRealserver(upgradeCtx, markHealthchecks[1].rs, hcs); err != nil {
			t.Error("Unable to replace HCs for mark 2")
			return
		}
		subscribed := true
		for {
			select {
			case result := <-resultCh:
				if result.HealthcheckName == anotherTestHealthcheckName {
					if err := hcm.Unsubscribe(context.Background(), resultCh); err != nil {
						t.Fatalf("failed to unsubscribe: %v", err)
					}
					subscribed = false
				}
			case <-time.After(100 * time.Millisecond):
				t.Error("timeout waiting for a result")
				return
			case <-doneCh:
				if subscribed {
					t.Error("subscription ended unexpectedly")
				}
				return
			case <-watchdog.C:
				t.Error("test timeout")
				return
			}
		}
	})

	// Test that stopping HCMonitor triggers doneCh to be closed
	t.Run("Close", func(t *testing.T) {
		resultCh := make(chan Result, 128)
		defer close(resultCh)
		subscriptionCtx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()
		doneCh, err := hcm.Subscribe(subscriptionCtx, resultCh, "")
		if err != nil {
			t.Fatalf("unable to subscribe: %v", err)
			return
		}
		hcm.Close()
		watchdog := time.NewTimer(1 * time.Second)
		defer watchdog.Stop()
		select {
		case <-doneCh:
		case <-watchdog.C:
			t.Error("test timeout")
		}
	})
}
