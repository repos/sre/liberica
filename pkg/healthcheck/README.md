# Healtcheck package
Package that handles healthchecking of real servers

## Available healthchecks

* HTTPCheck: HTTP(S) Layer 7 check
* IdleTCPConnectionCheck: TCP L4 check

## Example
```go
go func() {
    prometheus.MustRegister(metrics.HttpCheck)
    prometheus.MustRegister(metrics.HttpCheckSeconds)
    prometheus.MustRegister(metrics.HealthcheckResult)
    prometheus.MustRegister(metrics.HealthcheckExecutions)
    http.Handle("/metrics", promhttp.Handler())
    http.ListenAndServe(":2112", nil)
}()
serviceID := "test"
marks := []int{1001, 1002}

hcs := make([]healthcheck.Healthcheck, 2*len(marks), 2*len(marks))
i := 0
for _, mark := range marks {
    hcs[i] = healthcheck.NewHTTPCheck(serviceID, "http://10.200.200.1",
        "localhost", 1000*time.Millisecond, 200, mark)
    i++
    hcs[i] = healthcheck.NewIdleTCPConnectionCheck(serviceID, "10.200.200.1:80",
        300*time.Millisecond, 300*time.Millisecond, mark)
    i++
}
ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)
c := healthcheck.StartHealthchecking(ctx, hcs)
for {
    select {
    case result := <-c:
        log.Printf("check result: %v\n", result)
    case <-ctx.Done():
        return
    }
}
```

The example shown above will trigger the two available healthchecks against a VIP 10.200.200.1 using SO_MARK 1001 and 1002 and it will provide prometheus metrics on http://127.0.0.1:2112/metrics

## Tests
Tests can be triggered using `go test -v`:
```
 go test -v
=== RUN   TestHTTPCheck
=== RUN   TestHTTPCheck/200
=== RUN   TestHTTPCheck/201
--- PASS: TestHTTPCheck (0.00s)
    --- PASS: TestHTTPCheck/200 (0.00s)
    --- PASS: TestHTTPCheck/201 (0.00s)
=== RUN   TestHTTPCheckTimeout
=== RUN   TestHTTPCheckTimeout/100ms
=== RUN   TestHTTPCheckTimeout/210ms
--- PASS: TestHTTPCheckTimeout (0.30s)
    --- PASS: TestHTTPCheckTimeout/100ms (0.10s)
    --- PASS: TestHTTPCheckTimeout/210ms (0.20s)
=== RUN   TestHTTPS
--- PASS: TestHTTPS (0.00s)
=== RUN   TestIdleTCPConnectionCheck
=== RUN   TestIdleTCPConnectionCheck/5s
=== RUN   TestIdleTCPConnectionCheck/20ms
--- PASS: TestIdleTCPConnectionCheck (0.20s)
    --- PASS: TestIdleTCPConnectionCheck/5s (0.10s)
    --- PASS: TestIdleTCPConnectionCheck/20ms (0.10s)
PASS
ok      gitlab.wikimedia.org/repos/sre/liberica/pkg/healthcheck      0.513s
```
