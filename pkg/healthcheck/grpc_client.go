package healthcheck

import (
	"context"
	"fmt"
	"log/slog"
	"net/netip"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/healthcheck"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

type HCMonitorClient interface {
	WatchResults(ctx context.Context, service string, resultCh chan<- Result) (<-chan bool, chan struct{})
	CreateHealthcheck(ctx context.Context, rs Realserver, hcs []HealthcheckConfigurer) error
	DeleteHealthcheck(ctx context.Context, rs Realserver) error
	Close()
}

type HCMonitorGrpcClient struct {
	logger *slog.Logger
	conn   *grpc.ClientConn
}

func NewGrpcClient(logger *slog.Logger, network, address string) (*HCMonitorGrpcClient, error) {
	addr := grpcutils.GetConnectAddress(network, address)

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, grpc.WithKeepaliveParams(grpcutils.KeepaliveClientParameters))
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// DialContext returns before connecting, ctx only controls the setup steps. More details on https://pkg.go.dev/google.golang.org/grpc@v1.46.2#DialContext
	conn, err := grpc.DialContext(ctx, addr, opts...)
	if err != nil {
		return nil, fmt.Errorf("unable to create connection: %w", err)
	}

	return &HCMonitorGrpcClient{
		logger: logger,
		conn:   conn,
	}, nil
}

func (client *HCMonitorGrpcClient) WatchResults(ctx context.Context, service string, resultCh chan<- Result) (<-chan bool, chan struct{}) {
	readyCh := make(chan bool, 1)
	doneCh := make(chan struct{})
	if ctx.Err() != nil {
		close(doneCh)
		return readyCh, doneCh
	}

	c := pb.NewHCMonitorServiceClient(client.conn)
	req := &pb.WatchResultsRequest{
		Svc: service,
	}

	// omitting WaitForReady here as we need WatchResults() to fail fast
	results, err := c.WatchResults(ctx, req)
	if status.Code(err) != codes.OK {
		client.logger.Error("unable to watch results",
			slog.String("service", service),
			slog.Any("error", err),
		)
		close(doneCh)
		return readyCh, doneCh
	}

	go func() {
		readyCh <- true
		defer close(doneCh)
		for {
			res, err := results.Recv()
			switch status.Code(err) {
			case codes.OK:
			case codes.Canceled:
				return
			default:
				client.logger.Error("error receiving healthcheck result",
					slog.Any("error", err),
				)
				return
			}
			addrPort, err := netip.ParseAddrPort(res.Result.Realserver.Address)
			if err != nil {
				client.logger.Warn("invalid address on healthcheck result",
					slog.String("address", res.Result.Realserver.Address),
					slog.Any("error", err),
				)
			}
			r := Result{
				HealthcheckName: res.Result.HealthcheckName,
				Successful:      res.Result.Successful,
				Realserver: Realserver{
					Service:  res.Result.Realserver.Svc,
					Mark:     int(res.Result.Realserver.Mark),
					AddrPort: addrPort,
				},
				HealthcheckID: HealthcheckID(res.Result.HealthcheckId),
			}
			select {
			case resultCh <- r:
			default:
				client.logger.Warn("ignoring healthcheck result")

			}
		}
	}()

	return readyCh, doneCh
}
func (client *HCMonitorGrpcClient) CreateHealthcheck(ctx context.Context, rs Realserver, hcs []HealthcheckConfigurer) error {
	if err := ctx.Err(); err != nil {
		return err
	}

	req := &pb.CreateHealthcheckRequest{
		Realserver: &pbrs.Realserver{
			Address: rs.AddrPort.String(),
			Mark:    uint32(rs.Mark),
			Svc:     rs.Service,
		},
	}

	for _, hc := range hcs {
		switch cfg := hc.AsProto().(type) {
		case pb.HTTPCheckConfiguration:
			httpHcCfg := &pb.HealthcheckConfiguration_HttpConfig{
				HttpConfig: &cfg,
			}
			req.HealthcheckConfiguration = append(req.HealthcheckConfiguration, &pb.HealthcheckConfiguration{Configuration: httpHcCfg})
		case pb.IdleTCPConnectionCheckConfiguration:
			tcpHcCfg := &pb.HealthcheckConfiguration_IdleTcpConfig{
				IdleTcpConfig: &cfg,
			}
			req.HealthcheckConfiguration = append(req.HealthcheckConfiguration, &pb.HealthcheckConfiguration{Configuration: tcpHcCfg})
		default:
		}
	}
	c := pb.NewHCMonitorServiceClient(client.conn)
	if _, err := c.CreateHealthcheck(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to create healthcheck: %w", err)
	}

	return nil
}
func (client *HCMonitorGrpcClient) DeleteHealthcheck(ctx context.Context, rs Realserver) error {
	if err := ctx.Err(); err != nil {
		return err
	}

	req := &pb.DeleteHealthcheckRequest{
		Realserver: &pbrs.Realserver{
			Address: rs.AddrPort.String(),
			Mark:    uint32(rs.Mark),
			Svc:     rs.Service,
		},
	}

	c := pb.NewHCMonitorServiceClient(client.conn)
	if _, err := c.DeleteHealthcheck(ctx, req, grpc.WaitForReady(true)); status.Code(err) != codes.OK {
		return fmt.Errorf("unable to delete healthcheck: %w", err)
	}

	return nil
}

func (client *HCMonitorGrpcClient) Close() {
	if client.conn != nil {
		client.conn.Close()
	}
}
