/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"crypto/tls"
	"errors"
	"log/slog"
	"net"
	"net/http"
	"os"
	"strconv"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

// User-Agent header sent on every request performed by the HTTP(s) healthcheck
const UserAgent = "liberica/0.0.1 (https://wikitech.wikimedia.org/wiki/liberica; sre-traffic@wikimedia.org)"

// String used to identify the HTTP Healthcheck
const HTTPHealthcheckName = "HTTPCheck"

type HTTPCheck struct {
	logger *slog.Logger
	config *HTTPCheckConfig
	client *http.Client
	rs     Realserver
	id     HealthcheckID
}

// Do not follow redirects
func checkRedirectFunc(_ *http.Request, _ []*http.Request) error {
	return http.ErrUseLastResponse
}

func (hc *HTTPCheck) GetCheckPeriod() time.Duration {
	return hc.config.CheckPeriod
}

func (hc *HTTPCheck) GetID() HealthcheckID {
	return hc.id
}

// timeout includes connection time + any redirects + reading the response body
func NewHTTPCheck(logger *slog.Logger, cfg *HTTPCheckConfig, rs Realserver) (*HTTPCheck, error) {
	checkLogger := logger.With(
		slog.String("service", rs.Service),
		slog.String("url", cfg.Url),
		slog.String("host_header", cfg.HostHeader),
		slog.Int("mark", rs.Mark),
	)
	tr := &http.Transport{
		DisableKeepAlives:  true,
		DisableCompression: true,
		TLSClientConfig:    &tls.Config{ServerName: cfg.HostHeader},
		DialContext: (&net.Dialer{
			Control: func(network, address string, rawc syscall.RawConn) error {
				var fdErr error
				ctl := func(fd uintptr) {
					if rs.Mark != 0 {
						fdErr = setSocketMark(int(fd), rs.Mark)
					}
				}
				if err := rawc.Control(ctl); err != nil {
					return err
				}
				return fdErr
			},
		}).DialContext,
	}

	id, err := newHealthcheckID()
	if err != nil {
		checkLogger.Error("unable to generate a healthcheck id",
			slog.Any("error", err),
		)
		return nil, err
	}

	return &HTTPCheck{
		logger: checkLogger,
		id:     id,
		client: &http.Client{
			CheckRedirect: checkRedirectFunc,
			Timeout:       cfg.Timeout,
			Transport:     tr,
		},
		config: cfg,
		rs:     rs,
	}, nil
}

func (hc *HTTPCheck) updateMetrics(statusCode int, d time.Duration, err error) {
	s := strconv.Itoa(statusCode)
	m := strconv.Itoa(hc.rs.Mark)
	rs := hc.rs.AddrPort.String()
	result := 0.0
	r := "down"
	if err == nil && statusCode == hc.config.ExpectedStatusCode {
		result = 1.0
		r = "up"
	} else {
		if errors.Is(err, os.ErrPermission) || errors.Is(err, syscall.EMFILE) {
			r = resultError
			var msg string
			if errors.Is(err, os.ErrPermission) {
				msg = strErrPermission
			} else { // syscall.EMFILE
				msg = strEMFILE
			}
			hc.logger.Error(msg, slog.Any("error", err))
		}
	}

	metrics.HttpCheck.With(prometheus.Labels{
		"status_code": s,
		"service":     hc.rs.Service,
		"url":         hc.config.Url,
		"mark":        m,
		"realserver":  rs,
	}).Inc()
	metrics.HttpCheckSeconds.With(prometheus.Labels{
		"status_code": s,
		"service":     hc.rs.Service,
		"url":         hc.config.Url,
		"mark":        m,
		"realserver":  rs,
	}).Add(float64(d) / float64(time.Second))
	metrics.HealthcheckResult.With(prometheus.Labels{
		"service":     hc.rs.Service,
		"healthcheck": HTTPHealthcheckName,
		"mark":        m,
		"realserver":  rs,
	}).Set(result)
	metrics.HealthcheckExecutions.With(prometheus.Labels{
		"service":     hc.rs.Service,
		"healthcheck": HTTPHealthcheckName,
		"result":      r,
		"mark":        m,
		"realserver":  rs,
	}).Inc()
}

func (hc *HTTPCheck) SingleCheck() Result {
	req, err := http.NewRequest("GET", hc.config.Url, nil)
	result := Result{
		HealthcheckName: HTTPHealthcheckName,
		HealthcheckID:   hc.id,
		Successful:      false,
		Realserver:      hc.rs,
	}
	if err != nil {
		return result
	}
	req.Host = hc.config.HostHeader
	req.Header.Set("User-Agent", UserAgent)
	start := time.Now()
	resp, err := hc.client.Do(req)
	elapsedTime := time.Since(start)
	if err != nil {
		// Metrics should be updated when a HTTP response isn't received too
		hc.updateMetrics(0, elapsedTime, err)
		return result
	}
	defer resp.Body.Close()
	defer hc.updateMetrics(resp.StatusCode, elapsedTime, nil)

	result.Successful = resp.StatusCode == hc.config.ExpectedStatusCode
	return result
}

func (hc *HTTPCheck) Close() {}
