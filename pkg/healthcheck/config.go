/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"cmp"
	"fmt"
	"net/url"
	"slices"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/healthcheck"
	"google.golang.org/protobuf/types/known/durationpb"
)

type HealthcheckConfigurer interface {
	AsProto() any
}

type healthcheckConfig struct {
	Config map[string]any `mapstructure:",remain"`
	Type   string         `mapstructure:"type"`
}

type HTTPCheckConfig struct {
	Url                string        `mapstructure:"url"`
	HostHeader         string        `mapstructure:"host_header"`
	ExpectedStatusCode int           `mapstructure:"status_code"`
	Timeout            time.Duration `mapstructure:"timeout"`
	CheckPeriod        time.Duration `mapstructure:"check_period"`
}

func HTTPCheckConfigSortFn(a, b HTTPCheckConfig) int {
	if c := cmp.Compare(a.Url, b.Url); c != 0 {
		return c
	}
	if c := cmp.Compare(a.HostHeader, b.HostHeader); c != 0 {
		return c
	}
	if c := cmp.Compare(a.ExpectedStatusCode, b.ExpectedStatusCode); c != 0 {
		return c
	}

	if c := cmp.Compare(a.Timeout.Nanoseconds(), b.Timeout.Nanoseconds()); c != 0 {
		return c
	}
	if c := cmp.Compare(a.CheckPeriod.Nanoseconds(), b.CheckPeriod.Nanoseconds()); c != 0 {
		return c
	}

	return 0
}

func (cfg *HTTPCheckConfig) AsProto() any {
	return pb.HTTPCheckConfiguration{
		Url:                cfg.Url,
		HostHeader:         cfg.HostHeader,
		ExpectedStatusCode: uint32(cfg.ExpectedStatusCode),
		Timeout:            durationpb.New(cfg.Timeout),
		CheckPeriod:        durationpb.New(cfg.CheckPeriod),
	}
}

type IdleTCPConnectionCheckConfig struct {
	Address         string        `mapstructure:"address"`
	ConnectTimeout  time.Duration `mapstructure:"timeout"`
	CheckPeriod     time.Duration `mapstructure:"check_period"`
	ReconnectPeriod time.Duration `mapstructure:"reconnect_period"`
}

func IdleTCPConnectionCheckConfigSortFn(a, b IdleTCPConnectionCheckConfig) int {
	if c := cmp.Compare(a.Address, b.Address); c != 0 {
		return c
	}
	if c := cmp.Compare(a.ConnectTimeout.Nanoseconds(), b.ConnectTimeout.Nanoseconds()); c != 0 {
		return c
	}
	if c := cmp.Compare(a.CheckPeriod.Nanoseconds(), b.CheckPeriod.Nanoseconds()); c != 0 {
		return c
	}
	if c := cmp.Compare(a.ReconnectPeriod.Nanoseconds(), b.ReconnectPeriod.Nanoseconds()); c != 0 {
		return c
	}

	return 0
}

func (cfg IdleTCPConnectionCheckConfig) AsProto() any {
	return pb.IdleTCPConnectionCheckConfiguration{
		Address:         cfg.Address,
		ConnectTimeout:  durationpb.New(cfg.ConnectTimeout),
		ReconnectPeriod: durationpb.New(cfg.ReconnectPeriod),
		CheckPeriod:     durationpb.New(cfg.CheckPeriod),
	}
}

func loadHealthchecksConfig(service string) map[string]any {
	key := fmt.Sprintf("services.%s.healthchecks", service)

	return viper.GetStringMap(key)
}

// get HTTPCheck configuration from viper for the given service
func LoadHTTPCheckConfig(service string) ([]HTTPCheckConfig, error) {
	cfgMap := loadHealthchecksConfig(service)

	var ret []HTTPCheckConfig

	for _, cfg := range cfgMap {
		var hcConfig healthcheckConfig
		mapstructure.Decode(cfg, &hcConfig)
		if hcConfig.Type != HTTPHealthcheckName {
			continue
		}
		var httpConfig HTTPCheckConfig
		decoderCfg := &mapstructure.DecoderConfig{
			DecodeHook: mapstructure.StringToTimeDurationHookFunc(),
			Result:     &httpConfig,
		}
		decoder, _ := mapstructure.NewDecoder(decoderCfg)
		decoder.Decode(hcConfig.Config)
		if httpConfig.ExpectedStatusCode == 0 {
			httpConfig.ExpectedStatusCode = 200
		}
		if httpConfig.HostHeader == "" {
			u, err := url.Parse(httpConfig.Url)
			if err != nil {
				return nil, err
			}
			httpConfig.HostHeader = u.Host
		}

		ret = append(ret, httpConfig)
	}

	slices.SortFunc(ret, HTTPCheckConfigSortFn)

	return ret, nil
}

// get IdleTcpConnectionCheck configuration from viper for the given service
func LoadIdleTcpConnectionCheckConfig(service string) ([]IdleTCPConnectionCheckConfig, error) {
	cfgMap := loadHealthchecksConfig(service)
	var ret []IdleTCPConnectionCheckConfig

	for _, cfg := range cfgMap {
		var hcConfig healthcheckConfig
		mapstructure.Decode(cfg, &hcConfig)
		if hcConfig.Type != IdleTCPConnectionHealthcheckName {
			continue
		}

		var itcConfig IdleTCPConnectionCheckConfig
		decoderCfg := &mapstructure.DecoderConfig{
			DecodeHook: mapstructure.StringToTimeDurationHookFunc(),
			Result:     &itcConfig,
		}
		decoder, err := mapstructure.NewDecoder(decoderCfg)
		if err != nil {
			return nil, err
		}
		decoder.Decode(hcConfig.Config)

		ret = append(ret, itcConfig)
	}

	slices.SortFunc(ret, IdleTCPConnectionCheckConfigSortFn)

	return ret, nil
}
