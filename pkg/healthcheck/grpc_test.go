package healthcheck

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net"
	"net/http"
	"net/netip"
	"sync"
	"testing"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	GRPC_NETWORK      = "tcp"
	GRPC_BIND_ADDRESS = "127.0.0.1:5678"
)

// Spawns an HTTP server that returns a 200 on / till the context
// is cancelled, returns the address where the server is listening
// for requests
func httpServer(ctx context.Context) (string, error) {
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		return "", err
	}
	server := &http.Server{
		// aggresive timeout to be able to trigger several runs of IdleTcpConnectionCheck in a short time
		IdleTimeout: 10 * time.Millisecond,
		ReadTimeout: 10 * time.Millisecond,
	}
	// empty handler returns 200 responses
	http.HandleFunc("/", func(http.ResponseWriter, *http.Request) {})
	wg := &sync.WaitGroup{}
	go func() {
		wg.Add(1)
		defer wg.Done()
		server.Serve(listener)
	}()

	go func() {
		// wait till the context is done
		<-ctx.Done()
		shutdownCtx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		defer cancel()
		server.Shutdown(shutdownCtx)
		// wait till the server goroutine has finished
		wg.Wait()
	}()

	return listener.Addr().String(), nil
}

func TestHealthcheckGrpcServer(t *testing.T) {
	signalCtx, signalCancel := context.WithCancel(context.Background())
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	hcm := NewHCMonitor(logger)
	defer hcm.Close()
	srv := NewGrpcServer(logger, hcm)
	serverCh, err := grpcutils.RunServer(signalCtx, logger, srv, GRPC_NETWORK, GRPC_BIND_ADDRESS)
	if err != nil {
		t.Fatalf("unable to run grpc server: %v", err)
		return
	}
	client, err := NewGrpcClient(logger, GRPC_NETWORK, GRPC_BIND_ADDRESS)
	if err != nil {
		t.Fatalf("unable to get conn: %v", err)
		return
	}
	defer client.Close()
	httpCtx, httpCancel := context.WithCancel(context.Background())
	defer httpCancel()
	httpAddr, err := httpServer(httpCtx)
	if err != nil {
		t.Fatalf("unable to spawn local http server: %v", err)
		return
	}
	rs := Realserver{
		AddrPort: netip.MustParseAddrPort(httpAddr),
		Service:  "test-svc",
	}
	t.Run("gRPC/CreateHealthcheck", func(t *testing.T) {
		testCases := []struct {
			name  string
			rs    Realserver
			hcCfg []HealthcheckConfigurer
			want  codes.Code
		}{
			{"Missing HTTPCheckConfig.CheckPeriod", rs,
				[]HealthcheckConfigurer{
					&HTTPCheckConfig{
						Url:                fmt.Sprintf("http://%s/", httpAddr),
						HostHeader:         httpAddr,
						Timeout:            500 * time.Millisecond,
						ExpectedStatusCode: 200,
					},
				}, codes.InvalidArgument,
			},
			{"Missing IdleTCPConnectionCheckConfig.CheckPeriod", rs,
				[]HealthcheckConfigurer{
					&IdleTCPConnectionCheckConfig{
						Address:         httpAddr,
						ConnectTimeout:  10 * time.Millisecond,
						ReconnectPeriod: 10 * time.Millisecond,
					},
				}, codes.InvalidArgument,
			},
			{"Missing IdleTCPConnectionCheckConfig.ReconnectPeriod", rs,
				[]HealthcheckConfigurer{
					&IdleTCPConnectionCheckConfig{
						Address:        httpAddr,
						ConnectTimeout: 10 * time.Millisecond,
						CheckPeriod:    300 * time.Millisecond,
					},
				}, codes.InvalidArgument,
			},
			{"OK", rs,
				[]HealthcheckConfigurer{
					&HTTPCheckConfig{
						Url:                fmt.Sprintf("http://%s/", httpAddr),
						HostHeader:         httpAddr,
						Timeout:            500 * time.Millisecond,
						ExpectedStatusCode: 200,
						CheckPeriod:        1 * time.Second,
					},
					&IdleTCPConnectionCheckConfig{
						Address:         httpAddr,
						ConnectTimeout:  10 * time.Millisecond,
						ReconnectPeriod: 10 * time.Millisecond,
						CheckPeriod:     300 * time.Millisecond,
					},
				}, codes.OK,
			},
		}
		for _, tc := range testCases {
			t.Run(tc.name, func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
				err := client.CreateHealthcheck(ctx, tc.rs, tc.hcCfg)
				switch tc.want {
				case codes.OK:
					if err != nil {
						t.Fatalf("CreateHealthcheck() failed: %v", err)
						return
					}
				default:
					if err != nil {
						got := status.Code(errors.Unwrap(err))
						if tc.want != got {
							t.Fatalf("unexpected CreateHealthcheck() return value. want %v, got %v (err = %v)", tc.want, got, err)
							return
						}
					}
				}
				cancel()

				if tc.want != codes.OK {
					return
				}

				watchCtx, watchCancel := context.WithCancel(context.Background())
				resultsCh := make(chan Result, 128)
				defer close(resultsCh)
				readyCh, doneCh := client.WatchResults(watchCtx, "", resultsCh)
				select {
				case <-readyCh:
				case <-time.After(1 * time.Second):
					t.Fatal("test timeout waiting for WatchResults() to be ready")
				}

				gotResults := map[string]bool{}
				gotResults[HTTPHealthcheckName] = false
				gotResults[IdleTCPConnectionHealthcheckName] = false
				wait := true
				startedAt := time.Now()
				for wait {
					select {
					case r := <-resultsCh:
						name := r.HealthcheckName
						if _, ok := gotResults[name]; !ok {
							t.Errorf("unexpected healthcheck result: %s", name)
						} else {
							gotResults[name] = true
						}
						stop := true
						for _, received := range gotResults {
							if !received {
								stop = false
								break
							}
						}
						if stop { // we received results for every HC that we configured
							wait = false
							watchCancel()
						} else if time.Since(startedAt) >= 1*time.Second {
							t.Fatalf("timeout, finishing test: %+v", gotResults)
							return
						}
					case <-doneCh:
						if wait {
							t.Error("WatchResults() stopped earlier than expected")
						}
					case <-time.After(2 * time.Second):
						t.Fatalf("timeout, finishing test: %+v", gotResults)
						return
					}
				}
			})
		}
	})

	t.Run("gRPC/DeleteHealthcheck", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 200*time.Millisecond)
		defer cancel()
		if err := client.DeleteHealthcheck(ctx, rs); err != nil {
			t.Fatalf("DeleteHealthcheck() failed: %v", err)
			return
		}

		watchCtx, watchCancel := context.WithCancel(context.Background())
		resultsCh := make(chan Result, 10)
		defer close(resultsCh)
		client.WatchResults(watchCtx, "", resultsCh)
		resultCounter := 0
		maxResultExpected := 2 // 1 in-flight result per healthcheck
		for {
			select {
			case <-resultsCh:
				resultCounter += 1
				if resultCounter > maxResultExpected {
					t.Error("healthcheck hasn't been deleted as expected")
					return
				}
			case <-time.After(500 * time.Millisecond):
				watchCancel()
				return
			}
		}
	})

	wait := true
	for wait {
		signalCancel()
		select {
		case <-serverCh:
			wait = false
		case <-time.After(5 * time.Second):
			t.Error("grpc server didn't react to TestSignalContext cancel() in 5 seconds")
			wait = false
		}
	}
}
