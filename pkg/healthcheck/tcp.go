/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net"
	"os"
	"strconv"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

type ConnectionStatus int

const (
	Disconnected ConnectionStatus = iota
	Connected
)

// String used to identify the IdleTCPConnection Healthcheck
const IdleTCPConnectionHealthcheckName = "IdleTCPConnectionCheck"

type IdleTCPConnectionCheck struct {
	lastAttempt     time.Time
	conn            net.Conn
	logger          *slog.Logger
	config          *IdleTCPConnectionCheckConfig
	d               net.Dialer
	rs              Realserver
	status          ConnectionStatus
	attempts        uint
	reconnectPeriod time.Duration
	id              HealthcheckID
}

func (itc *IdleTCPConnectionCheck) String() string {
	return fmt.Sprintf("IdleConnectionCheck: %s/%s", itc.rs.Service, itc.config.Address)
}

func (itc *IdleTCPConnectionCheck) GetCheckPeriod() time.Duration {
	return itc.config.CheckPeriod
}

func (itc *IdleTCPConnectionCheck) GetID() HealthcheckID {
	return itc.id
}

func NewIdleTCPConnectionCheck(logger *slog.Logger, cfg *IdleTCPConnectionCheckConfig, rs Realserver) (*IdleTCPConnectionCheck, error) {
	checkLogger := logger.With(
		slog.String("service", rs.Service),
		slog.String("address", cfg.Address),
		slog.Int("mark", rs.Mark),
	)

	id, err := newHealthcheckID()
	if err != nil {
		checkLogger.Error("unable to generate a healthcheck id",
			slog.Any("error", err),
		)
		return nil, err
	}

	return &IdleTCPConnectionCheck{
		logger: checkLogger,
		id:     id,
		config: cfg,
		d: net.Dialer{Timeout: cfg.ConnectTimeout,
			Control: func(network, address string, rawc syscall.RawConn) error {
				var fdErr error
				ctl := func(fd uintptr) {
					if rs.Mark != 0 {
						fdErr = setSocketMark(int(fd), rs.Mark)
					}
				}
				if err := rawc.Control(ctl); err != nil {
					return err
				}
				return fdErr
			},
		},
		status:   Disconnected,
		attempts: 0,
		rs:       rs,
	}, nil
}

func (itc *IdleTCPConnectionCheck) updateMetrics(err error) {
	m := strconv.Itoa(itc.rs.Mark)
	rs := itc.rs.AddrPort.String()
	result := 0.0
	r := resultDown
	if err == nil && itc.healthy() {
		result = 1.0
		r = resultUp
	} else {
		if errors.Is(err, os.ErrPermission) || errors.Is(err, syscall.EMFILE) {
			r = resultError
			var msg string
			if errors.Is(err, os.ErrPermission) {
				msg = strErrPermission
			} else { // syscall.EMFILE
				msg = strEMFILE
			}
			itc.logger.Error(msg, slog.Any("error", err))
		}
	}

	metrics.HealthcheckResult.With(prometheus.Labels{
		"service":     itc.rs.Service,
		"healthcheck": IdleTCPConnectionHealthcheckName,
		"mark":        m,
		"realserver":  rs,
	}).Set(result)
	metrics.HealthcheckExecutions.With(prometheus.Labels{
		"service":     itc.rs.Service,
		"healthcheck": IdleTCPConnectionHealthcheckName,
		"result":      r,
		"mark":        m,
		"realserver":  rs,
	}).Inc()
}

func (itc *IdleTCPConnectionCheck) connectionClosed() bool {
	one := make([]byte, 1)
	itc.conn.(*net.TCPConn).SetReadDeadline(time.Now().Add(100 * time.Millisecond))
	_, err := itc.conn.Read(one)
	if errors.Is(err, io.EOF) {
		itc.status = Disconnected
		itc.conn.Close()
		return true
	}

	return false
}

func (itc *IdleTCPConnectionCheck) SingleCheck() Result {
	var err error
	if itc.status == Disconnected {
		err = itc.connect()
	} else {
		closed := itc.connectionClosed()
		// immediately reconnect to te be able to update the monitor status
		// to failed in one singleCheck() call if the service is actually down
		if closed {
			err = itc.connect()
		}
	}
	itc.updateMetrics(err)

	return Result{
		HealthcheckName: IdleTCPConnectionHealthcheckName,
		HealthcheckID:   itc.id,
		Successful:      itc.healthy(),
		Realserver:      itc.rs,
	}
}

func (itc *IdleTCPConnectionCheck) connect() error {
	// Attempt to reconnect immediately just once
	if itc.attempts >= 1 && time.Since(itc.lastAttempt) < itc.reconnectPeriod {
		time.Sleep(itc.reconnectPeriod)
	}

	var err error
	itc.attempts++
	itc.lastAttempt = time.Now()
	itc.conn, err = itc.d.Dial("tcp", itc.config.Address)
	if err != nil {
		return err
	}

	itc.conn.(*net.TCPConn).SetKeepAlive(true)
	itc.conn.(*net.TCPConn).SetKeepAlivePeriod(15 * time.Second)
	itc.status = Connected
	itc.attempts = 0
	return nil
}

func (itc *IdleTCPConnectionCheck) healthy() bool {
	// Healthcheck will report an unhealthy endpoint iff
	// it is unable to reconnect after detecting a closed connection
	if itc.attempts >= 1 && itc.status == Disconnected {
		return false
	}

	return true
}

func (itc *IdleTCPConnectionCheck) Close() {
	if itc.status == Connected {
		itc.conn.Close()
	}
}
