/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"context"
	"errors"
	"log/slog"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

// each worker runs healthchecks for a given realserver
// closing stopCh will make the worker to stop
type worker struct {
	resultCh chan Result
	stopCh   chan struct{}
	rs       Realserver
}

// each subscriber gets results via resultCh until
// doneCh is closed. Subscriber needs to provide
// resultCh, doneCh is returned on succesful subscription
type subscriber struct {
	resultCh chan Result
	doneCh   chan struct{}
	service  string
}

// the Healthcheck broker is in charge of spawning workers
// collecting results from them and multiplexing them to any
// subscribers
type HCBroker struct {
	logger           *slog.Logger
	stopCh           chan struct{}    // channel used to stop HCBroker
	publishResultsCh chan Result      // channel used by workers to report Results
	addWorkerCh      chan worker      // channel used to add new workers
	removeWorkerCh   chan Realserver  // channel used to remove workers
	subCh            chan subscriber  // channel used to add subscribers
	unsubCh          chan chan Result // channel used to remove subscribers
}

func NewHCBroker(logger *slog.Logger) *HCBroker {
	return &HCBroker{
		logger:           logger,
		stopCh:           make(chan struct{}),
		publishResultsCh: make(chan Result, 1024),
		addWorkerCh:      make(chan worker),
		removeWorkerCh:   make(chan Realserver),
		subCh:            make(chan subscriber),
		unsubCh:          make(chan chan Result),
	}
}

func (b *HCBroker) Start() {
	workers := map[Realserver]worker{}
	subscribers := map[chan Result]subscriber{}
	results := map[Realserver]map[HealthcheckID]Result{}

	sendResultFunc := func(result Result, resultCh chan Result) {
		select {
		case resultCh <- result:
			updateHCBrokerMetrics("broadcast_result", nil)
		default:
			updateHCBrokerMetrics("broadcast_result", errors.New("resultCh busy"))
		}
	}

	b.logger.Debug("starting HCBroker")
	for {
		select {
		case <-b.stopCh:
			b.logger.Debug("stopping HCBroker")
			for _, subscriber := range subscribers {
				close(subscriber.doneCh)
			}
			return
		case wrk := <-b.addWorkerCh:
			if oldWrk, present := workers[wrk.rs]; present {
				b.logger.Debug("updating worker", slog.Int("mark", wrk.rs.Mark))
				close(oldWrk.stopCh)
				delete(workers, wrk.rs)
				delete(results, wrk.rs)
			}
			workers[wrk.rs] = wrk
			results[wrk.rs] = map[HealthcheckID]Result{}
		case rs := <-b.removeWorkerCh:
			wrk, present := workers[rs]
			if present {
				close(wrk.stopCh)
				delete(workers, rs)
				delete(results, rs)
			}
		case subscriber := <-b.subCh:
			subscribers[subscriber.resultCh] = subscriber
			for _, rsResults := range results {
				for _, result := range rsResults {
					if subscriber.service == "" || subscriber.service == result.Realserver.Service {
						sendResultFunc(result, subscriber.resultCh)
					}
				}
			}
		case resultCh := <-b.unsubCh:
			subscriber, present := subscribers[resultCh]
			if present {
				close(subscriber.doneCh)
				delete(subscribers, resultCh)
			}
		case result := <-b.publishResultsCh:
			results[result.Realserver][result.HealthcheckID] = result

			for resultCh, subscriber := range subscribers {
				if subscriber.service == "" || subscriber.service == result.Realserver.Service {
					sendResultFunc(result, resultCh)
				}
			}
		}
	}
}

func (b *HCBroker) Stop() {
	close(b.stopCh)
}

func (b *HCBroker) AddWorker(ctx context.Context, rs Realserver, resultCh chan Result) (chan struct{}, error) {
	var err error
	wrk := worker{
		rs:       rs,
		resultCh: resultCh,
		stopCh:   make(chan struct{}),
	}

	select {
	case b.addWorkerCh <- wrk:
	case <-ctx.Done():
		// context cancelled, clean up stop channel before returning
		close(wrk.stopCh)
		err = ctx.Err()
	}

	updateHCBrokerMetrics("add_worker", err)
	return wrk.stopCh, err
}

func (b *HCBroker) RemoveWorker(ctx context.Context, rs Realserver) error {
	var err error
	select {
	case b.removeWorkerCh <- rs:
	case <-ctx.Done():
		err = ctx.Err()
	}

	updateHCBrokerMetrics("remove_worker", err)
	return err
}

func (b *HCBroker) Publish(r Result) {
	var err error
	select {
	case b.publishResultsCh <- r:
	default:
		err = errors.New("Publish would block")
	}
	updateHCBrokerMetrics("publish_result", err)
}

func (b *HCBroker) Subscribe(ctx context.Context, resultCh chan Result, service string) (chan struct{}, error) {
	sub := subscriber{
		resultCh: resultCh,
		doneCh:   make(chan struct{}),
		service:  service,
	}
	var err error
	select {
	case b.subCh <- sub:
	case <-ctx.Done():
		close(sub.doneCh)
		err = ctx.Err()
	}

	updateHCBrokerMetrics("subscribe", err)
	return sub.doneCh, err
}

func (b *HCBroker) Unsubscribe(ctx context.Context, resultCh chan Result) error {
	var err error
	select {
	case b.unsubCh <- resultCh:
	case <-ctx.Done():
		err = ctx.Err()
	}

	updateHCBrokerMetrics("unsubscribe", err)
	return err
}

type HCMonitor struct {
	logger *slog.Logger
	broker *HCBroker
	wg     sync.WaitGroup
}

func NewHCMonitor(logger *slog.Logger) *HCMonitor {
	broker := NewHCBroker(logger)
	hcm := &HCMonitor{
		logger: logger,
		broker: broker,
	}
	go func() {
		hcm.wg.Add(1)
		defer hcm.wg.Done()
		broker.Start()

	}()
	return hcm
}

func (hcm *HCMonitor) Close() {
	hcm.logger.Debug("stopping HCBroker")
	hcm.broker.Stop()
	hcm.wg.Wait()
	hcm.logger.Debug("HCBroker stopped")
}

// Adds/Updates healthchecks for the given realserver
func (hcm *HCMonitor) AddRealserver(ctx context.Context, rs Realserver, hcs []Healthcheck) error {
	resultCh := make(chan Result, 128)
	stopCh, err := hcm.broker.AddWorker(ctx, rs, resultCh)
	if err != nil {
		return err
	}

	checkCtx, cancel := context.WithCancel(context.Background())
	doneCh := StartHealthchecking(checkCtx, hcs, resultCh)

	running := true
	cleanup := func() {
		hcm.logger.Debug("stopping healthcheck goroutine", slog.Int("mark", rs.Mark))
		cancel()
		running = false
	}

	go func() {
		for running {
			select {
			case result := <-resultCh:
				hcm.logger.Debug("new healthcheck result",
					slog.String("healthcheck", result.HealthcheckName),
					slog.Int("mark", result.Realserver.Mark),
					slog.Bool("successful", result.Successful),
				)
				hcm.broker.Publish(result)
			case <-doneCh:
				cleanup()
			case <-stopCh:
				cleanup()
			}
		}
		<-doneCh // wait till StartHealthchecking goroutine is actually done
		hcm.logger.Debug("healthcheck goroutine stopped", slog.Int("mark", rs.Mark))
	}()
	return nil
}

// Stops healthchecking the given realserver
func (hcm *HCMonitor) RemoveRealserver(ctx context.Context, rs Realserver) error {
	err := hcm.broker.RemoveWorker(ctx, rs)
	return err
}

// Subscribe for healthcheck results. HCMonitor will discard results if
// the results channel isn't consumed fast enough
// Results will be sent while the returned channel is not closed
func (hcm *HCMonitor) Subscribe(ctx context.Context, resultCh chan Result, service string) (chan struct{}, error) {
	doneCh, err := hcm.broker.Subscribe(ctx, resultCh, service)
	return doneCh, err
}

// Unsubscribe for healthcheck results
func (hcm *HCMonitor) Unsubscribe(ctx context.Context, resultCh chan Result) error {
	err := hcm.broker.Unsubscribe(ctx, resultCh)
	return err
}

func updateHCBrokerMetrics(op string, err error) {
	result := "ok"
	if err != nil {
		result = "ko"
	}
	metrics.HealthcheckMonitorOperations.With(prometheus.Labels{
		"operation": op,
		"result":    result,
	}).Inc()
}
