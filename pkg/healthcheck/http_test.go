/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
	"time"
)

func testHttpHeaderValue(t *testing.T, headerName string, header map[string][]string, expected []string) {
	got := header[headerName]

	if len(got) != len(expected) {
		t.Errorf("Unexpected amount of HTTP Header %v occurrences: %v VS %v\n", headerName, len(got), len(expected))
	}
	for i, v := range got {
		if v != expected[i] {
			t.Errorf("Unexpected %v value: %v VS %v\n", headerName, v, expected[i])
		}
	}
}

func TestHTTPCheck(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("OK"))
		testHttpHeaderValue(t, "User-Agent", req.Header, []string{UserAgent})
		testHttpHeaderValue(t, "Connection", req.Header, []string{"close"})
	}))
	defer server.Close()

	var tests = []struct {
		statusCode int
		want       Result
	}{
		{200, Result{HealthcheckName: HTTPHealthcheckName, Realserver: Realserver{}, Successful: true}},
		{201, Result{HealthcheckName: HTTPHealthcheckName, Realserver: Realserver{}, Successful: false}},
	}

	url, _ := url.Parse(server.URL)

	for _, tt := range tests {
		testname := strconv.Itoa(tt.statusCode)
		t.Run(testname, func(t *testing.T) {
			cfg := &HTTPCheckConfig{
				Url:                server.URL,
				HostHeader:         url.Host,
				Timeout:            1 * time.Second,
				ExpectedStatusCode: tt.statusCode,
				CheckPeriod:        1 * time.Second,
			}
			hc, err := NewHTTPCheck(logger, cfg, Realserver{})
			if err != nil {
				t.Fatalf("unable to get HTTP check: %v", err)
				return
			}
			tt.want.HealthcheckID = hc.GetID() // inject generated healthcheckID
			ctx, cancel := context.WithCancel(context.TODO())
			resultCh := make(chan Result, 1)
			defer close(resultCh)
			doneCh := check(ctx, hc, resultCh)
			result := <-resultCh
			cancel()
			select {
			case <-doneCh:
			case <-time.After(1 * time.Second):
				t.Error("check() didn't finish after canceling the context")
			}
			if result != tt.want {
				t.Errorf("got %v, want %v", result, tt.want)
			}

		})
	}
}

func TestHTTPCheckTimeout(t *testing.T) {
	serverLag := 200 * time.Millisecond
	clientTimeout := 100 * time.Millisecond

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("OK"))
		// Slow server
		time.Sleep(serverLag)
	}))

	var tests = []struct {
		timeout time.Duration
		want    Result
	}{
		{clientTimeout, Result{HealthcheckName: HTTPHealthcheckName, Realserver: Realserver{}, Successful: false}},
		{serverLag + (10 * time.Millisecond), Result{HealthcheckName: HTTPHealthcheckName, Realserver: Realserver{}, Successful: true}},
	}

	url, _ := url.Parse(server.URL)
	for _, tt := range tests {
		testname := fmt.Sprintf("%v", tt.timeout)
		t.Run(testname, func(t *testing.T) {
			cfg := &HTTPCheckConfig{
				Url:                server.URL,
				HostHeader:         url.Host,
				Timeout:            tt.timeout,
				ExpectedStatusCode: 200,
				CheckPeriod:        1 * time.Second,
			}
			hc, err := NewHTTPCheck(logger, cfg, Realserver{})
			if err != nil {
				t.Fatalf("unable to get HTTP check: %v", err)
				return
			}
			tt.want.HealthcheckID = hc.GetID() // inject healthcheck ID
			ctx, cancel := context.WithCancel(context.TODO())
			resultCh := make(chan Result, 1)
			defer close(resultCh)
			doneCh := check(ctx, hc, resultCh)
			result := <-resultCh
			cancel()
			select {
			case <-doneCh:
			case <-time.After(1 * time.Second):
				t.Error("check() didn't finish after canceling the context")
			}
			if result != tt.want {
				t.Errorf("got %v, want %v", result, tt.want)
			}
		})
	}

	// test TCP connection timeout as well
	server.Close()
	cfg := &HTTPCheckConfig{
		Url:                server.URL,
		HostHeader:         url.Host,
		ExpectedStatusCode: 200,
		Timeout:            clientTimeout,
		CheckPeriod:        1 * time.Second,
	}
	hc, err := NewHTTPCheck(logger, cfg, Realserver{})
	if err != nil {
		t.Fatalf("unable to get HTTP check: %v", err)
		return
	}
	ctx, cancel := context.WithTimeout(context.TODO(), clientTimeout*2)
	resultCh := make(chan Result, 1)
	defer close(resultCh)
	doneCh := check(ctx, hc, resultCh)
	waitResults := true
	for waitResults {
		select {
		case check := <-resultCh:
			if check.Successful != false {
				t.Error("Unexpected test result")
			}
			waitResults = false
		case <-ctx.Done():
			t.Error("check should have finished earlier than this")
			waitResults = false
		}
	}
	cancel()
	select {
	case <-doneCh:
	case <-time.After(1 * time.Second):
		t.Errorf("check() didn't finish after canceling the context")
	}
}

func TestHTTPS(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	server := httptest.NewTLSServer(http.HandlerFunc(func(rw http.ResponseWriter, _ *http.Request) {
		rw.Write([]byte("OK"))
	}))
	defer server.Close()

	url, _ := url.Parse(server.URL)
	cfg := &HTTPCheckConfig{
		Url:                server.URL,
		HostHeader:         url.Hostname(),
		Timeout:            1 * time.Second,
		ExpectedStatusCode: 200,
		CheckPeriod:        1 * time.Second,
	}
	hc, err := NewHTTPCheck(logger, cfg, Realserver{})
	if err != nil {
		t.Fatalf("unable to get HTTP check: %v", err)
		return
	}

	// inject test server RootCA into client to avoid certificate errors
	transport := server.Client().Transport.(*http.Transport)
	clientConfig := transport.TLSClientConfig
	clientTransport := hc.client.Transport.(*http.Transport)
	clientTransport.TLSClientConfig.RootCAs = clientConfig.RootCAs
	ctx, cancel := context.WithCancel(context.TODO())
	resultCh := make(chan Result, 1)
	doneCh := check(ctx, hc, resultCh)
	if !(<-resultCh).Successful {
		t.Error("Unexpected HTTPCheck failure")
	}
	cancel()
	select {
	case <-doneCh:
	case <-time.After(1 * time.Second):
		t.Error("check() didn't finish after canceling the context")
	}
}
