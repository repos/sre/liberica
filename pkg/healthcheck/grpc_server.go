package healthcheck

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/netip"
	"time"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	pb "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/healthcheck"
	pbrs "gitlab.wikimedia.org/repos/sre/liberica/pkg/protos/realserver"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct {
	pb.UnimplementedHCMonitorServiceServer
	HealthcheckMonitor *HCMonitor
	logger             *slog.Logger
}

var ErrSubscriptionEnded = errors.New("subscription ended")

func NewGrpcServer(logger *slog.Logger, hcm *HCMonitor) *grpc.Server {
	s := &server{
		logger:             logger,
		HealthcheckMonitor: hcm,
	}

	var opts []grpc.ServerOption
	opts = append(opts, grpc.KeepaliveParams(grpcutils.KeepaliveServerParameters))
	srv := grpc.NewServer(opts...)
	pb.RegisterHCMonitorServiceServer(srv, s)

	return srv
}

func (s *server) WatchResults(in *pb.WatchResultsRequest, stream pb.HCMonitorService_WatchResultsServer) error {
	ctx := stream.Context()
	resultCh := make(chan Result, 128)
	defer close(resultCh)
	// no need to validate in.Svc as it's treated as an optional request parameter
	// and the default string value (empty string) disables filtering by service
	doneCh, err := s.HealthcheckMonitor.Subscribe(ctx, resultCh, in.Svc)
	if err != nil {
		return fmt.Errorf("unable to subscribe: %v", err)
	}
	for {
		select {
		case result := <-resultCh:
			mark := uint32(result.Realserver.Mark)
			rs := pbrs.Realserver{
				Mark:    mark,
				Address: result.Realserver.AddrPort.String(),
				Svc:     result.Realserver.Service,
			}
			stream.Send(&pb.WatchResultsResponse{
				Result: &pb.Result{
					HealthcheckName: result.HealthcheckName,
					Successful:      result.Successful,
					Realserver:      &rs,
					HealthcheckId:   uint32(result.HealthcheckID),
				},
			})
		case <-doneCh:
			return status.Errorf(codes.Aborted, "subscription ended: %v", ErrSubscriptionEnded)
		case <-ctx.Done():
			unsubscribeCtx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancel()
			if err := s.HealthcheckMonitor.Unsubscribe(unsubscribeCtx, resultCh); err != nil {
				return err
			}
			return nil
		}
	}

	return nil
}

func (s *server) CreateHealthcheck(ctx context.Context, in *pb.CreateHealthcheckRequest) (*pb.CreateHealthcheckResponse, error) {
	if in.Realserver.Svc == "" {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Realserver.Svc")
	}
	if in.Realserver.Address == "" {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Realserver.Address")
	}
	if len(in.HealthcheckConfiguration) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required ffield: Healthchecks")
	}

	ap, err := netip.ParseAddrPort(in.Realserver.Address)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid field: Realserver.Address")
	}
	rs := Realserver{
		Service:  in.Realserver.Svc,
		AddrPort: ap,
		Mark:     int(in.Realserver.Mark),
	}
	var hcs []Healthcheck
	for i, hcConfig := range in.HealthcheckConfiguration {
		switch cfg := hcConfig.Configuration.(type) {
		case *pb.HealthcheckConfiguration_HttpConfig:
			if cfg.HttpConfig.Timeout == nil {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].HttpConfig.Timeout", i)
			}
			if cfg.HttpConfig.CheckPeriod == nil {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].HttpConfig.CheckPeriod", i)
			}
			if cfg.HttpConfig.Url == "" {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].HttpConfig.Url", i)
			}
			if cfg.HttpConfig.HostHeader == "" {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].HttpConfig.HostHeader", i)
			}
			if cfg.HttpConfig.ExpectedStatusCode == 0 {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].HttpConfig.ExpectedStatusCode", i)
			}
			timeout := cfg.HttpConfig.Timeout.AsDuration()
			checkPeriod := cfg.HttpConfig.CheckPeriod.AsDuration()
			if checkPeriod <= 0 {
				return nil, status.Errorf(codes.InvalidArgument, "invalid field (non-positive) interval): Healthchecks[%d].HttpConfig.CheckPeriod", i)
			}

			httpHcCfg := &HTTPCheckConfig{
				Url:                cfg.HttpConfig.Url,
				HostHeader:         cfg.HttpConfig.HostHeader,
				ExpectedStatusCode: int(cfg.HttpConfig.ExpectedStatusCode),
				Timeout:            timeout,
				CheckPeriod:        checkPeriod,
			}
			httpHc, err := NewHTTPCheck(s.logger, httpHcCfg, rs)
			if err != nil {
				return nil, status.Errorf(codes.Unavailable, "unable to get a HTTP check instance")
			}

			hcs = append(hcs, httpHc)
		case *pb.HealthcheckConfiguration_IdleTcpConfig:
			if cfg.IdleTcpConfig.ConnectTimeout == nil {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].TcpConfig.ConnectTimeout", i)
			}
			if cfg.IdleTcpConfig.ReconnectPeriod == nil {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].TcpConfig.ReconnectPeriod", i)
			}
			if cfg.IdleTcpConfig.CheckPeriod == nil {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].TcpConfig.CheckPeriod", i)
			}
			if cfg.IdleTcpConfig.Address == "" {
				return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Healthchecks[%d].TcpConfig.Address", i)
			}
			if _, err := netip.ParseAddrPort(cfg.IdleTcpConfig.Address); err != nil {
				return nil, status.Errorf(codes.InvalidArgument, "invalid field: Healthchecks[%d].TcpConfig.Address", i)
			}
			reconnectPeriod := cfg.IdleTcpConfig.ReconnectPeriod.AsDuration()
			if reconnectPeriod <= 0 {
				return nil, status.Errorf(codes.InvalidArgument, "invalid field (non-positive) interval): Healthchecks[%d].TcpConfig.ReconnectPeriod", i)
			}

			checkPeriod := cfg.IdleTcpConfig.CheckPeriod.AsDuration()
			if checkPeriod <= 0 {
				return nil, status.Errorf(codes.InvalidArgument, "invalid field (non-positive) interval): Healthchecks[%d].TcpConfig.CheckPeriod", i)
			}
			tcpHcCfg := &IdleTCPConnectionCheckConfig{
				Address:         cfg.IdleTcpConfig.Address,
				ConnectTimeout:  cfg.IdleTcpConfig.ConnectTimeout.AsDuration(),
				ReconnectPeriod: cfg.IdleTcpConfig.ReconnectPeriod.AsDuration(),
				CheckPeriod:     cfg.IdleTcpConfig.CheckPeriod.AsDuration(),
			}
			tcpHc, err := NewIdleTCPConnectionCheck(s.logger, tcpHcCfg, rs)
			if err != nil {
				return nil, status.Errorf(codes.Unavailable, "unable to get a TCP connection check instance")
			}

			hcs = append(hcs, tcpHc)

		default:
			// silently ignore unsupported healthcheck type
		}
	}

	if len(hcs) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "invalid field: HealthcheckConfiguration didn't contain any supported healthcheck")
	}

	if err := s.HealthcheckMonitor.AddRealserver(ctx, rs, hcs); err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "unable to create the specified Realserver: %v", err)
	}
	return &pb.CreateHealthcheckResponse{}, nil
}

func (s *server) DeleteHealthcheck(ctx context.Context, in *pb.DeleteHealthcheckRequest) (*pb.DeleteHealthcheckResponse, error) {
	if in.Realserver.Address == "" {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Realserver.Address")
	}
	if in.Realserver.Svc == "" {
		return nil, status.Errorf(codes.InvalidArgument, "request missing required field: Realserver.Svc")
	}
	ap, err := netip.ParseAddrPort(in.Realserver.Address)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid field: Realserver.Address")
	}
	rs := Realserver{
		Service:  in.Realserver.Svc,
		AddrPort: ap,
		Mark:     int(in.Realserver.Mark),
	}

	if err := s.HealthcheckMonitor.RemoveRealserver(ctx, rs); err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "unable to remove the specified Realserver: %v", err)
	}

	return &pb.DeleteHealthcheckResponse{}, nil
}
