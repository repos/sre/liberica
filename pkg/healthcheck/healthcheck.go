/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/package healthcheck

import (
	"context"
	"fmt"
	"net/netip"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/mtchavez/jenkins"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

type Healthcheck interface {
	SingleCheck() Result
	Close()
	GetCheckPeriod() time.Duration
	GetID() HealthcheckID
}

type Result struct {
	HealthcheckName string
	Realserver      Realserver
	HealthcheckID   HealthcheckID
	Successful      bool
}

type Realserver struct {
	AddrPort netip.AddrPort // Address and port
	Service  string         // String identifier useful for logging/debugging purposes
	Mark     int            // Mark will be used to send healthchecks when IPIP encapulation is in place
}

const (
	strErrPermission = "access denied performing TCP connection. CAP_NET_ADMIN required if mark > 0"
	strEMFILE        = "check ulimit -n"
)

const (
	resultUp    = "up"
	resultDown  = "down"
	resultError = "error"
)

func updateMetrics(result Result) {
	m := strconv.Itoa(result.Realserver.Mark)
	r := resultDown
	if result.Successful {
		r = resultUp
	}
	metrics.HealthcheckResultDiscarded.With(prometheus.Labels{
		"mark":   m,
		"result": r,
	}).Inc()
}

// Start healthchecking several Healthchecks. Returns a channel that will be closed
// when healthchecking finishes. Healtchecks results are sent to resultCh, if the
// consumer of that channel isn't fast enough results will be dropped
func StartHealthchecking(ctx context.Context, hcs []Healthcheck, resultCh chan<- Result) <-chan struct{} {
	ch := make(chan struct{}, 1)
	var wg sync.WaitGroup

	wg.Add(len(hcs))
	for _, hc := range hcs {
		go func(hc Healthcheck) {
			defer wg.Done()
			doneCh := check(ctx, hc, resultCh)
			for {
				select {
				case <-doneCh:
					return
				case <-ctx.Done():
					return
				}
			}
		}(hc)
	}

	// Clean up after we are done
	go func() {
		// Waits till all the goroutines writing on c have finished
		wg.Wait()
		close(ch)
	}()

	return ch
}

// Trigger a healthcheck periodically
func check(ctx context.Context, hc Healthcheck, resultCh chan<- Result) <-chan struct{} {
	doneCh := make(chan struct{}, 1)
	ticker := time.NewTicker(hc.GetCheckPeriod())
	go func() {
		defer ticker.Stop()
		defer hc.Close()

		result := hc.SingleCheck()
		// issue a check immediately
		select {
		case resultCh <- result:
		default:
			updateMetrics(result)
		}

		lastResult := result

		for {
			select {
			case <-ticker.C:
				result := hc.SingleCheck()
				if lastResult.Successful != result.Successful {
					select {
					case resultCh <- result:
					default:
						updateMetrics(result)
					}
				}
				lastResult = result
			case <-ctx.Done():
				close(doneCh)
				return
			}
		}
	}()
	return doneCh
}

// returns a unique ID for a healthcheck instance
// this will be used by HCMonitor to tell between
// instances of the same healthcheck with different
// configuration
type HealthcheckID uint32

func newHealthcheckID() (HealthcheckID, error) {
	uid, err := uuid.NewRandom()
	if err != nil {
		return 0, fmt.Errorf("unable to get a new UUIDv4: %w", err)
	}
	uuidBytes, err := uid.MarshalBinary()
	if err != nil {
		return 0, fmt.Errorf("unable to marshal uuid: %w", err)
	}
	h := jenkins.New()
	if _, err := h.Write(uuidBytes); err != nil {
		return 0, fmt.Errorf("unable to calculate jenkins hash: %w", err)
	}
	return HealthcheckID(h.Sum32()), nil
}
