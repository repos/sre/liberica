/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"net"
	"testing"
	"time"
)

func handleConnections(t *testing.T, l net.Listener, closeAfter time.Duration) {
	for {
		conn, err := l.Accept()
		if err != nil {
			return
		}
		go func(c net.Conn) {
			time.Sleep(closeAfter)
			c.Close()
		}(conn)
	}
}

func tcpClient(address string) error {
	const maxAttempts = 16
	var d net.Dialer
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var (
		err  error
		conn net.Conn
	)
	for i := 0; i < maxAttempts; i++ {
		conn, err = d.DialContext(ctx, "tcp", address)
		if err != nil {
			time.Sleep(time.Duration(i) * time.Millisecond)
			continue
		}
		conn.Close()
		return nil
	}
	return err
}

func TestIdleTCPConnectionCheck(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	var tests = []struct {
		closeConnectionAfter time.Duration
	}{
		{5 * time.Second},
		{20 * time.Millisecond},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf("%v", tt.closeConnectionAfter)
		t.Run(testname, func(t *testing.T) {
			l, err := net.Listen("tcp", ":0")
			if err != nil {
				t.Fatalf("Unable to listen: %v\n", err)
			}
			defer l.Close()
			go handleConnections(t, l, tt.closeConnectionAfter)
			// block till the tcp server is ready
			if err := tcpClient(l.Addr().String()); err != nil {
				t.Fatal("tcp server isn't ready")
				return
			}

			reconnectPeriod := 10 * time.Millisecond

			cfg := &IdleTCPConnectionCheckConfig{
				Address:         l.Addr().String(),
				ConnectTimeout:  100 * time.Millisecond,
				ReconnectPeriod: reconnectPeriod,
				CheckPeriod:     10 * time.Millisecond,
			}
			hc, err := NewIdleTCPConnectionCheck(logger, cfg, Realserver{})
			if err != nil {
				t.Fatalf("unable to get IdleTCPConnection check: %v", err)
				return
			}
			ctx, cancel := context.WithCancel(context.TODO())
			defer cancel()

			resultCh := make(chan Result, 1)
			doneCh := check(ctx, hc, resultCh)
			timer := time.NewTimer(10 * reconnectPeriod)
			stopServerTimer := time.NewTimer(5 * reconnectPeriod)
			serverRunning := true
			var serverStoppedAt time.Time
			want := Result{HealthcheckName: IdleTCPConnectionHealthcheckName, HealthcheckID: hc.GetID(), Realserver: Realserver{}, Successful: true}

			for {
				select {
				case check := <-resultCh:
					if !serverRunning && time.Since(serverStoppedAt) >= reconnectPeriod {
						want.Successful = false
					}
					if check != want {
						t.Errorf("Unexpected healthcheck result, got %v want %v", check, want)
					}
				case <-stopServerTimer.C:
					l.Close()
					serverStoppedAt = time.Now()
					serverRunning = false
				case <-timer.C:
					return
				case <-doneCh:
					t.Error("check() finished unexpectedly")
				}
			}
		})
	}
}

func TestIdleTCPSingleCheck(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatalf("Unable to listen: %v\n", err)
	}
	defer l.Close()
	// server will close the connection after 60 seconds
	go handleConnections(t, l, 60*time.Second)
	// block till the tcp server is ready
	if err := tcpClient(l.Addr().String()); err != nil {
		t.Fatal("tcp server isn't ready")
		return
	}
	cfg := &IdleTCPConnectionCheckConfig{
		Address:         l.Addr().String(),
		ConnectTimeout:  100 * time.Millisecond,
		ReconnectPeriod: 100 * time.Millisecond,
		CheckPeriod:     300 * time.Millisecond,
	}
	hc, err := NewIdleTCPConnectionCheck(logger, cfg, Realserver{})
	if err != nil {
		t.Fatalf("unable to get IdleTCPConnection check: %v", err)
		return
	}
	// buffered channel to avoid false negatives if code doesn't consume
	// rCh fast enough
	iterations := 3
	rCh := make(chan Result, iterations)
	// SingleCheck is a blocking function so spawn it on a goroutine
	go func() {
		for i := 0; i < iterations; i++ {
			rCh <- hc.SingleCheck()
		}
	}()

	for i := 0; i < iterations; i++ {
		select {
		case r := <-rCh:
			if !r.Successful {
				t.Error("unexpected healthcheck fail")
			}
		case <-time.After(200 * time.Millisecond):
			t.Fatal("SingleCheck() didn't return in the expected time")
		}
	}
}
