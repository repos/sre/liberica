package healthcheck

import (
	"context"
	"testing"
	"time"
)

const testHealthcheckName = "test healthcheck name"

type TestHealthcheck struct {
	singleCheckCalledTimes int
	closedCalledTimes      int
	Rs                     Realserver
}

func (thc *TestHealthcheck) GetCheckPeriod() time.Duration {
	return 1 * time.Millisecond
}

func (thc *TestHealthcheck) Close() {
	thc.closedCalledTimes++
}

func (thc *TestHealthcheck) SingleCheck() Result {
	thc.singleCheckCalledTimes++
	return Result{
		HealthcheckName: testHealthcheckName,
		Successful:      false,
		Realserver:      thc.Rs,
	}
}

func (thc *TestHealthcheck) AsProto() any {
	return nil
}

func (thc *TestHealthcheck) GetID() HealthcheckID {
	id, _ := newHealthcheckID()
	return id
}

func TestCheck(t *testing.T) {
	hc := &TestHealthcheck{}
	timeout := 20 * time.Millisecond
	ctx, _ := context.WithTimeout(context.TODO(), timeout)
	resultCh := make(chan Result, 10)
	c := check(ctx, hc, resultCh)
	want := Result{
		HealthcheckName: testHealthcheckName,
		Successful:      false,
		Realserver:      Realserver{},
	}
	readResults := true
	for readResults {
		select {
		case result := <-resultCh:
			if result != want {
				t.Errorf("Unexpected result: %v\n", result)
			}
		case <-c:
			readResults = false
		case <-time.After(10 * time.Second):
			t.Error("check() didn't finish after 10 seconds")
			break
		}
	}
	if hc.singleCheckCalledTimes < 9 {
		t.Errorf("Unexpected number of executions: %v\n", hc.singleCheckCalledTimes)
	}
	if hc.closedCalledTimes < 1 {
		t.Error("Close() hasn't been called")
	}
}
