package shandler

import (
	"context"
	"os"
	"os/signal"
)

type SignalHandler interface {
	NotifyContext(context.Context, ...os.Signal) (context.Context, context.CancelFunc)
}

type StdlibSignalHandler struct{}

func (StdlibSignalHandler) NotifyContext(parent context.Context, signals ...os.Signal) (context.Context, context.CancelFunc) {
	return signal.NotifyContext(parent, signals...)
}
