/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package etcd

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"log/slog"
	"os"
	"testing"
	"time"

	etcd_harness "github.com/jvgutierrez/go-etcd-harness"
	etcd "go.etcd.io/etcd/client"
)

var harness *etcd_harness.Harness

var dcs []string = []string{"eqiad", "codfw", "esams"}

func newContext(timeouts ...time.Duration) context.Context {
	var timeout time.Duration
	if len(timeouts) == 0 {
		timeout = 1 * time.Second
	} else {
		timeout = timeouts[0]
	}
	c, _ := context.WithTimeout(context.TODO(), timeout)
	return c
}

func setupEtcd() {
	var err error
	harness, err = etcd_harness.New(os.Stderr)
	if err != nil {
		log.Panicf("Failed starting etcd harness: %v\n", err)
	}

	keys := etcd.NewKeysAPI(harness.Client)
	for _, dc := range dcs {
		key := fmt.Sprintf("/conftool/v1/pools/%s", dc)
		keys.Set(newContext(), key, "", &etcd.SetOptions{Dir: true})
	}
	keys.Set(newContext(), "/conftool/v1/pools/eqiad/ncredir/nginx", "", &etcd.SetOptions{Dir: true})

	// Insert some realservers under /eqiad/ncredir/nginx
	for i := 1001; i <= 1002; i++ {
		hostname := fmt.Sprintf("ncredir%d.eqiad.wmnet", i)
		node_key := fmt.Sprintf("/conftool/v1/pools/eqiad/ncredir/nginx/%s", hostname)
		node := Node{
			Hostname: hostname,
			Weight:   1,
			Pooled:   PooledYes,
		}
		n, err := json.Marshal(node)
		if err != nil {
			continue
		}
		keys.Set(newContext(), node_key, string(n), &etcd.SetOptions{})
	}
}

func TestMain(m *testing.M) {
	if etcd_harness.LocalEtcdAvailable() {
		setupEtcd()
	}
	result := m.Run()
	if etcd_harness.LocalEtcdAvailable() {
		harness.Stop()
	}
	os.Exit(result)
}

func TestEtcdv2Client(t *testing.T) {
	if !etcd_harness.LocalEtcdAvailable() {
		t.Skipf("etcd is not available in $PATH, skipping tests")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	key := "/conftool/v1/pools/eqiad/ncredir/nginx"
	client, err := NewEtcdv2Client(logger, []string{harness.Endpoint}, key)
	if err != nil {
		t.Fatal("Unable to get an Etcd client")
		return
	}
	defer client.Close()

	ctx, cancel := context.WithCancel(context.Background())
	nodes, err := client.Get(ctx)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
		return
	}
	if client.(*Etcdv2Client).watchopts.AfterIndex == 0 {
		t.Error("etcd client watchopts.AfterIndex is 0, want >0")
	}
	if len(nodes) != 2 {
		t.Errorf("Unexpected number of nodes received: %v\n", len(nodes))
	}
	for _, node := range nodes {
		if node.Weight != 1 {
			t.Errorf("Unexpected weight %v for node %v\n", node.Weight, node.Hostname)
		}
		if node.Pooled != PooledYes {
			t.Errorf("Unexpected Pooled value %v for node %v\n", node.Pooled, node.Hostname)
		}
	}
	eventChan := make(chan Event, 10)
	readyCh, watchDone := client.Watch(ctx, eventChan)
	select {
	case <-readyCh:
	case <-time.After(1 * time.Second):
		t.Fatal("test timeout waiting for etcd Watch to be ready")
	}
	// Fetch the initial status
	// Insert a new node
	newNode := Node{
		Hostname: "ncredir1003.eqiad.wmnet",
		Weight:   2,
		Pooled:   PooledNo,
	}
	keys := etcd.NewKeysAPI(harness.Client)
	n, _ := json.Marshal(newNode)
	keys.Set(newContext(), fmt.Sprintf("%s/ncredir1003.eqiad.wmnet", key), string(n), &etcd.SetOptions{})
	// new node should be available on the channel
	var event Event
	select {
	case event = <-eventChan:
	case <-time.After(1 * time.Second):
		t.Fatal("no events available on the events channel")
		return
	}
	if event.EventType != EventSet {
		t.Errorf("Unexpected EventType: %v\n", event.EventType)
	}
	if len(event.Nodes) != 1 {
		t.Errorf("Unexpected number of nodes received: %v\n", len(event.Nodes))
	}
	if event.Nodes[0].Hostname != "ncredir1003.eqiad.wmnet" {
		t.Errorf("Unexpected hostname: %v\n", event.Nodes[0].Hostname)
	}
	if event.Nodes[0].Pooled != PooledNo {
		t.Errorf("Unexpected Pooled status: %v\n", event.Nodes[0].Pooled)
	}

	// Update node
	updateNode := Node{
		Hostname: "ncredir1003.eqiad.wmnet",
		Weight:   2,
		Pooled:   PooledYes,
	}
	n, _ = json.Marshal(updateNode)
	keys.Update(newContext(), fmt.Sprintf("%s/ncredir1003.eqiad.wmnet", key), string(n))
	// the updated node should be available on the channel
	select {
	case event = <-eventChan:
	case <-time.After(1 * time.Second):
		t.Fatal("no events available on the events channel")
		return
	}
	if event.EventType != EventUpdate {
		t.Errorf("Unexpected EventType: %v\n", event.EventType)
	}
	if len(event.Nodes) != 1 {
		t.Errorf("Unexpected number of nodes received: %v\n", len(event.Nodes))
	}
	if event.Nodes[0].Hostname != "ncredir1003.eqiad.wmnet" {
		t.Errorf("Unexpected hostname: %v\n", event.Nodes[0].Hostname)
	}
	if event.Nodes[0].Pooled != PooledYes {
		t.Errorf("Unexpected Pooled status: %v\n", event.Nodes[0].Pooled)
	}

	// Delete an existing node
	keys.Delete(newContext(), fmt.Sprintf("%s/ncredir1001.eqiad.wmnet", key), &etcd.DeleteOptions{})
	// Fetch the new set of nodes
	select {
	case event = <-eventChan:
	case <-time.After(1 * time.Second):
		t.Fatal("no events available on the events channel")
		return
	}
	if event.EventType != EventDelete {
		t.Errorf("Unexpected EventType: got %v, want %v\n", event.EventType, EventDelete)
	}
	if len(event.Nodes) != 1 {
		t.Errorf("Unexpected number of nodes received: %v\n", len(event.Nodes))
	}
	if event.Nodes[0].Hostname != "ncredir1001.eqiad.wmnet" {
		t.Errorf("Unexpected deleted hostname: %v\n", event.Nodes[0].Hostname)
	}

	// Delete the whole thing
	keys.Delete(newContext(), key, &etcd.DeleteOptions{Recursive: true, Dir: true})
	select {
	case event = <-eventChan:
	case <-time.After(1 * time.Second):
		t.Fatal("no events available on the events channel")
		return
	}
	if event.EventType != EventDelete {
		t.Errorf("Unexpected EventType: got %v, want %v\n", event.EventType, EventDelete)
	}
	if len(event.Nodes) > 0 {
		t.Errorf("Unexpected number of nodes: %v\n", len(event.Nodes))
	}
	// We are done, let's cancel WatchKey()
	cancel()
	// Wait till context is done
	<-ctx.Done()
	// watchDone should be closed
	select {
	case <-watchDone:
	case <-time.After(200 * time.Millisecond):
		t.Errorf("WatchKey() done channel isn't closed yet")
	}
}

func TestEtcdv2ClientGetUnexistentKey(t *testing.T) {
	if !etcd_harness.LocalEtcdAvailable() {
		t.Skipf("etcd is not available in $PATH, skipping tests")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	client, err := NewEtcdv2Client(logger, []string{harness.Endpoint}, "/foo")
	if err != nil {
		t.Fatal("Unable to get an Etcd client")
		return
	}

	if _, err := client.Get(newContext()); !errors.Is(err, ErrNotFound) {
		t.Errorf("unexpected error: %v", err)
	}

	defer client.Close()
}

func TestEtcdV2ClientIsUsingHighestModifiedIndex(t *testing.T) {
	if !etcd_harness.LocalEtcdAvailable() {
		t.Skipf("etcd is not available in $PATH, skipping tests")
	}

	logger := slog.New(slog.NewTextHandler(io.Discard, nil))
	keys := etcd.NewKeysAPI(harness.Client)
	keys.Set(newContext(), "/conftool/v1/pools/esams/ncredir/nginx", "", &etcd.SetOptions{Dir: true})
	for i := 3001; i <= 3002; i++ {
		hostname := fmt.Sprintf("ncredir%d.esams.wmnet", i)
		node_key := fmt.Sprintf("/conftool/v1/pools/esams/ncredir/nginx/%s", hostname)
		node := Node{
			Hostname: hostname,
			Weight:   1,
			Pooled:   PooledYes,
		}
		n, err := json.Marshal(node)
		if err != nil {
			continue
		}
		keys.Set(newContext(), node_key, string(n), &etcd.SetOptions{})
	}
	// generate some events with ncredir3002.esams.wmnet
	node := Node{
		Hostname: "ncredir3002.esams.wmnet",
		Weight:   1,
		Pooled:   PooledYes,
	}
	n, err := json.Marshal(node)
	if err != nil {
		panic(err)
	}

	node_key := "/conftool/v1/pools/esams/ncredir/nginx/ncredir3002.esams.wmnet"
	for _ = range 10 {
		keys.Delete(newContext(), node_key, nil)
		keys.Create(newContext(), node_key, string(n))
	}

	// trigger the bug
	resp, err := keys.Delete(newContext(), node_key, nil) // delete it before start watching
	if err != nil {
		t.Fatal("unable to delete key")
		return
	}

	key := "/conftool/v1/pools/esams/ncredir/nginx"
	client, err := NewEtcdv2Client(logger, []string{harness.Endpoint}, key)
	if err != nil {
		t.Fatal("Unable to get an Etcd client")
		return
	}
	defer client.Close()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err = client.Get(ctx)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
		return
	}
	if client.(*Etcdv2Client).watchopts.AfterIndex < resp.Node.ModifiedIndex {
		t.Errorf("etcd client watchopts.AfterIndex is %d, want >=%d",
			client.(*Etcdv2Client).watchopts.AfterIndex,
			resp.Node.ModifiedIndex,
		)
	}
}
