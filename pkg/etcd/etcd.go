/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package etcd

import (
	"context"
)

type EtcdClient interface {
	Get(ctx context.Context) ([]Node, error)
	Watch(ctx context.Context, outputCh chan<- Event) (<-chan bool, <-chan struct{})
	Close()
}

type ForwardType string // Not implemented yet

const (
	DirectRoute ForwardType = "DR"
	IPIP        ForwardType = "IPIP"
)

type Pooled string

const (
	PooledYes      Pooled = "yes"
	PooledNo              = "no"
	PooledInactive        = "inactive" // only used by PyBal
	PooledDrain           = "drain"    // only used by LiBerica
)

type EventType string

const (
	EventDelete           EventType = "delete"
	EventGet                        = "get"
	EventSet                        = "set"
	EventUpdate                     = "update"
	EventCreate                     = "create"
	EventCompareAndSwap             = "compareAndSwap"
	EventCompareAndDelete           = "compareAndDelete"
)

type Node struct {
	Hostname string `json:"-"`
	Pooled   Pooled `json:"pooled"`
	//	ForwardType ForwardType TODO: Allow picking between DirectRoute and IPIP per node
	Weight int `json:"weight"`
}

type Event struct {
	EventType EventType
	Nodes     []Node
}
