/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package etcd

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
	etcd "go.etcd.io/etcd/client"
)

var ErrNotFound = errors.New("key not found")

// Etcdv2 client
type Etcdv2Client struct {
	cli        etcd.Client
	kapi       etcd.KeysAPI
	logger     *slog.Logger
	getopts    *etcd.GetOptions
	watchopts  *etcd.WatcherOptions
	cancelSync context.CancelFunc
	key        string
}

func DiscoverEndpoints(ctx context.Context, domain string) ([]string, error) {
	ch := make(chan []string, 1)
	// SRVDiscoverer https://github.com/etcd-io/etcd/blob/973882f697a8db3d59815bf132c6c506434334bd/client/discover.go#L22
	// doesn't accept a context so let's spawn a context to be able
	// to set a timeout in our end
	go func() {
		defer close(ch)
		discoverer := etcd.NewSRVDiscover()
		endpoints, err := discoverer.Discover(domain, "")
		if err != nil {
			return
		}
		ch <- endpoints
	}()

	select {
	case endpoints := <-ch:
		return endpoints, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

// translates a key into a hostname (FQDN)
// example: keyToHostname(/conftool/v1/pools/eqiad/ncredir/nginx/ncredir1001.eqiad.wmnet) = ncredir1001.eqiad.wmnet
func keyToHostname(key string) string {
	hostname := key[strings.LastIndex(key, "/")+1:]
	// Basic normalization
	hostname = strings.ToLower(strings.TrimSpace(hostname))

	return hostname
}

func unmarshalNode(key, value string, node *Node) error {
	node.Hostname = keyToHostname(key)

	return json.Unmarshal([]byte(value), node)
}

func NewEtcdv2Client(logger *slog.Logger, endpoints []string, key string) (EtcdClient, error) {
	cfg := etcd.Config{
		Endpoints:               endpoints,
		HeaderTimeoutPerRequest: 3000 * time.Millisecond,
	}

	cli, err := etcd.New(cfg)
	if err != nil {
		return nil, err
	}

	syncContext, cancelSync := context.WithTimeout(context.Background(), 5000*time.Millisecond)
	go cli.AutoSync(syncContext, 10*time.Second)

	etcdLogger := logger.With(
		slog.String("key", key),
	)

	return &Etcdv2Client{
		logger: etcdLogger,
		cli:    cli,
		kapi:   etcd.NewKeysAPI(cli),
		getopts: &etcd.GetOptions{
			Recursive: true,
			Sort:      false,
			Quorum:    true,
		},
		watchopts: &etcd.WatcherOptions{
			AfterIndex: 0,
			Recursive:  true,
		},
		key:        key,
		cancelSync: cancelSync,
	}, nil
}

func (ec *Etcdv2Client) Close() {
	ec.cancelSync()
}

func (ec *Etcdv2Client) setWatchModifiedIndex(modifiedIndex uint64) {
	ec.logger.Debug("new after index",
		slog.Uint64("modified_index", modifiedIndex),
	)
	ec.watchopts.AfterIndex = modifiedIndex
}

func (ec *Etcdv2Client) getNodes(ctx context.Context) ([]Node, error) {
	resp, err := ec.kapi.Get(ctx, ec.key, ec.getopts)
	if err != nil {
		if etcd.IsKeyNotFound(err) {
			return nil, ErrNotFound
		}
		return nil, fmt.Errorf("Unable to getNodes(): %w", err)
	}

	modifiedIndex := resp.Index
	numberNodes := len(resp.Node.Nodes)

	nodes := make([]Node, numberNodes, numberNodes)
	for i, node := range resp.Node.Nodes {
		var n Node
		err := unmarshalNode(node.Key, node.Value, &n)
		if err != nil {
			ec.logger.Error("unable to unmarshal node data", slog.Any("error", err))
			return nil, fmt.Errorf("unable to unmarshal node data: %w", err)
		}
		if node.ModifiedIndex > modifiedIndex {
			modifiedIndex = node.ModifiedIndex
		}
		nodes[i] = n
	}
	ec.setWatchModifiedIndex(modifiedIndex)
	return nodes, nil
}

func (ec *Etcdv2Client) Get(ctx context.Context) ([]Node, error) {
	nodes, err := ec.getNodes(ctx)
	if err != nil {
		metrics.EtcdErrors.With(prometheus.Labels{
			"key": ec.key,
		}).Inc()
	}

	return nodes, err
}

// WatchKey makes an initial Get operation to retrieve the current state
// and proceeds to Watch the requested key till the context is cancelled
// or an error happens
// WatchKey() will discard events if outputCh consumer isn't fast enough
func (ec *Etcdv2Client) Watch(ctx context.Context, outputCh chan<- Event) (<-chan bool, <-chan struct{}) {
	readyCh := make(chan bool, 1)
	doneCh := make(chan struct{})

	go func() {
		defer close(doneCh)

		if ctx.Err() != nil {
			return
		}

		// Watch operation
		watcher := ec.kapi.Watcher(ec.key, ec.watchopts)
		readyCh <- true
		ec.logger.Debug("watching events")
		modifiedIndex := ec.watchopts.AfterIndex
		for {
			resp, err := watcher.Next(ctx)
			if err != nil {
				if !errors.Is(err, context.Canceled) {
					ec.logger.Error("unable to watch",
						slog.Any("error", err))
				}
				metrics.EtcdErrors.With(prometheus.Labels{
					"key": ec.key,
				}).Inc()
				// update the modified index to the last one seen for
				// potential retries on Watch()
				ec.setWatchModifiedIndex(modifiedIndex)
				return
			}
			if resp.Node.ModifiedIndex > modifiedIndex {
				modifiedIndex = resp.Node.ModifiedIndex
			}
			ec.logger.Debug("new event",
				slog.Uint64("modified_index", resp.Node.ModifiedIndex),
			)

			eventType := EventType(resp.Action)
			metrics.EtcdEventsReceived.With(prometheus.Labels{
				"event_type": string(eventType),
				"key":        ec.key,
			}).Inc()

			var ev Event
			switch eventType {
			case EventDelete:
				if len(resp.Node.Key) > len(ec.key) {
					// deleted host
					hostname := keyToHostname(resp.Node.Key)
					n := Node{Hostname: hostname}
					ev = Event{EventType: EventType(resp.Action), Nodes: []Node{n}}
				} else {
					// deleted service
					ev = Event{EventType: EventType(resp.Action)}
				}

			default:
				if resp.Node.Dir { // event targets a whole service
					numberNodes := len(resp.Node.Nodes)
					nodes := make([]Node, numberNodes, numberNodes)
					for i, node := range resp.Node.Nodes {
						var n Node
						err := unmarshalNode(node.Key, node.Value, &n)
						if err != nil {
							ec.logger.Error("unable to unmarshal node data",
								slog.Any("error", err),
								slog.Any("data", node.Value))
							return
						}
						if node.ModifiedIndex > modifiedIndex {
							modifiedIndex = node.ModifiedIndex
						}
						nodes[i] = n
					}

					ev = Event{EventType: EventType(resp.Action), Nodes: nodes}
				} else { // event targets a single node
					var n Node
					err := unmarshalNode(resp.Node.Key, resp.Node.Value, &n)
					if err != nil {
						ec.logger.Error("unable to unmarshal node data",
							slog.Any("error", err),
							slog.Any("data", resp.Node.Value))
						return
					}
					ev = Event{EventType: EventType(resp.Action), Nodes: []Node{n}}
				}
			}

			select {
			case outputCh <- ev:
			default:
				metrics.EtcdEventsDiscarded.With(prometheus.Labels{
					"event_type": string(eventType),
					"key":        ec.key,
				}).Inc()
			}
		}
	}()

	return readyCh, doneCh
}
