# Liberica

## Building and testing

### Dependencies (on Debian Bookworm host)

* ``clang``
* ``llvm``
* ``libbpf-dev``
* ``golang`` (min ``go1.22``)
* ``gcc-multilib``
* ``qemu-system`` (used by integration tests)
* ``bpftool`` (used by integration tests)
* ``libpcap-dev`` (used by integration tests)
* ``ipvsadm`` (used by ``fp`` integration tests)
* ``protoc-gen-go``
* ``protoc-gen-go-grpc``
* ``gobgpd`` (used by ``bgp`` integration tests)
* ``etcd`` (used by ``etcd`` integration tests)

go dependencies are currently handled by vendoring the dependencies rather than using debian packages. gobgp go module version is synced with what debian provides in our production environments given we are using gobgpd rather than embedding the complete BGP daemon within liberica

### Build

* ``make dep`` to fetch dependencies
* ``make``

### Tests

All tests can be run with ``make test``


## Signal handling
liberica daemons use the following signals:
* SIGHUP: perform a config reload. Allows changing config without performing a daemon restart. For every daemon logging level can be adjusted on the flight. Control plane allows service configuration changes as well (add/remove/update service configuration)
* SIGINT, SIGTERM: Performs a seamless shutdown
  * hcforwarder: eBPF program keeps running after cmd shutdown
  * fp:
    * IPVS: ipvs state isn't wiped
    * Katran: eBPF programs keep running after cmd shutdown
  * cp: BGP prefixes aren't withdrawn
  * healthcheck: Same as SIGUSR1
* SIGUSR1: Performs a shutdown cleaning state


## Config file
Liberica supports a config file on any of the file formats supported by viper (json, toml, yaml, hcl, INI, envfile and java properties file). If it exists it will use `/etc/liberica/config.yaml`. A custom location can be used using the global cmd flag "--config": `liberica --config /tmp/cfg.yaml hcforwarder`

### Config example
```yaml
hcforwarder:
  log_level: info
  grpc:
    network: tcp
    address: 127.0.0.1:1234
  interface:
    egress: lo
    v4: ipip0
    v6: ipip60
  prometheus:
    address: 127.0.0.1:3000
healthcheck:
  log_level: info
  gprc:
    network: tcp
    address: 127.0.0.1:1235
  prometheus:
    address: 127.0.0.1:3100
fp:
  log_level: info
  grpc:
    network: tcp
    address: 127.0.0.1:1236
  prometheus:
    address: 127.0.0.1:3200
  forwarding_plane: ipvs
cp:
  log_level: info
  grpc:
    network: tcp
    address: 127.0.0.1:1237
  prometheus:
    address: 127.0.0.1:3300
  bgp:
    asn: 64600
    peers:
      - 127.0.0.2
      - 127.0.0.3
    next_hop_ipv4: 10.0.0.1
    next_hop_ipv6: fc00::1
etcd:
  conftool_domain: eqiad.wmnet
  datacenter: drmrs
services:
  ncredir:
    forward_type: tunnel
    depool_threshold: 0.5
    cluster: ncredir
    service: nginx
    ip: 192.0.0.1
    port: 80
    healthchecks:
      L7:
        type: HTTPCheck
        url: "http://en.wikipedia.com"
      L4:
        type: IdleTCPConnectionCheck
        timeout: 60s
  text-https:
    depool_threshold: 0.66
    cluster: cache_text
    service: ats-tls
    ip: 192.0.0.2
    port: 443
    healthchecks:
    check1:
      type: HTTPCheck
      url: "https://en.wikipedia.org"
    check2:
      type: IdleTCPConnectionCheck
      timeout: 60s
```
