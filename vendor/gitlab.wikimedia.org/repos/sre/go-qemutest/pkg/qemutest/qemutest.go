//go:build linux
// +build linux

/*
Copyright 2023 Wikimedia Foundation Inc.
Copyright 2018 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package qemutest

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"testing"

	"golang.org/x/sys/unix"

	"github.com/u-root/u-root/pkg/cpio"
)

const (
	DEFAULT_QEMU_MEMORY_SIZE = "256"
	QEMU_MEMORY_SIZE_ENV     = "QEMU_GUEST_RAM_SIZE"
)

var inQemu bool

func InQemu() bool {
	return inQemu
}

func init() {
	if os.Getpid() != 1 {
		// Not in qemu.
		return
	}
	inQemu = true

	fmt.Println(":: hello from userspace")

	for _, mnt := range []struct {
		dev, path, fstype string
	}{
		{"sysfs", "/sys", "sysfs"},
		{"proc", "/proc", "proc"},
		{"udev", "/dev", "devtmpfs"},
	} {
		if err := unix.Mount(mnt.dev, mnt.path, mnt.fstype, 0, ""); err != nil {
			log.Fatalf("failed to mount %s: %v", mnt.path, err)
		}
	}

	// Connect to the monitor.
	var err error
	monc, err = dialMon()
	if err != nil {
		log.Fatalf("dialing monitor: %v", err)
	}
}

// to be called from TestMain():
//
//	func TestMain(m *testing.M) {
//		os.Exit(QemuTestMain(m))
//	}
func QemuTestMain(m *testing.M) int {
	status := m.Run()
	if inQemu {
		fmt.Printf(":: exit=%d\n", status)
		monc.run("quit") // to avoid a kernel panic with init exiting
	}
	return status
}

// QemuTestCase specifies the basic config required to spawn a qemu instance
type QemuTestCase struct {
	KernelImage string // full path to the kernel
	TestId      string // string identifying the test
	Verbose     bool
}

// RunQemuTests must be used to spawn the tests on the qemu guest
func RunQemuTests(t *testing.T, userQemuTests any) {
	if inQemu {
		rv := reflect.ValueOf(userQemuTests)
		tv := rv.Type()
		for i := 0; i < rv.NumMethod(); i++ {
			t.Run(tv.Method(i).Name, rv.Method(i).Interface().(func(*testing.T)))
		}
		return
	}
}

// RunQemu must be used to spawn qemu on the host
func RunQemu(t *testing.T, qemuTestCases []QemuTestCase, binaries []string) {
	if testing.Short() {
		t.Skip("skipping in short mode")
	}
	if _, err := exec.LookPath("qemu-system-x86_64"); err != nil {
		t.Skipf("skipping test due to qemu-system-x86_64 not found: %v", err)
	}
	td, err := os.MkdirTemp("", "*-qemu-tests")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(td)

	memory_size, memory_size_set := os.LookupEnv(QEMU_MEMORY_SIZE_ENV)
	if !memory_size_set {
		memory_size = DEFAULT_QEMU_MEMORY_SIZE
	}

	initrdPath := filepath.Join(td, "initrd")
	if err := genRootFS(initrdPath, binaries); err != nil {
		t.Fatalf("failed to generate/write initrd: %v", err)
	}

	monSockPath := filepath.Join(td, "monsock")

	ln, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()
	go func() {
		for {
			c, err := ln.Accept()
			if err != nil {
				return
			}
			mon, err := net.Dial("unix", monSockPath)
			if err != nil {
				c.Close()
				log.Printf("dial unix to monitor failed: %v", err)
				return
			}
			go func() {
				errc := make(chan error, 1)
				go func() {
					_, err := io.Copy(c, mon)
					errc <- err
				}()
				go func() {
					_, err := io.Copy(mon, c)
					errc <- err
				}()
				<-errc
				c.Close()
				mon.Close()
			}()
		}
	}()
	for _, tc := range qemuTestCases {
		t.Run(tc.TestId, func(t *testing.T) {
			cmd := exec.Command("qemu-system-x86_64",
				"-vga", "none",
				"-nographic",
				"-m", memory_size,
				"-display", "none",
				"-monitor", "unix:"+monSockPath+",server,nowait",
				"-device", "virtio-net,netdev=net0",
				"-netdev", "user,id=net0,guestfwd=tcp:10.0.2.100:1234-tcp:"+ln.Addr().String(),
				"-device", "virtio-serial",
				"-device", "virtio-scsi-pci,id=scsi",
				"-kernel", tc.KernelImage,
				"-initrd", initrdPath,
				"-no-reboot",
				"-append", "console=ttyS0,115200 panic=-1 acpi=off nosmp ip=dhcp -- -test.v")
			var out bytes.Buffer
			var std io.Writer = &out
			if tc.Verbose {
				std = io.MultiWriter(std, os.Stderr)
			}
			cmd.Stdout = std
			cmd.Stderr = std
			err = cmd.Run()
			if err != nil {
				t.Fatalf("run: %v", err)
			}
			if !bytes.Contains(out.Bytes(), []byte("\n:: exit=0")) {
				t.Error("non-zero exit status")
			}
		})
	}
}

type monClient struct {
	c   net.Conn
	buf bytes.Buffer
}

var monc *monClient

func dialMon() (*monClient, error) {
	if !inQemu {
		panic("dialMon is meant for tests in qemu")
	}
	c, err := net.Dial("tcp", "10.0.2.100:1234")
	if err != nil {
		return nil, err
	}
	mc := &monClient{c: c}
	if _, err := mc.readToPrompt(); err != nil {
		return nil, err
	}
	return mc, nil
}

var (
	qemuPrompt = []byte("\r\n(qemu) ")
	ansiCSI_K  = []byte("\x1b[K") // CSI K: Erase in Line; n == 0: clear from cursor to the end of the line
)

func (mc *monClient) readToPrompt() (pre string, err error) {
	buf := make([]byte, 100)
	for {
		n, err := mc.c.Read(buf)
		if err != nil {
			return "", err
		}
		mc.buf.Write(buf[:n])
		have := mc.buf.Bytes()
		if bytes.HasSuffix(have, qemuPrompt) {
			mc.buf.Reset()
			ret := bytes.TrimSuffix(have, qemuPrompt)
			if i := bytes.LastIndex(ret, ansiCSI_K); i != -1 {
				ret = ret[i+len(ansiCSI_K):]
			}
			return strings.TrimSpace(strings.Replace(string(ret), "\r\n", "\n", -1)), nil
		}
	}
}

func (mc *monClient) run(cmd string) (out string, err error) {
	if _, err := fmt.Fprintf(mc.c, "%s\n", cmd); err != nil {
		return "", err
	}
	return mc.readToPrompt()
}

func genRootFS(dst string, binaries []string) error {
	initProg, err := os.Executable()
	if err != nil {
		return err
	}
	files := rootFSFiles(initProg, binaries)

	f, err := os.Create(dst)
	if err != nil {
		log.Fatal(err)
	}
	bw := bufio.NewWriter(f)
	recw := cpio.Newc.Writer(bw)
	dedupw := recw.(*cpio.DedupWriter)
	recorder := cpio.NewRecorder()
	var fileRec []cpio.Record

	var addDirs func(string)
	// we need to add directories and symlinks to directories in the right order
	// so we don't get errors on decompression
	addDirs = func(dpath string) {
		els := strings.Split(dpath, "/")
		for i := range els {
			d := path.Join(els[:i+1]...)
			absPath := filepath.Join("/", d)
			if absPath == "/" {
				continue
			}
			drec, err := recorder.GetRecord(absPath)
			if err != nil {
				log.Fatal(err)
			}
			drec.Info.Name = cpio.Normalize(drec.Info.Name)
			fi, err := os.Lstat(absPath)
			if err != nil {
				log.Fatal(err)
			}
			if fi.Mode()&os.ModeSymlink != 0 { // follow the symlink
				target, err := os.Readlink(absPath)
				if err != nil {
					log.Fatal(err)
				}
				if !filepath.IsAbs(target) {
					target = filepath.Join(filepath.Dir(absPath), target)
				}
				addDirs(target)
			}
			fileRec = append(fileRec, drec)
		}
	}

	for _, file := range files {
		rec, err := recorder.GetRecord(file)
		if err != nil {
			log.Fatalf("GetRecord(%q): %v", file, err)
		}
		if file == initProg {
			rec.Info.Name = "init"
		} else {
			rec.Info.Name = cpio.Normalize(rec.Info.Name)
			addDirs(rec.Info.Name)
		}
		fileRec = append(fileRec, rec)
	}

	// get rid of custom uids/gids and dates
	cpio.MakeAllReproducible(fileRec)
	if err := cpio.WriteRecords(dedupw, fileRec); err != nil {
		return err
	}

	extraRec := []cpio.Record{
		cpio.Directory("proc", 0755),
		cpio.Directory("sys", 0755),
		cpio.Directory("dev", 0755),
		cpio.Directory("mnt", 0755),
	}
	if err := cpio.WriteRecords(recw, extraRec); err != nil {
		return err
	}

	if err := bw.Flush(); err != nil {
		return err
	}
	return f.Close()
}

// Return a list of files and symlinks that point to files
func rootFSFiles(initProg string, binaries []string) []string {
	set := map[string]bool{}

	var add func(string)
	add = func(f string) {
		if f == "/" {
			return
		}
		if set[f] {
			return
		}
		fi, err := os.Lstat(f)
		if os.IsNotExist(err) {
			return
		}
		if err != nil {
			log.Fatal(err)
		}
		if fi.Mode()&os.ModeSymlink == 0 { // file itself isn't a symlink but a directory part of the path could be
			rf, err := filepath.EvalSymlinks(f)
			if err != nil {
				log.Fatal(err)
			}
			add(filepath.Dir(f)) // base directory couldn't have been seen yet
			if set[rf] {
				return
			}
		}
		set[f] = true

		add(filepath.Dir(f))
		if fi.IsDir() {
			return
		}
		if fi.Mode()&os.ModeSymlink != 0 {
			target, err := os.Readlink(f)
			if err != nil {
				log.Fatal(err)
			}
			if !filepath.IsAbs(target) {
				target = filepath.Join(filepath.Dir(f), target)
			}
			add(target)
			return
		}
		out, _ := exec.Command("ldd", f).Output()
		for _, f := range strings.Fields(string(out)) {
			if strings.HasPrefix(f, "/") {
				add(f)
			}
		}
	}

	add(initProg)

	for _, binary := range binaries {
		add(binary)
	}

	// libc-bin:
	add("/etc/ld.so.conf")
	filepath.Walk("/etc/ld.so.conf.d", func(path string, fi os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		}
		add(path)
		return nil
	})

	var files []string
	for f := range set {
		files = append(files, f)
	}
	sort.Strings(files)
	return files
}
