package main

import (
	"context"
	"net/netip"
	"os"
	"testing"
	"time"

	"github.com/prometheus/client_golang/prometheus/testutil"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/fp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

// Mock FPClient
type MockFPClient struct {
	services    []fp.Service
	realservers []fp.Realserver
}

func (mfp MockFPClient) CreateService(context.Context, fp.Service) error { return nil }
func (mfp MockFPClient) DeleteService(context.Context, fp.Service) error { return nil }
func (mfp MockFPClient) CreateRealserver(context.Context, fp.Service, fp.Realserver) error {
	return nil
}
func (mfp MockFPClient) UpdateRealserver(context.Context, fp.Service, fp.Realserver) error {
	return nil
}
func (mfp MockFPClient) DeleteRealserver(context.Context, fp.Service, fp.Realserver) error {
	return nil
}
func (mfp MockFPClient) Close() {}
func (mfp MockFPClient) ListServices(context.Context) ([]fp.Service, error) {
	return mfp.services, nil
}
func (mfp MockFPClient) ListRealservers(context.Context, fp.Service) ([]fp.Realserver, error) {
	return mfp.realservers, nil
}

// Mock CPClient
type MockCPClient struct {
	services    []cp.Service
	realservers []cp.Realserver
}

func (mcp MockCPClient) ListServices(context.Context) ([]cp.Service, error) {
	return mcp.services, nil
}
func (mcp MockCPClient) ListRealservers(context.Context, cp.Service) ([]cp.Realserver, error) {
	return mcp.realservers, nil
}
func (mcp MockCPClient) Close() {}

func TestCpCheck(t *testing.T) {
	testcases := []struct {
		name          string
		fpServices    []fp.Service
		fpRealservers []fp.Realserver
		cpServices    []cp.Service
		cpRealservers []cp.Realserver
		wantMetric    float64
	}{
		{
			name: "healthy",
			fpServices: []fp.Service{
				fp.Service{
					Address: netip.MustParseAddr("127.0.0.1"),
					Port:    uint16(80),
				},
			},
			fpRealservers: []fp.Realserver{
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
				},
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
				},
			},
			cpServices: []cp.Service{
				cp.Service{
					AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
				},
			},
			cpRealservers: []cp.Realserver{
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
					Pooled:  cp.PooledYes,
				},
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
					Pooled:  cp.PooledYes,
				},
			},
			wantMetric: 1.0,
		},
		{
			name: "CPDepooledStillFPPooled",
			fpServices: []fp.Service{
				fp.Service{
					Address: netip.MustParseAddr("127.0.0.1"),
					Port:    uint16(80),
				},
			},
			fpRealservers: []fp.Realserver{
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
				},
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
				},
			},
			cpServices: []cp.Service{
				cp.Service{
					AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
				},
			},
			cpRealservers: []cp.Realserver{
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
					Pooled:  cp.DepoolFailed,
				},
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
					Pooled:  cp.PooledYes,
				},
			},
			wantMetric: 0.0,
		},
		{
			name: "MissingCPService",
			fpServices: []fp.Service{
				fp.Service{
					Address: netip.MustParseAddr("127.0.0.1"),
					Port:    uint16(80),
				},
			},
			fpRealservers: []fp.Realserver{
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
				},
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
				},
			},
			cpServices:    []cp.Service{},
			cpRealservers: []cp.Realserver{},
			wantMetric:    0.0,
		},
		{
			name:          "MissingFPService",
			fpServices:    []fp.Service{},
			fpRealservers: []fp.Realserver{},
			cpServices: []cp.Service{
				cp.Service{
					AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
				},
			},
			cpRealservers: []cp.Realserver{
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
					Pooled:  cp.PooledYes,
				},
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
					Pooled:  cp.PooledYes,
				},
			},
			wantMetric: 0.0,
		},
		{
			name: "MissingFPRealserver",
			fpServices: []fp.Service{
				fp.Service{
					Address: netip.MustParseAddr("127.0.0.1"),
					Port:    uint16(80),
				},
			},
			fpRealservers: []fp.Realserver{
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
				},
			},
			cpServices: []cp.Service{
				cp.Service{
					AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
				},
			},
			cpRealservers: []cp.Realserver{
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
					Pooled:  cp.PooledYes,
				},
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
					Pooled:  cp.PooledYes,
				},
			},
			wantMetric: 0.0,
		},
		{
			name: "MissingCPRealserver",
			fpServices: []fp.Service{
				fp.Service{
					Address: netip.MustParseAddr("127.0.0.1"),
					Port:    uint16(80),
				},
			},
			fpRealservers: []fp.Realserver{
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
				},
				fp.Realserver{
					Address: netip.MustParseAddr("127.0.0.3"),
				},
			},
			cpServices: []cp.Service{
				cp.Service{
					AddrPort: netip.MustParseAddrPort("127.0.0.1:80"),
				},
			},
			cpRealservers: []cp.Realserver{
				cp.Realserver{
					Address: netip.MustParseAddr("127.0.0.2"),
					Pooled:  cp.PooledYes,
				},
			},
			wantMetric: 0.0,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			fpClient := &MockFPClient{
				services:    tc.fpServices,
				realservers: tc.fpRealservers,
			}
			cpClient := &MockCPClient{
				services:    tc.cpServices,
				realservers: tc.cpRealservers,
			}
			ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancel()
			f, err := os.CreateTemp("", tc.name)
			if err != nil {
				t.Fatal(err)
				return
			}
			defer os.Remove(f.Name())
			f.Close()
			if err := cpCheck(ctx, cpClient, fpClient, f.Name()); err != nil {
				t.Errorf("unexpected error: %v", err)
			}

			if metricValue := testutil.ToFloat64(metrics.ControlPlaneMatchForwardingPlane); metricValue != tc.wantMetric {
				t.Errorf("unexpected metric value, want %v, got %v", tc.wantMetric, metricValue)
			}
		})
	}
}
