/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"fmt"
	"log/slog"
	"maps"
	"net/netip"
	"os"
	"slices"
	"time"

	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cmdutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/fp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
)

var (
	CpGrpcBindNetwork string
	CpGrpcBindAddress string
)

func BindCPFlags(cmd *cobra.Command) {
	cfgMap := map[string]string{
		"grpc":          "cp.grpc.address",
		"grpcNetwork":   "cp.grpc.network",
		"fpGrpc":        "fp.grpc.address",
		"fpGrpcNetwork": "fp.grpc.network",
	}
	cmdutils.BindFlagsToSettings(cfgMap, cmd)
}

func NewCpServicesCmd() *cobra.Command {

	cmd := &cobra.Command{
		Use:   "services",
		Short: "services information",
		PreRun: func(cmd *cobra.Command, args []string) {
			BindCPFlags(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			logger := slog.New(slog.NewTextHandler(os.Stderr, nil))
			cpClient, err := cp.NewGrpcClient(logger, CpGrpcBindNetwork, CpGrpcBindAddress)
			if err != nil {
				return err
			}
			defer cpClient.Close()
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			svcs, err := cpClient.ListServices(ctx)
			for _, svc := range svcs {
				fmt.Printf("%s:\n", svc.Name)
				rss, err := cpClient.ListRealservers(ctx, svc)
				if err != nil {
					return err
				}
				for _, rs := range rss {
					pooled := "unknown"
					switch rs.Pooled {
					case cp.PooledYes:
						pooled = "yes"
					case cp.PooledNo:
						pooled = "no"
					case cp.DepoolBlocked:
						pooled = "depool-blocked"
					case cp.DepoolFailed:
						pooled = "depool-failed"
					case cp.RepoolFailed:
						pooled = "repool-failed"
					case cp.ForceRepoolFailed:
						pooled = "force-repool-failed"
					}
					fmt.Printf("\t%s\t%d healthy: %v | pooled: %s\n", rs.Address.String(), rs.Weight, rs.Healthy, pooled)
				}
			}
			if err != nil {
				return err
			}

			return nil
		},
	}

	return cmd
}

func cpCheck(ctx context.Context, cpClient cp.CPClient, fpClient fp.FPClient, promFile string) error {
	var (
		healthy     bool
		metricValue float64
	)
	cpSnapshot := make(map[netip.AddrPort][]netip.Addr)
	fpSnapshot := make(map[netip.AddrPort][]netip.Addr)
	cpSvcs, err := cpClient.ListServices(ctx)
	if err != nil {
		return err
	}

	for _, svc := range cpSvcs {
		rss, err := cpClient.ListRealservers(ctx, svc)
		if err != nil {
			return fmt.Errorf("unable to fetch realservers for svc %v: %w", svc, err)
		}
		rssAddrs := make([]netip.Addr, 0, len(rss))
		for _, rs := range rss {
			if rs.Pooled == cp.PooledYes || rs.Pooled == cp.DepoolBlocked {
				rssAddrs = append(rssAddrs, rs.Address)
			}
		}
		cpSnapshot[svc.AddrPort] = rssAddrs
	}
	fpSvcs, err := fpClient.ListServices(ctx)
	if err != nil {
		return err
	}

	for _, svc := range fpSvcs {
		svcAddrPort := netip.AddrPortFrom(svc.Address, uint16(svc.Port))
		rss, err := fpClient.ListRealservers(ctx, svc)
		if err != nil {
			return fmt.Errorf("unable to fetch realservers for svc: %v: %w", svc, err)
		}
		rssAddrs := make([]netip.Addr, 0, len(rss))
		for _, rs := range rss {
			rssAddrs = append(rssAddrs, rs.Address)
		}
		fpSnapshot[svcAddrPort] = rssAddrs
	}

	sortAddrFn := func(a, b netip.Addr) int {
		return a.Compare(b)
	}

	healthy = maps.EqualFunc(cpSnapshot, fpSnapshot, func(v1, v2 []netip.Addr) bool {
		slices.SortFunc(v1, sortAddrFn)
		slices.SortFunc(v2, sortAddrFn)
		return slices.Equal(v1, v2)
	})

	metricsWriter := metrics.NewMetricsWriter(promFile)
	if err := metricsWriter.Register(metrics.ControlPlaneMatchForwardingPlane); err != nil {
		return err
	}

	if healthy {
		metricValue = 1.0
	}

	metrics.ControlPlaneMatchForwardingPlane.Set(metricValue)

	return metricsWriter.WriteToFile()
}

func NewCpCheckCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "check /path/to/metrics.prom",
		Short: "cp<->fp validation",
		Args:  cobra.ExactArgs(1),
		PreRun: func(cmd *cobra.Command, args []string) {
			BindCPFlags(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			logger := slog.New(slog.NewTextHandler(os.Stderr, nil))
			cpClient, err := cp.NewGrpcClient(logger, CpGrpcBindNetwork, CpGrpcBindAddress)
			if err != nil {
				return err
			}
			defer cpClient.Close()
			fpClient, err := fp.NewGrpcClient(logger, FpGrpcBindNetwork, FpGrpcBindAddress)
			if err != nil {
				return err
			}
			defer fpClient.Close()
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			return cpCheck(ctx, cpClient, fpClient, args[0])
		},
	}

	cmd.Flags().StringVarP(&FpGrpcBindAddress, "fpGrpc", "", "", "fp gRPC bind address")
	cmd.Flags().StringVarP(&FpGrpcBindNetwork, "fpGrpcNetwork", "", "", "fp gRPC network")
	cmd.MarkFlagRequired("fpGrpc")
	cmd.MarkFlagRequired("fpGrpcNetwork")
	return cmd
}

func NewCpCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "cp",
		Short: "control plane",
	}

	cmd.PersistentFlags().StringVarP(&CpGrpcBindAddress, "grpc", "g", "", "gRPC bind address")
	cmd.PersistentFlags().StringVarP(&CpGrpcBindNetwork, "grpcNetwork", "n", "", "gRPC network")
	cmd.MarkPersistentFlagRequired("grpc")
	cmd.MarkPersistentFlagRequired("grpcNetwork")

	cmd.AddCommand(NewCpServicesCmd())
	cmd.AddCommand(NewCpCheckCmd())

	return cmd
}

func init() {
	rootCmd.AddCommand(NewCpCmd())
}
