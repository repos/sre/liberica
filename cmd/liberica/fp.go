/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cmdutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/fp"
)

var (
	FpGrpcBindNetwork string
	FpGrpcBindAddress string
)

func BindFPFlags(cmd *cobra.Command) {
	cfgMap := map[string]string{
		"grpc":        "fp.grpc.address",
		"grpcNetwork": "fp.grpc.network",
	}
	cmdutils.BindFlagsToSettings(cfgMap, cmd)
}

func NewServicesCmd() *cobra.Command {

	cmd := &cobra.Command{
		Use:   "services",
		Short: "services information",
		PreRun: func(cmd *cobra.Command, args []string) {
			BindFPFlags(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			logger := slog.New(slog.NewTextHandler(os.Stderr, nil))
			fpClient, err := fp.NewGrpcClient(logger, FpGrpcBindNetwork, FpGrpcBindAddress)
			if err != nil {
				return err
			}
			defer fpClient.Close()
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			svcs, err := fpClient.ListServices(ctx)
			for _, svc := range svcs {
				fmt.Printf("%s:%d %s\n", svc.Address.String(), svc.Port, svc.Scheduler)
				rss, err := fpClient.ListRealservers(ctx, svc)
				if err != nil {
					return err
				}
				for _, rs := range rss {
					fmt.Printf("\t%s\t%d\n", rs.Address.String(), rs.Weight)
				}
			}
			if err != nil {
				return err
			}

			return nil
		},
	}

	return cmd
}

func NewFpCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fp",
		Short: "forwarding plane",
	}

	cmd.PersistentFlags().StringVarP(&FpGrpcBindAddress, "grpc", "g", "", "gRPC bind address")
	cmd.PersistentFlags().StringVarP(&FpGrpcBindNetwork, "grpcNetwork", "n", "", "gRPC network")
	cmd.MarkPersistentFlagRequired("grpc")
	cmd.MarkPersistentFlagRequired("grpcNetwork")

	cmd.AddCommand(NewServicesCmd())

	return cmd
}

func init() {
	rootCmd.AddCommand(NewFpCmd())
}
