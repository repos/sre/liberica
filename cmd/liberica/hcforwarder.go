/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cmdutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/hcforwarder"
)

var (
	HcForwarderGrpcBindNetwork string
	HcForwarderGrpcBindAddress string
)

func BindHcforwarderFlags(cmd *cobra.Command) {
	cfgMap := map[string]string{
		"grpc":        "hcforwarder.grpc.address",
		"grpcNetwork": "hcforwarder.grpc.network",
	}
	cmdutils.BindFlagsToSettings(cfgMap, cmd)
}

func NewHcmappingCmd() *cobra.Command {

	cmd := &cobra.Command{
		Use:   "mapping",
		Short: "realserver SOMARK mapping",
		PreRun: func(cmd *cobra.Command, args []string) {
			BindHcforwarderFlags(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			logger := slog.New(slog.NewTextHandler(os.Stderr, nil))
			hfClient, err := hcforwarder.NewGrpcClient(logger, HcForwarderGrpcBindNetwork, HcForwarderGrpcBindAddress)
			if err != nil {
				return err
			}
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			realservers, err := hfClient.ListRealservers(ctx)
			if err != nil {
				return err
			}
			for somark, address := range realservers {
				fmt.Printf("%d --> %s\n", somark, address.String())
			}
			defer hfClient.Close()

			return nil
		},
	}

	return cmd
}

func NewHcForwarderCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "hcf",
		Short: "hcforwarder",
	}

	cmd.PersistentFlags().StringVarP(&HcForwarderGrpcBindAddress, "grpc", "g", "", "gRPC bind address")
	cmd.PersistentFlags().StringVarP(&HcForwarderGrpcBindNetwork, "grpcNetwork", "n", "", "gRPC network")
	cmd.MarkPersistentFlagRequired("grpc")
	cmd.MarkPersistentFlagRequired("grpcNetwork")

	cmd.AddCommand(NewHcmappingCmd())

	return cmd
}

func init() {
	rootCmd.AddCommand(NewHcForwarderCmd())
}
