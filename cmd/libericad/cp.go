/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"slices"
	"sync"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bgp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cmdutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/shandler"
)

var (
	ErrNoServicesFound = errors.New("unable to find any configured services")
)

// return a slice containing the names of configured services
func getConfiguredServices() []string {
	servicesConfig := viper.GetStringMap("services")
	services := make([]string, 0, len(servicesConfig))
	for serviceName, _ := range servicesConfig {
		services = append(services, serviceName)
	}
	slices.Sort(services)

	return services
}

// compares the old config and new config and flags for shutdown control planes
// running with an outdated configuration or that are no longer needed
func reloadConfig(logger *slog.Logger, svcs *sync.Map, logLevel *slog.LevelVar) ([]string, []string) {
	oldSvcsConfig := viper.GetStringMap("services")
	viper.ReadInConfig() // read config from disk
	newSvcsConfig := viper.GetStringMap("services")

	seamlessShutdownSvcs := []string{}
	shutdownSvcs := []string{}

	for name, _ := range oldSvcsConfig {
		if _, present := newSvcsConfig[name]; !present {
			logger.Info("scheduling control plane shutdown",
				slog.String("service", name),
			)
			shutdownSvcs = append(shutdownSvcs, name)
			continue
		}
		//		cpr, present := svcs[name]
		cpr, present := svcs.Load(name)
		if !present {
			logger.Error("unknown service. This is probably a bug in liberica control plane code",
				slog.String("service", name),
				slog.String("scope", "reloadConfig"),
			)
			continue
		}
		respawn, err := cpr.(*cp.ControlPlaneRun).Cp.RespawnRequired()
		if err != nil {
			logger.Error("invalid config, aborting config reload",
				slog.String("service", name),
				slog.Any("error", err),
			)
			metrics.ControlPlaneConfigReload.With(prometheus.Labels{
				"result": "ko",
			}).Inc()
			return nil, nil
		}
		if respawn {
			logger.Info("new config detected, scheduling control plane respawn",
				slog.String("service", name),
			)
			seamlessShutdownSvcs = append(seamlessShutdownSvcs, name)
		}
	}

	metrics.ControlPlaneConfigReload.With(prometheus.Labels{
		"result": "ok",
	}).Inc()
	return shutdownSvcs, seamlessShutdownSvcs
}

// spawn control planes
func spawnControlPlanes(logger *slog.Logger, svcNames []string, svcs *sync.Map, errCh chan *cp.ControlPlaneError) error {
	var initSvcNames []string

	for _, name := range svcNames {
		if _, present := svcs.Load(name); present {
			continue // cp already running for this service
		}
		logger.Info("spawning control plane",
			slog.String("service", name),
		)
		cplane, err := cp.NewControlPlane(context.Background(), logger, name)
		if err != nil {
			logger.Error("unable to initialize control plane",
				slog.String("service", name),
				slog.Any("error", err),
			)
			return err
		}
		svcs.Store(name, &cp.ControlPlaneRun{
			Cp: cplane,
		})
		initSvcNames = append(initSvcNames, name)
	}

	for _, name := range initSvcNames {
		cpr, present := svcs.Load(name)
		if !present {
			logger.Error("unknown service. This is probably a bug in liberica control plane code",
				slog.String("service", name),
				slog.String("scope", "spawnControlPlanes"),
			)
			continue
		}
		ctx, cancelFn := context.WithCancel(context.Background())
		cpr.(*cp.ControlPlaneRun).Cp.Run(ctx, errCh)
		cpr.(*cp.ControlPlaneRun).CancelFn = cancelFn
	}

	return nil
}

// shutdown specified running control planes, used after a config reload and to perform a clean shutdown before quitting
func shutdownControlPlanes(logger *slog.Logger, svcNames []string, svcs *sync.Map, errCh chan *cp.ControlPlaneError, seamless bool) error {
	for _, name := range svcNames {
		cpr, present := svcs.Load(name)
		if !present {
			logger.Error("unknown service. This is probably a bug in liberica control plane code",
				slog.String("service", name),
				slog.String("scope", "shutdownControlPlanes"),
			)
			continue
		}
		cpr.(*cp.ControlPlaneRun).CancelFn() // start control plane shutdown
	}

	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	for i := len(svcNames); i > 0; i-- {
		select {
		case <-ctx.Done():
			return errors.New("timeout shutting down the control plane")
		case cpErr := <-errCh:
			name := cpErr.Service
			cpr, present := svcs.LoadAndDelete(name)
			if !present {
				logger.Error("unknown service. This is probably a bug in liberica control plane code",
					slog.String("service", name),
					slog.String("scope", "shutdownControlPlanes#2"),
				)
				continue
			}
			cpr.(*cp.ControlPlaneRun).Cp.Close(seamless)
		}
	}

	return nil
}

// shutdown all control planes
func shutdownAllControlPlanes(logger *slog.Logger, svcs *sync.Map, errCh chan *cp.ControlPlaneError, seamless bool) error {
	var svcNames []string
	svcs.Range(func(key, _ any) bool {
		name := key.(string)
		svcNames = append(svcNames, name)
		return true
	})

	return shutdownControlPlanes(logger, svcNames, svcs, errCh, seamless)
}

// init bgp
func initBgp(logger *slog.Logger, bgpClient bgp.BgpClient, bgpConfig *bgp.BgpConfig) error {

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	// configure communities
	enabled, err := bgpClient.AreCommunitiesEnabled(ctx, bgpConfig.Communities)
	if err != nil {
		return fmt.Errorf("unable to check if communities are enabled: %w", err)
	}

	softReset := false
	if bgpConfig.Communities == nil {
		if enabled != bgp.CommunitiesNotPresent {
			if err := bgpClient.DisableCommunities(ctx); err != nil {
				return fmt.Errorf("unable to disable communities: %w", err)
			}
			softReset = true
			logger.Info("Removed BGP communities")
		}
	} else {
		if enabled != bgp.CommunitiesMatch {
			softReset = true
		}
		// ensure that all the objects related to communities get wiped out
		if err := bgpClient.DisableCommunities(ctx); err != nil {
			return fmt.Errorf("unable to disable communities: %w", err)
		}
		if err := bgpClient.EnableCommunities(ctx, bgpConfig.Communities); err != nil {
			return fmt.Errorf("unable to enable communities: %w", err)
		}
		logger.Info("BGP communities (re)configured")
	}

	// configure Peers
	// fetch current list of peers
	presentPeers, err := bgpClient.ListPeers(ctx)
	if err != nil {
		return fmt.Errorf("unable to fetch configured BGP peers: %w", err)
	}

	if slices.Equal(presentPeers, bgpConfig.Peers) {
		if !softReset {
			return nil
		}
		for _, peer := range bgpConfig.Peers {
			if err := bgpClient.SoftResetPeer(ctx, peer); err != nil {
				return fmt.Errorf("failed to perform a soft reset for peer %v: %w", peer, err)
			}
			logger.Info("triggering BGP soft reset",
				slog.String("peer", peer.String()),
			)
		}
		return nil
	}

	// add new peers
	for _, peer := range bgpConfig.Peers {
		if slices.Contains(presentPeers, peer) {
			if softReset {
				if err := bgpClient.SoftResetPeer(ctx, peer); err != nil {
					return fmt.Errorf("failed to perform a soft reset for peer %v: %w", peer, err)
				}
				logger.Info("triggering BGP soft reset",
					slog.String("peer", peer.String()),
				)
			}
			continue
		}
		if err := bgpClient.AddPeer(ctx, peer); err != nil {
			return fmt.Errorf("unable to add peer %v: %w", peer, err)
		}
		logger.Info("adding BGP peer",
			slog.String("peer", peer.String()),
		)
	}

	// delete old peers
	for _, peer := range presentPeers {
		if slices.Contains(bgpConfig.Peers, peer) {
			continue
		}
		if err := bgpClient.DeletePeer(ctx, peer); err != nil {
			return fmt.Errorf("unable to delete peer %v: %w", peer, err)
		}
		logger.Info("deleting BGP peer",
			slog.String("peer", peer.String()),
		)
	}

	return nil
}

func shutdownBgp(logger *slog.Logger, bgpClient bgp.BgpClient, bgpConfig *bgp.BgpConfig) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// delete configured bgp peers
	success := true
	for _, peer := range bgpConfig.Peers {
		if err := bgpClient.DeletePeer(ctx, peer); err != nil {
			logger.Warn("unable to delete peer",
				slog.String("peer", peer.String()),
				slog.Any("error", err),
			)
			success = false
		}
	}

	// don't risk increasing the priority of this liberica instance if it failed to
	// remove any BGP peers
	if !success && bgpConfig.Communities != nil {
		logger.Warn("unable to disable communities due to errors during BGP peer removal")
		return
	}

	// disable communities
	if bgpConfig.Communities != nil {
		if err := bgpClient.DisableCommunities(ctx); err != nil {
			logger.Warn("unable to disable communities",
				slog.Any("error", err),
			)
		}
	}
}

func NewCPCmd(signalHandler shandler.SignalHandler) *cobra.Command {
	const cmdName = "cp"

	var (
		grpcBindNetwork       string
		grpcBindAddress       string
		prometheusBindAddress string
		logLevel              = new(slog.LevelVar)
	)

	cfgMap := map[string]string{
		"grpc":        fmt.Sprintf("%s.grpc.address", cmdName),
		"grpcNetwork": fmt.Sprintf("%s.grpc.network", cmdName),
		"prometheus":  fmt.Sprintf("%s.prometheus.address", cmdName),
	}

	cmd := &cobra.Command{
		Use:   cmdName,
		Short: "control plane daemon",
		PreRun: func(cmd *cobra.Command, args []string) {
			cmdutils.BindFlagsToSettings(cfgMap, cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			opts := &slog.HandlerOptions{
				Level: logLevel,
			}
			logger := slogNew(slog.NewTextHandler(os.Stdout, opts))

			configureLogLevel(logger, logLevel, cmdName)
			if len(getConfiguredServices()) == 0 {
				logger.Error("unable to find any configured services")
				return ErrNoServicesFound
			}

			var svcs sync.Map
			srv := cp.NewGrpcServer(logger, &svcs)
			grpcCtx, cancelGrpc := context.WithCancel(context.Background())
			grpcCh, err := grpcutils.RunServer(grpcCtx,
				logger,
				srv,
				grpcBindNetwork,
				grpcBindAddress,
			)

			if err != nil {
				logger.Error("unable to run grpc server",
					slog.String("bind_network", grpcBindNetwork),
					slog.String("bind_address", grpcBindAddress),
					slog.Any("error", err),
				)
				return err
			}
			// let grpc server perform a clean shutdown
			defer func() {
				<-grpcCh
			}()

			promCtx, cancelProm := context.WithCancel(context.Background())

			cs := []prometheus.Collector{
				// cp metrics
				metrics.DepoolThreshold,
				metrics.AdministrativelyPooledRealservers,
				metrics.PooledRealservers,
				metrics.UnhealthyPooledRealservers,
				metrics.HealthyRealserversNotPooled,
				metrics.ControlPlaneConfigReload,
				// etcd metrics
				metrics.EtcdEventsReceived,
				metrics.EtcdErrors,
				metrics.EtcdEventsDiscarded,
			}
			promCh, err := metrics.RunPrometheusServer(promCtx, logger, prometheusBindAddress, cs...)
			if err != nil {
				logger.Error("unable to run prometheus server",
					slog.Any("error", err),
				)
				return err
			}
			defer func() {
				<-promCh
			}()

			bgpConfig, err := bgp.LoadBgpConfig()
			if err != nil {
				logger.Error("unable to load BGP config",
					slog.Any("error", err),
				)
			}

			bgpClient, err := bgp.NewGoBgpClient(logger, bgpConfig)
			if err != nil {
				logger.Error("unable to get a gobgp client",
					slog.Any("error", err),
				)
				return err
			}

			if err := initBgp(logger, bgpClient, bgpConfig); err != nil {
				logger.Error("unable to add bgp peers",
					slog.Any("error", err),
				)
				return err
			}

			ctxSeamlessShutdown, cancelSeamlessShutdown := signalHandler.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
			defer cancelSeamlessShutdown()
			ctxShutdown, cancelShutdown := signalHandler.NotifyContext(context.Background(), syscall.SIGUSR1)
			defer cancelShutdown()
			ctx, cancel := context.WithCancel(context.Background())
			errCh := make(chan *cp.ControlPlaneError, 128)
			ctxReloadConfig, cancelReloadConfig := signalHandler.NotifyContext(context.Background(), syscall.SIGHUP)
			seamless := true
			for ctx.Err() == nil { // keep running while ctx isn't cancelled
				svcNames := getConfiguredServices()
				if err := spawnControlPlanes(logger, svcNames, &svcs, errCh); err != nil {
					return err
				}

				shutdownSvcs := []string{}
				seamlessShutdownSvcs := []string{}

				select {
				case <-ctxSeamlessShutdown.Done(): // os.Interrupt / SIGTERM received
					cancel() // stop the main loop
					break
				case <-ctxShutdown.Done(): // SIGUSR1 received
					seamless = false
					cancel() // stop the main loop
					shutdownBgp(logger, bgpClient, bgpConfig)
					break
				case <-ctxReloadConfig.Done(): // SIGHUP received
					logger.Info("SIGHUP received. Reloading config")
					cancelReloadConfig() // restore signal handling
					ctxReloadConfig, cancelReloadConfig = signalHandler.NotifyContext(context.Background(),
						syscall.SIGHUP) // capture SIGHUP again
					shutdownSvcs, seamlessShutdownSvcs = reloadConfig(logger, &svcs, logLevel)
					configureLogLevel(logger, logLevel, cmdName)
					var err error
					bgpConfig, err = bgp.LoadBgpConfig()
					if err != nil {
						logger.Error("unable to load BGP config, liberica will continue using the last valid one",
							slog.Any("error", err),
						)
					} else {
						if err := initBgp(logger, bgpClient, bgpConfig); err != nil {
							logger.Error("unable to init new BGP configuration",
								slog.Any("error", err),
							)
						}
					}
				case <-promCh: // unexpected prometheus server crash
					var err error
					promCh, err = metrics.RunPrometheusServer(promCtx, logger, prometheusBindAddress, cs...)
					if err != nil {
						logger.Error("unable to respawn prometheus server, shutting down",
							slog.Any("error", err),
						)
						cancel() // stop control planes && start shutdown
					}
				case cpErr := <-errCh: // unexpected control plane error
					name := cpErr.Service
					cpr, present := svcs.LoadAndDelete(name)
					if !present {
						logger.Error("unknown service. This is probably a bug in liberica control plane code",
							slog.String("service", name),
						)
						continue
					}
					cpr.(*cp.ControlPlaneRun).Cp.Close(true) // clean up after control plane error
					cpr.(*cp.ControlPlaneRun).CancelFn()     // cancel cp context
					// re-spawning of the defunct control plane is handled by spawnControlPlanes()
				}

				// shutdown control planes if needed
				if err := shutdownControlPlanes(logger, shutdownSvcs, &svcs, errCh, false); err != nil {
					logger.Error("unable to shutdown control planes",
						slog.Any("services", shutdownSvcs),
						slog.Any("error", err),
					)
					cancel() // stop the main loop
				}
				if err := shutdownControlPlanes(logger, seamlessShutdownSvcs, &svcs, errCh, true); err != nil {
					logger.Error("unable to shutdown control planes for config reload purposes",
						slog.Any("services", seamlessShutdownSvcs),
						slog.Any("error", err),
					)
					cancel() // stop the main loop
				}
			} // for loop

			cancelSeamlessShutdown() // restore signal handling
			cancelReloadConfig()     // restore signal handling
			cancelProm()             // stop prometheus server
			cancelGrpc()             // stop gRPC server

			// after ctx has been cancelled we need to shutdown any running control plane
			if err := shutdownAllControlPlanes(logger, &svcs, errCh, seamless); err != nil {
				logger.Error("unable to shutdown remaining control planes before exiting",
					slog.Any("error", err),
				)
			}

			return nil
		},
	}

	cmd.Flags().StringVarP(&prometheusBindAddress, "prometheus", "p", "", "Prometheus bind address")
	cmd.Flags().StringVarP(&grpcBindAddress, "grpc", "g", "", "gRPC bind address")
	cmd.Flags().StringVarP(&grpcBindNetwork, "grpcNetwork", "n", "", "gRPC network")
	cmd.MarkFlagRequired("prometheus")
	cmd.MarkFlagRequired("grpc")
	cmd.MarkFlagRequired("grpcNetwork")
	return cmd

}

func init() {
	CPCmd := NewCPCmd(shandler.StdlibSignalHandler{})
	rootCmd.AddCommand(CPCmd)
}
