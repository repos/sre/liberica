package main

import (
	"bytes"
	"context"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/testutils"
)

func TestCPCmd(t *testing.T) {
	outBytes := bytes.NewBufferString("")
	errBytes := bytes.NewBufferString("")
	slogNew = testutils.MockSlogNew
	parentCtx := make(map[os.Signal]context.Context)
	parentCtx[os.Interrupt] = context.Background()
	tsh := testutils.TestSignalHandler{
		ParentCtx: parentCtx,
	}
	cmd := NewCPCmd(tsh)
	cmd.SetOut(outBytes)
	cmd.SetErr(errBytes)
	cmd.Execute()
	stdout, err := ioutil.ReadAll(outBytes)
	if err != nil {
		t.Fatal(err)
	}
	stderr, err := ioutil.ReadAll(errBytes)
	if err != nil {
		t.Fatal(err)
	}
	testCases := []struct {
		flags  []string
		output string
	}{
		{
			flags:  []string{"prometheus", "grpc"},
			output: string(stderr),
		},
		{
			flags:  []string{"grpcNetwork", "help"},
			output: string(stdout),
		},
	}
	for _, testCase := range testCases {
		for _, flag := range testCase.flags {
			t.Run(flag, func(t *testing.T) {
				if !strings.Contains(string(testCase.output), flag) {
					t.Errorf("%s not listed on %s", flag, testCase.output)
				}
			})
		}
	}
}
