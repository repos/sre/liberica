package main

import (
	"fmt"
	"log/slog"

	"github.com/spf13/viper"
)

const (
	Tcp  = "tcp"
	Unix = "unix"
)

func isValidNetwork(n string) bool {
	switch n {
	case Tcp, Unix:
		return true
	default:
		return false
	}
}

const (
	Ipvs   = "ipvs"
	Katran = "katran"
)

// used for mocking purposes
var slogNew = slog.New

func isValidForwardingPlane(fp string) bool {
	switch fp {
	case Ipvs, Katran:
		return true
	default:
		return false
	}
}

// configure log level. NOTE(vgutierrez): I've explored the idea of using a cmd parameter
// but its value doesn't get updated after calling viper.ReadInConfig() so for consistency
// I prefer fetching its value via viper.GetString() for all use cases
func configureLogLevel(logger *slog.Logger, logLevel *slog.LevelVar, cmdName string) {
	path := fmt.Sprintf("%s.log_level", cmdName)
	cfgLogLevel := viper.GetString(path)
	if err := logLevel.UnmarshalText([]byte(cfgLogLevel)); err != nil {
		// don't crash on error, just ignore the new value
		logger.Warn("ignoring invalid log level",
			slog.Any("error", err),
		)
	}
}
