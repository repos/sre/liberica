/*
Copyright © 2023 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net"
	"os"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cmdutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/hcforwarder"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/shandler"
)

func NewHcforwarderCmd(signalHandler shandler.SignalHandler) *cobra.Command {
	var (
		prometheusBindAddress string
		grpcBindNetwork       string
		grpcBindAddress       string
		egressIfaceName       string
		egressIface           *net.Interface
		v4IfaceName           string
		v4Iface               *net.Interface
		v6IfaceName           string
		v6Iface               *net.Interface
		logLevel              = new(slog.LevelVar)
	)

	const cmdName = "hcforwarder"

	cfgMap := map[string]string{
		"grpc":            fmt.Sprintf("%s.grpc.address", cmdName),
		"grpcNetwork":     fmt.Sprintf("%s.grpc.network", cmdName),
		"prometheus":      fmt.Sprintf("%s.prometheus.address", cmdName),
		"egressInterface": fmt.Sprintf("%s.interface.egress", cmdName),
		"v4Interface":     fmt.Sprintf("%s.interface.v4", cmdName),
		"v6Interface":     fmt.Sprintf("%s.interface.v6", cmdName),
	}

	cmd := &cobra.Command{
		Use:   cmdName,
		Short: "healthcheck forwarder",
		PreRun: func(cmd *cobra.Command, args []string) {
			cmdutils.BindFlagsToSettings(cfgMap, cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			ifaceMapping := []struct {
				flagName string
				iface    **net.Interface
			}{
				{"egressInterface", &egressIface},
				{"v4Interface", &v4Iface},
				{"v6Interface", &v6Iface},
			}
			for _, mapping := range ifaceMapping {
				ifaceName := viper.GetString(cfgMap[mapping.flagName])
				iface, err := net.InterfaceByName(ifaceName)
				if err != nil {
					return fmt.Errorf("invalid argument %s specified on %s: %w", ifaceName, mapping.flagName, err)
				}
				*mapping.iface = iface

			}

			if !isValidNetwork(grpcBindNetwork) {
				return fmt.Errorf("invalid argument %s specified on %s", grpcBindNetwork, "network")
			}

			opts := &slog.HandlerOptions{
				Level: logLevel,
			}
			logger := slogNew(slog.NewTextHandler(os.Stdout, opts))
			configureLogLevel(logger, logLevel, cmdName)

			hf := hcforwarder.NewHealthcheckForwarder(
				hcforwarder.WithIPv4Interface(v4Iface),
				hcforwarder.WithIPv6Interface(v6Iface),
				hcforwarder.WithLogger(logger),
			)
			if err := hf.Init(egressIface); err != nil {
				logger.Error("unable to init healthcheck forwarder", slog.Any("error", err))
				return err
			}
			// Pin by default to be able to survive a CLI crash without impacting traffic
			hf.Pin()
			defer hf.Close()

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			srv := hcforwarder.NewGrpcServer(logger, hf)
			grpcCh, err := grpcutils.RunServer(ctx,
				logger,
				srv,
				grpcBindNetwork,
				grpcBindAddress,
			)
			if err != nil {
				logger.Error("unable to run grpc server",
					slog.String("bind_network", grpcBindNetwork),
					slog.String("bind_address", grpcBindAddress),
					slog.Any("error", err),
				)
				return err
			}
			// let grpc server perform a clean shutdown
			defer func() {
				<-grpcCh
			}()

			cs := []prometheus.Collector{
				&metrics.HCForwarderMetricCollector{
					IfaceName:            egressIface.Name,
					HealthcheckForwarder: hf,
				},
			}
			promCh, err := metrics.RunPrometheusServer(ctx, logger, prometheusBindAddress, cs...)
			if err != nil {
				logger.Error("unable to run prometheus server",
					slog.Any("error", err),
				)
				return err
			}
			// let prometheus server perform a clean shutdown
			defer func() {
				<-promCh
			}()

			ctxSeamlessShutdown, cancelSeamlessShutdown := signalHandler.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
			ctxShutdown, cancelShutdown := signalHandler.NotifyContext(context.Background(), syscall.SIGUSR1)
			ctxConfigReload, cancelConfigReload := signalHandler.NotifyContext(context.Background(), syscall.SIGHUP)

			for ctx.Err() == nil {
				select {
				case <-grpcCh:
					if ctx.Err() == nil {
						logger.Error("grpc server finished unexpectedly")
						return errors.New("grpc server finished unexpectedly")
					}
				case <-promCh:
					if ctx.Err() == nil {
						logger.Error("prometheus server finished unexpectedly")
						return errors.New("prometheus server finished unexpectedly")
					}
				case <-ctxSeamlessShutdown.Done():
					logger.Info("exiting (eBPF program still running)")
					cancelSeamlessShutdown() // restore signal behavior
					cancel()
					return nil
				case <-ctxShutdown.Done():
					logger.Info("stopping eBPF program and exiting")
					cancelShutdown() // restore signal behavior
					cancel()
					hf.Unpin()
					return nil
				case <-ctxConfigReload.Done():
					logger.Info("SIGHUP received. Reloading config")
					cancelConfigReload()
					ctxConfigReload, cancelConfigReload = signalHandler.NotifyContext(context.Background(), syscall.SIGHUP)
					viper.ReadInConfig() // we only support changing the log level
					configureLogLevel(logger, logLevel, cmdName)
				}
			}
			return nil
		},
	}
	cmd.Flags().StringVarP(&prometheusBindAddress, "prometheus", "p", "", "Prometheus bind address")
	cmd.Flags().StringVarP(&grpcBindAddress, "grpc", "g", "", "gRPC bind address")
	cmd.Flags().StringVarP(&egressIfaceName, "egressInterface", "e", "", "egress interface")
	cmd.Flags().StringVarP(&v4IfaceName, "v4Interface", "4", "", "IPIP interface")
	cmd.Flags().StringVarP(&v6IfaceName, "v6Interface", "6", "", "IP6IP6 interface")
	cmd.Flags().StringVarP(&grpcBindNetwork, "grpcNetwork", "n", "", "gRPC network")
	cmd.MarkFlagRequired("egressInterface")
	cmd.MarkFlagRequired("v4Interface")
	cmd.MarkFlagRequired("v6Interface")
	cmd.MarkFlagRequired("grpc")
	cmd.MarkFlagRequired("grpcNetwork")
	cmd.MarkFlagRequired("prometheus")

	return cmd
}

func init() {
	hcforwarderCmd := NewHcforwarderCmd(shandler.StdlibSignalHandler{})
	rootCmd.AddCommand(hcforwarderCmd)
}
