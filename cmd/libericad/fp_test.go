package main

import (
	"bytes"
	"context"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"syscall"
	"testing"
	"time"

	"gitlab.wikimedia.org/repos/sre/go-qemutest/pkg/qemutest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bpftest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/testutils"
	"golang.org/x/sys/unix"
)

const (
	FP_GRPC_NETWORK            = "tcp"
	FP_GRPC_BIND_ADDRESS       = "127.0.0.1:6789"
	FP_PROMETHEUS_BIND_ADDRESS = "127.0.0.1:2222"
)

func TestFpCmd(t *testing.T) {
	if qemutest.InQemu() {
		t.Skip("Check native execution")
	}
	outBytes := bytes.NewBufferString("")
	errBytes := bytes.NewBufferString("")
	slogNew = testutils.MockSlogNew
	parentCtx := make(map[os.Signal]context.Context)
	parentCtx[os.Interrupt] = context.Background()
	parentCtx[syscall.SIGHUP] = context.Background()
	cmd := NewFPCmd(testutils.TestSignalHandler{ParentCtx: parentCtx})
	cmd.SetOut(outBytes)
	cmd.SetErr(errBytes)
	cmd.Execute()
	stdout, err := ioutil.ReadAll(outBytes)
	if err != nil {
		t.Fatal(err)
	}
	stderr, err := ioutil.ReadAll(errBytes)
	if err != nil {
		t.Fatal(err)
	}
	testCases := []struct {
		flags  []string
		output string
	}{
		{
			flags:  []string{"prometheus", "grpc"},
			output: string(stderr),
		},
		{
			flags:  []string{"grpcNetwork", "fplane", "help"},
			output: string(stdout),
		},
	}
	for _, testCase := range testCases {
		for _, flag := range testCase.flags {
			t.Run(flag, func(t *testing.T) {
				if !strings.Contains(string(testCase.output), flag) {
					t.Errorf("%s not listed on %s", flag, testCase.output)
				}
			})
		}
	}
}

func runFpMain(t *testing.T, handler testutils.TestSignalHandler) <-chan bool {
	done := make(chan bool, 1)
	go func() {
		slogNew = testutils.MockSlogNew
		cmd := NewFPCmd(handler)
		/* log output doesn't seem to be captured by SetOut or SetErr
		cmd.SetOut(outBytes)
		cmd.SetErr(errBytes) */
		args := []string{
			"-n", FP_GRPC_NETWORK,
			"-g", FP_GRPC_BIND_ADDRESS,
			"-p", FP_PROMETHEUS_BIND_ADDRESS,
			"-f", "ipvs",
		}
		cmd.SetArgs(args)
		if err := cmd.Execute(); err != nil {
			t.Errorf("failed to execute the cmd: %v", err)
		}
		done <- true
	}()
	return done
}

// QemuTest is defined on qemu_test.go
func (QemuTest) TestFpSignalHandling(t *testing.T) {
	testCases := []struct {
		name               string
		ipvsPresent        bool
		signalTriggersExit bool
	}{
		{"SIGHUP", true, false},
		{"SIGTERM", true, true},
		{"SIGUSR1", false, true},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			bpftest.MustRun("/sbin/ipvsadm", "-C") // clean slate
			bpftest.MustRun("/sbin/ipvsadm", "-A", "-t", "127.0.0.1:443", "-s", "rr")
			parentCtx := make(map[os.Signal]context.Context, len(testCases))
			cancelFns := make(map[os.Signal]context.CancelFunc, len(testCases))
			for _, tc := range testCases {
				signal := unix.SignalNum(tc.name)
				if signal == 0 { // signal not found
					t.Fatalf("invalid signal name: %s", tc.name)
					return
				}
				ctx, cancelFn := context.WithCancel(context.Background())
				parentCtx[signal] = ctx
				cancelFns[signal] = cancelFn
			}
			handler := testutils.TestSignalHandler{
				ParentCtx:   parentCtx,
				ReturnedCtx: map[os.Signal]context.Context{},
			}
			done := runFpMain(t, handler)
			signal := unix.SignalNum(testCase.name)
			cancelFns[signal]()

			// channel that get closed when the returned context has been cancelled
			cancelledCh := make(chan struct{})
			if !testCase.signalTriggersExit {
				go func() {
					for {
						select {
						case <-time.After(10 * time.Millisecond):
							// we cannot use Done() cause it will cause a panic after ctx cancelFunc is called
							if handler.ReturnedCtx[signal] != nil && handler.ReturnedCtx[signal].Err() != nil {
								close(cancelledCh)
								return
							}
						}
					}
				}()
			}

			select {
			case <-cancelledCh:
			case <-done:
			case <-time.After(5 * time.Second):
				t.Errorf("cmd didn't react to %v channel in 5 seconds", testCase.name)
			}

			cmd := exec.Command("/sbin/ipvsadm", "-Ln")
			out, err := cmd.CombinedOutput()
			if err != nil {
				t.Fatal("unable to check ipvsdadm output")
			}
			if testCase.ipvsPresent || !testCase.signalTriggersExit {
				if !strings.Contains(string(out), "127.0.0.1:443") {
					t.Errorf("IPVS service should be present after %s", testCase.name)
				}
			} else {
				if strings.Contains(string(out), "127.0.0.1:443") {
					t.Errorf("IPVS service shouldn't be present after %s", testCase.name)
				}
			}
			if !testCase.signalTriggersExit { // we need to clean up in this scenario
				cancelFns[syscall.SIGUSR1]()
				select {
				case <-done:
				case <-time.After(5 * time.Second):
					t.Error("unable to clean up using SIGUSR1")
				}
			}
		})
	}
}
