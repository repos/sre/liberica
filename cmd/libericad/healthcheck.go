/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cmdutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/healthcheck"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/shandler"
)

func NewHealthcheckCmd(signalHandler shandler.SignalHandler) *cobra.Command {
	var (
		grpcBindNetwork       string
		grpcBindAddress       string
		prometheusBindAddress string
		logLevel              = new(slog.LevelVar)
	)
	const cmdName = "healthcheck"

	cfgMap := map[string]string{
		"grpc":        fmt.Sprintf("%s.grpc.address", cmdName),
		"grpcNetwork": fmt.Sprintf("%s.grpc.network", cmdName),
		"prometheus":  fmt.Sprintf("%s.prometheus.address", cmdName),
	}

	cmd := &cobra.Command{
		Use:   cmdName,
		Short: "healthcheck daemon",
		PreRun: func(cmd *cobra.Command, args []string) {
			cmdutils.BindFlagsToSettings(cfgMap, cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			if !isValidNetwork(grpcBindNetwork) {
				return fmt.Errorf("invalid argument %s specified on %s", grpcBindNetwork, "network")
			}

			opts := &slog.HandlerOptions{
				Level: logLevel,
			}
			logger := slogNew(slog.NewTextHandler(os.Stdout, opts))
			configureLogLevel(logger, logLevel, cmdName)

			hcm := healthcheck.NewHCMonitor(logger)
			defer hcm.Close()
			srv := healthcheck.NewGrpcServer(logger, hcm)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			grpcCh, err := grpcutils.RunServer(ctx,
				logger,
				srv,
				grpcBindNetwork,
				grpcBindAddress,
			)
			if err != nil {
				logger.Error("unable to run grpc server",
					slog.String("bind_network", grpcBindNetwork),
					slog.String("bind_address", grpcBindAddress),
					slog.Any("error", err),
				)
				return err
			}
			// let grpc server perform a clean shutdown
			defer func() {
				<-grpcCh
			}()

			cs := []prometheus.Collector{
				metrics.HttpCheck,
				metrics.HttpCheckSeconds,
				metrics.HealthcheckResult,
				metrics.HealthcheckExecutions,
				metrics.HealthcheckResultDiscarded,
				metrics.HealthcheckMonitorOperations,
			}

			promCh, err := metrics.RunPrometheusServer(ctx, logger, prometheusBindAddress, cs...)
			if err != nil {
				logger.Error("unable to run prometheus server",
					slog.Any("error", err),
				)
				return err
			}
			// let prometheus server perform a clean shutdown
			defer func() {
				<-promCh
			}()

			// for the healthcheck daemon we don't need to tell between shutdown and seamless shutdown
			ctxShutdown, cancelShutdown := signalHandler.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM, syscall.SIGUSR1)
			ctxConfigReload, cancelConfigReload := signalHandler.NotifyContext(context.Background(), syscall.SIGHUP)

			for ctx.Err() == nil {
				select {
				case <-grpcCh:
					if ctx.Err() == nil {
						logger.Error("grpc server finished unexpectedly")
						return errors.New("grpc server finished unexpectedly")
					}
				case <-promCh:
					if ctx.Err() == nil {
						logger.Error("prometheus server finished unexpectedly")
						return errors.New("prometheus server finished unexpectedly")
					}
				case <-ctxShutdown.Done():
					logger.Info("stopping healthcheck daemon")
					cancelShutdown() // Restore signal behavior
					cancel()
					return nil
				case <-ctxConfigReload.Done(): // Handle SIGHUP
					logger.Info("SIGHUP received. Reloading config")
					cancelConfigReload()
					ctxConfigReload, cancelConfigReload = signalHandler.NotifyContext(context.Background(), syscall.SIGHUP)
					viper.ReadInConfig() // we only support changing the log level
					configureLogLevel(logger, logLevel, cmdName)
				}
			}

			return nil
		},
	}
	cmd.Flags().StringVarP(&prometheusBindAddress, "prometheus", "p", "", "Prometheus bind address")
	cmd.Flags().StringVarP(&grpcBindAddress, "grpc", "g", "", "gRPC bind address")
	cmd.Flags().StringVarP(&grpcBindNetwork, "grpcNetwork", "n", "", "gRPC network")
	cmd.MarkFlagRequired("grpc")
	cmd.MarkFlagRequired("grpcNetwork")
	cmd.MarkFlagRequired("prometheus")

	return cmd
}

func init() {
	healthcheckCmd := NewHealthcheckCmd(shandler.StdlibSignalHandler{})
	rootCmd.AddCommand(healthcheckCmd)
}
