/*
Copyright © 2024 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net"
	"os"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/cmdutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/fp"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/grpcutils"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/metrics"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/shandler"
)

func NewFPCmd(signalHandler shandler.SignalHandler) *cobra.Command {
	var (
		forwardingPlane       string // contains the value of -fplane
		grpcBindNetwork       string
		grpcBindAddress       string
		prometheusBindAddress string
		katranInterface       string
		forwardingCores       []uint
		numaNode              int
		conntrackSize         uint32
		gatewayAddress        string
		logLevel              = new(slog.LevelVar)
	)

	const cmdName = "fp"

	cfgMap := map[string]string{
		"grpc":            fmt.Sprintf("%s.grpc.address", cmdName),
		"grpcNetwork":     fmt.Sprintf("%s.grpc.network", cmdName),
		"prometheus":      fmt.Sprintf("%s.prometheus.address", cmdName),
		"fplane":          fmt.Sprintf("%s.forwarding_plane", cmdName),
		"forwardingCores": fmt.Sprintf("%s.katran.forwarding_cores", cmdName),
		"interface":       fmt.Sprintf("%s.katran.interface", cmdName),
		"numaNode":        fmt.Sprintf("%s.katran.numa_node", cmdName),
		"gatewayAddress":  fmt.Sprintf("%s.katran.gateway_address", cmdName),
		"conntrackSize":   fmt.Sprintf("%s.katran.conntrack_size", cmdName),
	}

	cmd := &cobra.Command{
		Use:   "fp",
		Short: "forwarding plane daemon",
		PreRun: func(cmd *cobra.Command, args []string) {
			cmdutils.BindFlagsToSettings(cfgMap, cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			if !isValidNetwork(grpcBindNetwork) {
				return fmt.Errorf("invalid argument %s specified on %s", grpcBindNetwork, "network")
			}

			if !isValidForwardingPlane(forwardingPlane) {
				return fmt.Errorf("invalid argument %s specified on %s", forwardingPlane, "fplane")
			}

			opts := &slog.HandlerOptions{
				Level: logLevel,
			}
			logger := slogNew(slog.NewTextHandler(os.Stdout, opts))
			configureLogLevel(logger, logLevel, cmdName)

			var collectors []prometheus.Collector

			var fplane fp.ForwardingPlane
			var err error
			switch forwardingPlane {
			case Katran:
				if len(forwardingCores) == 0 {
					return fmt.Errorf("forwardingCores needs to be set for katran")
				}
				if gatewayAddress == "" {
					return fmt.Errorf("gatewayAddress needs to be set for katran")
				}
				iface, err := net.InterfaceByName(katranInterface)
				if err != nil {
					return err
				}
				fplane, err = fp.NewKatranForwardingPlane(logger, iface, uintToUint32Slice(forwardingCores), numaNode, conntrackSize)
				if err != nil {
					logger.Error("unable to init Katran forwarding plane", slog.Any("error", err))
					return err
				}
				katran := fplane.(*fp.KatranForwardingPlane)
				katran.SetMacAddress(gatewayAddress)
				defer katran.Close()
				collectors = append(collectors, &fp.KatranMetricCollector{Fplane: katran})
			case Ipvs:
				collectors = append(collectors, metrics.IpvsFpReadOps)
				collectors = append(collectors, metrics.IpvsFpWriteOps)
				fplane, err = fp.NewIPVSForwardingPlane(logger)
				if err != nil {
					logger.Error("unable to init IPVS forwarding plane", slog.Any("error", err))
					return err
				}
			default:
				return errors.New("not implemented")
			}

			logger.Info("starting forwarding plane daemon",
				slog.String("forwarding_plane", forwardingPlane),
			)
			fplane.Pin()

			srv := fp.NewGrpcServer(logger, fplane)
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			grpcCh, err := grpcutils.RunServer(ctx,
				logger,
				srv,
				grpcBindNetwork,
				grpcBindAddress,
			)
			if err != nil {
				logger.Error("unable to run grpc server",
					slog.String("bind_network", grpcBindNetwork),
					slog.String("bind_address", grpcBindAddress),
					slog.Any("error", err),
				)
				return err
			}
			// let grpc server perform a clean shutdown
			defer func() {
				<-grpcCh
			}()

			promCh, err := metrics.RunPrometheusServer(ctx, logger, prometheusBindAddress, collectors...)
			if err != nil {
				logger.Error("unable to run prometheus server",
					slog.Any("error", err),
				)
				return err
			}
			// let prometheus server perform a clean shutdown
			defer func() {
				<-promCh
			}()

			ctxSeamlessShutdown, cancelSeamlessShutdown := signalHandler.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
			defer cancelSeamlessShutdown()
			ctxShutdown, cancelShutdown := signalHandler.NotifyContext(context.Background(), syscall.SIGUSR1)
			defer cancelShutdown()
			ctxConfigReload, cancelConfigReload := signalHandler.NotifyContext(context.Background(), syscall.SIGHUP)

			for ctx.Err() == nil { // keep running while ctx isn't cancelled
				select {
				case <-grpcCh:
					if ctx.Err() == nil {
						logger.Error("grpc server finished unexpectedly")
						return errors.New("grpc server finished unexpectedly")
					}
				case <-promCh:
					if ctx.Err() == nil {
						logger.Error("prometheus server finished unexpectedly")
						return errors.New("prometheus server finished unexpectedly")
					}
				case <-ctxSeamlessShutdown.Done(): // Handle SIGINT / SIGTERM
					logger.Info("exiting (forwarding plane still working)",
						slog.String("forwarding_plane", forwardingPlane),
					)
					cancelSeamlessShutdown() // restore signal behavior
					cancel()
					return nil
				case <-ctxShutdown.Done(): // Handle SIGUSR1
					logger.Info("stopping forwarding plane and exiting",
						slog.String("forwarding_plane", forwardingPlane),
					)
					cancelShutdown() // restore signal behavior
					cancel()
					fplane.Unpin()
					return nil
				case <-ctxConfigReload.Done(): // Handle SIGHUP
					logger.Info("SIGHUP received. Reloading config")
					cancelConfigReload()
					ctxConfigReload, cancelConfigReload = signalHandler.NotifyContext(context.Background(), syscall.SIGHUP)
					viper.ReadInConfig() // we only support changing the log level
					configureLogLevel(logger, logLevel, cmdName)
				}
			}
			return nil
		},
	}

	cmd.Flags().StringVarP(&prometheusBindAddress, "prometheus", "p", "", "Prometheus bind address")
	cmd.Flags().StringVarP(&grpcBindAddress, "grpc", "g", "", "gRPC bind address")
	cmd.Flags().StringVarP(&grpcBindNetwork, "grpcNetwork", "n", "", "gRPC network")
	cmd.Flags().StringVarP(&forwardingPlane, "fplane", "f", "", "forwarding plane [ipvs, katran]")
	cmd.Flags().StringVarP(&katranInterface, "katranInterface", "k", "", "interface used by katran for inbound and outbound purposes")
	cmd.Flags().StringVarP(&gatewayAddress, "gatewayAddress", "a", "", "MAC address (11::22:33:44:55:66) used as gateway")
	cmd.Flags().IntVarP(&numaNode, "numaNode", "u", -1, "NUMA node where katran per CPU LRU maps should be stored")
	cmd.Flags().UintSliceVarP(&forwardingCores, "forwardingCores", "o", []uint{}, "List of CPU cores used by katran for forwarding traffic")
	cmd.Flags().Uint32VarP(&conntrackSize, "conntrackSize", "c", 8_000_000, "Katran conntrack table size, it will be split across the specified forwarding cores")
	cmd.MarkFlagRequired("grpc")
	cmd.MarkFlagRequired("grpcNetwork")
	cmd.MarkFlagRequired("prometheus")
	cmd.MarkFlagRequired("fplane")
	return cmd
}

func uintToUint32Slice(input []uint) []uint32 {
	result := make([]uint32, len(input))
	for i, v := range input {
		result[i] = uint32(v)
	}
	return result
}

func init() {
	FPCmd := NewFPCmd(shandler.StdlibSignalHandler{})
	rootCmd.AddCommand(FPCmd)
}
