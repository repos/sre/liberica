package main

import (
	"log"
	"os"
	"path/filepath"
	"testing"

	"gitlab.wikimedia.org/repos/sre/go-qemutest/pkg/qemutest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/bpftest"
	"gitlab.wikimedia.org/repos/sre/liberica/pkg/testutils"
	"golang.org/x/sys/unix"
)

const (
	NETWORK_INTERFACE = "lo"
	IPIP_INTERFACE    = "ipip0"
	IPIP6_INTERFACE   = "ipip60"
)

// binaries that will be shipped on initrd
var initrdBinaries = []string{"/usr/sbin/bpftool", "/bin/ip", "/sbin/ipvsadm"}

type QemuTest struct{}

func TestMain(m *testing.M) {
	// Before being able to use the eBPF program clsact qdisc
	// needs to be attached to the network interface
	if qemutest.InQemu() {
		// pinning requires /sys/fs/bpf
		if err := unix.Mount("bpf", "/sys/fs/bpf", "bpf", 0, ""); err != nil {
			log.Fatalf("failed to mount /sys/fs/bpf: %v", err)
		}
		if err := bpftest.AttachClsact(NETWORK_INTERFACE); err != nil {
			log.Fatalf("failed to attach clsact qdisc to %s: %v", NETWORK_INTERFACE, err)
		}

		// set up ipip0 && ipip60
		bpftest.MustRun("/bin/ip", "link", "add", "name", IPIP_INTERFACE, "type", "ipip", "external")
		bpftest.MustRun("/bin/ip", "link", "add", "name", IPIP6_INTERFACE, "type", "ip6tnl", "external")
		bpftest.MustRun("/bin/ip", "link", "set", "up", "dev", IPIP_INTERFACE)
		bpftest.MustRun("/bin/ip", "link", "set", "up", "dev", IPIP6_INTERFACE)
		// Add a IPv6 address to the loopback interface to be able to send traffic there
		bpftest.MustRun("/bin/ip", "address", "add", "fc00::1", "dev", "lo")
	}

	os.Exit(qemutest.QemuTestMain(m))
}

func TestQemu(t *testing.T) {
	if qemutest.InQemu() {
		qemutest.RunQemuTests(t, QemuTest{})
		return
	}
	tdPath, err := testutils.GetTestdata()
	if err != nil {
		t.Fatalf("unable to find testdata path: %v", err)
	}
	testCases := []qemutest.QemuTestCase{
		{
			KernelImage: filepath.Join(tdPath, "bzImage.6.1"),
			TestId:      "6.1.69-bookworm",
			Verbose:     true,
		},
	}

	qemutest.RunQemu(t, testCases, initrdBinaries)
}
